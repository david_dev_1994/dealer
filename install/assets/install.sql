-- phpMyAdmin SQL Dump
-- version 3.4.11.1
-- http://www.phpmyadmin.net
--
-- Host: 198.38.82.92
-- Generation Time: Feb 07, 2017 at 11:36 PM
-- Server version: 5.6.24
-- PHP Version: 5.4.31

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `limon460_taskshop2`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_access_lebel`
--

CREATE TABLE IF NOT EXISTS `tbl_access_lebel` (
  `access_lebel_id` int(2) NOT NULL AUTO_INCREMENT,
  `access_lebel_name` varchar(100) NOT NULL,
  `access_lebel` tinyint(1) NOT NULL,
  `block_or_suspend` tinyint(1) NOT NULL,
  `edit` tinyint(1) NOT NULL,
  `delete` tinyint(1) NOT NULL,
  PRIMARY KEY (`access_lebel_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `tbl_access_lebel`
--

INSERT INTO `tbl_access_lebel` (`access_lebel_id`, `access_lebel_name`, `access_lebel`, `block_or_suspend`, `edit`, `delete`) VALUES
(1, 'Super Admin', 1, 1, 1, 1),
(2, 'Admin', 2, 0, 1, 1),
(3, 'User', 3, 0, 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_file`
--

CREATE TABLE IF NOT EXISTS `tbl_file` (
  `file_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `file_title` varchar(255) NOT NULL,
  `file_description` text NOT NULL,
  `file_name` varchar(255) NOT NULL,
  `deletion_status` tinyint(1) NOT NULL DEFAULT '0',
  `date_added` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`file_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `tbl_file`
--

INSERT INTO `tbl_file` (`file_id`, `user_id`, `file_title`, `file_description`, `file_name`, `deletion_status`, `date_added`) VALUES
(1, 1, '', '', '(6).jpg', 0, '2016-07-17 10:10:42'),
(2, 1, '', '', '(3).jpg', 0, '2016-07-17 13:06:24'),
(3, 3, '', '', 'site.png', 0, '2016-08-11 09:56:17'),
(4, 3, '', '', '9030-1_sanitrclassic_neu.jpg', 0, '2016-09-14 00:15:45'),
(5, 3, '', '', '13439049_1046275372115214_2137520682153350722_n.jpg', 0, '2016-09-24 05:29:48'),
(6, 3, '', '', 'Promo-Banner-02-prev.jpg', 0, '2016-12-26 02:15:13'),
(7, 18, '', '', 'Resume_of_Sydul_Islam_Mazumder.pdf', 0, '2017-02-02 07:16:33');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_ftp_setting`
--

CREATE TABLE IF NOT EXISTS `tbl_ftp_setting` (
  `ftp_id` int(11) NOT NULL AUTO_INCREMENT,
  `ftp_hostname` varchar(100) NOT NULL,
  `ftp_username` varchar(100) NOT NULL,
  `ftp_password` varchar(100) NOT NULL,
  `date_added` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`ftp_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `tbl_ftp_setting`
--

INSERT INTO `tbl_ftp_setting` (`ftp_id`, `ftp_hostname`, `ftp_username`, `ftp_password`, `date_added`) VALUES
(1, 'ftp.example.com', 'ftp_username', 'ftp_password', '2016-07-20 07:42:27');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_general_settings`
--

CREATE TABLE IF NOT EXISTS `tbl_general_settings` (
  `id` int(2) NOT NULL AUTO_INCREMENT,
  `site_name` varchar(50) NOT NULL,
  `admin_email` varchar(100) NOT NULL,
  `logo` varchar(255) NOT NULL,
  `favicon` varchar(255) NOT NULL,
  `app_vrs` varchar(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `tbl_general_settings`
--

INSERT INTO `tbl_general_settings` (`id`, `site_name`, `admin_email`, `logo`, `favicon`, `app_vrs`) VALUES
(1, 'xcluesiv', 'admin@atiqueit.com', 'STACScholarshipFundLogo-1.png', 'pYKz7gE1.jpg', '2.0.1');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_job_category`
--

CREATE TABLE IF NOT EXISTS `tbl_job_category` (
  `job_category_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(7) NOT NULL,
  `job_category_name` varchar(150) NOT NULL,
  `job_category_description` text NOT NULL,
  `publication_status` tinyint(1) NOT NULL,
  `deletion_status` tinyint(1) NOT NULL DEFAULT '0',
  `date_added` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`job_category_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `tbl_job_category`
--

INSERT INTO `tbl_job_category` (`job_category_id`, `user_id`, `job_category_name`, `job_category_description`, `publication_status`, `deletion_status`, `date_added`) VALUES
(1, 1, 'Clipping Path', 'job category description', 1, 0, '2016-06-23 06:13:53'),
(2, 1, 'Photoshop Maskingg', '<p>Photoshop Masking<br></p>', 1, 0, '2016-06-23 06:46:24'),
(3, 1, 'Croping / Resizeing', '<p>Croping or  Resizeing<br></p>', 1, 1, '2016-06-23 06:48:13'),
(4, 1, 'Translations', '<p>asdfaf<br></p>', 0, 1, '2016-08-20 06:49:10'),
(5, 1, 'Тест', '<p><font><font>Описание рабобты</font></font></p>', 1, 1, '2016-10-27 12:19:04');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_mail_inbox`
--

CREATE TABLE IF NOT EXISTS `tbl_mail_inbox` (
  `inbox_mail_id` int(11) NOT NULL AUTO_INCREMENT,
  `full_name` varchar(100) NOT NULL,
  `from` varchar(100) NOT NULL,
  `subject` varchar(100) NOT NULL,
  `message` longtext NOT NULL,
  `reading_status` tinyint(1) NOT NULL,
  `deletion_status` tinyint(1) NOT NULL,
  `date_added` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`inbox_mail_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `tbl_mail_inbox`
--

INSERT INTO `tbl_mail_inbox` (`inbox_mail_id`, `full_name`, `from`, `subject`, `message`, `reading_status`, `deletion_status`, `date_added`) VALUES
(1, 'Shahid Nawaz', 'msnawazbd@gmail.com', 'welcome message', 'New members in a group are an added advantage to spread motive and business of the group. Welcoming new members to a group or an organization would make them feel good and special. one can welcome them with welcome wishes given with gifts to encourage them to work for the motive of the group. One can also arrange surprises for the new members which would make the welcoming much more special.\r\n\r\n    “Dear new member, I welcome you to the Save Forest group with much love. I hope you would work for the motive of the group as much as other members and save our forests.”', 1, 0, '2016-06-05 05:47:41'),
(2, 'Arshadul Haque', 'noyon2nil@gmail.com', 'Messages for Friends', 'Friends are the special companions of life. They are there by any moment of a person life, be it happiness or sorrow. Friends are welcomed with much fervor as they look into a healthy friendship forever. \r\n\r\n    “I welcome you friend to our group of five and look forward to a beautiful and healthy friendship forever. May we cherish every moment of our friendship always and create beautiful moments in life.”\r\nFriends are the special companions of life. They are there by any moment of a person life, be it happiness or sorrow. Friends are welcomed with much fervor as they look into a healthy friendship forever. \r\n\r\n    “I welcome you friend to our group of five and look forward to a beautiful and healthy friendship forever. May we cherish every moment of our friendship always and create beautiful moments in life.”\r\nFriends are the special companions of life. They are there by any moment of a person life, be it happiness or sorrow. Friends are welcomed with much fervor as they look into a healthy friendship forever. \r\n\r\n    “I welcome you friend to our group of five and look forward to a beautiful and healthy friendship forever. May we cherish every moment of our friendship always and create beautiful moments in life.”\r\n', 1, 0, '2016-06-05 05:49:01');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_mail_settings`
--

CREATE TABLE IF NOT EXISTS `tbl_mail_settings` (
  `mail_set_id` int(2) NOT NULL AUTO_INCREMENT,
  `activation_subj` varchar(255) NOT NULL,
  `activation_body` text NOT NULL,
  `forgot_subj` varchar(255) NOT NULL,
  `forgot_body` text NOT NULL,
  `add_user_subj` varchar(255) NOT NULL,
  `add_user_body` text NOT NULL,
  PRIMARY KEY (`mail_set_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `tbl_mail_settings`
--

INSERT INTO `tbl_mail_settings` (`mail_set_id`, `activation_subj`, `activation_body`, `forgot_subj`, `forgot_body`, `add_user_subj`, `add_user_body`) VALUES
(1, '', '', 'No reply-Reset Passwords1', 'Your reset code is given below. Go to this link to reset your password.1', 'No reply-You have been added as a member12', 'You have been added12');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_order`
--

CREATE TABLE IF NOT EXISTS `tbl_order` (
  `order_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `job_category_id` int(11) NOT NULL,
  `job_title` varchar(255) NOT NULL,
  `return_file_type` varchar(50) NOT NULL,
  `need_path` tinyint(1) NOT NULL,
  `number_of_image` varchar(3) NOT NULL,
  `job_type` tinyint(1) NOT NULL COMMENT '1 for one time and 0 for mounthly',
  `one_time_price_usd` varchar(10) NOT NULL,
  `dead_line_days` varchar(5) NOT NULL,
  `monthly_work_volume` varchar(3) NOT NULL,
  `monthly_amount_usd` varchar(10) NOT NULL,
  `instruction_message` text NOT NULL,
  `status` tinyint(1) NOT NULL,
  `deletion_status` tinyint(1) NOT NULL,
  `date_added` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`order_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `tbl_order`
--

INSERT INTO `tbl_order` (`order_id`, `user_id`, `job_category_id`, `job_title`, `return_file_type`, `need_path`, `number_of_image`, `job_type`, `one_time_price_usd`, `dead_line_days`, `monthly_work_volume`, `monthly_amount_usd`, `instruction_message`, `status`, `deletion_status`, `date_added`) VALUES
(1, 1, 1, 'graphics design', 'jpg', 1, '3', 1, '51', '5', '1', '1', 'instruction_message1', 1, 0, '2016-07-13 18:41:57'),
(2, 2, 2, 'Graphics Design', 'jpg', 0, '10', 0, '5', '4', '10', '30', '<p><span xss=removed>Instruction Message</span><br></p>', 0, 0, '2016-07-14 13:39:12');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_privacy_terms`
--

CREATE TABLE IF NOT EXISTS `tbl_privacy_terms` (
  `privacy_terms_id` int(2) NOT NULL AUTO_INCREMENT,
  `privacy` text NOT NULL,
  `terms` text NOT NULL,
  PRIMARY KEY (`privacy_terms_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `tbl_privacy_terms`
--

INSERT INTO `tbl_privacy_terms` (`privacy_terms_id`, `privacy`, `terms`) VALUES
(1, '<img k=" style=">                                                                                                                                                                                                                                                This is privacy222                                                                                                            ', '                                                                                                                        This is terms and conditions222');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_property`
--

CREATE TABLE IF NOT EXISTS `tbl_property` (
  `property_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `property_title` varchar(255) NOT NULL,
  `property_details` text NOT NULL,
  `image_one` varchar(255) NOT NULL,
  `image_one_alt` varchar(255) NOT NULL,
  `image_two` varchar(255) NOT NULL,
  `image_two_alt` varchar(255) NOT NULL,
  `image_three` varchar(255) NOT NULL,
  `image_three_alt` varchar(255) NOT NULL,
  `image_four` varchar(255) NOT NULL,
  `image_four_alt` varchar(255) NOT NULL,
  `image_five` varchar(255) NOT NULL,
  `image_five_alt` varchar(255) NOT NULL,
  `publication_status` tinyint(4) NOT NULL,
  `property_type_id` int(7) NOT NULL,
  `property_location_id` int(7) NOT NULL,
  `facing` varchar(255) NOT NULL,
  `address` varchar(255) NOT NULL,
  `land_area` varchar(255) NOT NULL,
  `flat_size` varchar(255) NOT NULL,
  `unit_per_floor` varchar(255) NOT NULL,
  `car_parking` varchar(255) NOT NULL,
  `total_flat` varchar(255) NOT NULL,
  `lift` varchar(255) NOT NULL,
  `others_facility` varchar(255) NOT NULL,
  `swimming_pool` tinyint(1) NOT NULL,
  `beach_house` tinyint(1) NOT NULL,
  `garage` tinyint(1) NOT NULL,
  `barbecue_grill` tinyint(1) NOT NULL,
  `sauna_in_house` tinyint(1) NOT NULL,
  `garden` tinyint(1) NOT NULL,
  `boats` tinyint(1) NOT NULL,
  `balcony` tinyint(1) NOT NULL,
  `beach_side` tinyint(1) NOT NULL,
  `playhouse` tinyint(1) NOT NULL,
  `meta_name` varchar(255) NOT NULL,
  `meta_title` varchar(255) NOT NULL,
  `meta_keyword` varchar(255) NOT NULL,
  `meta_description` text NOT NULL,
  `deletion_status` tinyint(1) NOT NULL,
  `date_added` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`property_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_quote`
--

CREATE TABLE IF NOT EXISTS `tbl_quote` (
  `quote_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `job_category_id` int(11) NOT NULL,
  `job_title` varchar(255) NOT NULL,
  `return_file_type` varchar(50) NOT NULL,
  `need_path` tinyint(1) NOT NULL,
  `number_of_image` varchar(2) NOT NULL,
  `instruction_message` text NOT NULL,
  `publication_status` tinyint(1) NOT NULL,
  `deletion_status` tinyint(1) NOT NULL,
  `date_added` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`quote_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `tbl_quote`
--

INSERT INTO `tbl_quote` (`quote_id`, `user_id`, `job_category_id`, `job_title`, `return_file_type`, `need_path`, `number_of_image`, `instruction_message`, `publication_status`, `deletion_status`, `date_added`) VALUES
(1, 1, 2, 'graphics designer', 'jpg', 1, '22', '<p>graphics designergraphics designergraphics designergraphics designergraphics designer<br></p>', 1, 0, '2016-07-24 07:09:19'),
(2, 1, 1, 'image resize', 'png', 2, '44', 'image resize<br>', 1, 0, '2016-07-24 07:11:35'),
(3, 1, 5, 'Test Job', 'jpg', 1, '20', '<p>Make The Clipping Path Active n</p>', 1, 0, '2017-02-02 06:18:45'),
(4, 1, 5, 'Another Test', 'psd', 0, '10', '<p>No need the path n</p>', 1, 0, '2017-02-02 06:22:10');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_reset_pass`
--

CREATE TABLE IF NOT EXISTS `tbl_reset_pass` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email_address` varchar(300) NOT NULL,
  `random_string` varchar(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `tbl_reset_pass`
--

INSERT INTO `tbl_reset_pass` (`id`, `email_address`, `random_string`) VALUES
(1, 'super_admin@atique-it.com', 'TyOF5hop');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_sent_mail`
--

CREATE TABLE IF NOT EXISTS `tbl_sent_mail` (
  `sent_mail_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `to` varchar(100) NOT NULL,
  `from` varchar(100) NOT NULL,
  `cc` varchar(255) NOT NULL,
  `subject` varchar(255) NOT NULL,
  `message` longtext NOT NULL,
  `recipents_name` varchar(100) NOT NULL,
  `sender_name` varchar(100) NOT NULL,
  `deletion_status` tinyint(1) NOT NULL,
  `date_added` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`sent_mail_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `tbl_sent_mail`
--

INSERT INTO `tbl_sent_mail` (`sent_mail_id`, `user_id`, `to`, `from`, `cc`, `subject`, `message`, `recipents_name`, `sender_name`, `deletion_status`, `date_added`) VALUES
(1, 1, 'msnawazbd@gmail.com', 'noyon2nil@gmail.com', '', 'welcome message', 'The Entrepreneurship Center at AOU was another notable accomplishment we made in the past year. In the changing dynamics of the world, we believe our students have a role in the economic cycle and we work very hard in bringing out both the potential and the Entrepreneurs in each and every one of you. We welcome you to join the center and benefit from the training programs and mentorship we are offering so you can give yourself the chance to be the next entrepreneurship success story.  The first entrepreneurs as a result of the training started their own business including Swaleen Abboud (Ibtikart), Toni Abou Madi (Tabkit Mama), Sarkis Magarian and Jihane Nader (Loyal Services), and Adel Al Saady (Finish Line Online). We have witnessed more businesses this year including a company by AOU student George Ghafari which won the Berytech entrepreneurship Award.  \r\n\r\nOn the community level, we are very pleased to have hosted the 1st Mediterranean Interdisciplinary Forum on Social Sciences and Humanities in April 2014. An academic summit aimed at bringing together researchers in the area of social sciences and humanities, who by presenting their scientific attainments will contribute to the development of the interdisciplinary concept in higher education. As a leading academic institution we strive to alleviate to the quality of education worldwide and play an instrumental role in making breakthrough progress for the well-being of the generations to come.', '', '', 0, '2016-06-05 07:08:40'),
(2, 1, 'admin@gmail.com', 'demo@gmail.com', '', 'Entrepreneurship ', 'The Entrepreneurship Center at AOU was another notable accomplishment we made in the past year. In the changing dynamics of the world, we believe our students have a role in the economic cycle and we work very hard in bringing out both the potential and the Entrepreneurs in each and every one of you. We welcome you to join the center and benefit from the training programs and mentorship we are offering so you can give yourself the chance to be the next entrepreneurship success story.  The first entrepreneurs as a result of the training started their own business including Swaleen Abboud (Ibtikart), Toni Abou Madi (Tabkit Mama), Sarkis Magarian and Jihane Nader (Loyal Services), and Adel Al Saady (Finish Line Online). We have witnessed more businesses this year including a company by AOU student George Ghafari which won the Berytech entrepreneurship Award.  \r\n\r\nOn the community level, we are very pleased to have hosted the 1st Mediterranean Interdisciplinary Forum on Social Sciences and Humanities in April 2014. An academic summit aimed at bringing together researchers in the area of social sciences and humanities, who by presenting their scientific attainments will contribute to the development of the interdisciplinary concept in higher education. As a leading academic institution we strive to alleviate to the quality of education worldwide and play an instrumental role in making breakthrough progress for the well-being of the generations to come.', '', '', 0, '2016-06-05 07:08:40'),
(3, 0, 'ms@gmail.com', 'msnawazbd@gmail.com', '', 'Welcome', 'Message for email\r\n                                    ', '', 'Shahid Nawaz', 0, '2016-06-06 11:10:31'),
(4, 1, 'user@gmail.com', 'admin@gmail.com', 'cc@gmail.com', 'Welcomw message', 'Welcome messages are sent to new members to welcome them to the family \r\nor any group or association. They are a way of introducing them and \r\nfamiliarizing them of the members of the family or the group. The \r\nmessages also tell them about the group or provide a brief description \r\nof the family. One can send the messages in different ways. Be it in \r\npaper or a beautifully decorated ornate, instead of forwarding text \r\nmessages one can send the welcome messages beautifully.\r\n                                    ', '', 'Atique It', 0, '2016-06-06 11:12:52'),
(5, 2, 'sas', 'sasa', '', 'sas', 'sasa', '', 'abab', 0, '2016-08-24 15:10:37');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_service`
--

CREATE TABLE IF NOT EXISTS `tbl_service` (
  `service_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `service_name` varchar(250) NOT NULL,
  `product_photo_before` varchar(255) NOT NULL,
  `product_photo_after` varchar(255) NOT NULL,
  `service_one_photo_before` varchar(255) NOT NULL,
  `service_one_photo_after` varchar(255) NOT NULL,
  `service_two_photo_before` varchar(255) NOT NULL,
  `service_two_photo_after` varchar(255) NOT NULL,
  `service_price` varchar(7) NOT NULL,
  `short_description` text NOT NULL,
  `description` text NOT NULL,
  `deletion_status` tinyint(1) NOT NULL,
  `publication_status` tinyint(1) NOT NULL,
  `date_added` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`service_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `tbl_service`
--

INSERT INTO `tbl_service` (`service_id`, `user_id`, `service_name`, `product_photo_before`, `product_photo_after`, `service_one_photo_before`, `service_one_photo_after`, `service_two_photo_before`, `service_two_photo_after`, `service_price`, `short_description`, `description`, `deletion_status`, `publication_status`, `date_added`) VALUES
(1, 1, 'graphics design', '', '', '', '', '', '', '10', '', '', 0, 0, '2016-07-18 07:04:21'),
(2, 1, 'Website image optimization', '(1)2.jpg', '(1)3.jpg', '(4)1.jpg', '(5)1.jpg', '(6)1.jpg', 'pYKz7gE1.jpg', '10', '<table class="table table_border table-striped dashboard_table_font" id="example"><tbody><tr class="even gradeC"><td></td>\r\n                                        <td class="des_width"> <p>\r\n Websites content is very important but depending on the quality of the \r\nimages it can either drive more people to a website or let people leave a\r\n website. Quality is obviously an important issue</p></td></tr></tbody></table>', '<table class="table table_border table-striped dashboard_table_font" id="example"><tbody><tr class="even gradeC"><td></td>\r\n                                        <td class="des_width"> <p>\r\n Websites content is very important but depending on the quality of the \r\nimages it can either drive more people to a website or let people leave a\r\n website. Quality is obviously an important issue</p></td></tr></tbody></table>', 0, 1, '2016-07-18 10:14:39'),
(3, 1, 'Website image optimization', 'celebrities_before_photoshop_touch_ups_640_07.jpg', 'celebrities_after_photoshop_touch_ups_640_07.jpg', '', '', '', '', '15', 'Website image optimization Website image optimization Website image optimization Website image optimization Website image optimization Website image optimization Website image optimization Website image optimization Website image optimization Website image optimization Website image optimization Website image optimization Website image optimization Website image optimization Website image optimization Website image optimization Website image optimization Website image optimization ', 'Website image optimization Website image optimization Website image optimization Website image optimization Website image optimization Website image optimization Website image optimization Website image optimization Website image optimization Website image optimization Website image optimization Website image optimization Website image optimization Website image optimization Website image optimization Website image optimization Website image optimization Website image optimization Website image optimization Website image optimization Website image optimization Website image optimization Website image optimization Website image optimization Website image optimization Website image optimization Website image optimization Website image optimization Website image optimization Website image optimization Website image optimization Website image optimization Website image optimization Website image optimization Website image optimization Website image optimization ', 0, 1, '2016-07-18 10:46:41'),
(4, 1, 'Website image optimization', 'photoshop-face-before.jpg', 'photoshop-face-after.jpg', '', '', '', '', '20', 'Website image optimization Website image optimization Website image optimization Website image optimization Website image optimization Website image optimization Website image optimization Website image optimization Website image optimization Website image optimization Website image optimization Website image optimization Website image optimization Website image optimization Website image optimization Website image optimization Website image optimization Website image optimization  ', 'Website image optimization Website image optimization Website image optimization Website image optimization Website image optimization Website image optimization Website image optimization Website image optimization Website image optimization Website image optimization Website image optimization Website image optimization Website image optimization Website image optimization Website image optimization Website image optimization Website image optimization Website image optimization ', 0, 1, '2016-07-18 10:47:09');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_sub_quote`
--

CREATE TABLE IF NOT EXISTS `tbl_sub_quote` (
  `sub_quote_id` int(11) NOT NULL AUTO_INCREMENT,
  `quote_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `admin_id` int(11) NOT NULL,
  `job_category_id` int(11) NOT NULL,
  `job_title` varchar(255) NOT NULL,
  `return_file_type` varchar(50) NOT NULL,
  `need_path` tinyint(1) NOT NULL,
  `number_of_image` varchar(2) NOT NULL,
  `job_type` tinyint(1) NOT NULL,
  `one_time_price_usd` varchar(7) NOT NULL,
  `one_time_dead_line_hr` varchar(7) NOT NULL,
  `dead_line_days` varchar(3) NOT NULL,
  `monthly_work_volume` varchar(7) NOT NULL,
  `monthly_amount_usd` varchar(7) NOT NULL,
  `instruction_message` text NOT NULL,
  `publication_status` tinyint(1) NOT NULL,
  `deletion_status` tinyint(1) NOT NULL,
  `user_confirm` tinyint(1) NOT NULL DEFAULT '0',
  `admin_confirm` tinyint(1) NOT NULL DEFAULT '0',
  `payment_status` tinyint(1) NOT NULL COMMENT '1 for paid and 0 for pending',
  `date_added` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`sub_quote_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=17 ;

--
-- Dumping data for table `tbl_sub_quote`
--

INSERT INTO `tbl_sub_quote` (`sub_quote_id`, `quote_id`, `user_id`, `admin_id`, `job_category_id`, `job_title`, `return_file_type`, `need_path`, `number_of_image`, `job_type`, `one_time_price_usd`, `one_time_dead_line_hr`, `dead_line_days`, `monthly_work_volume`, `monthly_amount_usd`, `instruction_message`, `publication_status`, `deletion_status`, `user_confirm`, `admin_confirm`, `payment_status`, `date_added`) VALUES
(1, 2, 3, 0, 1, 'Graphics Design', 'psd', 1, '10', 0, '', '', '', '', '', '<p>SAFdvsdv sfdsfdsf</p>', 1, 0, 1, 1, 1, '2016-07-24 07:13:30'),
(2, 2, 4, 0, 1, 'graphics designer', 'jpg', 1, '44', 0, '', '', '', '', '', '<p>asdsadasdasd<br></p>', 1, 0, 0, 0, 0, '2016-07-25 09:03:30'),
(3, 2, 4, 0, 3, 'image resize', 'png', 0, '44', 0, '', '', '', '', '', '<p>tryrtyrt  tyryrtyrt trytryty <br></p>', 1, 0, 0, 0, 0, '2016-07-25 10:10:07'),
(4, 2, 4, 0, 2, 'graphics designer', 'jpg', 1, '44', 1, '12', '', '2', '', '', '<p>sgsd gsdgfds sfdg fds sd sdfg sdf s<br></p>', 0, 0, 0, 1, 0, '2016-07-25 11:09:26'),
(5, 2, 4, 1, 3, 'Graphics Design', 'jpg', 0, '10', 1, '151', '16', '', '', '', '<p>fgfdsgfd fdsgfg sd sd </p>', 0, 0, 0, 0, 0, '2016-07-25 12:23:06'),
(6, 2, 4, 1, 3, 'Graphics Design', 'jpg', 0, '10', 1, '151', '4', '', '', '', '<p>fghghdgh  dfg fd </p>', 0, 0, 0, 1, 0, '2016-07-25 12:27:12'),
(7, 2, 4, 0, 3, 'image resize', 'jpg', 1, '44', 1, '12', '', '6', '', '', '<p>fdggfdg fgfg <br></p>', 0, 0, 1, 1, 1, '2016-07-25 12:42:16'),
(8, 2, 3, 1, 3, 'Graphics Design', 'tif', 0, '10', 1, '151', '5', '', '10', '', '<p>wrtwrtwet wert wert we</p>', 0, 0, 1, 1, 1, '2016-07-25 12:57:28'),
(9, 2, 1, 0, 2, 'eeeeeeeeee', 'jpg', 1, 'ee', 0, '455', '', '', '', '', 'dwdwdw', 0, 0, 0, 1, 0, '2016-10-14 13:32:00'),
(10, 2, 3, 0, 5, '14353', 'png', 0, '15', 1, '13232', '353', '535', '3525', '35235', '<p>767567567<br></p>', 0, 0, 1, 1, 1, '2016-11-14 07:29:54'),
(11, 2, 1, 0, 5, 'wergfwergerg', 'other', 0, 're', 1, '584', 'erger', 'rgr', 'gerg', 'erg', 'reg', 0, 0, 0, 0, 0, '2016-12-18 18:08:56'),
(12, 2, 3, 0, 2, 'werty', 'jpg', 0, '10', 1, '10', '12', '3', '3', '12', 'asdfg', 0, 0, 1, 0, 0, '2016-12-26 11:18:21'),
(13, 2, 2, 0, 3, 'sdfg', 'jpg', 0, '20', 1, '20', '2', '2', '2', '20', 'fgh', 0, 0, 0, 0, 0, '2017-01-02 17:32:35'),
(14, 2, 18, 0, 3, 'Test Job', 'png', 1, '2', 1, '200', '24', 'Sun', '2', '100', '<p>This is test message.</p>', 0, 0, 1, 0, 0, '2017-02-02 06:05:37'),
(15, 4, 1, 0, 3, 'Test Quote', 'jpg', 1, '20', 1, '200', '24', '2', '200', '200', 'Please This is Urgent', 0, 0, 0, 1, 0, '2017-02-02 06:31:13'),
(16, 4, 18, 0, 5, 'Sub Quote From Syd', 'jpg', 0, '50', 1, '500', '9', '4', '2', '3', 'Test Quote<br><p><br></p>', 0, 0, 1, 1, 1, '2017-02-02 07:11:23');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_testimonial`
--

CREATE TABLE IF NOT EXISTS `tbl_testimonial` (
  `testimonial_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `testimonial` text NOT NULL,
  `status` tinyint(1) NOT NULL,
  `deletion_status` tinyint(1) NOT NULL,
  `date_added` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`testimonial_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `tbl_testimonial`
--

INSERT INTO `tbl_testimonial` (`testimonial_id`, `user_id`, `testimonial`, `status`, `deletion_status`, `date_added`) VALUES
(1, 1, 'Hey are you there!!!!!!', 1, 0, '2016-07-14 12:14:29'),
(2, 3, '<p>1234545343434634634646<br></p>', 0, 0, '2016-11-14 07:28:44');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_user_login_info`
--

CREATE TABLE IF NOT EXISTS `tbl_user_login_info` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `full_name` varchar(100) NOT NULL,
  `email_address` varchar(100) NOT NULL,
  `user_password` varchar(300) NOT NULL,
  `access_label` tinyint(1) NOT NULL,
  `activation_status` tinyint(1) NOT NULL,
  `activation_key` varchar(300) NOT NULL,
  `deletion_status` tinyint(1) NOT NULL,
  `date_added` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=19 ;

--
-- Dumping data for table `tbl_user_login_info`
--

INSERT INTO `tbl_user_login_info` (`user_id`, `full_name`, `email_address`, `user_password`, `access_label`, `activation_status`, `activation_key`, `deletion_status`, `date_added`) VALUES
(1, 'Super Admin', 'super_admin@atique-it.com', 'fe01ce2a7fbac8fafaed7c982a04e229', 1, 1, '', 1, '2016-06-06 12:07:04'),
(2, 'Admin', 'admin@atique-it.com', 'fe01ce2a7fbac8fafaed7c982a04e229', 2, 1, '', 1, '2016-06-06 12:07:04'),
(3, 'User', 'user@atique-it.com', 'fe01ce2a7fbac8fafaed7c982a04e229', 3, 1, '', 0, '2016-06-06 12:07:28'),
(4, 'Nawaz', 'nawaz@atique-it.com', 'fe01ce2a7fbac8fafaed7c982a04e229', 3, 1, '', 0, '2016-06-06 12:31:22'),
(5, 'Daniel Arrington', 'danieltarrington2015@gmail.com', 'd41d8cd98f00b204e9800998ecf8427e', 3, 1, '', 1, '2016-08-07 05:36:59'),
(18, 'Syd Nirjhor', 'sydulislammazumder@gmail.com', 'c514c91e4ed341f263e458d44b3bb0a7', 3, 1, '5970942246aa9accb920cae682d7ef2b', 0, '2017-02-02 05:52:51');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_user_profile_info`
--

CREATE TABLE IF NOT EXISTS `tbl_user_profile_info` (
  `pro_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `first_name` varchar(50) NOT NULL,
  `last_name` varchar(50) NOT NULL,
  `address_1` varchar(255) NOT NULL,
  `address_2` varchar(255) NOT NULL,
  `web_site` varchar(300) DEFAULT NULL,
  `state` varchar(50) NOT NULL,
  `city` varchar(50) NOT NULL,
  `zip` varchar(20) NOT NULL,
  `country` varchar(50) NOT NULL,
  `phone` varchar(20) NOT NULL,
  `profile_picture` varchar(255) NOT NULL,
  `facebook` varchar(255) NOT NULL,
  `twitter` varchar(255) NOT NULL,
  `linkedin` varchar(255) NOT NULL,
  `google_plus` varchar(255) NOT NULL,
  PRIMARY KEY (`pro_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=18 ;

--
-- Dumping data for table `tbl_user_profile_info`
--

INSERT INTO `tbl_user_profile_info` (`pro_id`, `user_id`, `first_name`, `last_name`, `address_1`, `address_2`, `web_site`, `state`, `city`, `zip`, `country`, `phone`, `profile_picture`, `facebook`, `twitter`, `linkedin`, `google_plus`) VALUES
(1, 1, 'atique', 'it', 'House-6 (4th Floor), Road-2, Kalwalapara, Mirpur-1, Dhaka-1216, Bangladesh', 'House-6 (4th Floor), Road-2, Kalwalapara, Mirpur-1, Dhaka-1216, Bangladesh', NULL, 'Mirpur 1', 'Dhaka', '1216', 'Bangladesh', '(+880) 17 98079696 ', 'Auto-Icon-e1358838892537.png', '', '', '', ''),
(2, 2, '', '', '', '', NULL, '', '', '', '', '', '', '', '', '', ''),
(3, 3, 'Atique', 'IT', 'House-6 (4th Floor), Road-2, Kalwalapara, Mirpur-1, Dhaka-1216, Bangladesh', 'House-6 (4th Floor), Road-2, Kalwalapara, Mirpur-1, Dhaka-1216, Bangladesh', NULL, 'Mirpur 1', 'Dhaka', '1216', 'Bangladesh', '(+880) 17 98079696 ', '', '', '', '', ''),
(4, 4, '', '', '', '', NULL, '', '', '', '', '', '', '', '', '', ''),
(5, 5, '', '', '', '', NULL, '', '', '', '', '', '', '', '', '', ''),
(6, 6, '', '', '', '', NULL, '', '', '', '', '', '', '', '', '', ''),
(7, 8, 'adadasd', 'adasdasd', 'asdasdasdasdasdasdasd', 'asdasdasdasdasdasdasd', 'cham2.com', 'asdasd', 'asdasd', '1230', 'Bangladesh', '01688682085', '', '', '', '', ''),
(8, 9, 'test me', 'testttt', 'sfasdfsdfsdfsdfsdfsdf', 'sdfsdfsdfsdfsdfsdfsdff', 'sdfsdfsdfsdfsdf.com', 'fsdfsdfsdf', 'sdfsdfsdf', '1230', 'Bangladesh', '01686685245', '', '', '', '', ''),
(9, 10, 'adasdasd', 'asdasdasd', 'asdasdasd', 'asdasdasdasdasdasd', 'asdasd.com', 'asdasd', 'asdasd', '1230', 'Belgium', '01688682085', '', '', '', '', ''),
(10, 11, 'asdadsas', 'asdasdasd', 'asdasdasd', 'asdasdasdasd', 'asdasd.com', 'asdasdsd', 'asdasd', '1230', 'Belgium', '01688682085', '', '', '', '', ''),
(11, 12, 'adasdasdasd', 'asdasdasd', 'asdasdasdasd', 'asdasdasdasdasd', 'asdasd.com', 'asdasdsd', 'asdasd', '1230', 'Antigua & Deps', '01686685245', '', '', '', '', ''),
(12, 13, 'asdasdasd', 'asdasdasd', 'asdasdasdasdasd', 'asssssssssssssssdddd', 'asdasd.com', 'asdasd', 'asdasd', '1230', 'Belarus', '01688682085', '', '', '', '', ''),
(13, 14, 'Syd', 'Nirjhor', 'test address one', 'test address two and two and so on.', 'bytesncodes.com', 'Dhaka', 'Dhaka', '1230', 'Bangladesh', '01688682085', '', '', '', '', ''),
(14, 15, 'Syd', 'Nirjhor', 'This is test address one', 'This is test address two', 'mcha.com', 'Mirpur', 'Dhaka', '1230', 'Bangladesh', '01688682085', '', '', '', '', ''),
(15, 16, 'Test', 'Test2', 'this is test address one', 'this is test address two', 'mcha.com', 'Dhaka', 'Dhaka', '1230', 'Bangladesh', '01688682085', '', '', '', '', ''),
(16, 17, 'Syd', 'Nirjhor', 'thisjhsgdjahgdjaghsd', 'jhgjhgkjhgkjhgkjhgkjhgkjhg', 'mcha.com', 'Dhaka', 'Dhaka', '1230', 'Bangladesh', '01688682085', '', '', '', '', ''),
(17, 18, 'Syd', 'Nirjhor', 'this is test address one', 'this is test address two', 'mcha.com', 'Dhaka', 'Dhaka', '1230', 'Bangladesh', '01688682085', '', '', '', '', '');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
