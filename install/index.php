<?php

	error_reporting(0);
	require_once('includes/core_class.php');
	require_once('includes/database_class.php');
	
	if($_POST){
		
		$core = new Core();
		$database = new Database();
		$data=array(
			'hostname'=>$_POST['hostname'],
			'username'=>$_POST['username'],
			'password'=>$_POST['password'],
			'database'=>$_POST['database']
		);
		
		$result= $database->make_connection($data);
		
		//write database tables
		if($result){
			
			$result = $database->make_tables($data);
			
			if($result){
				//write the files
				
				$result=$core->write_db_file($data);
				if($result){
					
					$app_url='http://'.$_SERVER['HTTP_HOST'].str_replace('install','',dirname($_SERVER['SCRIPT_NAME']));
				
					echo "<p class='text-center lead'>Your Application Is Successfully Installed. </p>";
					echo "<p class='text-center'>Please rename your install folder or delete it to avaoid reinstalling your app again.</p>";
					echo "<p class='text-center'> Please visit <a href='".$app_url."'>YOUR APP after 10 Seconds</a> To go to your Application </p>";
					
				} else{
					
					echo "<p class='text-center lead'>Something went wrong.Please contact info@atique-it.com regarding installation subject.</p>";
					
				}
				
			}	
		}
	}
	
?>


<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <script type="text/javascript" src="http://cdnjs.cloudflare.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>
    <script type="text/javascript" src="http://netdna.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
    <link href="http://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.3.0/css/font-awesome.min.css"
    rel="stylesheet" type="text/css">
    <link href="http://pingendo.github.io/pingendo-bootstrap/themes/default/bootstrap.css"
    rel="stylesheet" type="text/css">
  </head>
  
  <body>
    <div class="section">
      <div class="container">
        <div class="row">
          <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-primary">
              <div class="panel-heading">
                <h2 class="text-center">Install Your App</h2>
              </div>
              <div class="panel-body">
                <form role="form" method="post" action="">
                  <div class="form-group">
                    <label class="control-label">HOSTNAME</label>
                    <input class="form-control input-lg" id="hostname" placeholder="localhost"
                    type="text" name="hostname" required="">
                  </div>
                  <div class="form-group">
                    <label class="control-label">USERNAME</label>
                    <input class="form-control input-lg" id="username" placeholder="root"
                    type="text" name="username" required="">
                  </div>
                  <div class="form-group">
                    <label class="control-label">USER PASSWORD</label>
                    <input class="form-control input-lg" id="password"
                    placeholder="User Password" type="password" name="password">
                  </div>
                  <div class="form-group">
                    <label class="control-label">DATABASE</label>
                    <input class="form-control input-lg" id="database" name="database"
                    placeholder="root" type="text" required="">
                  </div>
                  <br>
                  <button type="submit" class="btn btn-block btn-lg btn-primary">Install</button>
                </form>
              </div>
            </div>
          </div>
        </div>
        <p class="text-center">
          Powered By Atique-IT
        </p>
      </div>
    </div>
  </body>

</html>