<?php


//to write the file

class Core {
	
	
	function write_db_file($data) {
		
		$db_file_path='../taskshop/config/database.php';
		$template_path='database/database.php';
		$database_file= file_get_contents($template_path);
		
		$new= str_replace('%HOSTNAME%',$data['hostname'],$database_file);
		$new= str_replace('%USERNAME%',$data['username'],$new);
		$new= str_replace('%PASSWORD%',$data['password'],$new);
		$new= str_replace('%DATABASE%',$data['database'],$new);
		
		//echo $database_file;
		
		$handle=fopen($db_file_path,'w+');
		
		@chmod($db_file_path,0777);
		
		if(is_writeable($db_file_path)){
			
			if(fwrite($handle,$new)){
				
				return true;
			} else {
				return false;
			}
			
		} else {
			
			return false;
		}
		fclose($handle);
		
	}
	
}

?>