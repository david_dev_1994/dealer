<?php
//to write the database
class Database{
	
	function make_connection($data){
		
		//connect to db
		$con= mysqli_connect($data['hostname'],$data['username'],$data['password'],'');
		
		//check for errors
		if(mysqli_connect_errno()){
			echo "Failed to connect to MySQL: " . mysqli_connect_error();
		} else {
			
			
			//create db if not present
			$query= "CREATE DATABASE IF NOT EXISTS ".$data['database'];
			mysqli_query($con,$query);
			return true;
		}
		
		mysqli_close($con);
	}
	
	function make_tables($data){
		
		$sql_file_path = "assets/install.sql";
		
		//connect to db
		$con = mysqli_connect($data['hostname'],$data['username'],$data['password'],$data['database']);
		
		//check for errors
		if(mysqli_connect_errno()){
			echo "Failed to connect to MySQL: " . mysqli_connect_error();
			
		} else {
			
			$query= file_get_contents($sql_file_path);
			mysqli_multi_query($con,$query);
			return true;
		}
		mysqli_close($con);
		
	}
	
}

?>