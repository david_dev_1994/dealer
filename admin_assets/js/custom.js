// $(document).ready(function () {
    // $('.my').iconpicker();

    if ($('#keywords').length) {
        $('#keywords').tagsinput()
    }

    if ($('#category_name').length) {
        $('#category_name').tagsinput()
    }

    if ($('#title').length) {
        CKEDITOR.replace('title',
            {
                toolbar:
                    [
                        {name: 'basicstyles', items: ['Bold', 'Italic']}, {name: 'colors', items: ['TextColor']},
                        {name: 'styles', items: ['Font', 'FontSize']}
                    ]
            });
    }

    if ($('#sub_title').length) {

        CKEDITOR.replace('sub_title',
            {
                toolbar: [
                    {name: 'basicstyles', items: ['Bold', 'Italic']}, {name: 'colors', items: ['TextColor']},
                    {name: 'styles', items: ['Font', 'FontSize']}
                ]
            });
    }

    if ($('#content').length) {
        CKEDITOR.replace('content');
    }

    if ($('#content_1').length) {
        CKEDITOR.replace('content_1');
    }


    if ($('#copyright_text').length) {
        CKEDITOR.replace('copyright_text',
            {
                toolbar:
                    [
                        {name: 'basicstyles', items: ['Bold', 'Italic']}, {name: 'colors', items: ['TextColor']},
                        {name: 'styles', items: ['Font', 'FontSize']}
                    ]
            });
    }


    if ($('#color_picker').length) {
        $("#color_picker").spectrum({
            preferredFormat: "hex",
            showInput: true,
            showPalette: true,
            color: color
        });
    }


    $('#sliderOption').change(function () {
        var selected = $('#sliderOption option:selected');
        if (selected.val() == 'text') {
            $('#sliderTitle').removeClass('hide');
            if (!($('#sliderImage').hasClass('hide'))) {
                $('#sliderImage').addClass('hide');
                // $('#slideImage').val('');
                // $('#imagethumb').attr('src', '');
                // $('#imagethumb').attr('src', 'http://www.placehold.it/200x150/EFEFEF/AAAAAA&text=no+image');
            }
        }
        if (selected.val() == 'image') {
            $('#sliderImage').removeClass('hide');
            if (!($('#sliderTitle').hasClass('hide'))) {
                $('#sliderTitle').addClass('hide');
                // CKEDITOR.instances.title.setData('');
            }
        }

    });


    $('input[type=file]').change(function () {
        var input = this;
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $(input).closest('.fileinput-new').find('img').attr('src', e.target.result);
            };

            reader.readAsDataURL(input.files[0]);
        }
    });

    if ($('#removed').length) {
        var remove_list = [];
        $('.remove').on('click', function () {
            var id = $(this).data('id');
            if (!remove_list.includes(id)) {
                remove_list.push(id);
            }
            $('#removed').val(JSON.stringify(remove_list));
            $(this).closest('.fileinput-new').find('img').attr('src', 'http://www.placehold.it/200x150/EFEFEF/AAAAAA&text=no+image');
            $('input[name=image' + id + ']').val('');
        })
    }

    // if ($('#user_table').length) {
    //     $('#user_table').DataTable({
    //         //  ordering:false,
    //         // rowReorder: true,
    // //           "aaSorting": [],
    //         columnDefs: [
    //             // {orderable: false, className: 'reorder', targets: 0},
    //             {"targets": 1, "visible": false, "searchable": false},
    //             {orderable: true, targets: '_all'}
    //         ],
    //         // scrollY: 500,
    //         // paging: false
    //
    //     });
    // }

    if ($('#user_table').length) {
        $('#user_table').DataTable({
            // initComplete: function () {
            //     this.api().columns().every(function () {
            //         var column = this;
            //
            //         if (column.index() == 6) {  //skip if column 0
            //             $(column.footer()).append("<br>")
            //             var select = $('<select><option value=""></option></select>')
            //                 .appendTo($(column.header()))
            //                 .on('change', function () {
            //                     var val = $.fn.dataTable.util.escapeRegex(
            //                         $(this).val()
            //                     );
            //
            //                     column
            //                         .search(val ? '^' + val + '$' : '', true, false)
            //                         .draw();
            //                 });
            //             column.data().unique().sort().each(function (d, j) {
            //                 select.append('<option value="' + d + '">' + d + '</option>')
            //             } );
            //         }   //end of if
            //
            //     } );
            // }
            initComplete: function () {
                this.api().columns().every( function () {
                    var column = this;
                    if (column.index() == 6) {  //skip if column 0
                        var select = $('<select style="width: 100% !important;"><option value=""></option></select>')
                        .appendTo($(column.footer()).empty())
                        .on('change', function () {
                            var val = $.fn.dataTable.util.escapeRegex(
                                $(this).val()
                            );

                            column
                                .search(val ? '^' + val + '$' : '', true, false)
                                .draw();
                        });

                        column.data().unique().sort().each(function (d, j) {
                            select.append('<option value="' + d + '">' + d + '</option>')
                        });
                    }
                });
            }
        });
    }

    if ($('#user_table1').length) {
        $('#user_table1').DataTable({
            "aaSorting": [],
            initComplete: function () {
                this.api().columns().every( function () {
                    var column = this;
                    // if (column.index()) {  //skip if column 0
                        var select = $('<select style="width: 100% !important;"><option value=""></option></select>')
                        .appendTo($(column.footer()).empty())
                        .on('change', function () {
                            var val = $.fn.dataTable.util.escapeRegex(
                                $(this).val()
                            );

                            column
                                .search(val ? '^' + val + '$' : '', true, false)
                                .draw();
                        });

                        column.data().unique().sort().each(function (d, j) {
                            select.append('<option value="' + strip_tag(d) + '">' + d + '</option>')
                        });
                    // }
                });
            }
        });
    }

    if ($('#data_table').length) {
        var table_name = $('#data_table').data('name');
        var check = true;
        if (table_name == 'menu') {
            check = false;
        }
        var table = $('#data_table').DataTable({
            //  ordering:false,
            rowReorder: check,
//           "aaSorting": [],
            columnDefs: [
                {orderable: false, className: 'reorder', targets: 0},
                {"targets": 1, "visible": false, "searchable": false},
                {orderable: false, targets: '_all'}
            ],
            scrollY: 500,
            paging: false

        });

        table.on('row-reorder', function (e, diff, edit) {
            var changes = {};
            for (var i = 0, ien = diff.length; i < ien; i++) {
                var row_id = table.row(diff[i].node).data()[1];
                if (typeof  row_id != undefined)
                    changes[row_id] = strip_tag(diff[i].newData).trim();
            }

            var table_name = $('#data_table').data('name');
            var response = {};
            if (typeof table_name != undefined && Object.keys(changes).length > 0) {
                switch (table_name) {
                    case "services":
                        response = change_order(changes, table_name);
                        break;
                    case "slider":
                        response = change_order(changes, table_name);
                        break;
                    case "more_about":
                        response = change_order(changes, table_name);
                        break;
                    case "social_links":
                        response = change_order(changes, table_name);
                        break;
                    case "sub_service_list":
                        response = change_order(changes, table_name);
                        break;
                    case "menu":
                        response = change_order(changes, table_name);
                        break;
                    case "sub_menu":
                        response = change_order(changes, table_name);
                        break;
                    case "sub_menu_content":
                        response = change_order(changes, table_name);
                        break;
                    case "home_bottom":
                        response = change_order(changes, table_name);
                        break;
                    case "solution":
                        response = change_order(changes, table_name);
                        break;
                    case "service":
                        response = change_order(changes, table_name);
                        break;
                    default:
                        console.log("Invalid data provided");
                        break;
                }
            } else {
                console.log("Invalid data provided");
            }

        });

        function change_order(data, table) {
            $.ajax({
                url: base_url + "/admin/reorder",
                method: "POST",
                data: {"table": table, "data": JSON.stringify(data)},
                dataType: 'json',
                success: function (response) {
                    if (response.status != undefined) {
                        if (response.status == 200) {
                            console.log("Success");
                        } else {
                            console.log(response.message);
                        }
                    } else {
                        console.log("ERROR with status code");
                    }
                }
            })
        }
    }

    //Delete Record
    $('#confirm-delete').on('show.bs.modal', function (e) {
        $(this).find('.btn-ok').attr('href', $(e.relatedTarget).data('href'));
    });


// });

function strip_tag(str) {
    str = str.replace(/<{1}[^<>]{1,}>{1}/g, "");
    return str;
}

function showLoader() {
    $("#loading-image").removeClass("hidden");
    $("#textOfp").html("Please Wait...")
    $("#loading-image").show("slow");
    $("#loading-image").css("background", "none repeat scroll 0 0 rgba(0, 0, 0, 0.8");
}

function hideLoader() {
    $("#loading-image").addClass("hidden");
}

function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $(input).closest('.fileinput-new').find('img').attr('src', e.target.result);
        };

        reader.readAsDataURL(input.files[0]);
    }
}


function del_image(id, table,col_name) {

    var $el = $('#file');
    $el.wrap('<form>').closest('form').get(0).reset();
    $el.unwrap();

    $.ajax({
        url: base_url + "admin_inner/del_image",
        method: "POST",
        data: {table: table, id: id, col_name: col_name},
        dataType: 'json',
        success: function (response) {
            if (response.status == 200) {
                console.log("Success");
                $("#img").attr("src", "http://www.placehold.it/200x150/EFEFEF/AAAAAA&text=no+image");
            } else {
                console.log(response.message);
            }

        }
    });

}

function blockDealer(id) {
    $.ajax({
        url: admin_url+"/Registered_user/block_dealer",
        method: "POST",
        data: {id: id},
        dataType: 'json',
        beforeSend: function(){
            showLoader();
        },
        complete: function(){
            hideLoader();
        },
        success: function (response) {
            if (response.status == 200) {
                console.log("Success");
                $("#unblock_"+id).removeClass('hidden');
                $("#block_"+id).addClass('hidden');
                $("#successMsg").removeClass('hidden');
                $("#successMsg").html(response.message);
                setTimeout(function(){
                    $("#successMsg").addClass('hidden');
                    $("#successMsg").html('');
                }, 2000);
            } else {
                $("#errorMsg").removeClass('hidden');
                $("#errorMsg").html(response.message);
                setTimeout(function(){
                    $("#errorMsg").addClass('hidden');
                    $("#errorMsg").html('');
                }, 2000);
                console.log(response.message);
            }

        }
    });
}

function unblockDealer(id,email) {
    $.ajax({
        url: admin_url+"/Registered_user/unblock_dealer",
        method: "POST",
        data: {id: id,email:email},
        dataType: 'json',
        beforeSend: function(){
            showLoader();
        },
        complete: function(){
            hideLoader();
        },
        success: function (response) {
            if (response.status == 200) {
                console.log("Success");
                $("#block_"+id).removeClass('hidden');
                $("#unblock_"+id).removeClass('show');
                $("#unblock_"+id).addClass('hidden');
                $("#successMsg").removeClass('hidden');
                $("#successMsg").html(response.message);
                setTimeout(function(){
                    $("#successMsg").addClass('hidden');
                    $("#successMsg").html('');
                }, 2000);
            } else {
                $("#errorMsg").removeClass('hidden');
                $("#errorMsg").html(response.message);
                setTimeout(function(){
                    $("#errorMsg").addClass('hidden');
                    $("#errorMsg").html('');
                }, 2000);
                console.log(response.message);
            }

        }
    });
}

function deleteDealer(id,email) {
    $.ajax({
        url: admin_url+"/Registered_user/delete_dealer",
        method: "POST",
        data: {id: id,email:email},
        dataType: 'json',
        beforeSend: function(){
            showLoader();
        },
        complete: function(){
            hideLoader();
        },
        success: function (response) {
            if (response.status == 200) {
                console.log("Success");
                $('#dealerRow_'+id).fadeOut(300, function() { $(this).remove(); });
                $("#successMsg").removeClass('hidden');
                $("#successMsg").html(response.message);
                setTimeout(function(){
                    $("#successMsg").addClass('hidden');
                    $("#successMsg").html('');
                }, 2000);
            } else {
                $("#errorMsg").removeClass('hidden');
                $("#errorMsg").html(response.message);
                setTimeout(function(){
                    $("#errorMsg").addClass('hidden');
                    $("#errorMsg").html('');
                }, 2000);
                console.log(response.message);
            }

        }
    });
}