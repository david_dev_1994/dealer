<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/* * *******************************************#
  #      User Management System                 #
  #*********************************************#
  #      Author:     Atique IT                  #
  #      Email:      info@atique-it.com         #
  #      Website:    http://atique-it.com       #
  #                                             #
  #      Version:    15.2.1                     #
  #      Copyright:  (c) 2015 - Atique IT       #
  #                                             #
  #*********************************************# */

class Page_Model extends MSN_Model {

    public function __construct() {
        parent::__construct();
    }

    private $_service = 'tbl_service';
    private $_login = 'tbl_user_login_info';
    private $_basic = 'tbl_user_profile_info';

    public function select_all_service_info() {
        $result = $this->db->order_by('service_id', 'desc')->get_where($this->_service, array('publication_status' => 1, 'deletion_status' => 0));
        return $result->result_array();
    }

    public function update_privilege_by_id($access_id, $data) {
        $this->db->where('access_lebel_id', $access_id)->update($this->_access_lebel, $data);
        return $this->db->affected_rows();
    }

    public function insert_login_info($ldata) {
        $this->db->insert($this->_login, $ldata);
        return $this->db->insert_id();
    }

    public function insert_basic_info($bdata) {
        $this->db->insert($this->_basic, $bdata);
        return $this->db->insert_id();
    }
    
    public function check_info($data){
        
        $result=$this->db->get_where($this->_login,$data);
        return $result->num_rows();
    }
    
    public function insert($data,$table_name){
    	
    	
    	$result= $this->db->insert($table_name,$data);
    	return $result;
    }
    public function checkInfo($data,$table_name){
    
    	$result=$this->db->get_where($table_name,$data);
    	return $result->num_rows();
    
    }

    public function get_where($select = '', $where = '', $table)
    {
        if ((count($where) > 0 && is_array($where)) || (!is_array($where) && $where != '')) {
            $this->db->where($where);
        }

        if ($select == 'count') {
            return $this->db->count_all_results($table);
        } else {
            if ($select != '') {
                $this->db->select($select);
            }
            return $this->db->get($table)->row_array();
        }
    }

    public function getInfo($data,$table_name){
    
    	$result=$this->db->get_where($table_name,$data);
    	//print_r($result->result());
    	return $result->result();
    
    }
    public function updateInfo($data,$where,$table_name){
    	$result= $this->db->update($table_name,$data,$where);
    	//print_r($where);
    	return $result;
    	
    }

    public function get_user_info() {
        $user_id = $this->session->userdata('d_user_id');

        $this->db->select('*')
            ->from('tbl_user_login_info as log_info')
            ->join('tbl_user_profile_info as pro_info', 'log_info.user_id = pro_info.user_id')
            ->where('log_info.user_id', $user_id)
            ->where('log_info.activation_status', 1)
            ->where('log_info.deletion_status', 0);
        $query_result = $this->db->get();
        $result = $query_result->row_array();
        return $result;
    }
    
    public function get_sales_manager_info($user_id) {
        $this->db->select('*')
            ->from('tbl_user_login_info as log_info')
            ->join('tbl_user_profile_info as pro_info', 'log_info.user_id = pro_info.user_id')
            ->where('log_info.user_id', $user_id)
            ->where('log_info.activation_status', 1)
            ->where('log_info.deletion_status', 0);
        $query_result = $this->db->get();
        $result = $query_result->row_array();
        return $result;
    }

    public function get($table, $select = '', $order_by = '', $order = '')
    {
        if ($select != '') {
            $this->db->select($select);
        }
        if ($order_by != '') {
            $this->db->order_by($order_by, $order);
        }
        return $this->db->get($table)->result_array();
    }

}
