<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/* * *******************************************#
  #      User Management System                 #
  #*********************************************#
  #      Author:     Atique IT                  #
  #      Email:      info@atique-it.com         #
  #      Website:    http://atique-it.com       #
  #                                             #
  #      Version:    15.2.1                     #
  #      Copyright:  (c) 2015 - Atique IT       #
  #                                             #
  #*********************************************# */

class Setting_Model extends MSN_Model {
    
    private $_gen_setting = 'tbl_general_settings';
    private $_mail_setting = 'tbl_mail_settings';
    private $_priv_term_setting = 'tbl_privacy_terms';
    
    public function get_site_info(){
         $result = $this->db->order_by('id', 'desc')->get($this->_gen_setting);
         return $result->row_array();
    }
    
    public function update_general_info($data){
        $this->db->where('id', 1)->update($this->_gen_setting, $data);
        return $this->db->affected_rows();
    }
    
    public function update_logo_info($picture_name){
        $this->db->update($this->_gen_setting, array('logo' => $picture_name), array('id' => 1));
        return $this->db->affected_rows();
    }
    
    public function update_favicon_info($picture_name){
        $this->db->update($this->_gen_setting, array('favicon' => $picture_name), array('id' => 1));
        return $this->db->affected_rows();
    }
    
    public function get_mail_set_info(){
        $result = $this->db->order_by('mail_set_id', 'desc')->get($this->_mail_setting);
        return $result->row_array();
    }
    
    public function update_mail_set_info($data){
        $this->db->where('mail_set_id', 1)->update($this->_mail_setting, $data);
        return $this->db->affected_rows();
    }
    
    public function get_privacy_info(){
        $result = $this->db->order_by('privacy_terms_id', 'desc')->get($this->_priv_term_setting);
         return $result->row_array();
    }
    
    public function update_privacy($privacy){
        $this->db->update($this->_priv_term_setting, array('privacy' => $privacy), array('privacy_terms_id' => 1));
        return $this->db->affected_rows();
    }
    
    public function get_terms_info(){
        $result = $this->db->order_by('privacy_terms_id', 'desc')->get($this->_priv_term_setting);
         return $result->row_array();
    }
    
    public function update_terms($terms){
        $this->db->update($this->_priv_term_setting, array('terms' => $terms), array('privacy_terms_id' => 1));
        return $this->db->affected_rows();
    }
    
}
