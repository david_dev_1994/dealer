<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/* * *******************************************#
  #      User Management System                 #
  #*********************************************#
  #      Author:     Atique IT                  #
  #      Email:      info@atique-it.com         #
  #      Website:    http://atique-it.com       #
  #                                             #
  #      Version:    15.2.1                     #
  #      Copyright:  (c) 2015 - Atique IT       #
  #                                             #
  #*********************************************# */

class File_Manager_Model extends MSN_Model {

    public function __construct() {
        parent::__construct();
    }

    private $_file = 'tbl_file';

    public function insert_file_info($file_name) {
        $data['file_name'] = $file_name;
        $data['user_id'] = $this->session->userdata('user_id');
        $this->db->insert($this->_file, $data);
        return $this->db->insert_id();
    }

    public function get_all_file_info_by_user_id($user_id) {
        $result = $this->db->get_where($this->_file, array('user_id' => $user_id, 'deletion_status' => 0));
        return $result->result_array();
    }

    public function get_album_cover_photo_by_album_id($album_id) {
        $this->db->select('*')
                ->from('tbl_album as album')
                ->join('tbl_image as image', 'album.album_id = image.album_id')
                ->where('image.album_id', $album_id)
                ->where('image.deletion_status', 0)
                ->order_by('image.image_id', 'desc');
        $query_result = $this->db->get();
        $result = $query_result->result_array();
        return $result;
    }

    public function get_img_by_id($img_id) {
        $result = $this->db->get_where($this->_image, array('image_id' => $img_id));
        return $result->row_array();
    }

    public function delete_img_by_id($img_id) {
        $this->db->update($this->_image, array('deletion_status' => 1), array('image_id' => $img_id));
        return $this->db->affected_rows();
    }

    public function delete_album_info_by_id($album_id) {
        $this->db->update($this->_album, array('deletion_status' => 1), array('album_id' => $album_id));
        return $this->db->affected_rows();
    }
    
    public function save_image_info($picture_name){
         $data['img_name'] = $picture_name;

        $this->db->insert($this->_img, $data);
        return $this->db->insert_id();
    }

}
