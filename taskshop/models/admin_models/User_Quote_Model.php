<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class User_Quote_Model extends MSN_Model {

    public function __construct() {
        parent::__construct();
    }

    private $_quote = 'tbl_quote';
    private $_tbl_quote = 'quotes';
    private $_sub_quote = 'tbl_sub_quote';
    private $_jcat = 'tbl_job_category';

    public function get_all_published_quote_info() {
        $this->db->select('log_info.full_name, jcat.job_category_name, quote.*')
                ->from('tbl_user_login_info as log_info')
                ->join('tbl_quote as quote', 'log_info.user_id = quote.user_id')
                ->join('tbl_job_category as jcat', 'quote.job_category_id = jcat.job_category_id')
                ->where('quote.deletion_status', 0)
                ->where('quote.publication_status', 1)
                ->order_by('quote.quote_id', 'desc');
        $query_result = $this->db->get();
        $result = $query_result->result_array();
        return $result;
    }

    public function get_where($select = '', $where = '', $table)
    {
        if ((count($where) > 0 && is_array($where)) || (!is_array($where) && $where != '')) {
            $this->db->where($where);
        }

        if ($select == 'count') {
            return $this->db->count_all_results($table);
        } else {
            if ($select != '') {
                $this->db->select($select);
            }
            return $this->db->get($table)->row_array();
        }
    }
    
    //change 1/11/2019
    public function get_quotes_by_userID($type = 2,$user_id){
        $this->db->select('*')
            ->from($this->_tbl_quote)
            ->where("status=$type")
            ->where("user_id=$user_id");
        $query_result = $this->db->get();
        $result = $query_result->result_array();
        return $result;
    }

    public function get_all_quote_info_by_quote_id($quote_id) {
        $this->db->select('log_info.full_name, jcat.job_category_name, sub_quote.*')
                ->from('tbl_user_login_info as log_info')
                ->join('tbl_sub_quote as sub_quote', 'log_info.user_id = sub_quote.user_id')
                ->join('tbl_quote as quote', 'quote.quote_id = sub_quote.quote_id')
                ->join('tbl_job_category as jcat', 'quote.job_category_id = jcat.job_category_id')
                ->where('sub_quote.quote_id', $quote_id)
                ->where('quote.deletion_status', 0)
                ->order_by('quote.quote_id', 'desc');
        $query_result = $this->db->get();
        $result = $query_result->result_array();
        return $result;
    }

    public function get_all_quote_info_by_user_id($user_id) {
        $this->db->select('log_info.full_name, jcat.job_category_name, quote.*')
                ->from('tbl_user_login_info as log_info')
                ->join('tbl_quote as quote', 'log_info.user_id = quote.user_id')
                ->join('tbl_job_category as jcat', 'quote.job_category_id = jcat.job_category_id')
                ->where('quote.user_id', $user_id)
                ->where('quote.deletion_status', 0)
                ->order_by('quote.quote_id', 'desc');
        $query_result = $this->db->get();
        $result = $query_result->result_array();
        return $result;
    }
    
    public function get_all_quote_info_by_quote_id_and_user_id($quote_id, $user_id){
        $this->db->select('*')
                ->from('quotes')
                ->where('user_id', $user_id)
                ->where('id', $quote_id);
        $query_result = $this->db->get();
        $result = $query_result->row_array();
        return $result;
    }

    public function get_job_category_info() {
        $result = $this->db->order_by('job_category_id', 'desc')->get_where($this->_jcat, array('publication_status' => 1));
        return $result->result_array();
    }

    public function insert_quote_info($data) {
        $this->db->insert($this->_tbl_quote, $data);
        return $this->db->insert_id();
    }

    public function insert_sub_quote_info($data) {
        $this->db->insert($this->_sub_quote, $data);
        return $this->db->insert_id();
    }

    public function unpublished_quote_by_id($quote_id) {
        $this->db->update($this->_quote, array('publication_status' => 0), array('quote_id' => $quote_id));
        return $this->db->affected_rows();
    }

    public function published_quote_by_id($quote_id) {
        $this->db->update($this->_quote, array('publication_status' => 1), array('quote_id' => $quote_id));
        return $this->db->affected_rows();
    }

    public function get_quote_by_id($quote_id) {
        $result = $this->db->get_where($this->_quote, array('quote_id' => $quote_id));
        return $result->row_array();
    }

    public function update_quote_by_id($quote_id,$user_id, $data) {
        $this->db->where('id', $quote_id)
            ->where('user_id', $user_id)
            ->update($this->_tbl_quote, $data);
        return $this->db->affected_rows();
    }

//    public function delete_quote_by_id($quote_id) {
//        $this->db->update($this->_quote, array('deletion_status' => 1), array('quote_id' => $quote_id));
//        return $this->db->affected_rows();
//    }

    public function get_last_sub_quote_info_by_quote_id($quote_id) {
        $result = $this->db->where('quote_id', $quote_id)->order_by('sub_quote_id', 'desc')->get($this->_sub_quote);
        return $result->row_array();
    }

    public function get_sub_quote_info_by_sub_quote_id($sub_quote_id) {
        $result = $this->db->get_where($this->_sub_quote, array('sub_quote_id' => $sub_quote_id));
        return $result->row_array();
    }
    
    public function user_confirm_sub_quote_by_id($sub_quote_id){
        $this->db->update($this->_sub_quote, array('user_confirm' => 1), array('sub_quote_id' => $sub_quote_id));
        return $this->db->affected_rows();
    }
    
    public function admin_confirm_sub_quote_by_id($sub_quote_id){
        $this->db->update($this->_sub_quote, array('admin_confirm' => 1), array('sub_quote_id' => $sub_quote_id));
        return $this->db->affected_rows();
    }
    
    public function get_sub_quote_info_by_id($sub_quote_id){
        $this->db->select('log_info.full_name, jcat.job_category_name, sub_quote.*')
                ->from('tbl_user_login_info as log_info')
                ->join('tbl_sub_quote as sub_quote', 'log_info.user_id = sub_quote.user_id')
                ->join('tbl_job_category as jcat', 'sub_quote.job_category_id = jcat.job_category_id')
                ->where('sub_quote.sub_quote_id', $sub_quote_id)
                ->where('sub_quote.deletion_status', 0);
        $query_result = $this->db->get();
        $result = $query_result->row_array();
        return $result;
    }

    public function get_all_price_list(){
        $this->db->select('*')
            ->from('price_list');
        $query_result = $this->db->get();
        $result = $query_result->result_array();
        return $result;
    }

    public function get_all_raw_parts(){
        $this->db->select('*')
            ->from('raw_parts');
        $query_result = $this->db->get();
        $result = $query_result->result_array();
        return $result;
    }

    public function delete_quote_by_id($quote_id) {
        $quote_id = strip_tags($quote_id);
        $this->db->query("delete from quotes where id = $quote_id");
        return $this->db->affected_rows();
    }

    public function get_quotes($type=1){
        $user_id = $this->session->userdata('d_user_id');

        $this->db->select('*')
            ->from($this->_tbl_quote)
            ->where("status=$type")
            ->where("user_id=$user_id");
        $query_result = $this->db->get();
        $result = $query_result->result_array();
        return $result;
    }
    
        public function get_quotes_by_date(){
        $sql = 'SELECT * FROM quotes where status=1 and `created_date` <= UNIX_TIMESTAMP(NOW() - INTERVAL 80 day);';

        $result = $this->db->query($sql);
        $result = $result->result_array();
        return $result;
    }

    public function delete_quotes_by_date(){
        $sql = 'DELETE FROM quotes where status=1 and `created_date` <= UNIX_TIMESTAMP(NOW() - INTERVAL 90 day);';
        $result = $this->db->query($sql);
//        return $result;
    }


}
