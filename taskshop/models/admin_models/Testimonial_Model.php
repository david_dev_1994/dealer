<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/* * *******************************************#
  #      User Management System                 #
  #*********************************************#
  #      Author:     Atique IT                  #
  #      Email:      info@atique-it.com         #
  #      Website:    http://atique-it.com       #
  #                                             #
  #      Version:    15.2.1                     #
  #      Copyright:  (c) 2015 - Atique IT       #
  #                                             #
  #*********************************************# */

class Testimonial_Model extends MSN_Model {

    public function __construct() {
        parent::__construct();
    }

    private $_testimonial = 'tbl_testimonial';
    private $_jcat = 'tbl_job_category';

    public function get_all_testimonial_info() {
        $this->db->select('log_info.full_name, log_info.email_address, testimonial.*')
                ->from('tbl_user_login_info as log_info')
                ->join('tbl_testimonial as testimonial', 'log_info.user_id = testimonial.user_id')
                ->where('testimonial.deletion_status', 0)
                ->order_by('testimonial.testimonial_id', 'desc');
        $query_result = $this->db->get();
        $result = $query_result->result_array();
        return $result;
    }

    public function unpublished_testimonial_by_id($testimonial_id) {
        $this->db->update($this->_testimonial, array('status' => 0), array('testimonial_id' => $testimonial_id));
        return $this->db->affected_rows();
    }

    public function published_testimonial_by_id($testimonial_id) {
        $this->db->update($this->_testimonial, array('status' => 1), array('testimonial_id' => $testimonial_id));
        return $this->db->affected_rows();
    }

    public function get_testimonial_by_id($testimonial_id) {
        $result = $this->db->get_where($this->_testimonial, array('testimonial_id' => $testimonial_id, 'deletion_status' => 0));
        return $result->row_array();
    }

    public function delete_testimonial_by_id($testimonial_id) {
        $this->db->update($this->_testimonial, array('deletion_status' => 1), array('testimonial_id' => $testimonial_id));
        return $this->db->affected_rows();
    }

    public function insert_testimonial_info($data){
        $this->db->insert($this->_testimonial, $data);
        return $this->db->insert_id();
    }
}
