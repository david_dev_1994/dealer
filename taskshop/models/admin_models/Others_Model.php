<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Others_Model extends MSN_Model {

    public function __construct() {
        parent::__construct();
    }

    private $_inbox = 'tbl_mail_inbox';
    private $_general = 'tbl_general_settings';

    public function get_all_unred_notification() {
        $this->db->from('tbl_mail_inbox')
                ->where('reading_status', 0);
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function count_all_admin() {
        $this->db->from('tbl_user_login_info')
                ->where('access_label', 2);
        $query = $this->db->get();
        return $query->num_rows();
    }
    
    public function count_all_dealers($days = null) {
        $this->db->from('tbl_user_login_info')
                ->where('access_label', 3);
        if($days){
            $this->db->where(' FROM_UNIXTIME(date_added) >= now() - interval 30 day');
        }
        $query = $this->db->get();
        return $query->num_rows();
    }
    
     public function active_users($days = null){
        $this->db->from('tbl_user_login_info')
            ->where('activation_status',1);
        if($days){
            $this->db->where(' FROM_UNIXTIME(date_added) >= now() - interval 30 day');
        }
        $query = $this->db->get();

        return $query->num_rows();
    }

    public function count_users() {
        $this->db->from('tbl_user_login_info')
                ->where('access_label', 3);
        $query = $this->db->get();
        return $query->num_rows();
    }

    // public function open_quote($days = null) {
    //     $this->db->from('quotes')
    //             ->where('status', 1);
    //     if($days){
    //         $this->db->where(' FROM_UNIXTIME(created_date) >= now() - interval 30 day');
    //     }
    //     $query = $this->db->get();
    //     return $query->num_rows();
    // }
    
    public function open_quote($days = null) {
        $this->db->from('quotes')
                ->where('status', 1);
        if($days == 1){
            $this->db->where(' FROM_UNIXTIME(created_date) >= now() - interval 30 day');
        }elseif ($days == 2){
            $this->db->where(' FROM_UNIXTIME(created_date) >= now() - interval 7 day');
        }
        $query = $this->db->get();
        $return  = array();
        $return['count'] = $query->num_rows();
        $return['data'] = $query->result_array();
        return $return;
    }

    // public function open_drafted_quote($days = null) {
    //     $this->db->from('quotes')
    //         ->where('status', 2);
    //     if($days){
    //         $this->db->where(' FROM_UNIXTIME(created_date) >= now() - interval 30 day');
    //     }
    //     $query = $this->db->get();
    //     return $query->num_rows();
    // }
    
    public function open_drafted_quote($days = null)
    {
        
        $this->db->from('quotes')
            ->where('status', 2);
            if($this->session->userdata('access_label') == 2 && !empty($this->session->userdata('email_address'))){
                $this->db->like('sales_manager_email', $this->session->userdata('email_address'));
            }
        if($days == 1){
            $this->db->where(' FROM_UNIXTIME(created_date) >= now() - interval 30 day');
        }elseif ($days == 2){
            $this->db->where(' FROM_UNIXTIME(created_date) >= now() - interval 7 day');
        }
        $query = $this->db->get();
        $return  = array();
        $return['count'] = $query->num_rows();
        $return['data'] = $query->result_array();
        return $return;
    }

    public function total_dollars($days = null) {
        if(!$days){
            $query = 'SELECT SUM(grandTotal) AS `total_dollars` FROM quotes WHERE status=1 ';
        }else{
            $query = 'SELECT SUM(grandTotal) AS `total_dollars` FROM quotes WHERE status=1 AND FROM_UNIXTIME(created_date) >= now() - interval 30 day';
        }

        $result = $this->db->query($query)->result_array();
        return $result;
    }

    public function drafted_total_dollars($days = null) {
        if(!$days){
            $query = 'SELECT SUM(grandTotal) AS `total_dollars` FROM quotes WHERE status=2';
        }else{
            $query = 'SELECT SUM(grandTotal) AS `total_dollars` FROM quotes WHERE status=2 AND FROM_UNIXTIME(created_date) >= now() - interval 30 day';
        }

        $result = $this->db->query($query)->result_array();
        return $result;
    }

    public function get($table, $select = '', $order_by = '', $order = '')
    {
        if ($select != '') {
            $this->db->select($select);
        }
        if ($order_by != '') {
            $this->db->order_by($order_by, $order);
        }
        return $this->db->get($table)->result_array();
    }

    public function update($row_id, $data, $table)
    {
        $this->db->where('id', $row_id);
        return $this->db->update($table, $data);
    }

    public function updateWithUserId($row_id, $data, $table)
    {
        $this->db->where('user_id', $row_id);
        return $this->db->update($table, $data);
    }

    public function save($table, $data)
    {
        $this->db->insert($table, $data);
        return $this->db->insert_id();
    }
    public function get_where($select = '', $where = '', $table)
    {
        if ((count($where) > 0 && is_array($where)) || (!is_array($where) && $where != '')) {
            $this->db->where($where);
        }

        if ($select == 'count') {
            return $this->db->count_all_results($table);
        } else {
            if ($select != '') {
                $this->db->select($select);
            }
            return $this->db->get($table)->row_array();
        }
    }

    public function delete($table, $id)
    {
        return $this->db->delete($table, array('id' => $id));
    }

}
