<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/* * *******************************************#
  #      User Management System                 #
  #*********************************************#
  #      Author:     Atique IT                  #
  #      Email:      info@atique-it.com         #
  #      Website:    http://atique-it.com       #
  #                                             #
  #      Version:    15.2.1                     #
  #      Copyright:  (c) 2015 - Atique IT       #
  #                                             #
  #*********************************************# */

class Mail_Model extends MSN_Model {

    public function __construct() {
        parent::__construct();
    }

    private $_inbox = 'tbl_mail_inbox';
    private $_sent = 'tbl_sent_mail';

    public function get_inbox_mail() {
        $result = $this->db->order_by('inbox_mail_id', 'desc')
                ->where('deletion_status', 0)
                ->get($this->_inbox);
        return $result->result_array();
    }

    public function get_inbox_mail_by_id($mail_id) {
        $result = $this->db->get_where($this->_inbox, array('inbox_mail_id' => $mail_id, 'deletion_status' => 0));
        return $result->row_array();
    }

    public function read_by_id($mail_id) {
        $this->db->update($this->_inbox, array('reading_status' => 1), array('inbox_mail_id' => $mail_id));
        return $this->db->affected_rows();
    }

    public function delete_inbix_mail_by_id($mail_id) {
        $this->db->update($this->_inbox, array('deletion_status' => 1), array('inbox_mail_id' => $mail_id));
        return $this->db->affected_rows();
    }

    public function get_sent_mail() {
        $result = $this->db->order_by('sent_mail_id', 'desc')
                ->where('deletion_status', 0)
                ->get($this->_sent);
        return $result->result_array();
    }

    public function get_sent_mail_by_id($mail_id) {
        $result = $this->db->get_where($this->_sent, array('sent_mail_id' => $mail_id, 'deletion_status' => 0));
        return $result->row_array();
    }

    public function delete_sent_mail_by_id($mail_id) {
        $this->db->update($this->_sent, array('deletion_status' => 1), array('sent_mail_id' => $mail_id));
        return $this->db->affected_rows();
    }

    function send_email($data, $templateName) {
        $this->load->library('email');
        $this->email->set_mailtype('html');
        $this->email->from($data['from'], $data['sender_name']);
        $this->email->to($data['to']);
        $this->email->cc($data['cc']);
        $this->email->subject($data['subject']);
        $body = $this->load->view('admin_panel/email_templates/' . $templateName, $data, true);
//        echo $body;
//        exit();
        $this->email->message($body);
//        $this->email->send();
//        $this->email->clear();
    }

    public function save_sent_email($mdata) {
        $this->db->insert($this->_sent, $mdata);
        return $this->db->insert_id();
    }

}
