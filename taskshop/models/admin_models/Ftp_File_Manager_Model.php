<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/* * *******************************************#
  #      User Management System                 #
  #*********************************************#
  #      Author:     Atique IT                  #
  #      Email:      info@atique-it.com         #
  #      Website:    http://atique-it.com       #
  #                                             #
  #      Version:    15.2.1                     #
  #      Copyright:  (c) 2015 - Atique IT       #
  #                                             #
  #*********************************************# */

class Ftp_File_Manager_Model extends MSN_Model {

    public function __construct() {
        parent::__construct();
    }

    private $_ftp = 'tbl_ftp_setting';
    
    public function get_ftp_info(){
        $result = $this->db->get($this->_ftp);
        return $result->row_array();
    }
    
    public function update_ftp_info_by_id($ftp_id, $data){
        $this->db->where('ftp_id', $ftp_id)->update($this->_ftp, $data);
        return $this->db->affected_rows();
    }
}
