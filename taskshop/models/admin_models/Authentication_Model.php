<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/* * *******************************************#
  #      User Management System                 #
  #*********************************************#
  #      Author:     Atique IT                  #
  #      Email:      info@atique-it.com         #
  #      Website:    http://atique-it.com       #
  #                                             #
  #      Version:    15.2.1                     #
  #      Copyright:  (c) 2015 - Atique IT       #
  #                                             #
  #*********************************************# */

class Authentication_Model extends MSN_Model {

    public function __construct() {
        parent::__construct();
    }
    
    private $_log_info = 'tbl_user_login_info';
    private $_pro_info = 'tbl_user_profile_info';

    public function check_login() {
        $email_address = $this->input->post('email_address', true);
        $user_password = $this->input->post('user_password', true);

        $this->db->select('*');
        $this->db->from('tbl_user_login_info');
        //$this->db->where("(username = '$username_or_email_address_or_phone_number' OR email_address = '$username_or_email_address_or_phone_number' OR phone_number = '$username_or_email_address_or_phone_number')");
        $this->db->where('email_address', $email_address);
        $this->db->where('user_password', md5($user_password));
        $this->db->where("(access_label = '1' OR access_label = '2' OR access_label = '3' OR access_label = '4' OR access_label = '5')");
        $this->db->where('activation_status', 1);
        $query_result = $this->db->get();
        $result = $query_result->row();
        // var_dump($result);
        // die();
        return $result;
    }
    
    public function insert_user_registration_info($data){
        $this->db->insert($this->_log_info, $data);
        return $this->db->insert_id();
    }
    
    public function create_user_profile($pdata){
        $this->db->insert($this->_pro_info, $pdata);
        return $this->db->insert_id();
    }
    
    public function ajax_email_check_info($email_address) {
        $this->db->select('*');
        $this->db->from('tbl_user_login_info');
        $this->db->where('email_address', $email_address);
        $query_result = $this->db->get();
        $result = $query_result->row();
        return $result;
    }

}
