<?php

if(! function_exists('debug'))
{
    function debug($data, $exit = true) {
        echo "<pre>".print_r($data, true)."</pre>";
        if($exit)
            exit(0);
    }
}


function build_post_fields( $data,$existingKeys='',&$returnArray=[]){
    if(($data instanceof CURLFile) or !(is_array($data) or is_object($data))){
        $returnArray[$existingKeys]=$data;
        return $returnArray;
    }
    else{
        foreach ($data as $key => $item) {
            build_post_fields($item,$existingKeys?$existingKeys."[$key]":$key,$returnArray);
        }
        return $returnArray;
    }
}


if(! function_exists('sendemail'))
{
    function sendemail($to,$from,$data,$subject) { //to,from,data/body,subject
        $url = 'https://api.sendgrid.com/';
        $user = 'ABDE_Villiers';
        $pass = 'pakistan234';

        $json_string = array(

            'to' => array($to),
            'category' => 'test_category'
        );

        $params = array(
            'api_user'  => $user,
            'api_key'   => $pass,
            'x-smtpapi' => json_encode($json_string),
            'to'        => $to,
            'subject'   => $subject,
            'html'      => $data,
            'text'      => $data,
            'from'      => $from,
        );

        $request =  $url.'api/mail.send.json';

// Generate curl request
        $session = curl_init($request);
// Tell curl to use HTTP POST
        curl_setopt ($session, CURLOPT_POST, true);
// Tell curl that this is the body of the POST
        curl_setopt ($session, CURLOPT_POSTFIELDS, $params);
// Tell curl not to return headers, but do return the response
        curl_setopt($session, CURLOPT_HEADER, false);
// Tell PHP not to use SSLv3 (instead opting for TLS)
        curl_setopt($session, CURLOPT_SSLVERSION, CURL_SSLVERSION_TLSv1_2);
        curl_setopt($session, CURLOPT_RETURNTRANSFER, true);

// obtain response
        $response = curl_exec($session);
        curl_close($session);

// print everything out
        return  json_decode($response);
    }
}

if(! function_exists('sendemailtomany'))
{
    function sendemailtomany($to,$from,$data,$subject) { //to,from,data/body,subject
        $url = 'https://api.sendgrid.com/';
        $user = 'ABDE_Villiers';
        $pass = 'pakistan234';

        $json_string = array(

            'to' => $to,
            'category' => 'test_category'
        );

        $params = array(
            'api_user'  => $user,
            'api_key'   => $pass,
            'x-smtpapi' => json_encode($json_string),
            'to'        => $to,
            'subject'   => $subject,
            'html'      => $data,
            'text'      => $data,
            'from'      => $from,
        );

        $request =  $url.'api/mail.send.json';

// Generate curl request
        $session = curl_init($request);
// Tell curl to use HTTP POST
        curl_setopt ($session, CURLOPT_POST, true);
// Tell curl that this is the body of the POST
        curl_setopt ($session, CURLOPT_POSTFIELDS, build_post_fields($params));
// Tell curl not to return headers, but do return the response
        curl_setopt($session, CURLOPT_HEADER, false);
// Tell PHP not to use SSLv3 (instead opting for TLS)
        curl_setopt($session, CURLOPT_SSLVERSION, CURL_SSLVERSION_TLSv1_2);
        curl_setopt($session, CURLOPT_RETURNTRANSFER, true);

// obtain response
        $response = curl_exec($session);
        curl_close($session);

// print everything out
        return  json_decode($response);
    }
}