<?php

defined('BASEPATH') OR exit('No direct script access allowed');

function admin_url() {
    $ci = &get_instance();
    $administrator_url = base_url('admin');
    //$administrator_url=base_url('admin/loginsetup');
    return $administrator_url;
}

function user_url(){
    $ci = &get_instance();
    $user_url = base_url();
    return $user_url;
}
