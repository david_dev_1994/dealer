<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'page';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;



//admin routes
$route['cms']='admin/authentication';
$route['admin']='admin/authentication';

$route['admin/add_admin']='admin/registered_user/add_admin';

$route['admin/price_limit']='admin/General/price_limit';

$route['admin/ProductAnnouncements']='admin/General/productAnnouncements';
$route['admin/addProductAnnouncement'] = 'admin/General/addProductAnnouncement';
$route['admin/addProductAnnouncement/(:num)'] = 'admin/General/addProductAnnouncement/$1';
$route['admin/deleteProductAnnouncement/(:num)'] = 'admin/General/deleteProductAnnouncement/$1';

$route['admin/dealerAgreement']='admin/General/dealerAgreement';
$route['admin/addDealerAgreement'] = 'admin/General/addDealerAgreement';
$route['admin/addDealerAgreement/(:num)'] = 'admin/General/addDealerAgreement/$1';
$route['admin/deleteDealerAgreement/(:num)'] = 'admin/General/deleteDealerAgreement/$1';

$route['admin/marketingMaterials']='admin/General/marketingMaterials';
$route['admin/addMarketingMaterial'] = 'admin/General/addMarketingMaterial';
$route['admin/addMarketingMaterial/(:num)'] = 'admin/General/addMarketingMaterial/$1';
$route['admin/deleteMarketingMaterial/(:num)'] = 'admin/General/deleteMarketingMaterial/$1';

$route['admin/brochures']='admin/General/brochures';
$route['admin/addBrochure'] = 'admin/General/addBrochure';
$route['admin/addBrochure/(:num)'] = 'admin/General/addBrochure/$1';
$route['admin/deleteBrochure/(:num)'] = 'admin/General/deleteBrochure/$1';

$route['admin/addBrochureLink'] = 'admin/General/addBrochureLink';
$route['admin/addBrochureLink/(:num)'] = 'admin/General/addBrochureLink/$1';
$route['admin/deleteBrochureLink/(:num)'] = 'admin/General/deleteBrochureLink/$1';

$route['admin/corporateAnnouncements']='admin/General/corporateAnnouncements';
$route['admin/addCorporateAnnouncement'] = 'admin/General/addCorporateAnnouncement';
$route['admin/addCorporateAnnouncement/(:num)'] = 'admin/General/addCorporateAnnouncement/$1';
$route['admin/deleteCorporateAnnouncement/(:num)'] = 'admin/General/deleteCorporateAnnouncement/$1';

$route['admin/videos']='admin/General/videos';
$route['admin/addVideo'] = 'admin/General/addVideo';
$route['admin/addVideo/(:num)'] = 'admin/General/addVideo/$1';
$route['admin/deleteVideo/(:num)'] = 'admin/General/deleteVideo/$1';

$route['admin/add_dealer']='admin/registered_user/add_dealer';
$route['admin/registered_dealers']='admin/registered_user/registered_dealers';
$route['admin/quote_listing']='admin/Admin_quote/quote_listing';
$route['admin/delete_quote/(:num)']='admin/Admin_quote/delete_quote/$1';
$route['admin/quote/show/(:num)'] = 'admin/Admin_quote/viewonly/$1';
//$route['admin/registered_dealers']='admin/registered_user/registered_dealers';




//$route['home/(:any)'] = "home/whatever/$1";
//$route['dashboard']='admin_controllers/dashboard';

//change 1/11/2019
$route['admin/add_gps_dealer_manager'] = 'admin/dashboard/addGpsDealerManager';
$route['admin/gps_info/(:num)'] = 'admin/registered_user/get_gps_d_info/$1';
$route['page/gpsmanager'] = 'Page/gpsManager';
//draft quotes for gps manager
$route['quote/dview/(:num)/(:num)'] = 'Quote/dlistquotes/$1/$2';
$route['quote/dredirect/(:num)/(:num)'] = 'Quote/dredirect/$1/$2';
$route['quote/ddredirect/(:num)/(:num)'] = 'Quote/ddredirect/$1/$2';
$route['dquote/(:num)/(:num)'] = 'Quote/dquote/$1/$2';
$route['quote/dshow/(:num)/(:num)'] = 'Quote/dviewonly/$1/$2';
$route['generate_quote/(:num)/(:num)'] = 'Quote/generate_quote/$1/$2';
$route['quote/singleQuote/(:num)'] = 'Quote/singleQuote/$1';



//public routes
$route['user_login'] = 'User_login/index';
$route['dealer_profile'] = 'User_login/user_profile';
$route['quote'] = 'Quote/quote';
$route['quote/(:num)'] = 'Quote/quote/$1';
$route['generate_quote/(:num)'] = 'Quote/generate_quote/$1';
$route['login'] = 'Page/registration';
$route['quote/view/(:num)'] = 'Quote/listquotes/$1';
$route['quote/show/(:num)'] = 'Quote/viewonly/$1';
$route['quote/redirect/(:num)'] = 'Quote/redirect/$1';
$route['quote/delete_quote/(:num)'] = 'Quote/delete_quote/$1';
$route['quote/getQuote/(:num)/(:num)'] = 'admin/Dashboard/getQuote/$1/$2';

$route['cronjob'] = 'Quote/email';


$route['training/videos'] = 'Front/training';

$route['training_course'] = 'Front/training_course';
$route['cbt'] = 'Front/cbt';
$route['schedule_training'] = 'Front/schedule_training';
$route['manuals'] = 'Front/manuals';

$route['press_release'] = 'Front/press_release';

$route['marketing_materials'] = 'Front/marketing_materials';
$route['dealer_agreement'] = 'Front/dealer_agreement';
$route['product_announcement'] = 'Front/product_announcement';
$route['viewProAnnouncement/(:num)'] = 'Front/viewProAnnouncement/$1';

$route['corporate_announcement'] = 'Front/corporate_announcement';
$route['viewCorpAnnouncement/(:num)'] = 'Front/viewCorpAnnouncement/$1';

$route['viewVideo/(:num)'] = 'Front/viewVideo/$1';

$route['brochures'] = 'Front/brochures';
$route['notices'] = 'Front/notices';
$route['documents'] = 'Front/documents';