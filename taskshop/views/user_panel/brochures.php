<section id="min-wrapper">
    <div id="main-content">
        <div class="container-fluid">
            <div>
                <h2>Brochures</h2>
                <div class="title text-center login-sub-title"><span><i>All Listings Will Be Displayed Below</i></span></div>
            </div>
        </div>

        <?php if ($this->session->flashdata('message')){?>
            <div class=" alert alert-success alert-dismissible text-center row">
                <?=$this->session->flashdata('message')?>
            </div>
        <?php }?>
        <style>.dataTables_length{padding-left:15px;}.dataTables_filter{padding-right:15px</style>
        <div class="container-fluid">
            <div class="row">
                <div class="as-table-wrapper">
                    <div class="row">
                        <div class="col-md-12">
                            <!-- BEGIN EXAMPLE TABLE PORTLET-->
                            <div class="portlet light no-border-top as-box-border">
                                <div class="portlet-body">
                                    <table class="table table-striped table-bordered table-hover dt-responsive" id="data_table" data-name="" style="background: none;width:100%;padding:0">
                                        <thead>
                                        <tr>
<!--                                            <th style="background:#767676;color:#FFF"> Serial Number </th>-->
                                            <th style="background:#767676;color:#FFF" class="hide"> id</th>
                                            <th style="background:#767676;color:#FFF"> Date Added</th>
                                            <th style="background:#767676;color:#FFF"> Title</th>
                                            <!--<th style="background:#767676;color:#FFF"> File</th>-->
                                            <th style="background:#767676;color:#FFF"> Action</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php
                                        foreach($data as $index=>$service){
                                            ?>
                                            <tr class="odd">
<!--                                                <td>--><?//= $index+1;?><!-- </td>-->
                                                <td class="hide"> <?= $service['id']; ?> </td>
                                                <td> <?= isset($service['created_at'])?date('m-d-Y',$service['created_at']):'';?> </td>
                                                <td> <?= isset($service['title'])?htmlentities($service['title']):'';?> </td>
                                                <!--<td> <?//= isset($service['docFile']) && !empty($service['docFile'])?$service['docFile']:$service['url'];?> </td>-->
                                                <td> <div class="btn-group">
                                                        <a href="<?= isset($service['url']) && !empty($service['url'])? $service['url'] : base_url('uploads/'.$service['docFile']); ?>" target="_blank" class="btn btn-primary">View</a>
                                                        <a href="<?= isset($service['url']) && !empty($service['url'])? $service['url'] : base_url('uploads/'.$service['docFile']); ?>" download class="btn btn-info">Download</a>
                                                </td>
                                            </tr>
                                            <?php
                                        }
                                        ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <!-- END EXAMPLE TABLE PORTLET-->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
