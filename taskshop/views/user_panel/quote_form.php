<?php

// echo "<pre>";print_r($_SERVER['SERVER_PORT']);exit;
$user['address_1'] = json_decode($user['address_1']);
$user['city'] = json_decode($user['city']);
$user['contactinfo'] = json_decode($user['contactinfo']);
$user['state'] = json_decode($user['state']);
$user['zip'] = json_decode($user['zip']);


if (isset($quote_info)) {
    $quote_info['shipping_address'] = json_decode($quote_info['shipping_address']);
    $quote_info['shipping_city'] = json_decode($quote_info['shipping_city']);
    $quote_info['shipping_contactinfo'] = json_decode($quote_info['shipping_contactinfo']);
    $quote_info['shipping_state'] = json_decode($quote_info['shipping_state']);
    $quote_info['shipping_zip'] = json_decode($quote_info['shipping_zip']);

    $quote_info['billing_address'] = json_decode($quote_info['billing_address']);
    $quote_info['billing_city'] = json_decode($quote_info['billing_city']);
    $quote_info['billing_contactinfo'] = json_decode($quote_info['billing_contactinfo']);
    $quote_info['billing_state'] = json_decode($quote_info['billing_state']);
    $quote_info['billing_zip'] = json_decode($quote_info['billing_zip']);

    $quote_info['model'] = json_decode($quote_info['model']);
    $quote_info['description'] = json_decode($quote_info['description']);
    $quote_info['family'] = json_decode($quote_info['family']);
    $quote_info['list_price'] = json_decode($quote_info['list_price']);
    $quote_info['dealer_price'] = json_decode($quote_info['dealer_price']);
    $quote_info['type'] = json_decode($quote_info['type']);
    $quote_info['quantity'] = json_decode($quote_info['quantity']);
    $quote_info['price'] = json_decode($quote_info['price']);
}
?>
<section id="min-wrapper">
    <div id="main-content">
        <div class="container-fluid">
        </div>

        <?php if ($this->session->flashdata('error')) { ?>
            <div class=" alert alert-danger alert-dismissible text-center row">
                <?= $this->session->flashdata('error') ?>
            </div>
        <?php } ?>

        <div class="container-fluid">
            <div class="row">
                <form class="needs-validation quote-form" enctype="multipart/form-data" method="post">

                    <div class="shadow-container">
                        <div class="container-fluid">
                            <div class="row">
                                <h2 class="text-center">Dealer Info</h2>
                                <div class="title text-center login-sub-title"><span><i>Dealer Information is displayed below</i></span>
                                </div>
                                <div class="form-row">
                                    <div class="col-md-4 mb-3">
                                        <label for="validationCustom02">Company Name: </label>
                                        <div style="display:inline-block"><?= isset($user['companyname']) && !empty($user['companyname']) ? $user['companyname'] : ''; ?></div>
                                        <input value="<?= isset($user['companyname']) && !empty($user['companyname']) ? $user['companyname'] : ''; ?>" class="form-control hidden" name="company">
                                    </div>
                                    <div class="col-md-4 mb-3">
                                        <label for="validationCustom01">Name: </label>
                                        <div style="display:inline-block"><?= isset($user['full_name']) && !empty($user['full_name']) ? $user['full_name'] : ''; ?></div>
                                        <input value="<?= isset($user['full_name']) && !empty($user['full_name']) ? $user['full_name'] : ''; ?>" class="form-control hidden" name="name">
                                    </div>
                                    <div class="col-md-4 mb-3">
                                        <label for="validationCustom02">Sales Manager: </label>
                                        <div style="display:inline-block"><?= isset($sales_manager_info['full_name']) && !empty($sales_manager_info['full_name']) ? $sales_manager_info['full_name']:''; ?></div>
                                        <input value="<?= isset($sales_manager_info['full_name']) && !empty($sales_manager_info['full_name']) ? $sales_manager_info['first_name']: ''; ?>" class="form-control hidden" name="sales_person">
                                    </div>
                                    <div class="col-md-4 mb-3">
                                        <label for="validationCustom01">Phone:</label>
                                        <div style="display:inline-block"><?= isset($user['phone']) && !empty($user['phone']) ? $user['phone'] : ''; ?></div>
                                        <input value="<?= isset($user['phone']) && !empty($user['phone']) ? $user['phone'] : ''; ?>" class="form-control hidden" name="pnum">
                                    </div>
                                    <div class="col-md-4 mb-3">
                                        <label for="validationCustom02">Email: </label>
                                        <div style="display:inline-block"><?= isset($user['email_address']) && !empty($user['email_address']) ? $user['email_address'] : ''; ?></div>
                                        <input value="<?= isset($user['email_address']) && !empty($user['email_address']) ? $user['email_address'] : ''; ?>" class="form-control hidden" name="email">
                                    </div>
                                    <div class="col-md-4 mb-3">
                                        <label for="validationCustom02">Sales Manager Email: </label>
                                        <div style="display:inline-block"><?= isset($sales_manager_info['email_address']) && !empty($sales_manager_info['email_address']) ? $sales_manager_info['email_address']: ''; ?></div>
                                        <input value="<?= isset($sales_manager_info['email_address']) && !empty($sales_manager_info['email_address']) ? $sales_manager_info['email_address']: ''; ?>" class="form-control hidden" name="sales_manager_email">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <br>
                        <br>
                        <div class="container-fluid">
                            <div class="row">
                                <div class="form-row">
                                    <div class="col-md-3 mb-3">
                                        <label for="validationCustom02">Dealer Purchase Order Number</label>
                                        <input type="text" value="<?= isset($quote_info['orderNumber']) && !empty($quote_info['orderNumber']) ? $quote_info['orderNumber'] : ''; ?>" class="form-control orderNumber" name="ordernumber">
                                        <div class="alert alert-danger alert-dismissible text-center orderNumberAlert" hidden="hidden"></div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-content1">
                            <div class="container-fluid">
                                <div class="inner">
                                    <p class="initial"></p>
                                </div>
                                <div class="quote-row-line"></div>
                                <div class="form-row" style="margin-top: 20px">
                                    <h3 class="dealer-form-sub-heading">Billing Address</h3>
                                </div>
                            </div>

                            <div class="row" style="padding-right: 28px; padding-bottom: 10px;">
                                <!--<button type="button" id="btnAdd" class="btn btn-primary col-md-1 col-md-offset-11 "style="margin-top:10px">Add More</button>-->
                            </div>
                            <?php if (!isset($quote_info)) {
                                foreach ($user['address_1'] as $i => $value) {
                                    ?>
                                    <div class="col-md-12 form-group row group">
                                        <div class="col-md-3 form-group">
                                            <label>Address</label>
                                            <input class="req form-control" type="text" name="o_address[]" id="address"
                                                   placeholder="Address" value="<?= $value ?>" required="required">
                                        </div>
                                        <div class="col-md-2 form-group">
                                            <label>Phone</label>
                                            <input class="req form-control" type="text" name="o_phone[]" id="b_name"
                                                   placeholder="Office Phone" value="<?= $user['contactinfo'][$i]; ?>"
                                                   required="required">
                                        </div>
                                        <div class="col-md-2 form-group">
                                            <label>City</label>
                                            <input class="req form-control" type="text" name='o_city[]' id="city"
                                                   placeholder="City" value="<?= $user['city'][$i]; ?>"
                                                   required="required">
                                        </div>
                                        <div class="col-md-2 form-group">
                                            <label>State</label>
                                            <input class="req form-control" type="text" name='o_state[]' id="state"
                                                   placeholder="State" value="<?= $user['state'][$i]; ?>"
                                                   required="required">
                                        </div>
                                        <div class="col-md-2 form-group">
                                            <label>PO Box / Zip Code</label>
                                            <input class="req form-control" type="text" name='o_zip[]' id="b_zip"
                                                   placeholder="PO / Zip Code" value="<?= $user['zip'][$i]; ?>"
                                                   required="required">
                                        </div>
                                        <div class="col-md-1 form-group">
                                            <label style="visibility:hidden;">Button</label>
                                            <!--<button type="button" class="btn btn-danger btnRemove bt_pos">X</button>-->
                                        </div>
                                    </div>
                                <?php }
                            } else {
                                foreach ($quote_info['billing_address'] as $i => $value) { ?>
                                    <div class="col-md-12 form-group row group">
                                        <div class="col-md-3 form-group">
                                            <label>Address</label>
                                            <input class="req form-control" type="text" name="o_address[]" id="address"
                                                   placeholder="Address" value="<?= $value ?>" required="required">
                                        </div>
                                        <div class="col-md-2 form-group">
                                            <label>Phone</label>
                                            <input class="req form-control" type="text" name="o_phone[]" id="b_name"
                                                   placeholder="Office Phone"
                                                   value="<?= $quote_info['billing_contactinfo'][$i]; ?>"
                                                   required="required">
                                        </div>
                                        <div class="col-md-2 form-group">
                                            <label>City</label>
                                            <input class="req form-control" type="text" name='o_city[]' id="city"
                                                   placeholder="City" value="<?= $quote_info['billing_city'][$i]; ?>"
                                                   required="required">
                                        </div>
                                        <div class="col-md-2 form-group">
                                            <label>State</label>
                                            <input class="req form-control" type="text" name='o_state[]' id="state"
                                                   placeholder="State" value="<?= $quote_info['billing_state'][$i]; ?>"
                                                   required="required">
                                        </div>
                                        <div class="col-md-2 form-group">
                                            <label>PO Box / Zip Code</label>
                                            <input class="req form-control" type="text" name='o_zip[]' id="b_zip"
                                                   placeholder="PO / Zip Code" value="<?= $quote_info['billing_zip'][$i]; ?>"
                                                   required="required">
                                        </div>
                                        <!--<div class="col-md-1 form-group">-->
                                            <!--<button type="button" class="btn btn-danger btnRemove bt_pos">X</button>-->
                                        <!--</div>-->
                                    </div>
                                <?php }
                            } ?>
                        </div>

                        <div class="form-content2">
                            <div class="container-fluid">
                                <div class="inner">
                                    <p class="initial"></p>
                                </div>
                                <div class="quote-row-line"></div>
                                <div class="form-row" style="margin-top: 20px">
                                    <h3 class="dealer-form-sub-heading">Shipping Address</h3>
                                </div>
                            </div>

                            <div class="row" style="padding-right: 28px; padding-bottom: 10px;">
<!--                                <button type="button" id="addShipping" class="btn btn-primary col-md-1 col-md-offset-11" style="margin-top:10px">Add More</button>-->
                            </div>

                            <div class="col-md-12 form-group row group">
                                <div class="col-md-3 form-group">
                                    <label>Company *</label>
                                    <input class="req form-control" type="text" name="ship_to_company"
                                           placeholder="Company Name" value="<?= isset($quote_info['ship_to_company']) && !empty($quote_info['ship_to_company']) ? $quote_info['ship_to_company'] : ''; ?>"
                                           required="required">
                                </div>
                                <div class="col-md-3 form-group">
                                    <label>Attn *</label>
                                    <input class="req form-control" type="text" name="ship_to_name"
                                           placeholder="Person Name" value="<?= isset($quote_info['ship_to_name']) && !empty($quote_info['ship_to_name']) ? $quote_info['ship_to_name'] : $user['full_name']; ?>"
                                           required="required">
                                </div>
                            </div>

                            <?php if (!isset($quote_info)) {
                                foreach ($user['address_1'] as $i => $value) {
                                    ?>
                                    <div class="col-md-12 form-group row group">
                                        <div class="col-md-3 form-group">
                                            <label>Address</label>
                                            <input class="req form-control" type="text" name="s_address[]"
                                                   id="s_address" placeholder="Address" value="<?= $value ?>"
                                                   required="required">
                                        </div>
                                        <div class="col-md-2 form-group">
                                            <label>Phone</label>
                                            <input class="req form-control" type="text" name="s_phone[]" id="s_b_name"
                                                   placeholder="Office Phone" value="<?= $user['contactinfo'][$i]; ?>"
                                                   required="required">
                                        </div>
                                        <div class="col-md-2 form-group">
                                            <label>City</label>
                                            <input class="req form-control" type="text" name='s_city[]' id="s_city"
                                                   placeholder="City" value="<?= $user['city'][$i]; ?>"
                                                   required="required">
                                        </div>
                                        <div class="col-md-2 form-group">
                                            <label>State</label>
                                            <input class="req form-control" type="text" name='s_state[]' id="s_state"
                                                   placeholder="State" value="<?= $user['state'][$i]; ?>"
                                                   required="required">
                                        </div>
                                        <div class="col-md-2 form-group">
                                            <label>PO Box / Zip Code</label>
                                            <input class="req form-control" type="text" name='s_zip[]' id="s_b_zip"
                                                   placeholder="PO / Zip Code" value="<?= $user['zip'][$i]; ?>"
                                                   required="required">
                                        </div>
                                        <div class="col-md-1 form-group">
                                            <label style="visibility:hidden;">Button</label>
<!--                                            <button type="button" class="btn btn-danger btnRemoveShipping bt_pos">X</button>-->
                                        </div>
                                    </div>
                                <?php }
                            } else {
                                foreach ($quote_info['shipping_address'] as $i => $value) { ?>
                                    <div class="col-md-12 form-group row group">
                                        <div class="col-md-3 form-group">
                                            <label>Address</label>
                                            <input class="req form-control" type="text" name="s_address[]"
                                                   id="s_address" placeholder="Address" value="<?= $value ?>"
                                                   required="required">
                                        </div>
                                        <div class="col-md-2 form-group">
                                            <label>Phone</label>
                                            <input class="req form-control" type="text" name="s_phone[]" id="s_phone"
                                                   placeholder="Office Phone"
                                                   value="<?= $quote_info['shipping_contactinfo'][$i]; ?>"
                                                   required="required">
                                        </div>
                                        <div class="col-md-2 form-group">
                                            <label>City</label>
                                            <input class="req form-control" type="text" name='s_city[]' id="s_city"
                                                   placeholder="City" value="<?= $quote_info['shipping_city'][$i]; ?>"
                                                   required="required">
                                        </div>
                                        <div class="col-md-2 form-group">
                                            <label>State</label>
                                            <input class="req form-control" type="text" name='s_state[]' id="s_state" placeholder="State" value="<?= $quote_info['shipping_state'][$i]; ?>" required="required">
                                        </div>
                                        <div class="col-md-2 form-group">
                                            <label>PO Box / Zip Code</label>
                                            <input class="req form-control" type="text" name='s_zip[]' id="s_zip" placeholder="PO / Zip Code" value="<?= $quote_info['shipping_zip'][$i]; ?>" required="required">
                                        </div>
<!--                                        <div class="col-md-1 form-group">-->
<!--                                            <button type="button" class="btn btn-danger btnRemoveShipping bt_pos">X-->
<!--                                            </button>-->
<!--                                        </div>-->
                                    </div>
                                <?php }
                            } ?>
                        </div>

                        <div class="container-fluid">
                            <div class="inner">
                                <p class="initial"></p>
                            </div>
                            <div class="quote-row-line"></div>
                            <div class="form-row" style="margin-top: 20px">
                                <h3 class="dealer-form-sub-heading">Special Instructions <small style="margin-left: 30px;">Do not use this section to add parts to your order.  Must create a new quote or attach a purchase order.</small></h3>
                            </div>
                        </div>
                        <br>
                        <div class="container-fluid">
                            <div class="row">
                                <div class="col-md-11 form-group">
                                    <textarea name="special_instruction" class="req form-control" rows="6" cols="90"><?= isset($quote_info['special_instruction']) && !empty($quote_info['special_instruction']) ? $quote_info['special_instruction']:''; ?></textarea>
                                </div>
                            </div>
                        </div>

                        <div class="container-fluid">
                            <div class="inner">
                                <p class="initial"></p>
                            </div>
                        </div>
                    </div>

                    <div id="error" class="alert alert-danger hide"></div>

                    <br>
                    <br>
                    <div class="container-fluid">
                        <div class="row">
                            <div class="form-row">
                                <div class="col-md-12 form-group">
                                    <label>Select Price Book</label><br>

                                    <input type="radio" name="price_book" id="rawPart" <?= isset($quote_info['price_book']) && !empty($quote_info['price_book']) && $quote_info['price_book'] != "Raw Parts" ? 'Disabled' : ''; ?> <?= isset($quote_info['price_book']) && !empty($quote_info['price_book']) && $quote_info['price_book'] == "Raw Parts" ? "checked='checked'" : ''; ?> value="Raw Parts"> Raw Parts<br>

                                    <?php if(isset($quote_info['price_book']) && !empty(isset($quote_info['price_book']))){ ?>
                                        <input type="radio" name="price_book" id="kngFamily" <?= isset($quote_info['price_book']) && !empty($quote_info['price_book']) && $quote_info['price_book'] != "KNG Family" ? 'Disabled' : ''; ?> <?= isset($quote_info['price_book']) && !empty($quote_info['price_book']) && $quote_info['price_book'] == "KNG Family" ? "checked='checked'" : ''; ?>  value="KNG Family" > KNG Family<br>
                                    <?php }else{ ?>
                                        <input type="radio" name="price_book" id="kngFamily" value="KNG Family" checked="checked" > KNG Family<br>
                                    <?php } ?>

                                    <input type="radio" name="price_book" id="bkrFamily" <?= isset($quote_info['price_book']) && !empty($quote_info['price_book']) && $quote_info['price_book'] != "BKR Family" ? 'Disabled' : ''; ?> <?= isset($quote_info['price_book']) && !empty($quote_info['price_book']) && $quote_info['price_book'] == "BKR Family" ? "checked='checked'" : ''; ?>  value="BKR Family"> BKR Family

                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="as-table-wrapper main-table-div hide">
                        <div class="row">
                            <div class="col-md-12">

                                <div class="form-row" style="margin-top: 20px">
                                    <h2 class="priceListHeading">Price List</h2>
                                    <div class="title text-center login-sub-title"><span><i>View our products and price list</i></span>
                                    </div>
                                </div>
                                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                                <div class="portlet light no-border-top as-box-border">
                                    <div class="portlet-body">
                                        <div class="quote-form-line"></div>
                                        <table class="table table-striped table-bordered table-hover dt-responsive"
                                               id="data_table" data-name=""
                                               style="background: none;width:100%;padding:0;">
                                            <thead>
                                            <tr>
                                                <th style="background:#767676;color:#FFF"> Product Model</th>
                                                <th style="background:#767676;color:#FFF" class="hide"> id</th>
                                                <th style="background:#767676;color:#FFF"> Product Description</th>
                                                <th style="background:#767676;color:#FFF"> Product Type</th>
                                                <th style="background:#767676;color:#FFF"> Product Family</th>
                                                <th style="background:#767676;color:#FFF"> List Price</th>
                                                <th style="background:#767676;color:#FFF"> Dealer Price</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <?php if (isset($price_list)) {
                                                foreach ($price_list as $index => $price) {
                                                    ?>
                                                    <tr>
                                                        <td>
                                                            <a href="#" data-id="<?= $price['id']; ?>"
                                                               data-type="<?= $price['type']; ?>"
                                                               data-dealer="<?= $price['dealer_price']; ?>"
                                                               data-first_dealer="<?= $price['dealer_price']; ?>"
                                                               data-list="<?= $price['list_price']; ?>"
                                                               data-family="<?= $price['product_family']; ?>"
                                                               data-model="<?= $price['model']; ?>"
                                                               data-desc="<?= $price['description']; ?>"
                                                               data-toggle="modal"
                                                               data-target="#select-Product"><?= $price['model']; ?></a>
                                                        </td>
                                                        <td class="hide"> <?= $price['id']; ?> </td>
                                                        <td> <?= $price['description']; ?> </td>
                                                        <td> <?= $price['type']; ?>  </td>
                                                        <td> <?= $price['product_family']; ?> </td>
                                                        <td> <?= $price['list_price']; ?> </td>
                                                        <td> <?= $price['dealer_price']; ?> </td>
                                                    </tr>
                                                    <?php
                                                }
                                            }
                                            ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <!-- END EXAMPLE TABLE PORTLET-->
                            </div>
                        </div>
                    </div>

                    <div class="as-table-wrapper raw-table-div hide">
                        <div class="row">
                            <div class="col-md-12">

                                <div class="form-row" style="margin-top: 20px">
                                    <h2 class="priceListHeading">Raw Parts Price List</h2>
                                    <div class="title text-center login-sub-title"><span><i>View our products and price list</i></span>
                                    </div>
                                </div>
                                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                                <div class="portlet light no-border-top as-box-border">
                                    <div class="portlet-body">
                                        <div class="quote-form-line"></div>
                                        <table class="table table-striped table-bordered table-hover dt-responsive" id="user_table" data-name="" style="background: none;width:100%;padding:0;">
                                            <thead>
                                            <tr>
                                                <th style="background:#767676;color:#FFF"> Product Model</th>
                                                <th style="background:#767676;color:#FFF" class="hide"> id</th>
                                                <th style="background:#767676;color:#FFF"> Product Description</th>
                                                <th style="background:#767676;color:#FFF"> Product Family</th>
                                                <th style="background:#767676;color:#FFF"> List Price</th>
                                                <th style="background:#767676;color:#FFF"> Dealer Price</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <?php if (isset($raw_parts)) {
                                                foreach ($raw_parts as $index => $raw_part) {
                                                    ?>
                                                    <tr>
                                                        <td>
                                                            <a href="#" data-id="<?= $raw_part['id']; ?>"
                                                               data-dealer="<?= $raw_part['dealer_price']; ?>"
                                                               data-first_dealer="<?= $raw_part['dealer_price']; ?>"
                                                               data-list="<?= $raw_part['list_price']; ?>"
                                                               data-family="<?= $raw_part['product_family']; ?>"
                                                               data-model="<?= $raw_part['model']; ?>"
                                                               data-desc="<?= $raw_part['description']; ?>"
                                                               data-toggle="modal"
                                                               data-target="#select-Product"><?= $raw_part['model']; ?></a>
                                                        </td>
                                                        <td class="hide"> <?= $raw_part['id']; ?> </td>
                                                        <td> <?= $raw_part['description']; ?> </td>
                                                        <td> <?= $raw_part['product_family']; ?> </td>
                                                        <td> <?= $raw_part['list_price']; ?> </td>
                                                        <td> <?= $raw_part['dealer_price']; ?> </td>
                                                    </tr>
                                                    <?php
                                                }
                                            }
                                            ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <!-- END EXAMPLE TABLE PORTLET-->
                            </div>
                        </div>
                    </div>

                    <div class="container-fluid addedProductHeading hide">
                        <div class="inner">
                            <p class="initial"></p>
                        </div>
                        <div class="form-row" style="margin-top: 20px">
                            <h2 class="text-center">Added Products</h2>
                            <div class="title text-center login-sub-title"><span><i>The details of the added products will be displayed below</i></span>
                            </div>
                        </div>
                    </div>

                    <?php if (isset($quote_info)) {
                        foreach ($quote_info['model'] as $i => $value) {
                            ?>
                            <div class="container-fluid">
                                <div class="row savedRows" id="savedRow<?= $i; ?>">
                                    <div class="form-row">
                                        <div class="col-md-2 mb-3">
                                            <label for="validationCustom01">Product Model</label>
                                            <div><?= $value ?></div>
                                            <input value="<?= $value ?>" class="form-control hidden" name="model[]">
                                        </div>
                                        <div class="col-md-2 mb-3">
                                            <label for="validationCustom02">Product Description</label>
                                            <div><?= $quote_info['description'][$i]; ?></div>
                                            <input value="<?= $quote_info['description'][$i]; ?>"
                                                   class="form-control hidden" name="description[]">
                                        </div>
                                        <div class="form-row">
                                            <div class="col-md-2 mb-3">
                                                <label for="validationCustom01">Product Family</label>
                                                <div><?= $quote_info['family'][$i]; ?></div>
                                                <input value="<?= $quote_info['family'][$i]; ?>"
                                                       class="form-control hidden" name="family[]">
                                            </div>
                                            <div class="col-md-1 mb-3">
                                                <label for="validationCustom02">Product Type</label>
                                                <div><?= $quote_info['type'][$i]; ?></div>
                                                <input value="<?= $quote_info['type'][$i]; ?>"
                                                       class="form-control hidden" name="type[]">
                                            </div>
                                            <div class="col-md-1 mb-3">
                                                <label for="validationCustom01">List Price</label>
                                                <div><?= $quote_info['list_price'][$i]; ?></div>
                                                <input value="<?= $quote_info['list_price'][$i]; ?>"
                                                       class="form-control hidden" name="list_price[]">
                                            </div>
                                            <div class="col-md-1 mb-3">
                                                <label for="validationCustom01">Dealer Price</label>
                                                <div><?= $quote_info['dealer_price'][$i]; ?></div>
                                                <input value="<?= $quote_info['dealer_price'][$i]; ?>"
                                                       class="form-control hidden" name="dealer_price[]">
                                            </div>
                                            <div class="col-md-1 mb-3">
                                                <label for="validationCustom02">Total Quantity</label>
                                                <div><?php //echo number_format($quote_info['quantity'][$i]); ?></div>
                                                <input value="<?= $quote_info['quantity'][$i]; ?>"
                                                       class="form-control " name="quantity[]" type="number" min="1">
                                            </div>
                                            <div class="col-md-1 mb-3">
                                                <label for="validationCustom02">Total Price</label>
                                                <div><?= $quote_info['price'][$i]!=0?'$'.number_format($quote_info['price'][$i],2):'CFQ'; ?></div>
                                                <input id="thisInputSavedPriceValue<?= $i; ?>"
                                                       value="<?= $quote_info['price'][$i]; ?>"
                                                       class="form-control hidden" name="price[]">
                                            </div>
                                            <div class="col-md-1 mb-3">
                                                <button type="button" id="<?= $i; ?>"
                                                        class="btn btn-danger saved_remove_btn">
                                                    <i class="fa fa-remove"></i>
                                                </button>
                                            </div>
                                            <br>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php }
                    } ?>
                    <style>#newInput br {
                            display: none
                        }

                        #newInput .form-row {
                            border-bottom: 1px solid #ccc;
                            padding: 15px 0;
                            float: left;
                            width: 100%
                        }</style>
                    <div id="newInput"></div>

                    <div><br></div>
                    <div class="container-fluid grand-total-row" style="background:#eee;padding:15px">
                        <div class="row">
                            <!--                            '$'.$quote_info['grandTotal'].'.00'-->
                            <div class="col-md-12 pull-right">
                                <b>
                                    <div style="text-align:right;" class="col-md-10">Grand Total:</div>
                                    <div id="grandTotal"
                                         class="col-md-2 grandTotal"><?= isset($quote_info['grandTotal']) && !empty($quote_info['grandTotal']) ? '$' . number_format($quote_info['grandTotal'],2) : '' ?></div>
                                </b>
                                <input value="<?= isset($quote_info['grandTotal']) && !empty($quote_info['grandTotal']) ? $quote_info['grandTotal'] : '' ?>"
                                       class="form-control totalGrandPrice hidden" name="grandTotal">
                            </div>
                        </div>
                    </div>
                    <br/>
                    <button type="submit"
                            class="generateQuote btn btn-warning <?= isset($quote_info) && !empty($quote_info) ? '' : 'hide' ?> pull-right">
                        Generate Quote
                    </button>

                </form>

                <script>
                    $('.form-content1').multifield({
                        section: '.group',
                        btnAdd: '#btnAdd',
                        btnRemove: '.btnRemove',
                    });

                    $("#btnAdd").one('click', function () {
                        $(".inner").append("<p></p>");
                        $("p.initial").addClass("hide");
                    });

                    $('.form-content2').multifield({
                        section: '.group',
                        btnAdd: '#addShipping',
                        btnRemove: '.btnRemoveShipping',
                    });

                    $("#addShipping").one('click', function () {
                        $(".inner2").append("<p></p>");
                        $("p.initial2").addClass("hide");
                    });
                    // Example starter JavaScript for disabling form submissions if there are invalid fields
                    (function () {
                        'use strict';
                        window.addEventListener('load', function () {
                            // Fetch all the forms we want to apply custom Bootstrap validation styles to
                            var forms = document.getElementsByClassName('needs-validation');
                            // Loop over them and prevent submission
                            var validation = Array.prototype.filter.call(forms, function (form) {
                                form.addEventListener('submit', function (event) {
                                    if (form.checkValidity() === false) {
                                        event.preventDefault();
                                        event.stopPropagation();
                                    }
                                    form.classList.add('was-validated');
                                }, false);
                            });
                        }, false);
                    })();

                    function profileValidation() {
                        var error = 0;
                        if ($(".ptotal").val() < 0 || $(".ptotal").val() == '' || $(".ptotal").val() == 0) {
                            error++;
                            $('#error').removeClass('hide');
                            $('#error').html('Quantity cannot be negative value, empty and zero.');
                            $('html, body').animate({
                                scrollTop: $("#error").offset().top
                            }, 500);
                        }

                        // alert(str1.search(str2));

                        var fid = $(".quantityTotal").val();
                        // fid = fid != "CFQ"?fid.substring(1):'';
                        // if(fid < 1000) {
                        //     var subStringed  = fid.replace(',','');
                        // }
                        var subStringed = fid.replace(',', '');
                        // alert(fid);
                        if (!$.isNumeric(subStringed)) {
                            error++;
                            $('#error').removeClass('hide');
                            $('#error').html('List price is invalid');
                            $('html, body').animate({
                                scrollTop: $("#error").offset().top
                            }, 500);

                            return false;
                        }


                        if (error === 0) {
                            return true;
                        } else {
                            return false;
                        }
                    }

                </script>

            </div>
        </div>
    </div>
</section>

<!-- Product POPUP -->
<div class="modal fade" id="select-Product" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel">Product Details</h4>
            </div>

            <div class="modal-body">
                <div class="container-fluid">
                    <div class="row">
                        <form>
                            <div class="col-md-12 form-group row-group">
                                <div class="col-md-6 form-group">
                                    <label for="validationCustom01">Product Model: </label>
                                    <div class="pmodeldiv"></div>
                                    <input name="pmodel" class="pmodel form-control hidden" type="text">
                                </div>
                                <div class="col-md-6 form-group">
                                    <label for="validationCustom01">Product Description: </label>
                                    <div class="pdescdiv"></div>
                                    <input name="pdesc" class="pdesc form-control hidden" type="text">
                                </div>
                                <div class="col-md-6 form-group">
                                    <label for="validationCustom01">Product Type: </label>
                                    <div class="ptypediv"></div>
                                    <input name="ptype" class="ptype form-control hidden">
                                </div>
                                <div class="col-md-6 form-group">
                                    <label for="validationCustom01">Product Family: </label>
                                    <div class="pfamilydiv"></div>
                                    <input name="pfamily" class="pfamily form-control hidden">
                                </div>
                                <div class="col-md-6 form-group">
                                    <label for="validationCustom01">List Price: </label>
                                    <div class="plistdiv"></div>
                                    <input name="plist" class="plist form-control hidden">
                                </div>
                                <div class="col-md-6 form-group">
                                    <label for="validationCustom01">Dealer Price: </label>
                                    <div class="pdealerdiv"></div>
                                    <input name="pdealer" class="pdealer form-control hidden">
                                </div>
                                <div class="col-md-6 form-group">
                                    <label for="validationCustom01">Quantity: </label>
                                    <input name="pquantity" class="ptotal form-control" value="1" type="number" min="1">
                                </div>
                                <div class="col-md-6 form-group">
                                    <label for="validationCustom01">Total Price by Dealer Price: </label>
                                    <input name="quantityTotal" id="quantitytotal_fi" class="quantityTotal-fi form-control hidden">
                                    <div class="quantityTotaldiv"></div>
                                </div>
                                <input name="pid" class="pid hidden">
                            </div>
                        </form>
                    </div>
                </div>
            </div>

            <div class="modal-footer">
                <button type="button" data-dismiss="modal" class="btn btn-default saveAndMore" onclick="return profileValidation();">Add</button>
                <!--                <a  data-dismiss="modal" class="btn btn-primary generateQuote" onclick="return profileValidation();">Generate Quote</a>-->
                <button data-dismiss="modal" class="btn btn-primary">Close</button>
            </div>
        </div>
    </div>
</div>
<!-- END HELP POPUP -->

<style>
    .form-group {
        margin-bottom: 10px;
    }

    input.form-control {
        margin-bottom: 0px !important;
    }

    button#btnAdd, button#addShipping {
        width: 130px !important;
        margin-left: 30px !important;
    }

    button.btnRemove.bt_pos, button.btnRemoveShipping.bt_pos {
        margin-left: 0px !important;
        height: 42px !important;
        border-radius: 0px !important;
    }

</style>