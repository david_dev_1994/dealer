<section id="min-wrapper">
    <div id="main-content">
        <div class="container-fluid">
            <div>
                <h2 style=""><?php echo $type; ?></h2>
                <div class="title text-center login-sub-title"><span><i>All Listings Will Be Displayed Below</i></span></div>
            </div>
        </div>

        <?php if ($this->session->flashdata('message')){?>
            <div class=" alert alert-success alert-dismissible text-center row">
                <?= $this->session->flashdata('message'); ?>
            </div>
        <?php }?>
<style>.dataTables_length{padding-left:15px;}.dataTables_filter{padding-right:15px</style>
        <div class="container-fluid">
            <div class="row">
            <div class="as-table-wrapper">
                        <div class="row">
                            <div class="col-md-12">
                                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                                <div class="portlet light no-border-top as-box-border">
                                    <div class="portlet-body">
                                        <table class="table table-striped table-bordered table-hover dt-responsive" id="data_table" data-name="" style="background: none;width:100%;padding:0">
                                            <thead>
                                            <tr>
                                                <th style="background:#767676;color:#FFF"> Quotes</th>
                                                <th style="background:#767676;color:#FFF" class="hide"> id</th>
                                                <th style="background:#767676;color:#FFF"> Name</th>
                                                <th style="background:#767676;color:#FFF"> Email</th>
                                                <th style="background:#767676;color:#FFF"> Company</th>
                                                <th style="background:#767676;color:#FFF"> Sales Manager</th>
                                                <th style="background:#767676;color:#FFF"> Action</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <?php if(isset($quotes_list)) {
                                                foreach ($quotes_list as $index => $singlequote) {
                                                    ?>
                                                    <tr>
                                                        <td>
                                                            <a > Quote- <?= $singlequote['id']; ?></a>
                                                        </td>
                                                        <td class="hide"> <?= $singlequote['id']; ?> </td>
                                                        <td> <?= $singlequote['name']; ?> </td>
                                                        <td> <?= $singlequote['email']; ?>  </td>
                                                        <td> <?= $singlequote['company']; ?> </td>
                                                        <td> <?= $singlequote['sales_person']; ?> </td>
                                                        <td> <div class="btn-group">
                                                             <?php  if($user['access_label'] != 3 || $singlequote['status'] == 2): ?>
                                                        <a  href="<?php echo base_url();?>quote/<?= $singlequote['id']; ?>" class="btn btn-primary">Edit</a>
                                                         <?php  endif; ?>
                                                        <a  href="<?php echo base_url();?>quote/show/<?= $singlequote['id']; ?>" class="btn btn-info">View</a>
                                                                <?php if($type == 'Draft Quotes'){ ?>
                                                                    <a  href="<?php echo base_url(); ?>quote/redirect/1/<?= $singlequote['id']; ?>" class="btn btn-success">Submit As Order</a>
                                                                <?php } ?>
                                                                <?php if($user['access_label'] != 3 || $singlequote['status'] == 2): ?>
                                                                <a  href="<?php echo base_url();?>quote/delete_quote/<?= ($type == 'Draft Quotes')?'2':'1'; ?>/<?= $singlequote['id']; ?>" class="btn btn-danger">Delete</a>
                                                                <?php endif; ?>
                                                        </td>
                                                    </tr>
                                                    <?php
                                                }
                                            }
                                            ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <!-- END EXAMPLE TABLE PORTLET-->
                            </div>
                        </div>
                    </div>
            </div>
        </div>
    </div>
</section>
