<!--Page main section start-->
<section id="min-wrapper">
    <div id="main-content">
        <div class="container-fluid">

            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title">Invoice</h3>
                        </div>

                        <div class="panel-body">
                            <!--Table Wrapper Start-->
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="panel panel-default">
                                        <!-- <div class="panel-heading">
                                            <h4>Invoice</h4>
                                        </div> -->
                                        <div class="panel-body">
                                            <div class="clearfix">
                                                <div class="pull-left">
                                                    <h4 class="text-right">Atique-IT</h4>

                                                </div>
                                                <div class="pull-right">
                                                    <h4>Invoice # <br>
                                                        <strong>00000<?php echo $invoice_info['sub_quote_id']; ?></strong>
                                                    </h4>
                                                </div>
                                            </div>
                                            <hr>
                                            <div class="row">
                                                <div class="col-md-12">

                                                    <div class="pull-left m-t-30">
                                                        <address>
                                                            <strong><?php echo $invoice_info['first_name'] . $invoice_info['last_name']; ?></strong><br>
                                                            <?php echo $invoice_info['address_1']; ?><br>
                                                            <?php echo $invoice_info['address_2']; ?><br>
                                                            <abbr title="Phone">P:</abbr> <?php echo $invoice_info['phone']; ?>
                                                        </address>
                                                    </div>
                                                    <div class="pull-right m-t-30">
                                                        <p><strong>Order Date: </strong> <?php echo date("D d F Y", strtotime($invoice_info['date_added'])); ?></p>
                                                        <p class="m-t-10"><strong>Order Status: </strong>
                                                            <?php
                                                            if ($invoice_info['payment_status'] == 0) {
                                                                echo 'Pending';
                                                            } else {
                                                                echo 'Paid';
                                                            }
                                                            ?>
                                                        </p>
                                                        <p class="m-t-10"><strong>Order ID: </strong> #<?php echo $invoice_info['sub_quote_id']; ?></p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="m-h-50"></div>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="table-responsive">
                                                        <table class="table m-t-30">
                                                            <thead>
                                                                <tr><th>SL</th>
                                                                    <th>Service</th>
                                                                    <th>Order ID</th>
                                                                    <th>Quantity</th>
                                                                    <th>Amount</th>
                                                                </tr></thead>
                                                            <tbody>
                                                                <tr>
                                                                    <td>1</td>
                                                                    <td><?php echo $invoice_info['job_category_name']; ?></td>
                                                                    <td><?php echo $invoice_info['sub_quote_id']; ?></td>
                                                                    <td><?php echo $invoice_info['number_of_image']; ?></td>
                                                                    <td>$
                                                                        <?php
                                                                        if ($invoice_info['one_time_price_usd'] != 0) {
                                                                            echo $invoice_info['one_time_price_usd'];
                                                                        } else {
                                                                            echo $invoice_info['monthly_amount_usd'];
                                                                        }
                                                                        ?></td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row" style="border-radius: 0px;">
                                                <div class="col-md-3 col-md-offset-9">
                                                    <p class="text-right"><b>Total:</b>
                                                        <?php
                                                        if ($invoice_info['one_time_price_usd'] != 0) {
                                                            echo $invoice_info['one_time_price_usd'];
                                                        } else {
                                                            echo $invoice_info['monthly_amount_usd'];
                                                        }
                                                        ?>
                                                    </p>
                                                    <?php if ($invoice_info['payment_status'] == 0) { ?>
                                                    <p class="text-right">Paid: $
                                                    <?php
                                                        if ($invoice_info['one_time_price_usd'] != 0) {
                                                            echo $invoice_info['one_time_price_usd'];
                                                        } else {
                                                            echo $invoice_info['monthly_amount_usd'];
                                                        }
                                                        ?>
                                                    </p>
                                                    <?php } else { ?>
                                                    <p class="text-right">Due: $
                                                    <?php
                                                        if ($invoice_info['one_time_price_usd'] != 0) {
                                                            echo $invoice_info['one_time_price_usd'];
                                                        } else {
                                                            echo $invoice_info['monthly_amount_usd'];
                                                        }
                                                        ?>
                                                    </p>
                                                    <?php } ?>
                                                    <hr>
                                                    <h3 class="text-right">USD: 
                                                    <?php
                                                        if ($invoice_info['one_time_price_usd'] != 0) {
                                                            echo $invoice_info['one_time_price_usd'];
                                                        } else {
                                                            echo $invoice_info['monthly_amount_usd'];
                                                        }
                                                        ?>
                                                    </h3>
                                                </div>
                                            </div>
                                            <hr>
                                            <div class="hidden-print">
                                                <div class="pull-right">
                                                    <a href="#" class="btn btn-inverse waves-effect waves-light"><i class="fa fa-print"></i></a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!--Page main section end -->
