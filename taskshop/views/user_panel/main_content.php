

<div class="responsive-header">
    <div class="topbar">
        <div class="container">
            <ul class="top-social">
                <li><a href="#" title=""><i class="fa fa-twitter"></i></a></li>
                <li><a href="#" title=""><i class="fa fa-facebook"></i></a></li>
                <li><a href="#" title=""><i class="fa fa-google-plus"></i></a></li>
                <li><a href="#" title=""><i class="fa fa-linkedin"></i></a></li>
            </ul><!-- Top Social -->
            <div class="pull-right">
                <ul class="contact-information">
                    <li><i class="fa fa-phone"></i>Tel: 0056 764 234 5621</li>			
                    <li><i class="fa fa-envelope-o"></i>Mail: office@eclipse.com</li>
                </ul><!-- Contact Information -->
                <form>
                    <input type="text" placeholder="search" />
                    <button><i class="fa fa-search"></i></button>
                </form><!-- Search Form -->
            </div>
        </div>
    </div><!-- Top Bar -->	

    <div class="responsive-logo">
        <a href="#" title=""><img src="<?php echo base_url(); ?>user_assets/images/logo.png" alt="Logo" /></a>
    </div><!-- Responsive Logo -->	
    <span><i class="fa fa-align-justify"></i></span>
    <ul>
        <li><a href="<?php echo base_url(); ?>" title=""><i class="fa fa-home"></i>Home</a></li>
        <li><a href="<?php echo base_url(); ?>quote.html" title=""><i class="fa fa-arrows"></i>Submit Quote</a></li>
        <li><a href="<?php echo base_url(); ?>order.html" title=""><i class="fa fa-tty"></i>Training</a></li>
        <li><a href="<?php echo base_url(); ?>order.html" title=""><i class="fa fa-tty"></i>Press Release</a></li>
        <?php if ($this->session->userdata('logged_info') == FALSE) { ?>
                                    <li><a href="<?php echo base_url(); ?>page/sign_up.html" title=""><i class="fa fa-user-plus"></i>Sign Up</a></li>
                                    <li><a href="<?php echo base_url(); ?>user_login.html" title=""><i class="fa fa-sign-in"></i>Sing in</a></li>
                                <?php } else { ?>
                                         <li><a href="<?php echo base_url(); ?>page/user_logout.html" title=""><i class="fa fa-sign-in"></i>Logout</a></li>
                                    <?php } ?>
    </ul>
</div><!--Responsive header-->

<!--
<section>
    <div class="block no-padding">
        <div class="">
            <div class="row">
                <div class="col-md-12 column">
                    <div class="simple-slider">
                        <div class="gallery_kenburns">
                            <canvas id="kenburns">
                                <p>Your browser doesn't support canvas!</p>
                            </canvas>
                        </div>
                        <div class="kenburns-text">
                            <div class="container">
                                <div class="simple-icon"><span><img src="<?php echo base_url(); ?>user_assets/images/icon.png" alt="" /></span></div>
                                <h3>welcome to</h3>
                                <h2>Taskshop</h2>
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Iure facere nam accusamus adipisci molestiae, aliquid veritatis dolores. Officia expedita, reiciendis natus eligendi sequi totam officiis dignissimos.</p>
                            </div>
                        </div> 
                    </div>      

                </div>
            </div>
        </div>
    </div>
</section>
-->

<section class="home-blocks">
    <div class="block">
        <div class="container">
            <div class="row">
                    <div class="heading2">
                        <h2>Dealer Portal</h2>
                        <span>BK TECHNOLOGIES<span style="vertical-align: super !important;">&reg;</span></span>
                    </div>
                <div class="col-lg-3">
                    <div class="row quote-block">
                        <h1>Ordering</h1>
                            <ul>
                              <!--<li><a href="#">Create a Quote</a></li> -->
                              <li><a href="<?php echo base_url('quote'); ?>">Generate Quote</a></li>
                              <li><a href="<?php echo base_url();?>quote/view/2"  >View Draft</a></li>
                                <?php if($this->session->userdata['d_access_label'] != 5): ?>
                              <li><a href="<?php echo base_url();?>quote/view/1" >Submitted Quotes</a></li>
                                <?php endif; ?>
                            </ul>
                    </div>
                </div>
                
                <div class="col-lg-3">
                    <div class="row training-block">
                        <h1>Training</h1>
                        <ul>
                            <li><a href="<?php echo base_url('training_course'); ?>">Training Courses</a></li>
                            <li><a href="https://www.bktechnologies.com/contact-us/" target="_blank">Schedule A Consultation</a></li>
                            <!-- <li><a href="#<?php //echo base_url('cbt'); ?>">CBT</a></li>
                            <li><a href="#<?php //echo base_url('schedule_training'); ?>">Schedule In-Person Training</a></li>
                            <li><a href="#<?php //echo base_url('manuals'); ?>">Manuals</a></li>  -->
                        </ul>
                    </div>
                </div>
                
                <div class="col-lg-3">
                    <div class="row corp-block">
                        <h1>Marketing  & Bulletins</h1>
                        <ul>
                            <li><a href="https://www.bktechnologies.com/press" target="_blank">Press Release</a></li>
                            <li><a href="<?php echo base_url('marketing_materials'); ?>">Marketing Materials</a></li>
                            <li><a href="<?php echo base_url('product_announcement'); ?>">Product Announcements</a></li>
                            <li><a href="<?php echo base_url('corporate_announcement'); ?>">Corporate Announcements</a></li>
                            <li><a href="<?php echo base_url('brochures'); ?>">Brochures</a></li>
<!--                            <li><a href="https://bktechnologies.com/service-portal/">Brochures</a></li>-->
                            <!-- <li><a href="#<?php //echo base_url('notices'); ?>">End of Life Notices</a></li> -->
                        </ul>
                    </div>
                </div>
                
                <div class="col-lg-3">
                    <div class="row doc-block">
                        <h1>Bk Documents</h1>
                        <ul>
                            <!-- <li><a href="#<?php //echo base_url('documents'); ?>">Docu Sign</a></li> -->
                            <li><a href="<?php echo base_url('dealer_agreement'); ?>">Dealer Agreement</a></li>
                            <li><a href="https://bktechnologies.com/service-portal/return-merchandise-authorization" target="_blank">RMA Form</a></li>
                            <li><a href="https://bktechnologies.com/service-portal/assets/pdf/Serviceform2019.pdf" target="_blank">Factory Service Form</a></li>
                            <li><a href="https://bktechnologies.com/service-portal/factory-service/warranty" target="_blank">Warranty Statement</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-sm-12 text-center contact-link-page">
                    For Dealer Portal support, contact <a href="mailto:busops@bktechnologies.com">busops@bktechnologies.com</a>.
                </div>
            </div>
        </div>
    </div>
</section>



