        </div>
    </div>
</div>
<br><br><br>
<footer>
    <div class="block no-padding">
        <div class="container">
            <div class="row">
                <div class="col-lg-4">
                    <h3 class="widget-title"><span>Navigation Links</span></h3>
                    <ul id="menu-footer-navigation-1" class="menu">
                        <li class="menu-item"><a href="https://www.bktechnologies.com/">Home</a></li>
                        <li class="menu-item"><a href="https://www.bktechnologies.com/about-us/">About Us</a></li>
                        <li class="menu-item"><a href="https://www.bktechnologies.com/agencies/">Solutions</a></li>
                        <li class="menu-item"><a href="https://www.bktechnologies.com/bk-sales/">Where To Buy</a></li>
                        <li class="menu-item"><a href="http://www.bktechnologies.com/service-portal" class="gtrackexternal">Support</a></li>
                        <li class="menu-item"><a href="https://www.bktechnologies.com/press/">Press</a></li>
                        <li class="menu-item"><a href="https://www.bktechnologies.com/investor-relations/">Investor Relations</a></li>
                    </ul>
                </div>
                <div class="col-lg-4">
                    <h3 class="widget-title"><span>Connect With Us</span></h3>
                    <ul style="margin-top: 5px;margin-bottom: 20px;" class="ut-sociallinks">
                        <li><a href="https://www.facebook.com/BK-Technologies-106082389889629/" target="_blank" class="gtrackexternal"><i class="fa fa-facebook"></i></a></li>
                        <li><a href="https://www.instagram.com/bktechnologies/" target="_blank" class="gtrackexternal"><i class="fa fa-instagram"></i></a></li>
                        <li><a href="https://www.linkedin.com/company/bk-technologies" target="_blank" class="gtrackexternal"><i class="fa fa-linkedin"></i></a></li>
                        <li><a href="https://twitter.com/BKTechUSA" target="_blank" class="gtrackexternal"><i class="fa fa-twitter"></i></a></li>
                        <li><a href="https://www.youtube.com/channel/UCMKSiuxlKiXn5YV04Vh4xgw" target="_blank" class="gtrackexternal"><i class="fa fa-youtube"></i></a></li>
                    </ul>
                    <h3 class="widget-title"><span>Certifications</span></h3>
                    <div class="textwidget">
                        <img style="width: 40% !important; margin-right: 25px !important;" class="iso-logo" src="https://www.bktechnologies.com/wp-content/uploads/2019/04/ISO-9001.jpg">
                        <img style="width: 20% !important;" class="jas-anz-logo" src="https://www.bktechnologies.com/wp-content/uploads/2019/04/JAS-ANZ-Logo.jpg">
                    </div>
                    
                </div>
                <div class="col-lg-4">
                    <h3 class="widget-title"><span>Company Resources</span></h3>
                    <div class="menu-footer-navigation-2-container">
                        <ul id="menu-footer-navigation-2" class="menu">
                            <li class="menu-item"><a href="https://www.bktechnologies.com/ourevents/">Community Events</a></li>
                            <li class="menu-item"><a href="https://www.bktechnologies.com/career/">Careers</a></li>
                            <li class="menu-item"><a href="https://www.bktechnologies.com/contact-us/">Contact Us</a></li>
                            <li class="menu-item"><a href="https://www.bktechnologies.com/privacy-policy/">Privacy Policy</a></li>
                            <li class="menu-item"><a href="https://www.bktechnologies.com/terms-of-service/">Terms Of Service</a></li>
                            <li class="menu-item"><a href="https://www.bktechnologies.com/html-sitemap/">Sitemap</a></li>
                            <li class="menu-item"><a href="https://www.bktechnologies.com/508-accessibility-template/">508 Compliance</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer><!-- FOOTER -->

<div class="bottom-footer">
    <div class="container">
        <div class="row">
            <div class="col-lg-6">
                <p>© 2019 BK Technologies</p>
            </div>
            <div class="col-lg-6">
                <p style="float:right;">Web Design By <a href="http://theadleaf.com/" target="_blank">The AD Leaf</a></p>
            </div>
        </div>
    </div>
</div><!-- Bottom Footer -->

</div>

<!--po order pdf submit-->
        <div class="modal fade" id="poAttachmentModal2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel34"
             aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">

                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title" id="myModalLabel34">Add PO Attachment</h4>
                    </div>
                    <form method="post" id="poattachmentForm" enctype="multipart/form-data">
                        <div class="modal-body">
                            <div class="container-fluid">
                                <div class="row">
                                    <div class="col-md-12 form-group row-group">
                                        <div class="errorToModal alert alert-danger alert-dismissible text-center row" hidden></div>
                                        <div class="form-field attachmentDiv">
                                            <div><h4 style="color:blue;">Your order is over $5K and you will need to attach a hard copy PO</h4></div>
                                            <br>
                                            <div class="po-upload-error alert alert-danger alert-dismissible text-center row hidden" >Please Upload The File First</div>
                                            <div><b>Note: <span style="color: red;">.pdf, .xlsx, .doc, .docx, .opt</span> format are allowed to upload.</b></div>
                                            <div>
                                                <input type="hidden" value="" name="quote_id" id="quote_id_id">
                                                <input type="hidden" value="po" name="po">
                                                <input type="file" class="po-upload-btn" name="file" style="margin: 10px">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="modal-footer">
                            <button type="submit" class=" btn btn-default" value="Upload">Upload</button>
                            <button data-dismiss="modal" class="btn btn-primary">Close</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>


        <div class="modal fade" id="videoIframe" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">

                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modalTitle" id="myModalLabel" style="color: #FFF;"></h4>
                    </div>

                    <div class="modal-body">
                        <div class="row">
<!--                            <div class="container">-->
                                <div class="col-md-12">
                                    <iframe class="iframeSrc" width="100%" height="420" src="" frameborder="0" allow="encrypted-media" allowfullscreen></iframe>
                                </div>
<!--                            </div>-->
                        </div>
                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    </div>
                </div>
            </div>
        </div>

<!-- SCRIPTS-->


<script type="text/javascript" src="<?php echo base_url(); ?>user_assets/js/modernizr.custom.17475.js"></script>
<script src="<?php echo base_url(); ?>user_assets/js/jquery2.1.1.js" type="text/javascript"></script>
<script src='<?php echo base_url(); ?>user_assets/js/jquery.min.js'></script>
<script src="<?php echo base_url(); ?>user_assets/js/bootstrap.min.js"></script>
<script src="<?php echo base_url(); ?>user_assets/js/jquery.scrolly.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>user_assets/js/jquery.prettyPhoto.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>user_assets/js/owl.carousel.min.js"></script>
<script src="<?php echo base_url(); ?>user_assets/js/TableBarChart.js"></script>
<script src="<?php echo base_url(); ?>user_assets/js/script.js"></script>
<script src="<?php echo base_url(); ?>user_assets/js/waypoints.js"></script>
<script src="<?php echo base_url(); ?>user_assets/js/zoominout.js"></script>
<script src="<?php echo base_url(); ?>user_assets/js/comparison.js"></script>
<script src="<?php echo base_url('user_assets/plugins/datatables/datatables.min.js'); ?>" type="text/javascript"></script>
<script src="<?php echo base_url('user_assets/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js'); ?>" type="text/javascript"></script>
<script src="<?php echo base_url('user_assets/js/datatable.min.js'); ?>" type="text/javascript"></script>
<script src="<?php echo base_url('user_assets/plugins/bootstrap-fileinput-master/js/fileinput.js'); ?>" type="text/javascript"></script>
<script src="<?php echo base_url('user_assets/js/custom.js'); ?>" type="text/javascript"></script>


        <script>
    jQuery(document).ready(function ($) {
        /*================== COUNTER =====================*/
        $('.count').counterUp({
            delay: 10,
            time: 2000
        });

        /*================== BAR CHART =====================*/
        $('#source').tableBarChart('#target', '', false);

        /*================== SERVICE CAROUSEL =====================*/
        $(".services-carousel").owlCarousel({
            autoplay: true,
            autoplayTimeout: 3000,
            smartSpeed: 2000,
            loop: true,
            dots: false,
            nav: true,
            margin: 0,
            items: 1,
            singleItem: true
        });


        gallery_set = [
            'http://placehold.it/1366x700',
            'http://placehold.it/1366x700',
            'http://placehold.it/1366x700',
        ]
        "use strict";
        jQuery('#kenburns').attr('width', jQuery(window).width());
        jQuery('#kenburns').attr('height', 500);
        jQuery('#kenburns').kenburns({
            images: gallery_set,
            frames_per_second: 30,
            display_time: 5000,
            fade_time: 1000,
            zoom: 1.2,
            background_color: '#000000'
        });
    });
</script>

</body>