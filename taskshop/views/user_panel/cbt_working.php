<section>
    <div class="block">
        <div class="container">
            <div class="row">
                    <div class="heading2">
                        <h2>Dealer Portal</h2>
                        <span>BK TECHNOLOGIES CBT</span>
                    </div>
            </div>
        </div>
    </div>
    <section class="services-section">
        <div class="container pad-2">
            <?php if( $this->session->flashdata('success')){
                ?>
                <br>
                <div class="alert alert-success alert-dismissable center">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>
                    <?php echo $this->session->flashdata('success'); ?>
                </div>
                <?php
            }
            ?>
            <div class="row">
                <?php
                if(isset($videos)){
                if (count($videos) > 0){
                foreach ($videos

                as $key => $val){
                if ($key != 0 && ($key + 1) % 3 == 0){
                ?>
                <div class="col-md-4">
                    <div class="form-group">
                        <!--<label>Video title</label>-->
                        <video controls preload="none" class="vid-width">
                            <source src="<?= base_url('uploads/videos/' . $val['video']); ?>" type="video/mp4">
                            Your browser does not support the video tag.
                        </video>
                        <h5 class="video-title"><?= $val['title']; ?></h5>
                    </div>
                </div>
            </div>
            <div class="row">

                <?php
                } else {
                    ?>
                    <div class="col-md-4">
                        <div class="form-group">
                            <!--<label>Video title</label>-->
                            <video controls preload="none" class="vid-width">
                                <source src="<?= base_url('uploads/videos/' . $val['video']); ?>" type="video/mp4">
                                Your browser does not support the video tag.
                            </video>
                            <h5 class="video-title"><?= $val['title']; ?></h5>
                        </div>
                    </div>
                    <?php
                }
                }
                }
                }else {
                    ?>
                    <div class="col-md-4">
                        <div class="single-video">
                            <div class="video-code">
                                <iframe width="100%" height="300" src="https://www.youtube.com/embed/8knG6gMtRTA?rel=0&amp;showinfo=0" frameborder="0" allow=" encrypted-media" allowfullscreen></iframe>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="single-video">
                            <div class="video-code">
                                <iframe width="100%" height="300" src="https://www.youtube.com/embed/8knG6gMtRTA?rel=0&amp;showinfo=0" frameborder="0" allow=" encrypted-media" allowfullscreen></iframe>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="single-video">
                            <div class="video-code">
                                <iframe width="100%" height="300" src="https://www.youtube.com/embed/8knG6gMtRTA?rel=0&amp;showinfo=0" frameborder="0" allow=" encrypted-media" allowfullscreen></iframe>
                            </div>
                        </div>
                    </div>
<!--                    <div class="col-md-12 center"><h1>Sorry! No Content is uploaded yet.</h1><br></div>-->
                    <?php
                }
                ?>
            </div>
        </div>
    </section>

</section>