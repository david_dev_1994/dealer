<section>
    <div class="block">
        <div class="container">
            <div class="row">
                <div class="heading2">
                    <h2><?= $data['title']; ?></h2>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div><?= $data['content']; ?></div>
                </div>
            </div>
        </div>
    </div>
</section>