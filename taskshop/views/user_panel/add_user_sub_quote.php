<!--Page main section start-->
<section id="min-wrapper">
    <div id="main-content">
        <div class="container-fluid">

            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title">Add Sub Quote</h3>
                        </div>
                        <div class="panel-body">
                            <form name="add_job_sub_quote_form" id="defaultForm" method="post" class="form-horizontal ls_form" action="<?php echo base_url(); ?>quote/save_sub_quote.html">

                                <div class="form-group">
                                    <label class="col-lg-3 control-label">Job Title</label>
                                    <div class="col-lg-6">
                                        <input type="text" class="form-control" name="data[job_title]" placeholder="Add job title" value="<?php echo set_value('data[job_title]'); ?>" />
                                        <input type="hidden" name="quote_id" value="<?php echo $primary_quote_id; ?>">
                                        <span class="errorMesage" style="color:red;"><?php echo form_error('data[job_title]'); ?></span>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="job_category_id" class="col-lg-3 control-label">Job Category</label>
                                    <div class="col-lg-3">
                                        <select class="form-control" name="data[job_category_id]" id="job_category_id">
                                            <option selected="selected" disabled="">Select One</option>
                                            <?php foreach ($job_category_info as $v_job_category_info) { ?>
                                                <option value="<?php echo $v_job_category_info['job_category_id']; ?>"><?php echo $v_job_category_info['job_category_name']; ?></option>
                                            <?php } ?>
                                        </select>
                                        <span class="errorMesage"><?php echo form_error('data[job_category_id]'); ?></span>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="return_file_type" class="col-lg-3 control-label">Return File Type</label>
                                    <div class="col-lg-3">
                                        <select class="form-control" name="data[return_file_type]" id="return_file_type">
                                            <option selected="selected" disabled="">Select One</option>
                                            <option value="jpg">JPG</option>
                                            <option value="psd">PSD</option>
                                            <option value="tif">TIF</option>
                                            <option value="png">PNG</option>
                                            <option value="ai">AI</option>
                                            <option value="eps">EPS</option>
                                            <option value="pdf">PDF</option>
                                            <option value="other">OTHER</option>
                                        </select>
                                        <span class="errorMesage"><?php echo form_error('data[return_file_type]'); ?></span>
                                    </div>
                                </div>
                                
                                <div class="form-group">
                                    <label for="product" class="col-lg-3 control-label">Product</label>
                                    <div class="col-lg-3">
                                        <select class="form-control" name="data[product]" id="return_file_type">
                                            <option selected="selected" disabled="">Select One</option>
                                            <option value="product1">Product 1</option>
                                            <option value="product2">Product 2</option>
                                           
                                        </select>
                                        <span class="errorMesage"><?php echo form_error('data[return_file_type]'); ?></span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="need_path" class="col-lg-3 control-label">Do you need path ?</label>
                                    <div class="col-lg-3">
                                        <select class="form-control" name="data[need_path]" id="need_path">
                                            <option selected="selected" disabled="">Select One</option>
                                            <option value="0">No need Path</option>
                                            <option value="1">Yes Clipping Path active</option>
                                            <option value="2">Yes Clipping Path inactive</option>
                                            <option value="3">Don't know please advice</option>
                                        </select>
                                        <span class="errorMesage"><?php echo form_error('data[need_path]'); ?></span>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-lg-3 control-label">Number of Image</label>
                                    <div class="col-lg-6">
                                        <input type="text" class="form-control" name="data[number_of_image]" placeholder="Add number of images" value="<?php echo set_value('data[number_of_image]'); ?>" />
                                        <span class="errorMesage"><?php echo form_error('data[number_of_image]'); ?></span>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="job_type" class="col-lg-3 control-label">Job Type:</label>
                                    <div class="col-lg-3">
                                        <select class="form-control" name="data[job_type]" id="job_type">
                                            <option selected="selected" disabled="">Select One</option>
                                            <option value="1">One time</option>
                                            <option value="0">Monthly</option>
                                        </select>
                                        <span class="errorMesage"><?php echo form_error('data[job_type]'); ?></span>
                                    </div>
                                </div>
                                
                                <div class="form-group">
                                    <label class="col-lg-3 control-label">Price (USD)</label>
                                    <div class="col-lg-6">
                                        <input type="text" class="form-control" name="data[one_time_price_usd]" placeholder="Price" value="<?php echo set_value('data[one_time_price_usd]'); ?>" />
                                        <span class="errorMesage"><?php echo form_error('data[one_time_price_usd]'); ?></span>
                                    </div>
                                </div>
                                
                                <div class="form-group">
                                    <label class="col-lg-3 control-label">Dead Line (Hr)</label>
                                    <div class="col-lg-6">
                                        <input type="text" class="form-control" name="data[one_time_dead_line_hr]" placeholder="Job Dead Line in Hr" value="<?php echo set_value('data[one_time_dead_line_hr]'); ?>" />
                                        <span class="errorMesage"><?php echo form_error('data[one_time_dead_line_hr]'); ?></span>
                                    </div>
                                </div>
                                
                                <div class="form-group">
                                    <label class="col-lg-3 control-label">Dead Line (Day)</label>
                                    <div class="col-lg-6">
                                        <input type="text" class="form-control" name="data[dead_line_days]" placeholder="Job Dead Line in Day" value="<?php echo set_value('data[dead_line_days]'); ?>" />
                                        <span class="errorMesage"><?php echo form_error('data[dead_line_days]'); ?></span>
                                    </div>
                                </div>
                                
                                <div class="form-group">
                                    <label class="col-lg-3 control-label">Estimated Monthly Work Volume (Number Of Image):</label>
                                    <div class="col-lg-6">
                                        <input type="text" class="form-control" name="data[monthly_work_volume]" placeholder="Estimated Monthly Work Volume" value="<?php echo set_value('data[monthly_work_volume]'); ?>" />
                                        <span class="errorMesage"><?php echo form_error('data[monthly_work_volume]'); ?></span>
                                    </div>
                                </div>
                                
                                <div class="form-group">
                                    <label class="col-lg-3 control-label">Estimated Monthly Amount (USD):</label>
                                    <div class="col-lg-6">
                                        <input type="text" class="form-control" name="data[monthly_amount_usd]" placeholder="Estimated Monthly Amount" value="<?php echo set_value('data[monthly_amount_usd]'); ?>" />
                                        <span class="errorMesage"><?php echo form_error('data[monthly_amount_usd]'); ?></span>
                                    </div>
                                </div>
                                
                                <div class="form-group">
                                    <label class="col-lg-3 control-label">
                                        Instruction Message
                                    </label>
                                    <div class="col-lg-6">
                                        <textarea name="data[instruction_message]" class="form-control" placeholder="Instruction Message" rows="5" cols="74"><?php echo set_value('data[instruction_message]'); ?></textarea>
                                        <span class="errorMesage"><?php echo form_error('data[instruction_message]'); ?></span>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-lg-9 col-lg-offset-3">
                                        <button type="submit" class="btn btn-primary">Add Sub Quote</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<script type="text/javascript">
<?php if (set_value('data[need_path]') != null) { ?>
        document.forms['add_job_sub_quote_form'].elements['need_path'].value = '<?php echo set_value('data[need_path]'); ?>';
<?php }if (set_value('data[return_file_type]') != null) { ?>
        document.forms['add_job_sub_quote_form'].elements['return_file_type'].value = '<?php echo set_value('data[return_file_type]'); ?>';
<?php }if (set_value('data[job_category_id]') != null) { ?>
        document.forms['add_job_sub_quote_form'].elements['job_category_id'].value = '<?php echo set_value('data[job_category_id]'); ?>';
<?php }if (set_value('data[job_type]') != null) { ?>
        document.forms['add_job_sub_quote_form'].elements['job_type'].value = '<?php echo set_value('data[job_type]'); ?>';
<?php }?>
</script>