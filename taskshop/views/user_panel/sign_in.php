<?php
//print_r($_SESSION);
//exit;
?>
<section>
    <div class="block extra-gap blackish ">
        <div class="container shadow-container">
            <div class="row">
                <div class="col-md-12">
                    <?php if ($this->session->flashdata('error')){?>
                        <div class="col-lg-6 col-lg-offset-3 alert alert-danger alert-dismissible text-center">
                            <?=$this->session->flashdata('error')?>
                        </div>
                    <?php }?>
                    <?php if ($this->session->flashdata('success')){?>
                        <div class="col-lg-6 col-lg-offset-3 alert alert-success alert-dismissible text-center">
                            <?=$this->session->flashdata('success')?>
                        </div>
                    <?php }?>

                    <div class="row">
                        <div class="col-md-12">
                            <img style="margin: 0 auto; display: block;" src="<?= base_url('admin_assets/uploader_assets/img/login-service-icon.png');?>">
                        </div>
                        <br>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                        <h2 class="text-capitalize text-center circle-text">Dealer Login</h2>
                        <div class="title text-center login-sub-title"><span><i>Please log in using the Google Chrome browser to access your dealer account</i></span></div>
                        </div>
                        <br>
                    </div>
                    <div class="row col-md-6 col-md-offset-3 column">
                        <div class="title text-center login-sub-title" style="color: red;"><span><i>For Approved BK Dealers Only</i></span></div>
                        <div id="message"></div>
                        <form class="form-opened text-center border border-light p-5 contact" method="post" action="<?php echo base_url(); ?>user_login/check_user_login.html">
                                <div class="row col-md-12 "><input class="form-control " name="email_address" type="email" id="email_address"  placeholder="Email Address" /></div>
                                <div class="row col-md-12"><input class="form-control" name="user_password" type="password" id="user_password" size="30" value="" placeholder="Password" /></div>

                                <div class="row col-md-12"><button class=" btn btn-success btn-block" type="submit"><strong><span class="fa fa-user"></span> LOGIN</strong></button></div>
                            <div class="col-md-6"><a href="<?php echo base_url('page/sign_up'); ?>"><h4>Create a login</h4></a></div>
                            <div class="col-md-6"><a href="<?php echo base_url('user_login/forgot_password'); ?>"><h4>Forgot Password</h4></a></div>
                        </form><!-- Contact Form -->
                    </div>

                </div>
            </div>
        </div>
    </div>
</section>

