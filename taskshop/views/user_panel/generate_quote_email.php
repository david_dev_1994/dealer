<?php
$quote_info['shipping_address'] = json_decode($quote_info['shipping_address']);
$quote_info['shipping_city'] = json_decode($quote_info['shipping_city']);
$quote_info['shipping_contactinfo'] = json_decode($quote_info['shipping_contactinfo']);
$quote_info['shipping_state'] = json_decode($quote_info['shipping_state']);
$quote_info['shipping_zip'] = json_decode($quote_info['shipping_zip']);

$quote_info['billing_address'] = json_decode($quote_info['billing_address']);
$quote_info['billing_city'] = json_decode($quote_info['billing_city']);
$quote_info['billing_contactinfo'] = json_decode($quote_info['billing_contactinfo']);
$quote_info['billing_state'] = json_decode($quote_info['billing_state']);
$quote_info['billing_zip'] = json_decode($quote_info['billing_zip']);

$quote_info['model'] = json_decode($quote_info['model']);
$quote_info['description'] = json_decode($quote_info['description']);
$quote_info['family'] = json_decode($quote_info['family']);
$quote_info['type'] = json_decode($quote_info['type']);
$quote_info['quantity'] = json_decode($quote_info['quantity']);
$quote_info['price'] = json_decode($quote_info['price']);
$quote_info['list_price'] = json_decode($quote_info['list_price']);
$quote_info['dealer_price'] = json_decode($quote_info['dealer_price']);
?>

<style>
    .container-fluid-pdf {
        max-width: 1170px;
        padding: 0 15px;
        margin: 0 auto
    }

    .header-pdf {
        text-align: center;
        margin-bottom: 50px
    }

    .header-pdf img {
        max-height: 100px
    }

    .header-pdf img {
        display: inline-block;
        vertical-align: middle
    }

    .header-pdf h2 {
        display: inline-block;
        width: auto;
        text-transform: uppercase;
        margin: 0 15px
    }

    .content-pdf {
        width: 100%;
        box-shadow: 0px 0px 25px hsla(0, 0%, 0%, 0.70);
        background: ivory;
        float: left;
        padding: 50px 0
    }

    .content-pdf h2 {
        text-align: center;
    }

    .content-pdf > div {
        margin: 0 8.33333333%;
        width: 83.33333333%;
        float: left;
    }

    .content-pdf > div .row-content {
        width: 100%;
        border-top: 1px solid #555;
        float: left
    }

    .content-pdf > div .col-content {
        width: 50%;
        float: left;
        color: #555;
        padding: 10px 0
    }

    .content-pdf > div .col-content div {
        width: 100%;
        padding: 10px 0;
        display: table
    }

    .content-pdf > div .col-content b {
        font-weight: normal;
        color: #333;
        width: 150px;
        display: table-cell;
        vertical-align: top;
    }

    .table {
        margin-top: 50px;
        float: left;
        width: 100%
    }

    .table table {
        width: 100%;
    }

    .table th {
        background: #767676;
        color: #fff;
        padding: 10px 5px;
    }

    .table tr:nth-child(odd) {
        background: #eee;
    }

    .table td {
        border-right: 1px solid #767676;
        color: #767676;
        padding: 10px 5px;
    }

    .table td:last-child {
        border: 0;
    }

    .footer-pdf {
        text-align: center;
        margin-top: 50px;
        float: left;
        width: 100%;
    }

    .footer-pdf b, .footer-pdf a {
        display: block;
    }
</style>

<form class="form-quote-page" action="" method="post" enctype="multipart/form-data">
    <div class="container-fluid-pdf" id="main-content">
        <!--end of header-->
        <div class="shadow-container content-pdf">
            <div class="header-pdf">
                <img style="float:left; width: 70px" src="<?= base_url('user_assets/img/bk-logo.png'); ?>" alt=""/>
                <h2>Price Quote</h2>
                <img style="float:right; width: 50px" src="<?= base_url('user_assets/img/phone-img.png'); ?>" alt=""/>
            </div>
            <div>
                <div class="row-content">
                    <div class="col-content">
                        <h3>PREPARED BY:</h3>
                        <div>
                            <b>Prepared
                                By: </b><?= isset($quote_info['name']) && !empty($quote_info['name']) ? $quote_info['name'] : ''; ?>
                        </div>
                        <div>
                            <b>Title: </b>Dealer
                        </div>
                        <div>
                            <b>Phone: </b><?= isset($quote_info['phone']) && !empty($quote_info['phone']) ? $quote_info['phone'] : ''; ?>
                        </div>
                        <div>
                            <b>Email: </b><?= isset($quote_info['email']) && !empty($quote_info['email']) ? $quote_info['email'] : ''; ?>
                        </div>
                    </div><!--end of col-content-->
                    <div class="col-content">
                        <h3>PREPARED FOR:</h3>
                        <div>
                            <b>Sales
                                Manager: </b><?= isset($quote_info['sales_person']) && !empty($quote_info['sales_person']) ? $quote_info['sales_person'] : ''; ?>
                        </div>
                        <div>
                            <b>Sales
                                Manager Email: </b><?= isset($quote_info['sales_manager_email']) && !empty($quote_info['sales_manager_email']) ? $quote_info['sales_manager_email'] : ''; ?>
                        </div>

                        <!--                            <div>-->
                        <!--                                <b>Email</b>ktest@bktechnologies.com-->
                        <!--                            </div>-->
                    </div><!--end of col-content-->
                </div><!--end of row-content-->


                <div class="row-content">
                    <div class="col-content">
                        <div>
                            <b>Dealer Purchase Order Number: </b><?= isset($quote_info['orderNumber']) && !empty($quote_info['orderNumber'])?$quote_info['orderNumber']:''; ?>
                        </div>
                        <div>
                            <b>Quote Number: </b><?= $quote_info['id'] ?>
                        </div>
                        <div>
                            <b>Created Date</b> <?= date('m/d/y',$quote_info['created_date']); ?>
                        </div>
                        <div>
                            <b>Expiration Date</b> <?= date('m/d/y',$quote_info['expiry_date']); ?>
                        </div>
                        <div>
                            <b>Type of Contract Name: </b>DEALER PRICE
                        </div>
                    </div><!--end of col-content-->
                </div><!--end of row-content-->
                <br>
                <br>
                <div class="row-content">

                    <div class="col-content">
                        <?php foreach ($quote_info['billing_address'] as $i => $value) { ?>
                            <div>
                                <b>Bill to Business Name: </b> <?= isset($quote_info['ship_to_company']) && !empty($quote_info['ship_to_company']) ? $quote_info['ship_to_company'] : '';//isset($quote_info['name']) && !empty($quote_info['name']) ? $quote_info['name'] : ''; ?>
                            </div>
                            <div>
                                <b>Bill To: </b>
                                <span>
                                <?= $value ?>
                                </span>
                            </div>
                            <div>
                                <b>City: </b><span><?= $quote_info['billing_city'][$i]; ?>
                                
                                </span>
                            </div>
                            <div>
                                <b>State: </b><span><?= $quote_info['billing_state'][$i]; ?>
                                
                                </span>
                            </div>
                            <div>
                                <b>PO Box / Zip Code: </b><span><?= $quote_info['billing_zip'][$i]; ?>
                                
                                </span>
                            </div>
                            <div>
                                <b>Contact Info: </b><span><?= $quote_info['billing_contactinfo'][$i]; ?>
                                
                                </span>
                            </div>
                        <?php } ?>
                    </div><!--end of col-content-->

                    <br>
                    <br>

                    <div class="col-content">
                        <?php foreach ($quote_info['shipping_address'] as $i => $value) { ?>
                            <div>
                                <b>Ship to Business Name: </b> <?= isset($quote_info['ship_to_company']) && !empty($quote_info['ship_to_company']) ? $quote_info['ship_to_company'] : $quote_info['name'];//isset($quote_info['ship_to_name']) && !empty($quote_info['ship_to_name']) ? $quote_info['ship_to_name'] : $quote_info['name']; ?>
                            </div>
                            <div>
                                <b>Ship To: </b>
                                <span>
                                <?= $value ?>
                                </span>
                            </div>
                            <div>
                                <b>City: </b><span><?= $quote_info['shipping_city'][$i]; ?>
                            </div>
                            <div>
                                <b>State: </b><span><?= $quote_info['shipping_state'][$i]; ?>
                                
                                </span>
                            </div>
                            <div>
                                <b>Zip Code: </b><span><?= $quote_info['shipping_zip'][$i]; ?>
                                </span>
                            </div>
                            <div>
                                <b>Contact Info: </b><span><?= $quote_info['shipping_contactinfo'][$i]; ?>
                                </span>
                            </div>
                        <?php } ?>
                    </div><!--end of col-content-->
                </div><!--end of row-content-->
            </div>

            <br>

            <?php if(isset($quote_info['special_instruction']) && !empty($quote_info['special_instruction'])){ ?>

                <div class="container-fluid">
                    <div class="inner">
                        <p class="initial"></p>
                    </div>
                    <div class="quote-row-line"></div>
                    <div class="form-row" style="margin-top: 20px">
                        <h3 class="dealer-form-sub-heading">Special Instructions</h3>
                    </div>
                </div>
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-11 form-group">
                                <?= isset($quote_info['special_instruction']) && !empty($quote_info['special_instruction']) ? $quote_info['special_instruction']:''; ?>
                        </div>
                    </div>
                </div>

            <?php } ?>

            <br>
            <div class="table">
                <table cellspacing="0" cellpadding="3">
                    <thead>
                    <tr>
                        <th>Product Model</th>
                        <th>Product Description</th>
                        <th>Product Family</th>
                        <th>Product Type</th>
                        <th>List Price</th>
                        <th>Dealer Price</th>
                        <th>Quantity</th>
                        <th>Total Price</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php if (isset($quote_info)) {
                        foreach ($quote_info['model'] as $i => $value) {
                            ?>
                            <tr>
                                <td>
                                    <?= $value ?>
                                </td>
                                <td style="width: 15px">
                                    <?= $quote_info['description'][$i]; ?>
                                </td>
                                <td>
                                    <?= $quote_info['family'][$i]; ?>
                                </td>
                                <td>
                                    <?= $quote_info['type'][$i]; ?>
                                </td>
                                <td>
                                    <?= $quote_info['list_price'][$i]; ?>
                                </td>
                                <td>
                                    <?= $quote_info['dealer_price'][$i]; ?>
                                </td>
                                <td>
                                    <?= number_format($quote_info['quantity'][$i]); ?>
                                </td>
                                <td>
                                    <?= $quote_info['price'][$i]!=0?'$'.number_format($quote_info['price'][$i],2):'CFQ'; ?>
                                </td>
                                <!--                    <td>$2,995.00</td>-->
                            </tr>
                        <?php }
                    } ?>
                    </tbody>
                </table>
            </div>
            <br>
            <br>
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <b>
                            <div style="margin-left: 65%;" class="col-md-10">Grand Total: <?= isset($quote_info['grandTotal']) && !empty($quote_info['grandTotal']) ? '$' . number_format($quote_info['grandTotal'],2): '' ?></div>
                        </b>
                    </div>
                </div>
            </div>  
            <!--http://www.bktechnologies.com/dealers/uploads/-->
            <br>
            <div><br></div>
            <div class="fileDiv <?= isset($quote_info['file'])&& !empty($quote_info['file'])?'show':'hidden'; ?> ">
                <b>Attachment:</b> <a class="attachedFile" target="_blank" href="<?= isset($quote_info['file'])&& !empty($quote_info['file'])?base_url('uploads/'.$quote_info['file']):'';?>"><?= isset($quote_info['file'])&& !empty($quote_info['file'])?'Attached File':'';?></a>
            </div>
            <?php if(isset($quote_info['po_file'])&& !empty($quote_info['po_file'])) : ?>
            <div class="poFileDiv">
                <b>PO Attachment:</b> <a href="http://www.bktechnologies.com/dealer/uploads/<?= $quote_info['po_file'] ;?>">Click here to download PO</a>
            </div>
            <?php endif; ?>
            <div><br></div>
            <div><br></div>

            <div class="footer-pdf">
                <b>BK Technologies</b>
                7100 Technology Drive<br> West Melbourne, FL 32904<br/>United States
                <b>www.bktechnologies.com</b>
            </div><!--end of footer-pdf-->

            <div><br></div>
            <div><br></div>
            <div><b><em>This quote will expire in 90-days from the date of creation</em></b></div>


        </div><!--end of content-->


        <script>
            function print() {
                var element = document.getElementById('main-content');
                var opt = {
                    margin: 1,
                    filename: (Math.floor(Math.random() * 10000) + 1) + '.pdf',
                };

// New Promise-based usage:
                html2pdf().set(opt).from(element).save();
            }

            //var csTotal = $(".grandTotal").html();
            //$(".print-price").html(csTotal);
            //console.log(csTotal);
        </script>

    </div><!--end of container-fluid-->
</form>

<style>

    .table {
        background: none !important;
    }

    .table {
        padding: 0px 0px !important;
        margin-top: 15px !important;
        margin-bottom: 20px !important;
    }

    .content-pdf h2 {
        text-align: center;
        font-size: 42px !important;
        margin-top: 25px !important;
    }

    .header-pdf {
        margin-bottom: 35px !important;
    }

</style>
