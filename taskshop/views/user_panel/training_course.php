<section id="min-wrapper">
    <div id="main-content">
        <div class="container-fluid">
            <div>
                <h2>Training Videos</h2>
                <div class="title text-center login-sub-title"><span><i>All Listings Will Be Displayed Below</i></span></div>
            </div>
        </div>

        <?php if ($this->session->flashdata('message')){?>
            <div class=" alert alert-success alert-dismissible text-center row">
                <?=$this->session->flashdata('message')?>
            </div>
        <?php }?>
        <style>.dataTables_length{padding-left:15px;}.dataTables_filter{padding-right:15px</style>
        <div class="container-fluid">
            <div class="row">
                <div class="as-table-wrapper">
                    <div class="row">
                        <div class="col-md-12">
                            <!-- BEGIN EXAMPLE TABLE PORTLET-->
                            <div class="portlet light no-border-top as-box-border">
                                <div class="portlet-body">
                                    <table class="table table-striped table-bordered table-hover dt-responsive" id="data_table" data-name="" style="background: none;width:100%;padding:0">
                                        <thead>
                                        <tr>
<!--                                            <th style="background:#767676;color:#FFF"> Serial Number </th>-->
                                            <th style="background:#767676;color:#FFF" class="hide"> id </th>
                                            <th style="background:#767676;color:#FFF"> Date Added </th>
                                            <th style="background:#767676;color:#FFF"> Thumbnail </th>
                                            <th style="background:#767676;color:#FFF"> Video Title </th>
                                            <th style="background:#767676;color:#FFF"> File </th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php
                                        $v = 'https://img.youtube.com/vi/';
                                        $xz = 'https://www.youtube.com/embed/';
                                        foreach($data as $index=>$video){
                                            $d = strchr($video['video_url'], '?');
                                            $c = str_replace('&t=', 'start=', $d);
                                            $parts = parse_url($video['video_url']);
                                            ?>
                                            <tr class="odd">
<!--                                                <td>--><?//= $index+1;?><!-- </td>-->
                                                <td class="hide"> <?= $video['id']; ?> </td>
                                                <td> <?= isset($video['created_at'])?date('m-d-Y',$video['created_at']):'';?> </td>
                                                <td><img src="<?= isset($video['video_id'])?$v.$video['video_id'].'/default.jpg':'';?>" alt="image" style="height:50px;width: 50px; "></td>
                                                <td> <?= isset($video['title'])?$video['title']:'';?> </td>
                                                <td> <div class="btn-group">
                                                        <a href="#" data-src="<?= $xz.$video['video_id'].$c; ?>" data-title="<?= $video['title']; ?>" class="btn btn-primary" data-toggle="modal" data-target="#videoIframe">View</a>
<!--                                                        <a href="--><?//= base_url('uploads/'.$video['video_id']); ?><!--" target="_blank" class="btn btn-primary">View</a>-->
                                                </td>
                                            </tr>
                                            <?php
                                        }
                                        ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <!-- END EXAMPLE TABLE PORTLET-->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


