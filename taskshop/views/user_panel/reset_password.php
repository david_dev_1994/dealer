<section>
    <div class="block extra-gap">
        <div class="container">
            <div class="row">
                <div class="col-md-12">

                        <h2 class="text-capitalize text-center circle-text">Reset Password</h2>
                        <div>
                            <?php
                            $msg = $this->session->userdata('message');
                            $error = $this->session->userdata('error');

                            if ($msg) {
                                echo "<div class='alert alert-success text-center'>" . $msg . "</div>";
                                $this->session->unset_userdata('message');
                            } else if ($error) {
                                echo "<div class='alert alert-danger text-center'>" . $error . "</div>";
                                $this->session->unset_userdata('error');
                            }
                            ?>
                        </div>
                        <div class="title text-center login-sub-title"><span><i>Please enter your associated email address</i></span></div>
                    <div class="col-md-4 col-md-offset-4 column">
                        <div id="message"></div>
                        <form class="contact" method="post" action="<?php echo base_url(); ?>user_login/forgot_password.html">
                            <!--<div class="row">-->
                                <div class="row col-md-12"><input class="form-control" name="email_address" type="email" id="email_address"  placeholder="Email address" required /></div>
                                <input type="text" name="reset_password" value="1" hidden />
                                <div class="row col-md-12"><button class=" btn btn-success btn-block" type="submit"><strong>Reset Password</strong></button></div>
                                
                            <!--</div>-->
                        </form><!-- Contact Form -->
                    </div>

                </div>
            </div>
        </div>
    </div>
</section>