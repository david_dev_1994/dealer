<?php
    $v = 'https://www.youtube.com/embed/';
    $d = strchr($data['video_url'], '?');
    $c = str_replace('&t=', 'start=', $d);
    $parts = parse_url($data['video_url']);
?>

<section>
    <div class="block">
        <div class="container">
            <?php
            if(isset($data['title']) && !empty($data['title'])){
            ?>
            <div class="row">
                <div class="heading2">
                    <h2><?= $data['title']; ?></h2>
                </div>
            </div>
            <?php }
            ?>

            <div class="row">
                <div class="container">
                    <div class="col-md-6 col-md-offset-3">
                        <iframe width="100%" height="420" src="<?= $v.$data['video_id'].$c; ?>" frameborder="0" allow="encrypted-media" allowfullscreen></iframe>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>