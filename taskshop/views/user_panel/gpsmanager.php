
<?php
$zip = json_decode($user_info['zip']);
$city = json_decode($user_info['city']);
$state = json_decode($user_info['state']);
//print_r($zip);
$ccc;
//foreach ($zip as $z){
//    $ccc .= $z;
//}
//echo '<pre>';print_r($user_info);die(); ?>
<section id="min-wrapper">
    <div id="main-content">
        <div class="container-fluid">
             <div class="row">
                 <div class="col-lg-3 pr">
                     <h3>Address:</h3>
                     <span><?= $city[0].' '.$state[0].' '.$zip[0] ; ?></span>
                 </div>
                 <div class="col-lg-8 pl">
                <h2 style="">All Gps Dealers</h2>
                <div class="title text-center login-sub-title"><span><i>All Listings Will Be Displayed Below</i></span></div>
                 </div>
            </div><!--end of row-->
        </div>

        <?php if ($this->session->flashdata('message')){?>
            <div class=" alert alert-success alert-dismissible text-center row">
                <?= $this->session->flashdata('message'); ?>
            </div>
        <?php }?>
        <style>.dataTables_length{padding-left:15px;}.dataTables_filter{padding-right:15px;}
        .pr{padding-right: 0;}
        .pr h3{
            color:#00a6d5;
        }
            .pl{padding-left: 0;}
        </style>
        <div class="container-fluid">
            <div class="row">
                <div class="as-table-wrapper">
                    <div class="row">
                        <div class="col-md-12">
                            <!-- BEGIN EXAMPLE TABLE PORTLET-->
                            <div class="portlet light no-border-top as-box-border">
                                <div class="portlet-body">
                                    <table class="table table-striped table-bordered table-hover dt-responsive" id="data_table" data-name="services" style="background: none;width:100%;padding:0">
                                        <thead>
                                        <tr>
                                            <th style="background:#767676;color:#FFF"> Company Name</th>
                                            <th style="background:#767676;color:#FFF"> Full Name</th>
                                            <th style="background:#767676;color:#FFF"> Email</th>
                                            <th style="background:#767676;color:#FFF"> Phone</th>
                                            <th style="background:#767676;color:#FFF"> User Level</th>
                                            <th style="background:#767676;color:#FFF"> Registered Date</th>
                                            <th style="background:#767676;color:#FFF"> Sales Manager</th>
                                            <?php if($this->session->userdata('access_label') == 1 || $this->session->userdata('d_access_label') == 4){ ?>
                                                <th style="background:#767676;color:#FFF"> Action</th>
                                            <?php } ?>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php foreach ($users_info as $v_user_info) { ?>
                                            <?php if ($v_user_info['u_id'] != $this->session->userdata('user_id')) { ?>
                                                <tr id="dealerRow_<?= $v_user_info['u_id']; ?>">
                                                    <td><?php echo $v_user_info['companyname']; ?></td>
                                                    <td><?php echo $v_user_info['full_name']; ?></td>
                                                    <td><a href="mailto:<?= $v_user_info['email_address']; ?>"><?= $v_user_info['email_address']; ?></a></td>
                                                    <td><?php echo $v_user_info['phone']; ?></td>
                                                    <td>
                                                        GPS Dealer
                                                    </td>
                                                    <!--                                            <td>--><?php //echo date("D d F Y h:ia", strtotime($v_user_info['date_added'])); ?><!--</td>-->
                                                    <td><?php echo date("m-d-y h:i A", strtotime($v_user_info['date_added'])); ?></td>
                                                    <td><?php echo $v_user_info['sales_manager_name']; ?></td>
                                                    <?php if($this->session->userdata('access_label') == 1 || $this->session->userdata('d_access_label') == 4) { ?>
                                                        <td>
                                                            <!--                                                    --><?php //if ($v_user_info['activation_status'] == 1) { ?>

                                                            <a  class="btn btn-xs btn-warning " href="<?php echo base_url();?>quote/dview/2/<?= $v_user_info['u_id']; ?>" title="View Drafts">
                                                                <!--                                                        <a href="--><?php //echo admin_url(); ?><!--/registered_user/unblock_dealer/--><?php //echo $v_user_info['u_id']; ?><!--/--><?php //echo $v_user_info['email_address']; ?><!--" id="unblock" class="btn btn-xs btn-warning hidden" title="Unblock">-->
                                                                <i class="fa fa-eye" aria-hidden="true"></i>
                                                            </a>
                                                            <a  class="btn btn-xs btn-success " href="<?php echo base_url();?>quote/dview/1/<?= $v_user_info['u_id']; ?>" title="Submitted Quotes">
                                                                <!--                                                        <a href="--><?php //echo admin_url(); ?><!--/registered_user/unblock_dealer/--><?php //echo $v_user_info['u_id']; ?><!--/--><?php //echo $v_user_info['email_address']; ?><!--" id="unblock" class="btn btn-xs btn-warning hidden" title="Unblock">-->
                                                                <i class="fa fa-eye" aria-hidden="true"></i>
                                                            </a>


                                                            <!--                                                        <a id="edit" class="btn btn-xs btn-info" data-toggle="modal" data-target="#myModal--><?php //echo $v_user_info['u_id']; ?><!--" title="Edit">-->
                                                            <!--                                                            <i class="fa fa-pencil-square-o"></i>-->
                                                            <!--                                                        </a>-->
                                                            <!--                                                    <a href="--><?php //echo admin_url(); ?><!--/registered_user/delete_dealer/--><?php //echo $v_user_info['u_id']; ?><!--" id="edit" class="btn btn-xs btn-danger" data-toggle="tooltip" data-placement="top" title="Delete"  onclick="return confirm('Are you sure to delete this ?');">-->

                                                        </td>
                                                    <?php } ?>
                                                </tr>
                                            <?php } ?>
                                        <?php } ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <!-- END EXAMPLE TABLE PORTLET-->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


