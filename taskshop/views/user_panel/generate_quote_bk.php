<?php
$quote_info['shipping_address'] = json_decode($quote_info['shipping_address']);
$quote_info['shipping_city'] = json_decode($quote_info['shipping_city']);
$quote_info['shipping_contactinfo'] = json_decode($quote_info['shipping_contactinfo']);
$quote_info['shipping_state'] = json_decode($quote_info['shipping_state']);
$quote_info['shipping_zip'] = json_decode($quote_info['shipping_zip']);

$quote_info['billing_address'] = json_decode($quote_info['billing_address']);
$quote_info['billing_city'] = json_decode($quote_info['billing_city']);
$quote_info['billing_contactinfo'] = json_decode($quote_info['billing_contactinfo']);
$quote_info['billing_state'] = json_decode($quote_info['billing_state']);
$quote_info['billing_zip'] = json_decode($quote_info['billing_zip']);

$quote_info['model'] = json_decode($quote_info['model']);
$quote_info['description'] = json_decode($quote_info['description']);
$quote_info['family'] = json_decode($quote_info['family']);
$quote_info['type'] = json_decode($quote_info['type']);
$quote_info['list_price'] = json_decode($quote_info['list_price']);
$quote_info['dealer_price'] = json_decode($quote_info['dealer_price']);
$quote_info['quantity'] = json_decode($quote_info['quantity']);
$quote_info['price'] = json_decode($quote_info['price']);
?>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>

<style>
    .container-fluid-pdf {
        max-width: 1170px;
        padding: 0 15px;
        margin: 0 auto
    }

    .header-pdf {
        text-align: center;
        margin-bottom: 50px
    }

    .header-pdf img {
        max-height: 100px
    }

    .header-pdf img {
        display: inline-block;
        vertical-align: middle
    }

    .header-pdf h2 {
        display: inline-block;
        width: auto;
        text-transform: uppercase;
        margin: 0 15px
    }

    .content-pdf {
        width: 100%;
        box-shadow: 0px 0px 25px hsla(0, 0%, 0%, 0.70);
        background: ivory;
        float: left;
        padding: 50px 0
    }

    .content-pdf h2 {
        text-align: center;
    }

    .content-pdf > div {
        margin: 0 8.33333333%;
        width: 83.33333333%;
        float: left;
    }

    .content-pdf > div .row-content {
        width: 100%;
        border-top: 1px solid #555;
        float: left
    }

    .content-pdf > div .col-content {
        width: 50%;
        float: left;
        color: #555;
        padding: 10px 0
    }

    .content-pdf > div .col-content div {
        width: 100%;
        padding: 10px 0;
        display: table
    }

    .content-pdf > div .col-content b {
        font-weight: normal;
        color: #333;
        width: 150px;
        display: table-cell;
        vertical-align: top;
    }

    .table {
        margin-top: 50px;
        float: left;
        width: 100%
    }

    .table table {
        width: 100%;
    }

    .table th {
        background: #767676;
        color: #fff;
        padding: 10px 5px;
    }

    .table tr:nth-child(odd) {
        background: #eee;
    }

    .table td {
        border-right: 1px solid #767676;
        color: #767676;
        padding: 10px 5px;
    }

    .table td:last-child {
        border: 0;
    }

    .footer-pdf {
        text-align: center;
        margin-top: 50px;
        float: left;
        width: 100%;
    }

    .footer-pdf b, .footer-pdf a {
        display: block;
    }
</style>

<form class="form-quote-page" action="" method="post" enctype="multipart/form-data">
    <div class="container-fluid-pdf" id="main-content">
        <!--end of header-->
        <div class="shadow-container content-pdf" id="content-pdf" style="width: 100%;box-shadow: 0px 0px 25px hsla(0, 0%, 0%, 0.70);background: ivory;float: left;padding: 50px 0">
            <div class="header-pdf" style="margin: 0 8.33333333%;width: 83.33333333%;float: left;margin-bottom:35px !important">
                <img style="float:left;height:100px" src="<?= base_url('user_assets/img/bk-logo.png'); ?>" alt=""/>
                <h2 style="display: inline-block;width: 70%;text-transform: uppercase;margin: 0 15px;line-height:100px;">Price Quote</h2>
                <img style="float:right;height:100px" src="<?= base_url('user_assets/img/phone-img.png'); ?>" alt=""/>
            </div>
            <div style="margin: 0 8.33333333%;width: 83.33333333%;float: left; page-break-after: always;">
                <div class="row-content" style="width: 100%;border-top: 1px solid #555;float: left">
                    <div class="col-content" style="width: 50%;float: left;color: #555;padding: 10px 0">
                        <h3>PREPARED BY:</h3>
                        <div style="width: 100%;padding: 10px 0;display: table">
                            <b style="font-weight: normal;color: #333;width: 150px;display: table-cell;vertical-align: top;">Prepared
                                By: </b><?= isset($quote_info['name']) && !empty($quote_info['name']) ? $quote_info['name'] : ''; ?>
                            <input value="<?= isset($quote_info['name']) && !empty($quote_info['name']) ? $quote_info['name'] : ''; ?>"
                                   class="form-control hidden" name="name">
                        </div>
                        <div style="width: 100%;padding: 10px 0;display: table">
                            <b style="font-weight: normal;color: #333;width: 150px;display: table-cell;vertical-align: top;">Title: </b>Dealer
                        </div>
                        <div style="width: 100%;padding: 10px 0;display: table">
                            <b style="font-weight: normal;color: #333;width: 150px;display: table-cell;vertical-align: top;">Phone: </b><?= isset($quote_info['phone']) && !empty($quote_info['phone']) ? $quote_info['phone'] : ''; ?>
                            <input value="<?= isset($quote_info['phone']) && !empty($quote_info['phone']) ? $quote_info['phone'] : ''; ?>"
                                   class="form-control hidden" name="pnum">
                        </div>
                        <div style="width: 100%;padding: 10px 0;display: table">
                            <b style="font-weight: normal;color: #333;width: 150px;display: table-cell;vertical-align: top;">Email: </b><?= isset($quote_info['email']) && !empty($quote_info['email']) ? $quote_info['email'] : ''; ?>
                            <input value="<?= isset($quote_info['email']) && !empty($quote_info['email']) ? $quote_info['email'] : ''; ?>"
                                   class="form-control hidden" name="email">
                        </div>
                    </div><!--end of col-content-->
                    <div class="col-content" style="width: 50%;float: left;color: #555;padding: 10px 0">
                        <h3>PREPARED FOR:</h3>
                        <div style="width: 100%;padding: 10px 0;display: table">
                            <b style="font-weight: normal;color: #333;width: 150px;display: table-cell;vertical-align: top;">Sales
                                Manager: </b><?= isset($quote_info['sales_person']) && !empty($quote_info['sales_person']) ? $quote_info['sales_person'] : ''; ?>
                            <input value="<?= isset($quote_info['sales_person']) && !empty($quote_info['sales_person']) ? $quote_info['sales_person'] : ''; ?>"
                                   class="form-control hidden" name="sales_person">
                        </div>
                        <div style="width: 100%;padding: 10px 0;display: table">
                            <b style="font-weight: normal;color: #333;width: 150px;display: table-cell;vertical-align: top;">Sales
                                Manager Email: </b><?= isset($quote_info['sales_manager_email']) && !empty($quote_info['sales_manager_email']) ? $quote_info['sales_manager_email'] : ''; ?>
                            <input value="<?= isset($quote_info['sales_manager_email']) && !empty($quote_info['sales_manager_email']) ? $quote_info['sales_manager_email'] : ''; ?>"
                                   class="form-control hidden" name="sales_manager_email">
                        </div>
                        <!--                            <div>-->
                        <!--                                <b>Email</b>ktest@bktechnologies.com-->
                        <!--                            </div>-->
                    </div><!--end of col-content-->
                </div><!--end of row-content-->

                <div class="row-content" style="width: 100%;border-top: 1px solid #555;float: left">
                    <div class="col-content" style="width: 50%;float: left;color: #555;padding: 10px 0">
                        <div style="width: 100%;padding: 10px 0;display: table">
                            <b style="font-weight: normal;color: #333;width: 150px;display: table-cell;vertical-align: top;">Dealer Purchase Order Number: </b><?= isset($quote_info['orderNumber']) && !empty($quote_info['orderNumber'])?$quote_info['orderNumber']:''; ?>
                        </div>
                        <div style="width: 100%;padding: 10px 0;display: table">
                            <b style="font-weight: normal;color: #333;width: 150px;display: table-cell;vertical-align: top;">Quote Number: </b><?= $quote_info['id']; ?>
                        </div>
                        <div style="width: 100%;padding: 10px 0;display: table">
                            <b style="font-weight: normal;color: #333;width: 150px;display: table-cell;vertical-align: top;">Created Date</b> <?= date('m/d/y',$quote_info['created_date']); ?>
                        </div>
                        <div style="width: 100%;padding: 10px 0;display: table">
                            <b style="font-weight: normal;color: #333;width: 150px;display: table-cell;vertical-align: top;">Expiration Date</b> <?= date('m/d/y',$quote_info['expiry_date']); ?>
                        </div>
                        <div style="width: 100%;padding: 10px 0;display: table">
                            <b style="font-weight: normal;color: #333;width: 150px;display: table-cell;vertical-align: top;">Type of Contract Name: </b>DEALER PRICE
                        </div>
                    </div><!--end of col-content-->
                    <div class="col-content" style="width: 50%;float: left;color: #555;padding: 10px 0">
                        <!--                            <div>-->
                        <!--                                <b>Expiration Date</b>12/31/2018-->
                        <!--                                </div><div>-->
                        <!--                                <b>Ship By Date</b>12/31/2018-->
                        <!--                                </div><div>-->
                        <!--                                <b>Shipping Payment</b>FEDEX-Cust Paid-->
                        <!--                            </div>-->
                    </div><!--end of col-content-->
                </div><!--end of row-content-->
                <div class="row-content" style="width: 100%;border-top: 1px solid #555;float: left">

                    <div class="col-content" style="width: 50%;float: left;color: #555;padding: 10px 0">
                        <?php foreach ($quote_info['billing_address'] as $i => $value) { ?>
                            <div style="width: 100%;padding: 10px 0;display: table">
                                <b style="font-weight: normal;color: #333;width: 150px;display: table-cell;vertical-align: top;">Bill to Business Name: </b><?= isset($quote_info['name']) && !empty($quote_info['name']) ? $quote_info['name'] : ''; ?>
                            </div>
                            <div style="width: 100%;padding: 10px 0;display: table">
                                <b style="font-weight: normal;color: #333;width: 150px;display: table-cell;vertical-align: top;">Bill To: </b>
                                <span>
                                <?= $value ?>
                                    <input class="req form-control hidden" type="text" name="o_address[]" id="address"
                                           placeholder="Address" value="<?= $value ?>" required="required">
                                </span>
                            </div>
                            <div style="width: 100%;padding: 10px 0;display: table">
                                <b style="font-weight: normal;color: #333;width: 150px;display: table-cell;vertical-align: top;">City: </b><span><?= $quote_info['billing_city'][$i]; ?>

                                    <input class="req form-control hidden" type="text" name='o_city[]' id="city"
                                           placeholder="City" value="<?= $quote_info['shipping_city'][$i]; ?>"
                                           required="required">
                                </span>
                            </div>
                            <div style="width: 100%;padding: 10px 0;display: table">
                                <b style="font-weight: normal;color: #333;width: 150px;display: table-cell;vertical-align: top;">State: </b><span><?= $quote_info['billing_state'][$i]; ?>

                                    <input class="req form-control hidden" type="text" name='o_state[]' id="state"
                                           placeholder="State" value="<?= $quote_info['shipping_state'][$i]; ?>"
                                           required="required">
                                </span>
                            </div>
                            <div style="width: 100%;padding: 10px 0;display: table">
                                <b style="font-weight: normal;color: #333;width: 150px;display: table-cell;vertical-align: top;">PO Box / Zip Code:</b><span><?= $quote_info['billing_zip'][$i]; ?>

                                    <input class="req form-control hidden" type="text" name='o_zip[]' id="b_zip"
                                           placeholder="Zip Code" value="<?= $quote_info['shipping_zip'][$i]; ?>"
                                           required="required">
                                </span>
                            </div>
                            <div style="width: 100%;padding: 10px 0;display: table">
                                <b style="font-weight: normal;color: #333;width: 150px;display: table-cell;vertical-align: top;">Contact Info: </b><span><?= $quote_info['billing_contactinfo'][$i]; ?>

                                    <input class="req form-control hidden" type="text" name="o_phone[]" id="b_name"
                                           placeholder="Office Phone"
                                           value="<?= $quote_info['shipping_contactinfo'][$i]; ?>" required="required">
                                </span>
                            </div>
                        <?php } ?>
                    </div><!--end of col-content-->

                    <div class="col-content" style="width: 50%;float: left;color: #555;padding: 10px 0">
                        <?php foreach ($quote_info['shipping_address'] as $i => $value) { ?>
                            <div style="width: 100%;padding: 10px 0;display: table">
                                <b style="font-weight: normal;color: #333;width: 150px;display: table-cell;vertical-align: top;">Ship to Company Name: </b><?= isset($quote_info['ship_to_company']) && !empty($quote_info['ship_to_company']) ? $quote_info['ship_to_company'] : $quote_info['name']; ?>
                            </div>
                            <div style="width: 100%;padding: 10px 0;display: table">
                                <b style="font-weight: normal;color: #333;width: 150px;display: table-cell;vertical-align: top;">Attn:: </b><?= isset($quote_info['ship_to_name']) && !empty($quote_info['ship_to_name']) ? $quote_info['ship_to_name'] : $quote_info['name']; ?>
                            </div>
                            <div style="width: 100%;padding: 10px 0;display: table">
                                <b style="font-weight: normal;color: #333;width: 150px;display: table-cell;vertical-align: top;">Ship To: </b>
                                <span>
                                <?= $value ?>
                                    <input class="req form-control hidden" type="text" name="o_address[]" id="address"
                                           placeholder="Address" value="<?= $value ?>" required="required">
                                </span>
                            </div>
                            <div style="width: 100%;padding: 10px 0;display: table">
                                <b style="font-weight: normal;color: #333;width: 150px;display: table-cell;vertical-align: top;">City: </b><span><?= $quote_info['shipping_city'][$i]; ?>
                                    <input class="req form-control hidden" type="text" name='o_city[]' id="city"
                                           placeholder="City" value="<?= $quote_info['shipping_city'][$i]; ?>"
                                           required="required"></span>
                            </div>
                            <div style="width: 100%;padding: 10px 0;display: table">
                                <b style="font-weight: normal;color: #333;width: 150px;display: table-cell;vertical-align: top;">State: </b><span><?= $quote_info['shipping_state'][$i]; ?>

                                    <input class="req form-control hidden" type="text" name='o_state[]' id="state"
                                           placeholder="State" value="<?= $quote_info['shipping_state'][$i]; ?>"
                                           required="required">
                                </span>
                            </div>
                            <div style="width: 100%;padding: 10px 0;display: table">
                                <b style="font-weight: normal;color: #333;width: 150px;display: table-cell;vertical-align: top;">PO Box / Zip Code:</b><span><?= $quote_info['shipping_zip'][$i]; ?>

                                    <input class="req form-control hidden" type="text" name='o_zip[]' id="b_zip"
                                           placeholder="Zip Code" value="<?= $quote_info['shipping_zip'][$i]; ?>"
                                           required="required">
                                </span>
                            </div>
                            <div style="width: 100%;padding: 10px 0;display: table">
                                <b style="font-weight: normal;color: #333;width: 150px;display: table-cell;vertical-align: top;">Contact Info: </b><span><?= $quote_info['shipping_contactinfo'][$i]; ?>
                                    <input class="req form-control hidden" type="text" name="o_phone[]" id="b_name"
                                           placeholder="Office Phone"
                                           value="<?= $quote_info['shipping_contactinfo'][$i]; ?>" required="required">
                                </span>
                            </div>
                        <?php } ?>
                    </div><!--end of col-content-->
                </div><!--end of row-content-->
            </div>

            <?php if(isset($quote_info['special_instruction']) && !empty($quote_info['special_instruction'])){ ?>

            <div class="container-fluid">
                <div class="inner">
                    <p class="initial"></p>
                </div>
                <div class="quote-row-line"></div>
                <div class="form-row" style="margin-top: 20px">
                    <h3 class="dealer-form-sub-heading">Special Instructions</h3>
                </div>
            </div>
            <div><br></div>
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-11 form-group">
                        <?= isset($quote_info['special_instruction']) && !empty($quote_info['special_instruction']) ? $quote_info['special_instruction']:''; ?>
                    </div>
                </div>
            </div>
            <?php } ?>

            <div class="table" style="margin-top: 50px;float: left; /*width:100%;*/ ">
                <table cellspacing="0" cellpadding="3">
                    <thead>
                    <tr>
                        <th style="background: #767676;color: #fff;padding: 10px 5px;">Product Model</th>
                        <th style="background: #767676;color: #fff;padding: 10px 5px;">Product Description</th>
                        <th style="background: #767676;color: #fff;padding: 10px 5px;">Product Family</th>
                        <th style="background: #767676;color: #fff;padding: 10px 5px;">Product Type</th>
                        <th style="background: #767676;color: #fff;padding: 10px 5px;">List Price</th>
                        <th style="background: #767676;color: #fff;padding: 10px 5px;">Dealer Price</th>
                        <th style="background: #767676;color: #fff;padding: 10px 5px;">Quantity</th>
                        <!--                    <th>Sales Price(Unit)</th>-->
                        <th style="background: #767676;color: #fff;padding: 10px 5px;">Total Price</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php if (isset($quote_info)) {
                        foreach ($quote_info['model'] as $i => $value) {
                            ?>
                            <tr>
                                <td style="border-right: 1px solid #767676;color: #767676;padding: 10px 5px;">
                                    <?= $value ?>
                                    <input value="<?= $value ?>" class="form-control hidden" name="model[]">
                                </td>
                                <td style="width: 20px;border-right: 1px solid #767676;color: #767676;padding: 10px 5px;">
                                    <?= $quote_info['description'][$i]; ?>
                                    <input value="<?= $quote_info['description'][$i]; ?>" class="form-control hidden"
                                           name="description[]">
                                </td>
                                <td style="border-right: 1px solid #767676;color: #767676;padding: 10px 5px;">
                                    <?= $quote_info['family'][$i]; ?>
                                    <input value="<?= $quote_info['family'][$i]; ?>" class="form-control hidden"
                                           name="family[]">
                                </td>
                                <td style="border-right: 1px solid #767676;color: #767676;padding: 10px 5px;">
                                    <?= $quote_info['type'][$i]; ?>
                                    <input value="<?= $quote_info['type'][$i]; ?>" class="form-control hidden"
                                           name="type[]">
                                </td>
                                <td style="border-right: 1px solid #767676;color: #767676;padding: 10px 5px;">
                                    <?= $quote_info['list_price'][$i]; ?>
                                    <input value="<?= $quote_info['list_price'][$i]; ?>" class="form-control hidden"
                                           name="list_price[]">
                                </td>
                                <td style="border-right: 1px solid #767676;color: #767676;padding: 10px 5px;">
                                    <?= $quote_info['dealer_price'][$i]; ?>
                                    <input value="<?= $quote_info['dealer_price'][$i]; ?>" class="form-control hidden" name="dealer_price[]">
                                </td>
                                <td style="border-right: 1px solid #767676;color: #767676;padding: 10px 5px;">
                                    <?= number_format($quote_info['quantity'][$i]); ?>
                                    <input value="<?= $quote_info['quantity'][$i]; ?>" class="form-control hidden" name="quantity[]">
                                </td>
                                <td style="color: #767676;padding: 10px 5px;">
                                    <?= $quote_info['price'][$i]!=0?'$'.number_format($quote_info['price'][$i],2):'CFQ'; ?>
                                    <input value="<?= $quote_info['price'][$i]; ?>" class="form-control hidden" name="price[]">
                                </td>
                                <!--                    <td>$2,995.00</td>-->
                            </tr>
                        <?php }
                    } ?>
                    </tbody>
                </table>
            </div>

            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12 pull-right">
                        <b>
                            <div style="text-align:right;" class="col-md-10">Grand Total:</div>
                            <div class="col-md-2 grandTotal"><?= isset($quote_info['grandTotal']) && !empty($quote_info['grandTotal']) ? '$' . number_format($quote_info['grandTotal'],2): '' ?></div>
                            <div class="hidden totalGrandValue" data-value="<?= isset($quote_info['grandTotal']) && !empty($quote_info['grandTotal']) ?number_format($quote_info['grandTotal'],2): '' ?>"></div>
                        </b>
                        <input class="form-control totalGrandPrice hidden" name="grandTotal">
                    </div>
                </div>
            </div>
            <br>
            <div><br></div>
            <div><br></div>
            <div class="attachedFileDiv alert alert-success alert-dismissible text-center row" hidden></div>
            <div><br></div>
            <div class="fileDiv" <?= isset($quote_info['file'])&& !empty($quote_info['file'])?'show':'hidden'; ?> >
                <b>Attachment:</b> <a class="attachedFile" target="_blank" href="<?= isset($quote_info['file'])&& !empty($quote_info['file'])?base_url('uploads/'.$quote_info['file']):'';?>"><?= isset($quote_info['file'])&& !empty($quote_info['file'])?'Attached File':'';?></a>
            </div>
            <div><br></div>

            <div class="footer-pdf" style="text-align: center;margin-top: 50px;float: left; width: 100%; page-break-inside: avoid;">
                <b style="display: block">BK Technologies</b>
                <br>7100 Technology Drive <br> West Melbourne, FL 32904<br/>United States
                <a style="display: block" href="#">www.bktechnologies.com</a>
            </div><!--end of footer-pdf-->
            <div><br></div>
            <div><br></div>
            <div><b><em>Note: The submitted quote will expire in 90-days from the date of creation</em></b></div>
        </div><!--end of content-->


        <script>
            // function printContent(el) {
            //     var restorePage = document.body.innerHTML;
            //     var printableContent = document.getElementById(el).innerHTML;
            //     document.body.innerHTML = printableContent;
            //     window.print();
            //     document.body.innerHTML = restorePage;
            // }

            function print() {
                var element = document.getElementById('main-content');
                var opt = {
                    margin: 1,
                    filename: (Math.floor(Math.random() * 10000) + 1) + '.pdf',
                };

// New Promise-based usage:
                html2pdf().set(opt).from(element).save();
            }
            
            
            $('body').on('click','.submit-or-draft',function(e){
                e.preventDefault();
                console.log('hello');
                // alert('are you sure');
                // console.log($('.totalGrandValue').data('value'));
                var string = $('.totalGrandValue').data('value');
                var grandValue = parseInt(string.replace(",",""));
                // console.log(grandValue);
                if(grandValue > 5000){
                    $('#poAttachmentModal').modal('show');
                    // alert('Your order is over $5K and you will need to attach a hard copy PO');
                }else{
                    location.href = $(this).attr("href");
                }
                // location.href = $(this).attr("href");
                // console.log($(this).html());
                // console.log($(this).attr("href"));
            })

            //var csTotal = $(".grandTotal").html();
            //$(".print-price").html(csTotal);
            //console.log(csTotal);
        </script>

    </div><!--end of container-fluid-->
    <?php if (!(isset($viewonly))) { ?>
        <a href="<?= base_url('quote/redirect/1/'.$quote_info['id']); ?>" class="btn btn-primary pull-right submit-or-draft"
           style="margin: 10px" >Submit As Order</a>
    <?php } ?>
        <a  onclick="print();"class="btn btn-warning pull-right" style="margin: 10px">Download PDF</a>
<!--    <a onclick="printContent('content-pdf');"class="btn btn-warning pull-right" style="margin: 10px">Print</a>-->
    <?php if (!(isset($viewonly))) { ?>
    <a class="btn btn-success pull-right attachment" style="margin: 10px" data-toggle="modal" data-target="#addAttachmentModal">Add an Attachment</a>
    <?php } ?>
    <?php if (!(isset($viewonly))) { ?>
        <a href="<?php echo base_url(); ?>quote/redirect/2/<?= $quote_info['id'] ?>" class="btn btn-default pull-right"
           style="margin: 10px">Save as draft</a>
    <?php } ?>
</form>

<style>

    .table {
        background: none !important;
    }

    .table {
        padding: 0px 0px !important;
        margin-top: 15px !important;
        margin-bottom: 20px !important;
    }

    .content-pdf h2 {
        text-align: center;
        font-size: 42px !important;
        margin-top: 25px !important;
    }

    .header-pdf {
        margin-bottom: 35px !important;
    }

</style>

<!-- Product POPUP -->
<div class="modal fade" id="addAttachmentModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel">Add an Attachment</h4>
            </div>
            <form method="post" id="attachmentForm" enctype="multipart/form-data">
            <div class="modal-body">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-12 form-group row-group">
                            <div class="errorToModal alert alert-danger alert-dismissible text-center row" hidden></div>
                            <div class="form-field attachmentDiv">
                                <div><b>Note: .pdf, .doc, .docx, .xls, and .xlsx are allowed to upload.</b></div>
                                <div>
                                    <input type="hidden" value="<?= $quote_info['id']; ?>" name="quote_id">
                                    <input type="file" name="file" style="margin: 10px">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="modal-footer">
                <button type="submit" class="upload btn btn-default" name="upload_button" value="Upload">Upload</button>
<!--                <input type="submit" data-dismiss="modal" class="btn btn-default saveAttachment">Add</input>-->
                <!--                <a  data-dismiss="modal" class="btn btn-primary generateQuote" onclick="return profileValidation();">Generate Quote</a>-->
                <button data-dismiss="modal" class="btn btn-primary">Close</button>
            </div>
            </form>
        </div>
    </div>
</div>
<!-- END HELP POPUP -->

<!--po order pdf submit-->
<div class="modal fade" id="poAttachmentModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel34"
     aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel34">Add PO Attachment</h4>
            </div>
            <form method="post" id="poattachmentForm" enctype="multipart/form-data">
                <div class="modal-body">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-md-12 form-group row-group">
                                <div class="errorToModal alert alert-danger alert-dismissible text-center row" hidden></div>
                                <div class="form-field attachmentDiv">
                                    <div><h4 style="color:blue;">Your order is over $5K and you will need to attach a hard copy PO</h4></div>
                                    <br>
                                    <div class="po-upload-error alert alert-danger alert-dismissible text-center row hidden" >Please Upload The File First</div>
                                    <div><b>Note: <span style="color: red;">.pdf, .xlsx, .doc, .docx, .opt</span> format are allowed to upload.</b></div>
                                    <div>
                                        <input type="hidden" value="<?= $quote_info['id']; ?>" name="quote_id">
                                        <input type="hidden" value="po" name="po">
                                        <input type="file" class="po-upload-btn" name="file" style="margin: 10px">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="modal-footer">
                    <button type="submit" class=" btn btn-default" value="Upload">Upload</button>
                    <button data-dismiss="modal" class="btn btn-primary">Close</button>
                </div>
            </form>
        </div>
    </div>
</div>


<!-- Modal -->