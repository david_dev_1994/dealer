<!--Page main section start-->
<section id="min-wrapper">
    <div id="main-content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <!--Top header start-->
                    <h3 class="ls-top-header">Testimonial</h3>
                    <!--Top header end -->

                    <!--Top breadcrumb start -->
                    <ol class="breadcrumb">
                        <li><a href="#"><i class="fa fa-home"></i></a></li>
                        <li class="active">Testimonial</li>
                    </ol>
                    <!--Top breadcrumb start -->
                </div>
            </div>
            <!-- Main Content Element  Start-->

            <div class="row">
                <div class="col-md-5">
                    <div class="input-group ls-group-input">
                        <span class="input-group-btn">
                            <button type="button" class="btn btn-success">Search</button>
                        </span>
                        <input class="form-control" type="text">
                    </div>
                </div>
                <div class="col-md-7">
                    <!-- Button trigger modal -->
                    
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title">My Order</h3>
                        </div>

                        <div class="panel-body">
                            <!--Table Wrapper Start-->
                            <div class="table-responsive ls-table">
                                <div>
                                    <?php
                                    $msg = $this->session->userdata('message');
                                    $error = $this->session->userdata('error');

                                    if ($msg) {
                                        echo "<div class='alert alert-success text-center'><i class='fa fa-check-circle-o fa-5x'></i>" . $msg . "</div>";
                                        $this->session->unset_userdata('message');
                                    } else if ($error) {
                                        echo "<div class='alert alert-danger text-center'><i class='fa fa-ban fa-5x'></i>" . $error . "</div>";
                                        $this->session->unset_userdata('error');
                                    }
                                    ?>
                                </div>
                                <table class="table table-btestimonialed table-striped">
                                    <thead>
                                        <tr>
                                            <th>SL#</th>
                                            <th>Client Name</th>
                                            <th>Email Address</th>
                                            <th>Testimonial</th>
                                            <th>Date Added</th>
                                            <th>Status</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>

                                        <?php
                                        $i = 1;
                                        foreach ($testimonial_info as $v_testimonial_info) {
                                            ?>
                                            <tr>
                                                <td><?php echo $i++; ?></td>
                                                <td><?php echo $v_testimonial_info['full_name']; ?></td>
                                                <td><?php echo $v_testimonial_info['email_address']; ?></td>
                                                <td><?php echo $v_testimonial_info['testimonial']; ?></td>
                                                <td><?php echo date("D d F Y h:ia", strtotime($v_testimonial_info['date_added'])); ?></td>
                                                <td>
                                                    <?php if ($v_testimonial_info['status'] == 1) { ?>
                                                        <span class="label-success label label-default">Published</span>
                                                    <?php } else { ?>
                                                        <span class="label-warning label label-default">Unpublished</span>
                                                    <?php } ?>
                                                </td>
                                                <td class="text-center">
                                                    <?php if ($v_testimonial_info['status'] == 1) { ?>
                                                        <a href="<?php echo admin_url(); ?>/testimonial/unpublished_testimonial/<?php echo $v_testimonial_info['testimonial_id']; ?>" class="btn btn-xs btn-warning"><i class="fa fa-arrow-down"></i></a>
                                                    <?php } else { ?>
                                                        <a href="<?php echo admin_url(); ?>/testimonial/published_testimonial/<?php echo $v_testimonial_info['testimonial_id']; ?>" class="btn btn-xs btn-success"><i class="fa fa-arrow-up"></i></a>
                                                    <?php } ?>
                                                    <a href="<?php echo admin_url(); ?>/testimonial/delete_testimonial/<?php echo $v_testimonial_info['testimonial_id']; ?>" class="btn btn-xs btn-danger" onclick="return confirm('Are you sure to delete this ?');"><i class="fa fa-trash-o"></i></a>
                                                </td>
                                            </tr>
                                        <?php } ?>

                                    </tbody>
                                </table>
                            </div>
                            <!--Table Wrapper Finish-->
                            <!--                            <div class="ls-button-group demo-btn ls-table-pagination">
                                                            <ul class="pagination ls-pagination" style="float: right;">
                                                                <li><a href="#"><i class="fa fa-arrow-left"></i> </a></li>
                                                                <li><a href="#">1</a></li>
                                                                <li><a href="#">2</a></li>
                                                                <li><a href="#">3</a></li>
                                                                <li><a href="#">4</a></li>
                                                                <li><a href="#">5</a></li>
                                                                <li><a href="#"><i class="fa fa-arrow-right"></i></a></li>
                                                            </ul>
                                                        </div>-->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!--Page main section end -->