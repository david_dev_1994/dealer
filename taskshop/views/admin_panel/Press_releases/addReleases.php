<section id="min-wrapper">
    <div id="main-content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <h3 class="ls-top-header"><a href="<?= base_url('admin/dashboard');?>">Product Announcement</a></h3>
                </div>
            </div>
            <!-- Main Content Element  Start-->

            <?php
            if(validation_errors()){
                ?>
                <br>
                <div class="alert alert-danger alert-dismissable">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>
                    <?php echo validation_errors(); ?>
                </div>
                <?php
            }

            if(isset($error)){
                ?>
                <br>
                <div class="alert alert-danger alert-dismissable">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>
                    <?php echo $error; ?>
                </div>
                <?php
            }

            if(isset($success)){
                ?>
                <br>
                <div class="alert alert-success alert-dismissable">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>
                    <?php echo $success; ?>
                </div>
                <?php
            }
            ?>

            <div class="row">
                <div class="col-md-9">
                    <div class="as-form">
                        <form method="post" action="" enctype="multipart/form-data">

                            <div class="row">
                                <div class="col-md-10">
                                    <div class="form-field">
                                        <div><b>Title*</b></div>
                                        <div class="form-input">
                                            <textarea name="title" id="title" cols="16" rows="1"><?= isset($data['title'])?$data['title']:$this->input->post('title'); ?></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-10">
                                    <div class="form-field">
                                        <div><b>Content (Optional)</b></div>
                                        <div class="form-input">
                                            <textarea name="content" id="content" cols="16" rows="1"><?= isset($data['content'])?$data['content']:$this->input->post('content'); ?></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-field">
                                        <div><b>File (Optional)</b> Note: File should be docs or pdf only.</div>
                                        <div><?= (isset($data['docFile'])&&!empty($data['docFile']))?$data['docFile']:'<h5><b>No File Found!</b></h5>'; ?></div>
                                        <div>
                                            <input type="file" name="file" style="margin: 10px"> </span>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-10">
                                    <div style="border-bottom: 1px solid #EFEFEF; margin-bottom: 15px;"></div>
                                </div>
                            </div>
                            <div class="row" style="margin-bottom: 15px">
                                <div class="col-md-10">
                                    <a href="<?= base_url('admin/dashboard'); ?>" class="btn btn-default"> Cancel </a>
                                        <button type="submit" class="btn btn-primary">Save</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <!-- Main Content Element  End-->
        </div>
    </div>

</section>
