<style type="text/css">
    .errorMesage{
        color: red;
    }
</style>
<!--Page main section start-->
<section id="min-wrapper">
    <div id="main-content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <!--Top header start-->
                    <h3 class="ls-top-header">Edit Property Type</h3>
                    <!--Top header end -->

                    <!--Top breadcrumb start -->
                    <ol class="breadcrumb">
                        <li><a href="#"><i class="fa fa-home"></i></a></li>
                        <li class="active">Edit Property Type</li>
                    </ol>
                    <!--Top breadcrumb start -->
                </div>
            </div>
            <!-- Main Content Element  Start-->

            <div>
                <?php
                $msg = $this->session->userdata('message');
                $error = $this->session->userdata('error');

                if ($msg) {
                    echo "<div class='alert alert-success'>" . $msg . "</div>";
                    $this->session->unset_userdata('message');
                } else if ($error) {
                    echo "<div class='alert alert-danger'>" . $error . "</div>";
                    $this->session->unset_userdata('error');
                }
                ?>
            </div>
            
            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title">Edit Property Type</h3>
                        </div>
                        <div class="panel-body">
                            <form name="edit_property_location_form" id="defaultForm" method="post" class="form-horizontal ls_form" action="<?php echo admin_url(); ?>/real_estate/property_location/update_property_location/<?php echo $property_location_info['property_location_id']; ?>">

                                <div class="form-group">
                                    <label class="col-lg-3 control-label">Property Location</label>
                                    <div class="col-lg-6">
                                        <input type="text" class="form-control" name="data[property_location]" placeholder="Add Property Location" value="<?php echo $property_location_info['property_location']; ?>" />
                                        <span class="errorMesage"><?php echo form_error('data[property_location]'); ?></span>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-lg-3 control-label">Details</label>
                                    <div class="col-lg-6">
                                        <textarea name="data[property_location_details]" class="summernote" placeholder="Write message"> <?php echo $property_location_info['property_location_details']; ?></textarea>
                                        <span class="errorMesage"><?php echo form_error('data[property_location_details]'); ?></span>
                                    </div>

                                </div>

                                <div class="form-group">
                                    <label for="publication_status" class="col-lg-3 control-label">Publication Status</label>
                                    <div class="col-lg-3">
                                        <select class="form-control" name="data[publication_status]" id="publication_status">
                                            <option selected="selected" disabled="">Select One</option>
                                            <option value="1">Published</option>
                                            <option value="0">Unpublished</option>
                                        </select>
                                        <span class="errorMesage"><?php echo form_error('data[publication_status]'); ?></span>
                                    </div>
                                </div>

                                <hr>

                                <div class="form-group">
                                    <label class="col-lg-3 control-label">Meta Name</label>
                                    <div class="col-lg-6">
                                        <input type="text" class="form-control" name="data[meta_name]" placeholder="Meta Name"  value="<?php echo $property_location_info['meta_name']; ?>"/>
                                        <span class="errorMesage"><?php echo form_error('data[meta_name]'); ?></span>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-lg-3 control-label">Meta Title</label>
                                    <div class="col-lg-6">
                                        <input type="text" class="form-control" name="data[meta_title]" placeholder="Meta Title"  value="<?php echo $property_location_info['meta_title']; ?>"/>
                                        <span class="errorMesage"><?php echo form_error('data[meta_title]'); ?></span>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-lg-3 control-label">Meta Keyword</label>
                                    <div class="col-lg-6">
                                        <input type="text" class="form-control" name="data[meta_keyword]" placeholder="Meta Keyword" value="<?php echo $property_location_info['meta_keyword']; ?>" />
                                        <span class="errorMesage"><?php echo form_error('data[meta_keyword]'); ?></span>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-lg-3 control-label">Meta description</label>
                                    <div class="col-lg-6">
                                        <textarea name="data[meta_description]" class="summernote" placeholder="Meta description"><?php echo $property_location_info['meta_description']; ?></textarea>
                                        <span class="errorMesage"><?php echo form_error('data[meta_description]'); ?></span>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-lg-9 col-lg-offset-3">
                                        <button type="submit" class="btn btn-primary">Update Property Location</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<script type="text/javascript">
    document.forms['edit_property_location_form'].elements['publication_status'].value = '<?php echo $property_location_info['publication_status']; ?>';
</script>