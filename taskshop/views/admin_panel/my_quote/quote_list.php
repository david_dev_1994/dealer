<div id="min-wrapper">
    <div id="main-content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <!--Top header start-->
                    <h3 class="ls-top-header">Submitted Quotes</h3>
                    <!--Top header end -->
<!--                    <ol class="breadcrumb">-->
<!--                        <li><a href="--><?//= base_url('admin/dashboard'); ?><!--"><i class="fa fa-home"></i></a></li>-->
<!--                        <li class="active">Quotes</li>-->
<!--                    </ol>-->
                </div>
            </div>

            <div>
                <?php
                $msg = $this->session->userdata('message');
                $error = $this->session->userdata('error');

                if ($msg) {
                    echo "<div class='alert alert-success'>" . $msg . "</div>";
                    $this->session->unset_userdata('message');
                } else if ($error) {
                    echo "<div class='alert alert-danger'>" . $error . "</div>";
                    $this->session->unset_userdata('error');
                }
                ?>
            </div>

            <!-- Main Content Element  Start-->
            <div class="row">
                <div class="col-md-12">
                    <div class="portlet light no-border-top as-box-border">
                        <div class="portlet-body">
                            <table class="table table-striped table-bordered table-hover dt-responsive" id="user_table" data-name="services">
                                <thead>
                                <tr>
                                    <th> Quotes</th>
                                    <th class="hide"> id</th>
                                    <th> Name</th>
<!--                                    <th> Email</th>-->
                                    <th> Company</th>
                                    <th> Created Date</th>
                                    <th> Date Submitted</th>
                                    <th> Expiration Date</th>
                                    <th> Sales Manager</th>
                                    <th> PO Attachment</th>
                                    <th> Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php if (isset($quotes_list)) {
                                    if ($this->session->userdata('access_label') == 1) {
                                        $redirect = 'admin/registered_dealers';
                                    }
                                    foreach ($quotes_list as $index => $singlequote) {
                                        ?>
                                        <tr>
                                            <td>
                                                <a> Quote- <?= $singlequote['id']; ?></a>
                                            </td>
                                            <td class="hide"> <?= $singlequote['id']; ?> </td>
                                            <td> <?= $singlequote['name']; ?> </td>
<!--                                            <td> --><?//= $singlequote['email']; ?><!--  </td>-->
                                            <td> <?= $singlequote['company']; ?> </td>
                                            <td> <?= date('m-d-y', $singlequote['created_date']); ?> </td>
                                            <td> <?= (!empty($singlequote['submitted_date']) ? date('m-d-y', $singlequote['submitted_date']) : 'N/A'); ?></td>
                                            <td> <?= date('m-d-y', $singlequote['expiry_date']); ?> </td>
                                            <td> <?= $singlequote['sales_person']; ?> </td>
                                            <?php if(!empty($singlequote['po_file'])){ ?>
                                                <td> <a href="<?php echo base_url(); ?>uploads/<?= $singlequote['po_file']; ?>" target="_blank"> PO ORDER</a></td>
                                            <?php }else{ ?>
                                                <td> N/A </td>
                                            <?php } ?>
                                            <td>
                                                <div class="btn-group">
                                                    <?php if ($this->session->userdata('access_label') == 1) { ?>
                                                        <a href="<?php echo base_url(); ?>admin/delete_quote/<?= $singlequote['id']; ?>"
                                                           class="btn btn-danger">Delete</a>
                                                    <?php } ?>
                                                    <a href="<?php echo base_url(); ?>admin/quote/show/<?= $singlequote['id']; ?>"
                                                       class="btn btn-info">View</a>
                                            </td>
                                        </tr>
                                        <?php
                                    }
                                }
                                ?>
                                </tbody>
                                <?php if($this->session->userdata('access_label') == 1){?>
                                <tfoot>
                                <tr>
                                    <th></th>
                                    <th class="hide"> id</th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                </tr>
                                </tfoot>
                                <?php } ?>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Main Content Element  End-->
        </div>
    </div>
</div>
</section>



<style>

    .btn {
        display: inline-block !important;
        padding: 6px 12px !important;
        margin-bottom: 0 !important;
        font-size: 14px !important;
        font-weight: 400 !important;
        line-height: 1.42857143 !important;
        text-align: center !important;
        white-space: nowrap !important;
        vertical-align: middle !important;
        cursor: pointer !important;
        -webkit-user-select: none !important;
        -moz-user-select: none !important;
        -ms-user-select: none !important;
        user-select: none !important;
        background-image: none !important;
        border: 1px solid transparent !important;
        border-radius: 0px !important;
        text-transform: inherit !important;
    }

</style>