<!--Page main section start-->
<section id="min-wrapper">
    <div id="main-content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <!--Top header start-->
                    <h3 class="ls-top-header">Submitted Quote List</h3>
                    <!--Top header end -->
                    <!--Top breadcrumb start -->
                    <ol class="breadcrumb">
                        <li><a href="<?= base_url('admin/dashboard');?>"><i class="fa fa-home"></i></a></li>
                        <li class="active">Quotes</li>
                    </ol>
                    <!--Top breadcrumb start -->
                </div>
            </div>

            <!-- Main Content Element  Start-->
            <div>
                <?php
                $msg = $this->session->userdata('message');
                $error = $this->session->userdata('error');

                if ($msg) {
                    echo "<div class='alert alert-success'>" . $msg . "</div>";
                    $this->session->unset_userdata('message');
                } else if ($error) {
                    echo "<div class='alert alert-danger'>" . $error . "</div>";
                    $this->session->unset_userdata('error');
                }
                ?>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title">Submitted Quotes</h3>
                        </div>
                        <div class="panel-body">
                            <!--Table Wrapper Start-->
                            <div class="table-responsive ls-table">
                                <table class="table">
                                    <thead>
                                    <tr>
                                        <th> Quotes</th>
                                        <th class="hide"> id</th>
                                        <th> Name</th>
                                        <th> Email</th>
                                        <th> Company</th>
                                        <th> Sales Person</th>
                                        <th> Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php if(isset($quotes_list)) {
                                        if($this->session->userdata('access_label') == 1){
                                            $redirect = 'admin/registered_dealers';
                                        }
                                        foreach ($quotes_list as $index => $singlequote) {
                                            ?>
                                            <tr>
                                                <td>
                                                    <a > Quote- <?= $singlequote['id']; ?></a>
                                                </td>
                                                <td class="hide"> <?= $singlequote['id']; ?> </td>
                                                <td> <?= $singlequote['name']; ?> </td>
                                                <td> <?= $singlequote['email']; ?>  </td>
                                                <td> <?= $singlequote['company']; ?> </td>
                                                <td> <?= $singlequote['sales_person']; ?> </td>
                                                <td> <div class="btn-group">
                                                        <?php if($this->session->userdata('access_label') == 1){ ?>
                                                            <a  href="<?php echo base_url();?>admin/delete_quote/<?= $singlequote['id']; ?>" class="btn btn-danger">Delete</a>
                                                        <?php } ?>
                                                        <a  href="<?php echo base_url();?>admin/quote/show/<?= $singlequote['id']; ?>" class="btn btn-info">View</a>
                                                </td>
                                            </tr>
                                            <?php
                                        }
                                    }
                                    ?>
                                    </tbody>
                                </table>
                            </div>
                            <!--Table Wrapper Finish-->
                        </div>
                    </div>
                </div>
            </div>


            <!-- Main Content Element  End-->

        </div>
    </div>



</section>
<!--Page main section end -->

<style>

.btn {
    display: inline-block !important;
    padding: 6px 12px !important;
    margin-bottom: 0 !important;
    font-size: 14px !important;
    font-weight: 400 !important;
    line-height: 1.42857143 !important;
    text-align: center !important;
    white-space: nowrap !important;
    vertical-align: middle !important;
    cursor: pointer !important;
    -webkit-user-select: none !important;
    -moz-user-select: none !important;
    -ms-user-select: none !important;
    user-select: none !important;
    background-image: none !important;
    border: 1px solid transparent !important;
    border-radius: 0px !important;
    text-transform: inherit !important;
}

</style>