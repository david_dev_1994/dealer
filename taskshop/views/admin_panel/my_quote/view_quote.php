<?php
$quote_info['shipping_address'] = json_decode($quote_info['shipping_address']);
$quote_info['shipping_city'] = json_decode($quote_info['shipping_city']);
$quote_info['shipping_contactinfo'] = json_decode($quote_info['shipping_contactinfo']);
$quote_info['shipping_state'] = json_decode($quote_info['shipping_state']);
$quote_info['shipping_zip'] = json_decode($quote_info['shipping_zip']);

$quote_info['billing_address'] = json_decode($quote_info['billing_address']);
$quote_info['billing_city'] = json_decode($quote_info['billing_city']);
$quote_info['billing_contactinfo'] = json_decode($quote_info['billing_contactinfo']);
$quote_info['billing_state'] = json_decode($quote_info['billing_state']);
$quote_info['billing_zip'] = json_decode($quote_info['billing_zip']);

$quote_info['model'] = json_decode($quote_info['model']);
$quote_info['description'] = json_decode($quote_info['description']);
$quote_info['family'] = json_decode($quote_info['family']);
$quote_info['type'] = json_decode($quote_info['type']);
$quote_info['quantity'] = json_decode($quote_info['quantity']);
$quote_info['price'] = json_decode($quote_info['price']);
$quote_info['list_price'] = json_decode($quote_info['list_price']);
$quote_info['dealer_price'] = json_decode($quote_info['dealer_price']);
// echo '<pre>';print_r($quote_info);exit;
?>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>

<section id="min-wrapper">
    <div id="main-content">
        <div class="container-fluid">
            <div>
                <h2 style="padding:20px 0px 20px 0px;letter-spacing: 1px;font-family: Lato;font-size: 36px;font-weight: 600; word-spacing: 5px;color: #00a6d5;border-bottom: 1px solid #000000;margin-bottom: 25px !important;">Quote Details</h2>
            </div>
        </div>

        <div class="container-fluid">
            <div class="row">
                <form class="needs-validation" enctype="multipart/form-data" method="post">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="form-row">
                                <div class="col-md-2 mb-3" style="width: 16.66666667%;float:left">
                                    <label for="validationCustom02" style="color: #000000 !IMPORTANT;font-weight: 700 !important;font-size: 16px !important;font-family: Lato !important;">Company Name</label>
                                    <div><?= isset($quote_info['company']) && !empty($quote_info['company']) ? $quote_info['company'] : ''; ?></div>
                                    <input value="<?= isset($quote_info['company']) && !empty($quote_info['company']) ? $quote_info['company'] : ''; ?>" class="form-control hidden" name="company">
                                </div>
                                <div class="col-md-2 mb-3" style="width: 16.66666667%;float:left">
                                    <label for="validationCustom01" style="color: #000000 !IMPORTANT;font-weight: 700 !important;font-size: 16px !important;font-family: Lato !important;">Name</label>
                                    <div><?= isset($quote_info['name']) && !empty($quote_info['name']) ? $quote_info['name'] : ''; ?></div>
                                    <input value="<?= isset($quote_info['name']) && !empty($quote_info['name']) ? $quote_info['name'] : ''; ?>" class="form-control hidden" name="name">
                                </div>
                                <div class="col-md-2 mb-3" style="width: 16.66666667%;float:left">
                                    <label for="validationCustom02" style="color: #000000 !IMPORTANT;font-weight: 700 !important;font-size: 16px !important;font-family: Lato !important;">Email</label>
                                    <div style="word-break: break-all;"><?= isset($quote_info['email']) && !empty($quote_info['email']) ? $quote_info['email'] : ''; ?></div>
                                    <input value="<?= isset($quote_info['email']) && !empty($quote_info['email']) ? $quote_info['email'] : ''; ?>" class="form-control hidden" name="email">
                                </div>
                                <div class="form-row">
                                    <div class="col-md-2 mb-3" style="width: 16.66666667%;float:left">
                                        <label for="validationCustom01" style="color: #000000 !IMPORTANT;font-weight: 700 !important;font-size: 16px !important;font-family: Lato !important;">Phone</label>
                                        <div><?= isset($quote_info['phone']) && !empty($quote_info['phone']) ? $quote_info['phone'] : ''; ?></div>
                                        <input value="<?= isset($quote_info['phone']) && !empty($quote_info['phone']) ? $quote_info['phone'] : ''; ?>" class="form-control hidden" name="pnum">
                                    </div>
                                </div>
                                <div class="col-md-2 mb-3" style="width: 16.66666667%;float:left">
                                    <label for="validationCustom02" style="color: #000000 !IMPORTANT;font-weight: 700 !important;font-size: 16px !important;font-family: Lato !important;">Sales Manager</label>
                                    <div><?= isset($quote_info['sales_person']) && !empty($quote_info['sales_person']) ? $quote_info['sales_person'] : ''; ?></div>
                                    <input value="<?= isset($quote_info['sales_person']) && !empty($quote_info['sales_person']) ? $quote_info['sales_person'] : ''; ?>" class="form-control hidden" name="sales_person">
                                </div>
                                <div class="col-md-2 mb-3" style="width: 16.66666667%;float:left">
                                    <label for="validationCustom02" style="color: #000000 !IMPORTANT;font-weight: 700 !important;font-size: 16px !important;font-family: Lato !important;">Sales Manager Email</label>
                                    <div style="word-break: break-all;"><?= isset($quote_info['sales_manager_email']) && !empty($quote_info['sales_manager_email']) ? $quote_info['sales_manager_email'] : ''; ?></div>
                                    <input value="<?= isset($quote_info['sales_manager_email']) && !empty($quote_info['sales_manager_email']) ? $quote_info['sales_manager_email'] : ''; ?>" class="form-control hidden" name="sales_person">
                                </div>

                            </div>
                        </div>
                    </div>
                    <br>
                    <div class="container-fluid">
                        <div class="row">
                            <div class="form-row">
                                <div class="col-md-2 mb-3" style="width: 16.66666667%;float:left">
                                    <label for="validationCustom02" style="color: #000000 !IMPORTANT;font-weight: 700 !important;font-size: 16px !important;font-family: Lato !important;">Dealer Purchase Order Number</label>
                                    <div><?= isset($quote_info['orderNumber']) && !empty($quote_info['orderNumber']) ? $quote_info['orderNumber'] : ''; ?></div>
                                    <input value="<?= isset($quote_info['orderNumber']) && !empty($quote_info['orderNumber']) ? $quote_info['orderNumber'] : ''; ?>" class="form-control hidden" name="orderNumber">
                                </div>
                            </div>
                            <div class="col-md-2 mb-3" style="width: 16.66666667%;float:left">
                                <label for="validationCustom01" style="color: #000000 !IMPORTANT;font-weight: 700 !important;font-size: 16px !important;font-family: Lato !important;">Quote Number</label>
                                <div><?= isset($quote_info['id']) && !empty($quote_info['id']) ? $quote_info['id'] : ''; ?></div>
                                <input value="<?= isset($quote_info['id']) && !empty($quote_info['id']) ? $quote_info['id'] : ''; ?>" class="form-control hidden" name="name">
                            </div>
                            <div class="col-md-2 mb-3" style="width: 16.66666667%;float:left">
                                <label for="validationCustom02" style="color: #000000 !IMPORTANT;font-weight: 700 !important;font-size: 16px !important;font-family: Lato !important;">Created Date</label>
                                <div><?= isset($quote_info['created_date']) && !empty($quote_info['created_date']) ? date('m-d-y', $quote_info['created_date']) : ''; ?></div>
                                <input value="<?= isset($quote_info['created_date']) && !empty($quote_info['created_date']) ? date('m-d-y', $quote_info['created_date']) : ''; ?>" class="form-control hidden" name="email">
                            </div>
                            <div class="col-md-2 mb-3" style="width: 16.66666667%;float:left">
                                <label for="validationCustom02" style="color: #000000 !IMPORTANT;font-weight: 700 !important;font-size: 16px !important;font-family: Lato !important;">Expiration Date</label>
                                <div><?= isset($quote_info['expiry_date']) && !empty($quote_info['expiry_date']) ? date('m-d-y', $quote_info['expiry_date']) : ''; ?></div>
                                <input value="<?= isset($quote_info['expiry_date']) && !empty($quote_info['expiry_date']) ? date('m-d-y', $quote_info['expiry_date']) : ''; ?>" class="form-control hidden" name="email">
                            </div>
                        </div>
                    </div>

                    <div class="container-fluid">
                        <div class="form-row" style="margin-top: 20px">
                            <h3 style="padding:20px 0px 20px 0px;color: #000000 !important;
    font-size: 22px !important;
    text-transform: capitalize !important;
    text-align: left !important;
    margin-left: 0px !important;
    font-family: Lato !important;
    letter-spacing: 0px !important;
    font-weight: 600 !important;
    padding: 20px 0px 10px 0px !important;
    border-bottom: 1px solid #000000 !important;">Billing Address</h3>
                        </div>
                        <?php foreach ($quote_info['billing_address'] as $i => $value) { ?>
                            <div class="col-md-12 form-group row group">
                                <div class="col-md-2 form-group" style="width: 16.66666667%;float:left">
                                    <label for="validationCustom02" style="color: #000000 !IMPORTANT;font-weight: 700 !important;font-size: 16px !important;font-family: Lato !important;">Address</label>
                                    <div><?= $value ?></div>
                                    <input class="req form-control hidden" type="text" name="o_address[]" id="address" placeholder="Address" value="<?= $value ?>" required="required">
                                </div>
                                <div class="col-md-2 form-group" style="width: 16.66666667%;float:left">
                                    <label for="validationCustom02" style="color: #000000 !IMPORTANT;font-weight: 700 !important;font-size: 16px !important;font-family: Lato !important;">City</label>
                                    <div><?= $quote_info['billing_city'][$i]; ?></div>
                                    <input class="req form-control hidden" type="text" name='o_city[]' id="city" placeholder="City" value="<?= $quote_info['shipping_city'][$i]; ?>" required="required">
                                </div>
                                <div class="col-md-2 form-group" style="width: 16.66666667%;float:left">
                                    <label for="validationCustom02" style="color: #000000 !IMPORTANT;font-weight: 700 !important;font-size: 16px !important;font-family: Lato !important;">State</label>
                                    <div><?= $quote_info['billing_state'][$i]; ?></div>
                                    <input class="req form-control hidden" type="text" name='o_state[]' id="state" placeholder="State" value="<?= $quote_info['shipping_state'][$i]; ?>" required="required">
                                </div>
                                <div class="col-md-2 form-group" style="width: 16.66666667%;float:left">
                                    <label for="validationCustom02" style="color: #000000 !IMPORTANT;font-weight: 700 !important;font-size: 16px !important;font-family: Lato !important;">PO Box / Zip Code</label>
                                    <div><?= $quote_info['billing_zip'][$i]; ?></div>
                                    <input class="req form-control hidden" type="text" name='o_zip[]' id="b_zip" placeholder="PO / Zip Code" value="<?= $quote_info['shipping_zip'][$i]; ?>" required="required">
                                </div>
                                <div class="col-md-2 form-group" style="width: 16.66666667%;float:left">
                                    <label for="validationCustom02" style="color: #000000 !IMPORTANT;font-weight: 700 !important;font-size: 16px !important;font-family: Lato !important;">Contact Number</label>
                                    <div><?= $quote_info['billing_contactinfo'][$i]; ?></div>
                                    <input class="req form-control hidden" type="text" name="o_phone[]" id="b_name" placeholder="Office Phone" value="<?= $quote_info['shipping_contactinfo'][$i]; ?>" required="required">
                                </div>
                            </div>
                        <?php } ?>
                        <div class="form-row" style="margin-top: 20px">
                            <h3 style="padding:20px 0px 20px 0px;color: #000000 !important;
    font-size: 22px !important;
    text-transform: capitalize !important;
    text-align: left !important;
    margin-left: 0px !important;
    font-family: Lato !important;
    letter-spacing: 0px !important;
    font-weight: 600 !important;
    padding: 20px 0px 10px 0px !important;
    border-bottom: 1px solid #000000 !important;">Shipping Address</h3>
                        </div>
                        <?php foreach ($quote_info['shipping_address'] as $i => $value) { ?>
                            <div class="col-md-12 form-group row group">
                                <div class="col-md-2 form-group" style="width: 16.66666667%;float:left">
                                    <label for="validationCustom02" style="color: #000000 !IMPORTANT;font-weight: 700 !important;font-size: 16px !important;font-family: Lato !important;">Address</label>
                                    <div><?= $value ?></div>
                                    <input class="req form-control hidden" type="text" name="o_address[]" id="address" placeholder="Address" value="<?= $value ?>" required="required">
                                </div>
                                <div class="col-md-2 form-group" style="width: 16.66666667%;float:left">
                                    <label for="validationCustom02" style="color: #000000 !IMPORTANT;font-weight: 700 !important;font-size: 16px !important;font-family: Lato !important;">City</label>
                                    <div><?= $quote_info['shipping_city'][$i]; ?></div>
                                    <input class="req form-control hidden" type="text" name='o_city[]' id="city" placeholder="City" value="<?= $quote_info['shipping_city'][$i]; ?>" required="required">
                                </div>
                                <div class="col-md-2 form-group" style="width: 16.66666667%;float:left">
                                    <label for="validationCustom02" style="color: #000000 !IMPORTANT;font-weight: 700 !important;font-size: 16px !important;font-family: Lato !important;">State</label>
                                    <div><?= $quote_info['shipping_state'][$i]; ?></div>
                                    <input class="req form-control hidden" type="text" name='o_state[]' id="state" placeholder="State" value="<?= $quote_info['shipping_state'][$i]; ?>" required="required">
                                </div>
                                <div class="col-md-2 form-group" style="width: 16.66666667%;float:left">
                                    <label for="validationCustom02" style="color: #000000 !IMPORTANT;font-weight: 700 !important;font-size: 16px !important;font-family: Lato !important;">PO Box / Zip Code</label>
                                    <div><?= $quote_info['shipping_zip'][$i]; ?></div>
                                    <input class="req form-control hidden" type="text" name='o_zip[]' id="b_zip" placeholder="PO / Zip Code" value="<?= $quote_info['shipping_zip'][$i]; ?>" required="required">
                                </div>
                                <div class="col-md-2 form-group" style="width: 16.66666667%;float:left">
                                    <label for="validationCustom02" style="color: #000000 !IMPORTANT;font-weight: 700 !important;font-size: 16px !important;font-family: Lato !important;">Contact Number</label>
                                    <div><?= $quote_info['shipping_contactinfo'][$i]; ?></div>
                                    <input class="req form-control hidden" type="text" name="o_phone[]" id="b_name" placeholder="Office Phone" value="<?= $quote_info['shipping_contactinfo'][$i]; ?>" required="required">
                                </div>
                            </div>
                        <?php } ?>
                    </div>
                    <div class="container-fluid">
                        <div class="form-row" style="margin-top: 20px">
                            <h3 style="padding:20px 0px 20px 0px;color: #000000 !important;
    font-size: 22px !important;
    text-transform: capitalize !important;
    text-align: left !important;
    margin-left: 0px !important;
    font-family: Lato !important;
    letter-spacing: 0px !important;
    font-weight: 600 !important;
    padding: 20px 0px 10px 0px !important;
    border-bottom: 1px solid #000000 !important;">Products</h3>
                        </div>
                    </div>

                    <?php if(isset($quote_info)){
                        foreach ($quote_info['model'] as $i => $value) {
                            ?>
                            <div class="container-fluid">
                                <div class="row savedRows" id="savedRow<?= $i; ?>">
                                    <div class="form-row">
                                        <div class="col-md-2 mb-3" style="width: 16.66666667%;float:left">
                                            <label for="validationCustom01" style="color: #000000 !IMPORTANT;font-weight: 700 !important;font-size: 16px !important;font-family: Lato !important;">Product Model</label>
                                            <div><?= $value ?></div>
                                            <input value="<?= $value ?>" class="form-control hidden" name="model[]">
                                        </div>
                                        <div class="col-md-2 mb-3" style="width: 16.66666667%;float:left">
                                            <label for="validationCustom02" style="color: #000000 !IMPORTANT;font-weight: 700 !important;font-size: 16px !important;font-family: Lato !important;">Product Description</label>
                                            <div><?= $quote_info['description'][$i]; ?></div>
                                            <input value="<?= $quote_info['description'][$i]; ?>" class="form-control hidden" name="description[]">
                                        </div>
                                        <div class="form-row">
                                            <div class="col-md-2 mb-3" style="width: 16.66666667%;float:left">
                                                <label for="validationCustom01" style="color: #000000 !IMPORTANT;font-weight: 700 !important;font-size: 16px !important;font-family: Lato !important;">Product Family</label>
                                                <div><?= $quote_info['family'][$i]; ?></div>
                                                <input value="<?= $quote_info['family'][$i]; ?>" class="form-control hidden" name="family[]">
                                            </div>
                                            <div class="col-md-1 mb-3" style="width: 16.66666667%;float:left">
                                                <label for="validationCustom02" style="color: #000000 !IMPORTANT;font-weight: 700 !important;font-size: 16px !important;font-family: Lato !important;">Product Type</label>
                                                <div><?= $quote_info['type'][$i]; ?></div>
                                                <input value="<?= $quote_info['type'][$i]; ?>" class="form-control hidden" name="type[]">
                                            </div>
                                            <div class="col-md-1 mb-3" style="width: 16.66666667%;float:left">
                                                <label for="validationCustom02" style="color: #000000 !IMPORTANT;font-weight: 700 !important;font-size: 16px !important;font-family: Lato !important;">List Price</label>
                                                <div><?= $quote_info['list_price'][$i]; ?></div>
                                                <input value="<?= $quote_info['list_price'][$i]; ?>" class="form-control hidden" name="list_price[]">
                                            </div>
                                            <div class="col-md-1 mb-3" style="width: 16.66666667%;float:left">
                                                <label for="validationCustom02" style="color: #000000 !IMPORTANT;font-weight: 700 !important;font-size: 16px !important;font-family: Lato !important;">Dealer Price</label>
                                                <div><?= $quote_info['dealer_price'][$i]; ?></div>
                                                <input value="<?= $quote_info['dealer_price'][$i]; ?>" class="form-control hidden" name="dealer_price[]">
                                            </div>
                                            <div class="col-md-1 mb-3" style="width: 16.66666667%;float:left">
                                                <label for="validationCustom02" style="color: #000000 !IMPORTANT;font-weight: 700 !important;font-size: 16px !important;font-family: Lato !important;">Total Quantity</label>
                                                <div><?= number_format($quote_info['quantity'][$i]); ?></div>
                                                <input value="<?= $quote_info['quantity'][$i]; ?>" class="form-control hidden" name="quantity[]">
                                            </div>
                                            <div class="col-md-2 mb-3" style="width: 16.66666667%;float:left">
                                                <label for="validationCustom02" style="color: #000000 !IMPORTANT;font-weight: 700 !important;font-size: 16px !important;font-family: Lato !important;">Total Price</label>
                                                <div>$<?= number_format($quote_info['price'][$i],2); ?></div>
                                                <input value="<?= $quote_info['price'][$i]; ?>" class="form-control hidden" name="price[]">
                                            </div>
                                            <br>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php } } ?>

                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-md-12 grand-total pull-right">
                                <b><div style="text-align:right;" class="col-md-10">Grand Total:</div><div class="col-md-2 grandTotal"><?= isset($quote_info['grandTotal'])&& !empty($quote_info['grandTotal'])?'$'.number_format($quote_info['grandTotal'],2):'' ?></div></b>
                                <input class="form-control totalGrandPrice hidden" name="grandTotal">
                            </div>
                            <br>
                            <br>
                            <div class="fileDiv" <?= isset($quote_info['file'])&& !empty($quote_info['file'])?'show':'hidden'; ?> >
                                <b>Attachment:</b> <a class="attachedFile" target="_blank" href="<?= isset($quote_info['file'])&& !empty($quote_info['file'])?base_url('uploads/'.$quote_info['file']):'';?>"><?= isset($quote_info['file'])&& !empty($quote_info['file'])?'Attached File':'';?></a>
                            </div>
                        </div>
                    </div>
                    <div><br></div>
                    <div><br></div>
                    <div style="padding-left:10px;"><b><em>Note: This quote will expire in 90-days from the date of creation</em></b></div>
                </form>
            </div>
        </div>

    </div>
<!--    --><?php //if(!(isset($viewonly))){?>
<!--        <a href="--><?php //echo base_url();?><!--quote/redirect/1/--><?//=$quote_info['id'] ?><!--" class="btn btn-primary pull-right" style="margin: 10px">Submit</a>-->
<!--    --><?php //}?>
        <a onclick="printContent('main-content');"class="btn btn-warning pull-right" style="margin: 10px">Print</a>
<!--    <a  onclick="print();"class="btn btn-warning pull-right" style="margin: 10px">Generate PDF</a>-->
<!--    --><?php //if(!(isset($viewonly))){?>
<!--        <a  href="--><?php //echo base_url();?><!--quote/redirect/2/--><?//=$quote_info['id'] ?><!--"  class="btn btn-default pull-right" style="margin: 10px">Save as draft</a>-->
<!--    --><?php //}?>

</section>
<script>
        function printContent(el) {
            var restorePage = document.body.innerHTML;
            var printableContent = document.getElementById(el).innerHTML;
            document.body.innerHTML = printableContent;
            window.print();
            document.body.innerHTML = restorePage;
        }

//         function printPdf(){
//             var element = document.getElementById('main-content');
//             var opt = {
//                 margin:       1,
//                 filename:     (Math.floor(Math.random() * 10000) + 1  )+'.pdf',
//             };
// // New Promise-based usage:
//             html2pdf().set(opt).from(element).save();
//         }
</script>

<style>

label {
    color: #000000 !IMPORTANT;
    font-weight: 700 !important;
    font-size: 16px !important;
    font-family: Lato !important;
}

h2 {
    letter-spacing: 1px;
    font-family: Lato;
    font-size: 36px;
    font-weight: 600;
    word-spacing: 5px;
    color: #00a6d5;
    border-bottom: 1px solid #000000;
    margin-bottom: 25px !important;
}

h3 {
    color: #000000 !important;
    font-size: 22px !important;
    text-transform: capitalize !important;
    text-align: left !important;
    margin-left: 0px !important;
    font-family: Lato !important;
    letter-spacing: 0px !important;
    font-weight: 600 !important;
    padding: 20px 0px 10px 0px !important;
    border-bottom: 1px solid #000000 !important;
}

.grand-total {
    font-size: 16px !important;
    font-family: Lato !important;
}

</style>