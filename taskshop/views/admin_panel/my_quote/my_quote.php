<!--Page main section start-->
<section id="min-wrapper">
    <div id="main-content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <!--Top header start-->
                    <h3 class="ls-top-header">My Quote</h3>
                    <!--Top header end -->

                    <!--Top breadcrumb start -->
                    <ol class="breadcrumb">
                        <li><a href="#"><i class="fa fa-home"></i></a></li>
                        <li class="active">My Quote</li>
                    </ol>
                    <!--Top breadcrumb start -->
                </div>
            </div>
            <!-- Main Content Element  Start-->

            <div class="row">
                <div class="col-md-5">
                    <div class="input-group ls-group-input">
                        <span class="input-group-btn">
                            <button type="button" class="btn btn-success">Search</button>
                        </span>
                        <input class="form-control" type="text">
                    </div>
                </div>
                <div class="col-md-7">
                    <!-- Button trigger modal -->
                    <?php if ($this->session->userdata('access_label') == 1 OR $this->session->userdata('access_label') == 2) { ?>
                    <a href="<?php echo admin_url(); ?>/admin_quote/add_quote.html" class="btn btn-success" style="position: absolute;right:20px">
                        <i class="fa fa-plus"></i> Add My Quote
                    </a>
                    <?php } ?>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title">My Quote</h3>
                        </div>

                        <div class="panel-body">
                            <!--Table Wrapper Start-->
                            <div class="table-responsive ls-table">
                                <div>
                                    <?php
                                    $msg = $this->session->userdata('message');
                                    $error = $this->session->userdata('error');

                                    if ($msg) {
                                        echo "<div class='alert alert-success text-center'><i class='fa fa-check-circle-o fa-5x'></i>" . $msg . "</div>";
                                        $this->session->unset_userdata('message');
                                    } else if ($error) {
                                        echo "<div class='alert alert-danger text-center'><i class='fa fa-ban fa-5x'></i>" . $error . "</div>";
                                        $this->session->unset_userdata('error');
                                    }
                                    ?>
                                </div>
                                <table class="table table-bordered table-striped">
                                    <thead>
                                        <tr>
                                            <th>SL#</th>
                                            <th>Job Title</th>
                                            <th>My Quote</th>
                                            <th>Added By</th>
                                            <th>Date Added</th>
                                            <?php if ($this->session->userdata('access_label') == 1 OR $this->session->userdata('access_label') == 2) { ?>
                                            <th>Publication Status</th>
                                            <?php } ?>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>

                                        <?php
                                        $i = 1;
                                        foreach ($quote_info as $v_quote_info) {
                                            ?>
                                            <tr>
                                                <td><?php echo $i++; ?></td>
                                                <td><?php echo $v_quote_info['job_title']; ?></td>
                                                <td><?php echo $v_quote_info['job_category_name']; ?></td>
                                                <td><?php echo $v_quote_info['full_name']; ?></td>
                                                <td><?php echo date("D d F Y h:ia", strtotime($v_quote_info['date_added'])); ?></td>
                                                <?php if ($this->session->userdata('access_label') == 1 OR $this->session->userdata('access_label') == 2) { ?>
                                                <td>
                                                    <?php if ($v_quote_info['publication_status'] == 1) { ?>
                                                        <span class="label-success label label-default">Published</span>
                                                    <?php } else { ?>
                                                        <span class="label-warning label label-default">Unpublished</span>
                                                    <?php } ?>
                                                </td>
                                                <?php } ?>
                                                <td class="text-center">
                                                    <?php if ($this->session->userdata('access_label') == 1 OR $this->session->userdata('access_label') == 2) { ?>
                                                        <a href="<?php echo admin_url(); ?>/admin_quote/sub_quote/<?php echo $v_quote_info['quote_id']; ?>" class="btn btn-xs btn-primary"><i class="fa fa-eye"></i></a>
                                                    <?php } else { ?>
                                                    <a href="<?php echo admin_url(); ?>/user_quote/user_sub_quote/<?php echo $v_quote_info['quote_id']; ?>" class="btn btn-xs btn-primary"><i class="fa fa-eye"></i></a>
                                                    <?php } ?>
                                                    
                                                    <?php if ($this->session->userdata('access_label') == 1 OR $this->session->userdata('access_label') == 2) { ?>
                                                    <?php if ($v_quote_info['publication_status'] == 1) { ?>
                                                        <a href="<?php echo admin_url(); ?>/admin_quote/unpublished_quote/<?php echo $v_quote_info['quote_id']; ?>" class="btn btn-xs btn-warning"><i class="fa fa-arrow-down"></i></a>
                                                    <?php } else { ?>
                                                        <a href="<?php echo admin_url(); ?>/admin_quote/published_quote/<?php echo $v_quote_info['quote_id']; ?>" class="btn btn-xs btn-success"><i class="fa fa-arrow-up"></i></a>
                                                    <?php } ?>
                                                    <a href="<?php echo admin_url(); ?>/admin_quote/edit_quote/<?php echo $v_quote_info['quote_id']; ?>" class="btn btn-xs btn-primary"><i class="fa fa-pencil-square-o"></i></a>
                                                    <a href="<?php echo admin_url(); ?>/admin_quote/delete_quote/<?php echo $v_quote_info['quote_id']; ?>" class="btn btn-xs btn-danger" onclick="return confirm('Are you sure to delete this ?');"><i class="fa fa-trash-o"></i></a>
                                                    <?php } ?>
                                                </td>
                                            </tr>
                                        <?php } ?>

                                    </tbody>
                                </table>
                            </div>
                            <!--Table Wrapper Finish-->
                            <!--                            <div class="ls-button-group demo-btn ls-table-pagination">
                                                            <ul class="pagination ls-pagination" style="float: right;">
                                                                <li><a href="#"><i class="fa fa-arrow-left"></i> </a></li>
                                                                <li><a href="#">1</a></li>
                                                                <li><a href="#">2</a></li>
                                                                <li><a href="#">3</a></li>
                                                                <li><a href="#">4</a></li>
                                                                <li><a href="#">5</a></li>
                                                                <li><a href="#"><i class="fa fa-arrow-right"></i></a></li>
                                                            </ul>
                                                        </div>-->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!--Page main section end -->