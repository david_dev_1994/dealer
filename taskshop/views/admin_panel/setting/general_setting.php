<script type="text/javascript">
    $(document).ready(function () {
        $('.successMsg').fadeOut(10000);
    });

    function siteSettingValidation() {
        var error = 0;

        var NormalPattern = /^[a-zA-Z0-9-_,. ]*$/i;
        var EmailPattern = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
        var SiteName = $("#site_name").val();
        var AdminEmail = $("#admin_email").val();

        if (!NormalPattern.test(SiteName)) {
            error++;
            $("#msg_site_name").html("Special Character Not Allowed !");
        } else if (SiteName.length < 3) {
            error++;
            $("#msg_site_name").html("Minimum 3 Character Required !");
        } else {
            $("#msg_site_name").html("");
        }

        if (!EmailPattern.test(AdminEmail) || AdminEmail === "") {
            error++;
            $("#msg_admin_email").html("Not Valid !");
        } else {
            $("#msg_admin_email").html("");
        }

        if (error === 0) {
            return true;
        } else {
            return false;
        }

    }
</script>

<!--Page main section start-->
<section id="min-wrapper">
    <div id="main-content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">

                    <!--Top breadcrumb start -->
                    <ol class="breadcrumb">
                        <li><a href="#"><i class="fa fa-home"></i></a></li>
                        <li><a href="#">Setting</a></li>
                        <li class="active">General Setting</li>
                    </ol>
                    <!--Top breadcrumb start -->
                </div>
            </div>
            <!-- Main Content Element  Start-->
            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <div id="wizard" class="swMain">
                                        <ul class="anchor">
                                            <li>
                                                <a rel="1" isdone="1" class="selected" href="#step-Login">
                                                    <span class="stepDesc">
                                                        Site <small>Setting</small>
                                                    </span>
                                                </a>
                                            </li>
                                            <li>
                                                <a rel="2" isdone="1" class="done" href="#step-user">
                                                    <span class="stepDesc">
                                                        Upload  <small>Logo</small>
                                                    </span>
                                                </a>
                                            </li>
                                            <li>
                                                <a rel="3" isdone="1" class="done" href="#step-bio">
                                                    <span class="stepDesc">
                                                        Upload  <small>Favicon</small>
                                                    </span>
                                                </a>
                                            </li>
                                        </ul>

                                        <div style="height: 186px;" class="stepContainer">
                                            <div style="display: block;" class="content" id="step-Login">
                                                <h2 class="StepTitle">Site Information</h2>

                                                <div>
                                                    <?php
                                                    $msg = $this->session->userdata('message');
                                                    $error = $this->session->userdata('error');

                                                    if ($msg) {
                                                        echo "<div class='alert alert-success'>" . $msg . "</div>";
                                                        $this->session->unset_userdata('message');
                                                    } else if ($error) {
                                                        echo "<div class='alert alert-danger'>" . $error . "</div>";
                                                        $this->session->unset_userdata('error');
                                                    }
                                                    ?>
                                                </div>
                                                <form id="formid2" class="formular form-horizontal ls_form" method="post" action="<?php echo admin_url(); ?>/setting/update_general_setting.html">
                                                    <div class="container-fluid">
                                                        <div class="row ">
                                                            <div class="form-group">
                                                                <label class="col-md-2 control-label">Site Name</label>

                                                                <div class="col-md-10">
                                                                    <input class="form-control text-input" id="site_name" name="data[site_name]" placeholder="Site Name" type="text" value="<?php echo $site_info['site_name']; ?>">
                                                                    <span class="errorMesage" id="msg_site_name" style="color:red;"></span>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="row ">
                                                            <div class="form-group">
                                                                <label class="col-md-2 control-label">Admin E-mail </label>

                                                                <div class="col-md-10">
                                                                    <input placeholder="Admin Email" class="form-control validate[required] text-input" name="data[admin_email]" id="admin_email" type="text" value="<?php echo $site_info['admin_email']; ?>">
                                                                    <span class="errorMesage" id="msg_admin_email" style="color:red;"></span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <button class="btn ls-red-btn" type="submit" onclick="return siteSettingValidation();">Update</button>
                                                        <br>
                                                    </div>
                                                </form>
                                            </div>
                                            <div style="display: none;" class="content" id="step-user">

                                                <h2 class="StepTitle">Upload Site Logo</h2>
                                                <form id="formid2" class="formular form-horizontal ls_form" method="post" action="<?php echo admin_url(); ?>/setting/update_logo.html" enctype="multipart/form-data">
                                                    <input type="file" name="logo" class="form-control" id="profile_picture">
                                                    <input type="hidden" name="prev_logo_name" value="<?php echo $site_info['logo']; ?>">
                                                    <br>
                                                    <button class="btn ls-red-btn" type="submit">Update</button>
                                                </form>
                                            </div>
                                            <div style="display: none;" class="content" id="step-bio">

                                                <h2 class="StepTitle">Upload Site Favicon</h2>
                                                <form id="formid2" class="formular form-horizontal ls_form" method="post" action="<?php echo admin_url(); ?>/setting/update_favicon.html" enctype="multipart/form-data">
                                                    <input type="file" name="favicon" class="form-control" id="profile_picture">
                                                    <input type="hidden" name="prev_favicon_name" value="<?php echo $site_info['favicon']; ?>">
                                                    <br>
                                                    <button class="btn ls-red-btn" type="submit">Update</button>
                                                </form>
                                            </div>
                                        </div>

                                        <div class="actionBar"><div class="msgBox"><div class="content"></div><a href="#" class="close">X</a></div><div class="loader">Loading</div></div></div>
                                    <!-- End SmartWizard Content -->

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


            <!-- Main Content Element  End-->

        </div>
    </div>

</section>
<!--Page main section end -->