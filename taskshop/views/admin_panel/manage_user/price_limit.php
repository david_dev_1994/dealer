<section id="min-wrapper">
    <div id="main-content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <!--Top header start-->
                    <h3 class="ls-top-header">Price List</h3>
                    <!--Top header end -->
                </div>
            </div>
            <!-- Main Content Element  Start-->

            <div class="row">
                <div class="col-md-8">
                    <?php
                    if($this->session->flashdata('error')){
                        ?>
                        <br>
                        <div class="alert alert-danger alert-dismissable">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>
                            <?php echo $this->session->flashdata('error'); ?>
                        </div>
                        <?php
                    }

                    if($this->session->flashdata('success')){
                        ?>
                        <br>
                        <div class="alert alert-success alert-dismissable">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>
                            <?php echo $this->session->flashdata('success'); ?>
                        </div>
                        <?php
                    }
                    ?>

                    <?php
                    if(validation_errors()){
                        ?>
                        <br>
                        <div class="alert alert-danger alert-dismissable">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>
                            <?php echo validation_errors(); ?>
                        </div>
                        <?php
                    }

                    if(isset($error)){
                        ?>
                        <br>
                        <div class="alert alert-danger alert-dismissable">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>
                            <?php echo $error; ?>
                        </div>
                        <?php
                    }

                    if(isset($success)){
                        ?>
                        <br>
                        <div class="alert alert-success alert-dismissable">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>
                            <?php echo $success; ?>
                        </div>
                        <?php
                    }
                    ?>
                </div>
            </div>

            <div class="row">
                <div class="col-md-9">
                    <div class="form">
                        <form method="post" action="" enctype="multipart/form-data">

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-field">
                                        <div><b>Upload File</b></div>
                                        <span><b>Note:</b> File should be CSV only.</span>
                                        <input value="1" name="upload" hidden />
                                        <div>
                                            <input type="file" name="file" style="margin: 10px"> </span>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-10">
                                    <div style="border-bottom: 1px solid #EFEFEF; margin-bottom: 15px;"></div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-field">
                                        <div><b>Click To Download:</b></div>
                                        <?php if( (isset($user_info['docFile']) && !empty($user_info['docFile']) ) ){ ?>
                                        <a href="<?= base_url('uploads/'.$user_info['docFile']); ?>" download><?= $user_info['docFile']; ?></a>
                                        <?php }else{ ?>
                                        <div><h5><b>No File Uploaded!</b></h5></div>
                                        <?php } ?>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-10">
                                    <div style="border-bottom: 1px solid #EFEFEF; margin-bottom: 15px;"></div>
                                </div>
                            </div>

                            <div class="row" style="margin: 0 0 15px 0px;">
                                <div class="col-md-10">
                                    <a href="<?= base_url('admin/dashboard'); ?>" class="btn btn-default"> Cancel </a>
                                    <button type="submit" class="btn btn-primary">Save</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <!-- Main Content Element  End-->
        </div>
    </div>

</section>
