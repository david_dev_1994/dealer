<?php //echo '<pre>';print_r($users_info);exit; ?>
<section id="min-wrapper">
    <div id="main-content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <!--Top header start-->
                    <h3 class="ls-top-header">Register Dealers</h3>
                    <!--Top header end -->

                <?php
                if($this->session->userdata('access_label') == 1) {?>

                <div class="product-tools pull-right">
                    <a href="<?= base_url('admin/add_dealer');?>" class="btn btn-primary" style="margin: 15px;">Add Dealer</a>
                </div>

                <?php } ?>
                </div>
            </div>

            <div>
                <?php
                $msg = $this->session->userdata('message');
                $error = $this->session->userdata('error');

                if ($msg) {
                    echo "<div class='alert alert-success'>" . $msg . "</div>";
                    $this->session->unset_userdata('message');
                } else if ($error) {
                    echo "<div class='alert alert-danger'>" . $error . "</div>";
                    $this->session->unset_userdata('error');
                }
                ?>
                <div id="successMsg" class='alert alert-success hidden'></div>
                <div id="errorMsg" class='alert alert-danger hidden'></div>
            </div>

            <!-- Main Content Element  Start-->
            <div class="row">
                <div class="col-md-12">
                    <div class="portlet light no-border-top as-box-border">
                        <div class="portlet-body">
                            <table class="table table-striped table-bordered table-hover dt-responsive" id="user_table1" data-name="services">
                                <thead>
                                <tr>
                                    <th>Company Name</th>
                                    <th>Full Name</th>
                                    <th>Email</th>
                                    <th>Phone</th>
                                    <th>User Level</th>
                                    <th>Registered Date</th>
                                    <th>Sales Manager</th>
                                    <?php if($this->session->userdata('access_label') == 1){ ?>
                                        <th> Action</th>
                                    <?php } ?>
                                </tr>
                                </thead>
                                <tbody>
                                <?php foreach ($users_info as $v_user_info) { ?>
                                    <?php if ($v_user_info['u_id'] != $this->session->userdata('user_id')) { ?>
                                        <tr id="dealerRow_<?= $v_user_info['u_id']; ?>">
                                            <td><?php echo $v_user_info['companyname']; ?></td>
                                            <td><?php echo $v_user_info['full_name']; ?></td>
                                            <td><a href="mailto:<?= $v_user_info['email_address']; ?>"><?= $v_user_info['email_address']; ?></a></td>
                                            <td><?php echo $v_user_info['phone']; ?></td>
                                            <td>
                                                Dealer
                                            </td>
<!--                                            <td>--><?php //echo date("D d F Y h:ia", strtotime($v_user_info['date_added'])); ?><!--</td>-->
                                            <td><?php echo date("m-d-y h:i A", strtotime($v_user_info['date_added'])); ?></td>
                                            <td><?php echo $v_user_info['sales_manager_name']; ?></td>
                                            <?php if($this->session->userdata('access_label') == 1) { ?>
                                                <td>
<!--                                                    --><?php //if ($v_user_info['activation_status'] == 1) { ?>
                                                        <a class="btn btn-xs btn-success <?= isset($v_user_info['activation_status'])&& !empty($v_user_info['activation_status'])&& $v_user_info['activation_status'] == 1? '':'hidden'; ?>" title="Block" id="block_<?= $v_user_info['u_id']; ?>" onclick="blockDealer('<?= $v_user_info["u_id"]; ?>')">
<!--                                                        <a href="--><?php //echo admin_url(); ?><!--/registered_user/block_dealer/--><?php //echo $v_user_info['u_id']; ?><!--" id="block" class="btn btn-xs btn-success" title="Block">-->
                                                            <i id="blockSpan" class="fa fa-unlock"></i>
                                                        </a>
                                                        <a onclick="unblockDealer('<?= $v_user_info["u_id"]; ?>','<?= $v_user_info["email_address"]; ?>')" id="unblock_<?= $v_user_info['u_id']; ?>" class="btn btn-xs btn-warning <?= isset($v_user_info['activation_status'])&& !empty($v_user_info['activation_status'])&& $v_user_info['activation_status'] == 1? 'hidden':''; ?>" title="Unblock">
<!--                                                        <a href="--><?php //echo admin_url(); ?><!--/registered_user/unblock_dealer/--><?php //echo $v_user_info['u_id']; ?><!--/--><?php //echo $v_user_info['email_address']; ?><!--" id="unblock" class="btn btn-xs btn-warning hidden" title="Unblock">-->
                                                            <i id="unblock" class="fa fa-lock"></i>
                                                        </a>


                                                    <!--                                                        <a id="edit" class="btn btn-xs btn-info" data-toggle="modal" data-target="#myModal--><?php //echo $v_user_info['u_id']; ?><!--" title="Edit">-->
                                                    <!--                                                            <i class="fa fa-pencil-square-o"></i>-->
                                                    <!--                                                        </a>-->
<!--                                                    <a href="--><?php //echo admin_url(); ?><!--/registered_user/delete_dealer/--><?php //echo $v_user_info['u_id']; ?><!--" id="edit" class="btn btn-xs btn-danger" data-toggle="tooltip" data-placement="top" title="Delete"  onclick="return confirm('Are you sure to delete this ?');">-->
                                                    <a id="delete" class="btn btn-xs btn-danger" title="Delete"  onclick="deleteDealer(<?= $v_user_info['u_id']; ?>);">
                                                        <i class="fa fa-trash-o"></i>
                                                    </a>
                                                </td>
                                            <?php } ?>
                                        </tr>
                                    <?php } ?>
                                <?php } ?>
                                </tbody>
                                <?php if($this->session->userdata('access_label') == 1){?>
                                    <tfoot>
                                    <tr>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                    </tr>
                                    </tfoot>
                                <?php } ?>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Main Content Element  End-->
        </div>
    </div>
</section>

<?php foreach ($users_info as $v_user_info) { ?>
<?php if ($v_user_info['u_id'] != $this->session->userdata('user_id')) { ?>
<!-- Modal -->
<div class="modal fade" id="myModal<?php echo $v_user_info['u_id']; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Privilege</h4>
            </div>
            <form class="form-2" method="post" action="<?php echo admin_url(); ?>/registered_user/update_access_label/<?php echo $v_user_info['u_id']; ?>" data-toggle="validator" role="form">
                <div class="modal-body">
                    <div class="radio">
                        <label>
                            <input type="radio" name="access_label" id="optionsRadios1" value="1" <?php if($v_user_info['access_label'] == 1){echo "checked='checked'"; }?> >
                            Super Admin
                        </label>
                    </div>
                    <div class="radio">
                        <label>
                            <input type="radio" name="access_label" id="optionsRadios2" value="2" <?php if($v_user_info['access_label'] == 2){echo "checked='checked'"; }?>>
                            Admin
                        </label>
                    </div>
                    <div class="radio">
                        <label>
                            <input type="radio" name="access_label" id="optionsRadios3" value="3" <?php if($v_user_info['access_label'] == 3){echo "checked='checked'"; }?>>
                            User
                        </label>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Update</button>
                </div>
            </form>
        </div>
    </div>
</div>
<?php } ?>
<?php } ?>