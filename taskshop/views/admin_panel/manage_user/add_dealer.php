<script type="text/javascript">
    $(document).ready(function () {
        $('.successMsg').fadeOut(10000);
    });


    function profileValidation() {
        var error = 0;

        var FirstName = $("#name").val();
        var Email = $("#email").val();
        var Pass = $("#password").val();
        var Address = $("#address").val();
        var State = $("#state").val();
        var City = $("#city").val();
        var Zip = $("#zip").val();
        var Phone = $("#phone").val();
        var Company = $("#company").val();
        var Num = $("#num").val();

        if (FirstName.length < 3) {
            error++;
            $('#msg_first_name').html('Minimum 3 character required !');
        } else if (FirstName.length > 15) {
            error++;
            $('#msg_first_name').html('Maximum 15 character allowed !');
        } else {
            $('#msg_first_name').html('');
        }

        if (Email.length == '') {
            error++;
            $('#msg_email').html('Email is required');
        } else {
            $('#msg_email').html('');
        }

        if (Pass.length < 8) {
            error++;
            $('#msg_pass').html('Minimum 8 character required !');
        } else {
            $('#msg_pass').html('');
        }

        if (Num.length < 8) {
            error++;
            $('#msg_num').html('Minimum 8 character required !');
        } else {
            $('#msg_num').html('');
        }


        if (Address.length < 5) {
            error++;
            $('#msg_address').html('Address is required !');
        } else if (Address.length > 500) {
            error++;
            $('#msg_address').html('Maximum 500 character allowed !');
        } else {
            $('#msg_address').html('');
        }

        if (City.length < 3) {
            error++;
            $('#msg_city').html('City is required !');
        } else if (City.length > 50) {
            error++;
            $('#msg_city').html('Maximum 50 character allowed !');
        } else {
            $('#msg_city').html('');
        }

        if (State.length < 2) {
            error++;
            $('#msg_state').html('State is required !');
        } else if (State.length > 151) {
            error++;
            $('#msg_state').html('Maximum 10 character allowed !');
        } else {
            $('#msg_state').html('');
        }
        
        if (Company.length < 5) {
            error++;
            $('#msg_cmp').html('Company is required !');
        } else if (Company.length > 151) {
            error++;
            $('#msg_cmp').html('Maximum 10 character allowed !');
        } else {
            $('#msg_cmp').html('');
        }

        if (Zip.length < 3) {
            error++;
            $('#msg_zip').html('Zip is required !');
        } else if (Address.length > 50) {
            error++;
            $('#msg_zip').html('Maximum 50 character allowed !');
        } else {
            $('#msg_zip').html('');
        }

        if (Phone.length < 3) {
            error++;
            $('#msg_phone').html('Phone is required !');
        } else if (Address.length > 50) {
            error++;
            $('#msg_phone').html('Maximum 50 character allowed !');
        } else {
            $('#msg_phone').html('');
        }

        if (error === 0) {
            return true;
        } else {
            return false;
        }

    }
</script>

<!--Page main section start-->

<section id="min-wrapper">
    <div id="main-content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <!--Top header start-->
                    <h3 class="ls-top-header">Dealers</h3>
                    <!--Top header end -->
                </div>
            </div>
            <!-- Main Content Element  Start-->


            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title">Manage Dealer Account</h3>
                        </div>
                        <div class="panel-body">

                            <div>

                                <?php if ($this->session->flashdata('error')){?>
                                    <div class="alert alert-danger alert-dismissible text-center row">
                                        <?=$this->session->flashdata('error')?>
                                    </div>
                                <?php }?>


                            </div>
                            <!-- Tab panes -->
                            <div class="container">

                                <div class="row" id="profile"><br/>
                                    <div class="col-md-8">
                                        <form class="form-horizontal ls_form ls_form_horizontal" method="post" action="">

                                            <div class="form-group">
                                                <label class="col-md-4 control-label">Company:</label>
                                                <div class="col-md-8">
                                                    <input name="company" id="company" placeholder="Company" class="form-control" type="text" value="">
                                                    <span class="errorMessage" id="msg_cmp"></span>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-md-4 control-label">Name:</label>

                                                <div class="col-md-8">
                                                    <input placeholder="Name" id="name" name="name" class="form-control" type="text" value="">
                                                    <span class="errorMessage" id="msg_first_name"></span>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-md-4 control-label">Email:</label>

                                                <div class="col-md-8">
                                                    <input name="email" id="email" placeholder="dealer@dealer.com " class="form-control" type="email" value="">
                                                    <span class="errorMessage" id="msg_email"></span>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-md-4 control-label">Direct Phone Number:</label>
                                                <div class="col-md-8">
                                                    <input name="num" id="num" placeholder="Direct Phone Number" class="form-control" type="text" value="">
                                                    <span class="errorMessage" id="msg_num"></span>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-md-4 control-label">Password:</label>
                                                <div class="col-md-8">
                                                    <input name="password" id="password" placeholder="Password" class="form-control" type="text" value="">
                                                    <span class="errorMessage" id="msg_pass"></span>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-md-4 control-label">Address:</label>
                                                <div class="col-md-8">
                                                    <input name="address[]" id="address" placeholder="Address" class="form-control" type="text" value="">
                                                    <span class="errorMessage" id="msg_address"></span>
                                                </div>
                                            </div>
                                            
                                            <div class="form-group">
                                                <label class="col-md-4 control-label">Office Phone:</label>
                                                <div class="col-md-8">
                                                    <input name="phone[]" id="phone" placeholder="Phone Number" class="form-control" type="text" value="">
                                                    <span class="errorMessage" id="msg_phone"></span>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-md-4 control-label">City:</label>
                                                <div class="col-md-8">
                                                    <input name="city[]" id="city" placeholder="City" class="form-control" type="text" value="">
                                                    <span class="errorMessage" id="msg_city"></span>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-md-4 control-label">State:</label>
                                                <div class="col-md-8">
                                                    <input name="state[]" id="state" placeholder="State" class="form-control" type="text" value="">
                                                    <span class="errorMessage" id="msg_state"></span>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-md-4 control-label">Zip Code:</label>
                                                <div class="col-md-8">
                                                    <input name="zip[]" id="zip" placeholder="Zip Code" class="form-control" type="text" value="">
                                                    <span class="errorMessage" id="msg_zip"></span>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-md-4 control-label">Sales Manager:</label>
                                                <div class="col-md-8">
                                                    <select required="required" name="sales_manager_id" class="form-control">

                                                        <option disabled selected value="default"> -- Select Sales Manager -- </option>
                                                        <?php
                                                        if (count($users_info) > 0) {
                                                            for ($count = 0; $count < count($users_info); $count++) {
                                                                ?>
                                                                <option value="<?= $users_info[$count]['u_id']; ?>"><?= $users_info[$count]['full_name']; ?></option>
                                                                <?php
                                                            }
                                                        }
                                                        ?>

                                                    </select>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <div class="col-lg-offset-4 col-lg-8">
                                                    <button class="btn btn-sm btn-default" type="submit" onclick="return profileValidation();">Submit
                                                    </button>
                                                    <a class="btn btn-sm btn-danger" href="<?= base_url('admin/registered_dealers');?>">Close</a>
                                                </div>
                                            </div>

                                            <div class="form-group">

                                            </div>

                                        </form>
                                    </div>
                                    <div class="col-md-6"></div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
            <!-- Main Content Element  End-->
        </div>
    </div>

</section>
