<style>
    input[type=text] {
        width: 100%;
        padding: 12px 20px;
        margin: 8px 0;
        display: inline-block;
        border: 1px solid #ccc;
        border-radius: 4px;
        box-sizing: border-box;
    }
    input[type=url] {
        width: 100%;
        padding: 12px 20px;
        margin: 8px 0;
        display: inline-block;
        border: 1px solid #ccc;
        border-radius: 4px;
        box-sizing: border-box;
    }
</style>
<section id="min-wrapper">
    <div id="main-content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <h3 class="ls-top-header"><a href="<?= base_url('admin/brochures');?>">Brochures</a></h3>
                </div>
            </div>
            <!-- Main Content Element  Start-->

            <div class="row">
                <div class="col-md-8">
                    <?php
                    if($this->session->flashdata('error')){
                        ?>
                        <br>
                        <div class="alert alert-danger alert-dismissable">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>
                            <?php echo $this->session->flashdata('error'); ?>
                        </div>
                        <?php
                    }

                    if($this->session->flashdata('success')){
                        ?>
                        <br>
                        <div class="alert alert-success alert-dismissable">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>
                            <?php echo $this->session->flashdata('success'); ?>
                        </div>
                        <?php
                    }
                    ?>

                    <?php
                    if(validation_errors()){
                        ?>
                        <br>
                        <div class="alert alert-danger alert-dismissable">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>
                            <?php echo validation_errors(); ?>
                        </div>
                        <?php
                    }

                    if(isset($error)){
                        ?>
                        <br>
                        <div class="alert alert-danger alert-dismissable">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>
                            <?php echo $error; ?>
                        </div>
                        <?php
                    }

                    if(isset($success)){
                        ?>
                        <br>
                        <div class="alert alert-success alert-dismissable">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>
                            <?php echo $success; ?>
                        </div>
                        <?php
                    }
                    ?>
                </div>
            </div>

            <div class="row">
                <div class="col-md-9">
                    <div class="as-form">
                        <form method="post" action="" enctype="multipart/form-data">

                            <div class="row">
                                <div class="col-md-10">
                                    <div class="form-field">
                                        <div><b>Title</b></div>
                                        <div class="form-input">
                                            <input type="text" placeholder="Title Goes Here" name="title" value="<?= isset($data['title'])?$data['title']:$this->input->post('title'); ?>"/>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-10">
                                    <div class="form-field">
                                        <div><b>Link</b></div>
                                        <div class="form-input">
                                            <input type="url" placeholder="https://www.xyz.com/" name="url" value="<?= isset($data['url'])? $data['url']:$this->input->post('url'); ?>"/>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-10">
                                    <div style="border-bottom: 1px solid #EFEFEF; margin-bottom: 15px;"></div>
                                </div>
                            </div>

                            <div class="row" style="margin: 0 0 15px 0px;">
                                <div class="col-md-10">
                                        <a href="<?= base_url('admin/brochures'); ?>" class="btn btn-default"> Cancel </a>
                                        <button type="submit" class="btn btn-primary">Save</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <!-- Main Content Element  End-->
        </div>
    </div>

</section>
