<script type="text/javascript">
    $(document).ready(function () {
        $('.successMsg').fadeOut(10000);
    });

    function changePasswordValidation() {
        var error = 0;

        var OldPassword = $("#old_password").val();
        var NewPassword = $("#new_password").val();
        var ConfirmPassword = $("#confirm_password").val();

        if (OldPassword.length < 1) {
            error++;
            $('#msg_old_password').html('Old password is required !');
        } else {
            $('#msg_old_password').html('');
        }

        if (NewPassword.length < 8) {
            error++;
            $('#msg_new_password').html('Minimum 8 character required !');
        } else if (NewPassword.length > 15) {
            error++;
            $('#msg_new_password').html('Maximum 15 character allowed !');
        } else {
            $('#msg_new_password').html('');
        }

        if (NewPassword !== ConfirmPassword) {
            error++;
            $('#msg_confirm_password').html('Password does not match !');
        } else {
            $('#msg_confirm_password').html('');
        }

        if (error === 0) {
            return true;
        } else {
            return false;
        }

    }
    
    function profileValidation() {
        var error = 0;

        var FirstName = $("#first_name").val();
        var LastName = $("#last_name").val();
        var Address = $("#address").val();
        var State = $("#state").val();
        var City = $("#city").val();
        var Zip = $("#zip").val();
        var Country = $("#country option:selected").text();
        var Phone = $("#phone").val();

        if (FirstName.length < 3) {
            error++;
            $('#msg_first_name').html('Minimum 3 character required !');
        } else if (FirstName.length > 15) {
            error++;
            $('#msg_first_name').html('Maximum 15 character allowed !');
        } else {
            $('#msg_first_name').html('');
        }
        
        if (LastName.length < 3) {
            error++;
            $('#msg_last_name').html('Minimum 3 character required !');
        } else if (FirstName.length > 15) {
            error++;
            $('#msg_last_name').html('Maximum 15 character allowed !');
        } else {
            $('#msg_last_name').html('');
        }
        
        if (Address.length < 5) {
            error++;
            $('#msg_address').html('Address is required !');
        } else if (Address.length > 151) {
            error++;
            $('#msg_address').html('Maximum 150 character allowed !');
        } else {
            $('#msg_address').html('');
        }
        
        if (City.length < 3) {
            error++;
            $('#msg_city').html('City is required !');
        } else if (City.length > 50) {
            error++;
            $('#msg_city').html('Maximum 50 character allowed !');
        } else {
            $('#msg_city').html('');
        }
        
        if (State.length < 5) {
            error++;
            $('#msg_state').html('State is required !');
        } else if (State.length > 151) {
            error++;
            $('#msg_state').html('Maximum 10 character allowed !');
        } else {
            $('#msg_state').html('');
        }
        
        if (Zip.length < 3) {
            error++;
            $('#msg_zip').html('Zip is required !');
        } else if (Address.length > 50) {
            error++;
            $('#msg_zip').html('Maximum 50 character allowed !');
        } else {
            $('#msg_zip').html('');
        }

        if (Country === 'Select One') {
            error++;
            $('#msg_country').html('Please select country name !');
        } else {
            $('#msg_country').html('');
        }
        
        if (Phone.length < 3) {
            error++;
            $('#msg_phone').html('Phone is required !');
        } else if (Address.length > 50) {
            error++;
            $('#msg_phone').html('Maximum 50 character allowed !');
        } else {
            $('#msg_phone').html('');
        }

        if (error === 0) {
            return true;
        } else {
            return false;
        }

    }
</script>

<style type="text/css">
    .edit_avater_image {
        max-width: 130px;
        max-height: 130px;
        float: left;
        margin-top: 20%;
        margin-bottom: 20px;
        margin-left: 8%;
    }
    .edit_avater_image img {
        border: solid 1px #ccc;
        border-radius: 80px;
        width: 100%;
        height: 130px;
        box-shadow: 0 1px 2px #999;
    }
    social_media_title {
        width: 100%;
        height: 30px;
        margin: 4px auto;
    }
    .social_media_title ul {
        width: 100%;
        height: auto;
        float: left;
    }
    .fb {
        padding: 0px 7px;
        background: #4769a5;
        border-radius: 3px;
        box-shadow: 0 -4px 0 rgba(0, 0, 0, 0.15) inset;
        padding-bottom: 10px;
        float: left;
        cursor: pointer;
    }
    .tw {
        padding: 0px 7px;
        background: #008ed6;
        border-radius: 3px;
        box-shadow: 0 -4px 0 rgba(0, 0, 0, 0.15) inset;
        padding-bottom: 10px;
        float: left;
        cursor: pointer;
    }
    .gp {
        padding: 0px 7px;
        background: #bf3727;
        border-radius: 3px;
        box-shadow: 0 -4px 0 rgba(0, 0, 0, 0.15) inset;
        padding-bottom: 10px;
        float: left;
        cursor: pointer;
    }
    .ln {
        padding: 0px 7px;
        background: #2ba3e1;
        border-radius: 3px;
        box-shadow: 0 -4px 0 rgba(0, 0, 0, 0.15) inset;
        padding-bottom: 10px;
        float: left;
        cursor: pointer;
    }
    .social_media_title ul li {
        float: left;
        list-style: none;
        display: block;
        margin-left: 6px;
        height: auto;
    }
</style>
<!--Page main section start-->
<section id="min-wrapper">
    <div id="main-content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <!--Top header start-->
                    <h3 class="ls-top-header">Profile</h3>
                    <!--Top header end -->

                    <!--Top breadcrumb start -->
                    <ol class="breadcrumb">
                        <li><a href="#"><i class="fa fa-home"></i></a></li>
                        <li class="active">Manage Your Account Setting</li>
                    </ol>
                    <!--Top breadcrumb start -->
                </div>
            </div>
            <!-- Main Content Element  Start-->
            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title">Manage Account</h3>
                        </div>
                        <div class="panel-body">

                            <div>
                                <?php
                                $msg = $this->session->userdata('message');
                                $error = $this->session->userdata('error');

                                if ($msg) {
                                    echo "<div class='alert alert-success'>" . $msg . "</div>";
                                    $this->session->unset_userdata('message');
                                } else if ($error) {
                                    echo "<div class='alert alert-danger'>" . $error . "</div>";
                                    $this->session->unset_userdata('error');
                                }
                                ?>
                            </div>

                            <ul class="nav nav-tabs" role="tablist">
                                <li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">Profile</a></li>
                                <li role="presentation"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">Edit</a></li>
                                <li role="presentation"><a href="#messages" aria-controls="messages" role="tab" data-toggle="tab">Change Password</a></li>
                                <li role="presentation"><a href="#settings" aria-controls="settings" role="tab" data-toggle="tab">Change Picture</a></li>
                            </ul>

                            <!-- Tab panes -->
                            <div class="tab-content">
                                <div role="tabpanel" class="tab-pane active" id="home">
                                    <div class="col-md-4">
                                        <hr />
                                        <h6>First Name: <span style="font-size:12px;float:right"><?php echo $user_info['first_name']; ?></span></h6><hr />
                                        <h6>Last Name: <span style="font-size:12px;float:right"><?php echo $user_info['last_name']; ?></span></h6><hr />
                                        <h6>Email: <span style="font-size:12px;float:right"><?php echo $user_info['email_address']; ?></span></h6><hr />
                                        <h6>Address-1: <span style="font-size:12px;float:right"><?php echo $user_info['address_1']; ?></span></h6><hr />
                                        <h6>Address-2: <span style="font-size:12px;float:right"><?php echo $user_info['address_2']; ?></span></h6><hr />
                                        <h6>State: <span style="font-size:12px;float:right"><?php echo $user_info['state']; ?></span></h6><hr />
                                        <h6>City: <span style="font-size:12px;float:right"><?php echo $user_info['city']; ?></span></h6><hr />
                                        <h6>Zip: <span style="font-size:12px;float:right"><?php echo $user_info['zip']; ?></span></h6><hr />
                                        <h6>Country: <span style="font-size:12px;float:right"><?php echo $user_info['country']; ?></span></h6><hr />
                                        <h6>Phone: <span style="font-size:12px;float:right"><?php echo $user_info['phone']; ?></span></h6><hr />
                                        <!--<h6>Public Profile link: <span style="font-size:12px;">http://loginsystem.atiqueit.com/ums/profile/5</span></h6><hr />-->
                                    </div>
                                    <div class="col-md-8">
                                        <div class="">
                                            <!--edit_avater_image-->
                                            <div class="edit_avater_image">
                                                <img alt="profile" src="<?= (isset($user_info['profile_picture']) && !empty($user_info['profile_picture'])? base_url('uploaded_files/profile_pictures/thumb/'.$user_info['profile_picture']):base_url('admin_assets/images/userimage/avatar2-80.png') ); ?>" onerror="this.src='<?= (isset($user_info['profile_picture']) && !empty($user_info['profile_picture'])? base_url('uploaded_files/profile_pictures/thumb/'.$user_info['profile_picture']):base_url('admin_assets/images/userimage/avatar2-80.png')  ); ?>'">
                                            </div>
                                            <div class="bclear"></div>
                                        </div>
                                    </div>
                                </div>
                                <div role="tabpanel" class="tab-pane" id="profile"><br />
                                    <div class="col-md-6">
                                        <form id="formid2" class="form-horizontal ls_form ls_form_horizontal" method="post" action="<?php echo admin_url(); ?>/profile/update_profile.html" name="edit_profile_info">
                                            <div class="form-group">
                                                <label class="col-md-4 control-label">First Name:</label>

                                                <div class="col-md-8">
                                                    <input placeholder="First Name" id="first_name" name="data[first_name]" class="form-control" type="text" value="<?php echo $user_info['first_name']; ?>">
                                                    <span class="errorMessage" id="msg_first_name"></span>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-4 control-label">Last Name:</label>

                                                <div class="col-md-8">
                                                    <input placeholder="Last Name" id="last_name" name="data[last_name]" class="form-control" type="text" value="<?php echo $user_info['last_name']; ?>">
                                                    <span class="errorMessage" id="msg_last_name"></span>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-4 control-label">Email:</label>

                                                <div class="col-md-8">
                                                    <input name="Disabled" placeholder="admin@atiqueit.com " class="form-control" type="email" value="<?php echo $user_info['email_address']; ?>">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-4 control-label">Address-1:</label>

                                                <div class="col-md-8">
                                                    <input placeholder="Address 1" id="address" name="data[address_1]" class="form-control" type="text" value="<?php echo $user_info['address_1']; ?>">
                                                    <span class="errorMessage" id="msg_address"></span>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-4 control-label">Address-2:</label>

                                                <div class="col-md-8">
                                                    <input placeholder="Address 2" id="" name="data[address_2]" class="form-control" type="text" value="<?php echo $user_info['address_2']; ?>">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-4 control-label">State:</label>

                                                <div class="col-md-8">
                                                    <input placeholder="State" id="state" name="data[state]" class="form-control" type="text" value="<?php echo $user_info['state']; ?>">
                                                    <span class="errorMessage" id="msg_state"></span>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-4 control-label">City:</label>

                                                <div class="col-md-8">
                                                    <input placeholder="City" id="city" name="data[city]" class="form-control" type="text" value="<?php echo $user_info['city']; ?>">
                                                    <span class="errorMessage" id="msg_city"></span>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-4 control-label">Zip:</label>

                                                <div class="col-md-8">
                                                    <input placeholder="Zip" id="zip" name="data[zip]" class="form-control" type="text" value="<?php echo $user_info['zip']; ?>">
                                                    <span class="errorMessage" id="msg_zip"></span>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-4 control-label">Country:</label>
                                                <div class="col-md-8">
                                                    <select id="country" name="data[country]" class="form-control">
                                                        <option value="" selected="selected" disabled="">Select One</option>
                                                        <option value="Afghanistan">Afghanistan</option>
                                                        <option value="Akrotiri">Akrotiri</option>
                                                        <option value="Albania">Albania</option>
                                                        <option value="Algeria">Algeria</option>
                                                        <option value="American Samoa">American Samoa</option>
                                                        <option value="Andorra">Andorra</option>
                                                        <option value="Angola">Angola</option>
                                                        <option value="Anguilla">Anguilla</option>
                                                        <option value="Antarctica">Antarctica</option>
                                                        <option value="Antigua and Barbuda">Antigua and Barbuda</option>
                                                        <option value="Argentina">Argentina</option>
                                                        <option value="Armenia">Armenia</option>
                                                        <option value="Aruba">Aruba</option>
                                                        <option value="Australia">Australia</option>
                                                        <option value="Austria">Austria</option>
                                                        <option value="Azerbaijan">Azerbaijan</option>
                                                        <option value="Bangladesh">Bangladesh</option>
                                                        <option value="Bahamas">Bahamas</option>
                                                        <option value="Bahrain">Bahrain</option>
                                                        <option value="Barbados">Barbados</option>
                                                        <option value="Belarus">Belarus</option>
                                                        <option value="Belgium">Belgium</option>
                                                        <option value="Belize">Belize</option>
                                                        <option value="Benin">Benin</option>
                                                        <option value="Bermuda">Bermuda</option>
                                                        <option value="Bhutan">Bhutan</option>
                                                        <option value="Bolivia">Bolivia</option>
                                                        <option value="Bosnia and Herzegovina">Bosnia and Herzegovina</option>
                                                        <option value="Botswana">Botswana</option>
                                                        <option value="Brazil">Brazil</option>
                                                        <option value="Brunei Darussalam">Brunei Darussalam</option>
                                                        <option value="Bulgaria">Bulgaria</option>
                                                        <option value="Burkina Faso">Burkina Faso</option>
                                                        <option value="Burundi">Burundi</option>
                                                        <option value="Cambodia">Cambodia</option>
                                                        <option value="Cameroon">Cameroon</option>
                                                        <option value="Canada">Canada</option>
                                                        <option value="Cape Verde">Cape Verde</option>
                                                        <option value="Cayman Islands">Cayman Islands</option>
                                                        <option value="Central African Republic">Central African Republic</option>
                                                        <option value="Chad">Chad</option>
                                                        <option value="Chile">Chile</option>
                                                        <option value="China">China</option>
                                                        <option value="Christmas Island">Christmas Island</option>
                                                        <option value="Christmas Island">Christmas Island</option>
                                                        <option value="Cocos">Cocos</option>
                                                        <option value="Colombia">Colombia</option>
                                                        <option value="Comoros">Comoros</option>
                                                        <option value="Congo">Democratic Republic of the Congo</option>
                                                        <option value="Cook Islands">Cook Islands</option>
                                                        <option value="Costa Rica">Costa Rica</option>
                                                        <option value="Ivory Coast">Ivory Coast</option>
                                                        <option value="Croatia">Croatia</option>
                                                        <option value="Cuba">Cuba</option>
                                                        <option value="Cyprus">Cyprus</option>
                                                        <option value="Czech Republic ">Czech Republic </option>
                                                        <option value="Denmark ">Denmark </option>
                                                        <option value="Djibouti ">Djibouti </option>
                                                        <option value="Dominica ">Dominica</option>
                                                        <option value="Dominican Republic">Dominican Republic</option>
                                                        <option value="Ecuador ">Ecuador </option>
                                                        <option value="Egypt ">Egypt </option>
                                                        <option value="El Salvador">El Salvador</option>
                                                        <option value="Eritrea ">Eritrea </option>
                                                        <option value="Estonia ">Estonia </option>
                                                        <option value="Ethiopia ">Ethiopia </option>
                                                        <option value="Falkland Islands">Falkland Islands</option>
                                                        <option value="Faroe Islands">Faroe Islands</option>
                                                        <option value="Fiji">Fiji</option>
                                                        <option value="Finland">Finland</option>
                                                        <option value="France">France</option>
                                                        <option value="Gabon ">Gabon </option>
                                                        <option value="Gambia">Gambia</option>
                                                        <option value="Georgia ">Georgia </option>
                                                        <option value="Germany ">Germany </option>
                                                        <option value="Ghana ">Ghana </option>
                                                        <option value="Gibraltar ">Gibraltar </option>
                                                        <option value="Great Britain ">Great Britain </option>
                                                        <option value="Greece ">Greece </option>
                                                        <option value="Greenland ">Greenland </option>
                                                        <option value="Grenada ">Grenada </option>
                                                        <option value="Guadeloupe ">Guadeloupe </option>
                                                        <option value="Guam ">Guam </option>
                                                        <option value="Guatemala ">Guatemala </option>
                                                        <option value="Guinea ">Guinea </option>
                                                        <option value="Guinea-Bissau">Guinea-Bissau</option>
                                                        <option value="Guyana">Guyana</option>
                                                        <option value="Haiti ">Haiti</option>
                                                        <option value="Honduras ">Honduras </option>
                                                        <option value="Hong Kong">Hungary</option>
                                                        <option value="Iceland ">Iceland </option>
                                                        <option value="Indonesia ">Indonesia </option>
                                                        <option value="India">India</option>
                                                        <option value="Iran ">Iran </option>
                                                        <option value="Iraq ">Iraq </option>
                                                        <option value="Ireland ">Ireland </option>
                                                        <option value="Israel ">Israel </option>
                                                        <option value="Italy ">Italy </option>
                                                        <option value="Jamaica ">Jamaica </option>
                                                        <option value="Japan  ">Japan </option>
                                                        <option value="Jordan ">Jordan </option>
                                                        <option value="Kazakhstan ">Kazakhstan </option>
                                                        <option value="Kenya ">Kenya </option>
                                                        <option value="Kiribati ">Kiribati</option>
                                                        <option value="Korea,Democratic People's Rep.">Korea,Democratic People's Rep.</option>
                                                        <option value="Korea, Republic of (South Korea)">Korea, Republic of (South Korea)</option>
                                                        <option value="Kosovo ">Kosovo </option>
                                                        <option value="Kuwait ">Kuwait</option>
                                                        <option value="Kyrgyzstan ">Kyrgyzstan</option>
                                                        <option value="Lao">Lao</option>
                                                        <option value="Laos">Laos </option>
                                                        <option value="Latvia ">Latvia </option>
                                                        <option value="Lebanon ">Lebanon </option>
                                                        <option value="Lesotho ">Lesotho </option>
                                                        <option value="Liberia ">Liberia </option>
                                                        <option value="Libya ">Libya </option>
                                                        <option value="Liechtenstein ">Liechtenstein </option>
                                                        <option value="Lithuania ">Lithuania </option>
                                                        <option value="Luxembourg ">Luxembourg </option>
                                                        <option value="Macau ">Macau </option>
                                                        <option value="Macedonia">Macedonia</option>
                                                        <option value="Madagascar ">Madagascar </option>
                                                        <option value="Malawi ">Malawi </option>
                                                        <option value="Malaysia ">Malaysia </option>
                                                        <option value="Maldives ">Maldives </option>
                                                        <option value="Mali ">Mali </option>
                                                        <option value="Malta ">Malta </option>
                                                        <option value="Marshall Islands">Marshall Islands</option>
                                                        <option value="Martinique ">Martinique </option>
                                                        <option value="Mauritania ">Mauritania </option>
                                                        <option value="Mauritius ">Mauritius </option>
                                                        <option value="Mayotte ">Mayotte </option>
                                                        <option value="Mexico ">Mexico </option>
                                                        <option value="Micronesia">Micronesia</option>
                                                        <option value="Moldova">Moldova</option>
                                                        <option value="Monaco ">Monaco </option>
                                                        <option value="Mongolia ">Mongolia </option>
                                                        <option value="Montenegro ">Montenegro </option>
                                                        <option value="Montserrat ">Montserrat </option>
                                                        <option value="Morocco ">Morocco </option>
                                                        <option value="Mozambique ">Mozambique </option>
                                                        <option value="Myanmar">Myanmar</option>
                                                        <option value="Namibia ">Namibia </option>
                                                        <option value="Nauru ">Nauru </option>
                                                        <option value="Nepal ">Nepal </option>
                                                        <option value="Netherlands ">Netherlands </option>
                                                        <option value="New Caledonia">New Caledonia</option>
                                                        <option value="New Zealand">New Zealand</option>
                                                        <option value="Nicaragua ">Nicaragua </option>
                                                        <option value="Niger ">Niger </option>
                                                        <option value="Nigeria ">Nigeria </option>
                                                        <option value="Niue ">Niue </option>
                                                        <option value="Northern Mariana Islands">Northern Mariana Islands</option>
                                                        <option value="Norway">Norway</option>
                                                        <option value="Oman ">Oman</option>
                                                        <option value="Pakistan ">Pakistan </option>
                                                        <option value="Palau ">Palau </option>
                                                        <option value="Palestinian territories">Palestinian territories</option>
                                                        <option value="Panama ">Panama </option>
                                                        <option value="Papua New Guinea">Papua New Guinea</option>
                                                        <option value="Paraguay">Paraguay</option>
                                                        <option value="Peru ">Peru </option>
                                                        <option value="Philippines ">Philippines </option>
                                                        <option value="Pitcairn Island">Pitcairn Island</option>
                                                        <option value="Poland">Poland</option>
                                                        <option value="Portugal ">Portugal </option>
                                                        <option value="Puerto Rico">Puerto Rico</option>
                                                        <option value="Qatar ">Qatar </option>
                                                        <option value="Reunion Island">Reunion Island</option>
                                                        <option value="Romania ">Romania </option>
                                                        <option value="Russian Federation">Russian Federation</option>
                                                        <option value="Rwanda ">Rwanda </option>
                                                        <option value="Saint Kitts">Saint Kitts</option>
                                                        <option value="Saint Lucia">Saint Lucia</option>
                                                        <option value="Saint Vincent and the Grenadines">Saint Vincent and the Grenadines</option>
                                                        <option value="Samoa ">Samoa </option>
                                                        <option value="San Marino">San Marino</option>
                                                        <option value="Sao Tome and Principe">Sao Tome and Principe</option>
                                                        <option value="Saudi Arabia">Saudi Arabia</option>
                                                        <option value="Senegal">Senegal</option>
                                                        <option value="Serbia">Serbia</option>
                                                        <option value="Seychelles ">Seychelles </option>
                                                        <option value="Sierra Leone">Sierra Leone</option>
                                                        <option value="Singapore ">Singapore </option>
                                                        <option value="Slovakia">Slovakia</option>
                                                        <option value="Slovenia ">Slovenia </option>
                                                        <option value="Solomon Islands">Solomon Islands</option>
                                                        <option value="Somalia">Somalia</option>
                                                        <option value="South Africa">South Africa</option>
                                                        <option value="South Sudan">South Sudan</option>
                                                        <option value="Spain">Spain</option>
                                                        <option value="Spratly Islands">Spratly Islands</option>
                                                        <option value="Sri Lanka">Sri Lanka</option>
                                                        <option value="Sudan">Sudan</option>
                                                        <option value="Suriname">Suriname</option>
                                                        <option value="Swaziland ">Swaziland </option>
                                                        <option value="Sweden">Sweden</option>
                                                        <option value="Switzerland">Switzerland</option>
                                                        <option value="Syria">Syria</option>
                                                        <option value="Taiwan">Taiwan</option>
                                                        <option value="Tajikistan">Tajikistan</option>
                                                        <option value="Tanzania">Tanzania</option>
                                                        <option value="Thailand ">Thailand </option>
                                                        <option value="Tibet ">Tibet </option>
                                                        <option value="Togo ">Togo </option>
                                                        <option value="Tokelau ">Tokelau </option>
                                                        <option value="Tonga ">Tonga </option>
                                                        <option value="Trinidad and Tobago ">Trinidad and Tobago </option>
                                                        <option value="Tunisia ">Tunisia </option>
                                                        <option value="Turkey ">Turkey </option>
                                                        <option value="Turkmenistan ">Turkmenistan </option>
                                                        <option value="Turks and Caicos Islands ">Turks and Caicos Islands </option>
                                                        <option value="Tuvalu ">Tuvalu </option>
                                                        <option value="Uganda ">Uganda</option>
                                                        <option value="Ukraine ">Ukraine</option>
                                                        <option value="United Arab Emirates">United Arab Emirates</option>
                                                        <option value="United Kingdom">United Kingdom</option>
                                                        <option value="United States of America">United States of America</option>
                                                        <option value="Uruguay ">Uruguay </option>
                                                        <option value="Uzbekistan ">Uzbekistan </option>
                                                        <option value="Vanuatu ">Vanuatu </option>
                                                        <option value="Vatican City">Vatican City</option>
                                                        <option value="Venezuela ">Venezuela </option>
                                                        <option value="Vietnam ">Vietnam </option>
                                                        <option value="Virgin Islands">Virgin Islands</option>
                                                        <option value="Wallis and Futuna Islands">Wallis and Futuna Islands</option>
                                                        <option value="Western Sahara">Western Sahara</option>
                                                        <option value="West Bank ">West Bank </option>
                                                        <option value="Yemen ">Yemen</option>
                                                        <option value="Zambia ">Zambia </option>
                                                        <option value="Zimbabwe ">Zimbabwe </option>
                                                    </select>
                                                    <span class="errorMessage" id="msg_country"></span>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-4 control-label">Phone:</label>
                                                <div class="col-md-8">
                                                    <input placeholder="Phone" id="phone" name="data[phone]" class="form-control" type="text" value="<?php echo $user_info['phone']; ?>">
                                                    <span class="errorMessage" id="msg_phone"></span>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <div class="col-lg-offset-2 col-lg-10">
                                                    <button class="btn btn-sm btn-default" type="submit" onclick="return profileValidation();">Update</button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                    <div class="col-md-6"></div>
                                </div>
                                <div role="tabpanel" class="tab-pane" id="messages"><br />
                                    <div class="col-md-6">
                                        <form action="<?php echo admin_url(); ?>/profile/change_password.html" method="post" class="form-horizontal ls_form ls_form_horizontal">
                                            <div class="form-group">
                                                <label class="col-md-4 control-label">Old Password: </label>

                                                <div class="col-md-8">
                                                    <input placeholder="Old Password" name="old_password" class="form-control" type="password" id="old_password">
                                                    <span class="errorMessage" id="msg_old_password"></span>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-4 control-label">New Password: </label>

                                                <div class="col-md-8">
                                                    <input placeholder="New Password" name="new_password" class="form-control" type="password" id="new_password">
                                                    <span class="errorMessage" id="msg_new_password"></span>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-4 control-label">Confirm Password: </label>

                                                <div class="col-md-8">
                                                    <input placeholder="Confirm Password" name="confirm_password" class="form-control" type="password" id="confirm_password">
                                                    <span class="errorMessage" id="msg_confirm_password"></span>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-lg-offset-2 col-lg-10">
                                                    <button class="btn btn-sm btn-default" type="submit" onclick="return changePasswordValidation();">Update</button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                    <div class="col-md-6"></div>
                                </div>
                                <div role="tabpanel" class="tab-pane" id="settings">
                                    <div class="col-md-8">
                                        <div class="row ls_divider last">
                                            <form id="formid2" class="form-horizontal ls_form ls_form_horizontal" method="post" action="<?php echo admin_url(); ?>/profile/update_profile_picture.html"  enctype="multipart/form-data">
                                                <div class="form-group">
                                                    <label class="col-md-3 control-label">Upload Profile Picture</label>

                                                    <div class="col-md-9 ls-group-input">
                                                        <input type="file" name="profile_picture" class="form-control" id="profile_picture">
                                                        <input type="hidden" name="prev_img_name" value="<?php echo $user_info['profile_picture']; ?>">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="col-lg-offset-3 col-lg-9">
                                                        <button class="btn btn-sm btn-default" type="submit">Update</button>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>

                                    <div class="col-md-4">
                                        <!--edit_avater_image-->
                                        <div class="edit_avater_image">
                                            <img alt="profile" src="<?= (isset($user_info['profile_picture']) && !empty($user_info['profile_picture'])? base_url('uploaded_files/profile_pictures/thumb/'.$user_info['profile_picture']):base_url('admin_assets/images/userimage/avatar2-80.png') ); ?>" onerror="this.src='<?= (isset($user_info['profile_picture']) && !empty($user_info['profile_picture'])? base_url('uploaded_files/profile_pictures/thumb/'.$user_info['profile_picture']):base_url('admin_assets/images/userimage/avatar2-80.png')  ); ?>'">
                                        </div>
                                        <!--edit_avater_image-->
                                    </div>

                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
            <!-- Main Content Element  End-->
        </div>
    </div>
</section>
<!--Page main section end -->
<script type="text/javascript">
    document.forms['edit_profile_info'].elements['country'].value = '<?php echo $user_info['country']; ?>';
</script>