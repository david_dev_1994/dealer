<!--Page main section start-->
<section id="min-wrapper">
    <div id="main-content">
        <div class="container-fluid">


            <div class="row">
                <div class="col-md-12">
                    <!--Top header start-->
                    <h3 class="ls-top-header">Dashboard</h3>
                    <!--Top header end -->

                    <!--Top breadcrumb start -->
                    <ol class="breadcrumb">
                        <li><a href="<?= base_url('admin/dashboard'); ?>"><i class="fa fa-home"></i></a></li>
                        <li class="active">Dashboard</li>
                    </ol>
                    <!--Top breadcrumb start -->
                </div>
            </div>
            <!-- Main Content Element  Start-->

            <div>
                <?php
                $msg = $this->session->userdata('message');
                $error = $this->session->userdata('error');

                if ($msg) {
                    echo "<div class='alert alert-success'>" . $msg . "</div>";
                    $this->session->unset_userdata('message');
                } else if ($error) {
                    echo "<div class='alert alert-danger'>" . $error . "</div>";
                    $this->session->unset_userdata('error');
                }
                ?>
            </div>

            <div class="row home-row-top">
                <div class="col-md-12">
                    <div class="panel panel-success">
                        <div class="panel-heading">
                            <h3 class="panel-title">User Dashboard </h3>
                        </div>
                        <div class="panel-body">
                            <h1>Welcome to dashboard </h1>
                        </div>
                    </div>
                </div>
            </div>

            <?php if ($this->session->userdata('access_label') < 3) { ?>
                <div class="row home-row-top">
                    <div class="col-md-12">
                        <div class="panel panel-success">
                            <div class="row" style="background: #294e6e !important;color: #ffffff;margin: 0;">
                            <div class="col-lg-6 col-md-6 panel-heading">
                                <h3 class="panel-title">User Dashboard </h3>
                            </div>
                            <div class="col-lg-6 col-md-6 panel-btn">
                                <?php if(!isset($get_by_30)){ ?>
                                <a href="/dealer/admin/dashboard/index/1" class="btn btn-sm btn-primary float-right">Last 30 Days</a>
                                <?php }else{ ?>
                                    <a href="/dealer/admin/dashboard/index" class="btn btn-sm btn-primary float-right">View All</a>
                                <?php } ?>
                            </div>
                            </div>
                            <div class="panel-body">

                                <div class="row">

                                    <div class="col-md-4 col-sm-4 col-xs-12">
                                        <div class="pie-widget">
                                            <div id="pie-widget-1" class="chart-pie-widget" data-percent="80">
                                                <span class="pie-widget-count-1 pie-widget-count"></span>
                                            </div>
                                            <p>
                                                Registered Dealers
                                            </p>
                                            <h5><i class="fa fa-child"></i> <?php echo $total_dealers; ?></h5>
                                        </div>
                                    </div>
                                    <div class="col-md-4 col-sm-4 col-xs-12">
                                        <div class="pie-widget">
                                            <div id="pie-widget-2" class="chart-pie-widget" data-percent="80">
                                                <span class="pie-widget-count-2 pie-widget-count"></span>
                                            </div>
                                            <p>
                                                Active Registered Dealers
                                            </p>
                                            <h5><i class="fa fa-child"></i> <?php echo $active_dealers; ?></h5>
                                        </div>
                                    </div>
                                    <div class="col-md-4 col-sm-4 col-xs-12">
                                        <div class="pie-widget">
                                            <div id="pie-widget-3" class="chart-pie-widget" data-percent="90">
                                                <span class="pie-widget-count-3 pie-widget-count"></span>
                                            </div>
                                            <p>
                                                Total Orders Dollars
                                            </p>
                                            <h5><i class="fa fa-dollar"></i> <?php echo number_format($total_dollars[0]); ?> </h5>

                                        </div>
                                    </div>
                                    <div class="col-md-4 col-sm-4 col-xs-12">
                                        <div class="pie-widget">
                                            <div id="pie-widget-5" class="chart-pie-widget" data-percent="90">
                                                <span class="pie-widget-count-5 pie-widget-count"></span>
                                            </div>
                                            <p>
                                                Total Drafted Quote Dollars
                                            </p>
                                            <h5><i class="fa fa-dollar"></i> <?php echo number_format($drafted_total_dollars[0]); ?> </h5>

                                        </div>
                                    </div>
                                    
                                    <div class="col-md-4 col-sm-4 col-xs-12">
                                        <div class="pie-widget">
                                            <div id="pie-widget-4" class="chart-pie-widget" data-percent="90">
                                                <span class="pie-widget-count-4 pie-widget-count"></span>
                                            </div>
                                            <p>
                                                Total Submitted Quotes
                                            </p>
                                            <h5><i class="fa fa-quote-left"></i> <?php echo $open_quote['count'];//$open_quote ?> </h5>

                                        </div>
                                    </div>
                                    <div class="col-md-4 col-sm-4 col-xs-12">
                                        <div class="pie-widget">
                                            <div id="pie-widget-6" class="chart-pie-widget" data-percent="90">
                                                <span class="pie-widget-count-6 pie-widget-count"></span>
                                            </div>
                                            <p>
                                                Total Drafted Quotes
                                            </p>
                                            <h5><i class="fa fa-quote-left"></i> <?php echo $open_drafted_quote['count'];//$open_drafted_quote ?> </h5>

                                        </div>
                                    </div>

<!--                                    <div class="col-md-4 col-sm-4 col-xs-12">-->
<!--                                        <div class="pie-widget">-->
<!--                                            <div id="pie-widget-3" class="chart-pie-widget" data-percent="50">-->
<!--                                                <span class="pie-widget-count-3 pie-widget-count"></span>-->
<!--                                            </div>-->
<!--                                            <p>-->
<!--                                                User-->
<!--                                            </p>-->
<!--                                            <h5><i class="fa fa-child"></i> --><?php //echo $t_user; ?><!-- </h5>-->
<!---->
<!--                                        </div>-->
<!--                                    </div>-->
                                </div>

                                <br>

                                <!--<div class="row">-->


                                <!--    <div class="col-md-4 col-sm-4 col-xs-12">-->
                                <!--        <div class="pie-widget">-->
                                <!--            <div id="pie-widget-3" class="chart-pie-widget" data-percent="90">-->
                                <!--                <span class="pie-widget-count-3 pie-widget-count"></span>-->
                                <!--            </div>-->
                                <!--            <p>-->
                                <!--                Total Dollars-->
                                <!--            </p>-->
                                <!--            <h5><i class="fa fa-dollar"></i> <?php// echo number_format($total_dollars[0]); ?> </h5>-->

                                <!--        </div>-->
                                <!--    </div>-->
                                <!--</div>-->
                            </div>
                        </div>
                    </div>
                </div>
                <div>
                   
                    <!--<button class="open-orders btn btn-success">See Orders</button>-->
                    <button class="open-drafted-orders btn btn-primary">See Drafted Quotes</button>
                    
                    <br><br>

                    <div class="open-order-table hidden">
                        <!--<a  onclick="printorders();"class="btn btn-warning pull-right" style="margin: 10px" >Download PDF</a>-->
                        <table class="table table-bordered table-striped " id="order-table-id">
                            <tr>
                                <th>Quotes</th>
                                <th>Name</th>
                                <th>Email</th>
                                <th>Company</th>
                                <th>Sales Manager</th>
                            </tr>
                            <?php
                            // echo "<pre>";
                            // print_r($open_quote['data']);
                            // die();
                            ?>
                            <?php foreach ($open_quote['data'] as $order): ?>
                            <tr>
                                <td><?php echo $order['id'] ?></td>
                                <td><?php echo $order['name'] ?></td>
                                <td><?php echo $order['email'] ?></td>
                                <td><?php echo $order['company'] ?></td>
                                <td><?php echo $order['sales_person'] ?></td>
                            </tr>
                            <?php endforeach;  ?>
                        </table>
                    </div>

                    <div class="open-drafted-table hidden">
                        <!--<a  onclick="printdraftedquotes();"class="btn btn-warning pull-right" style="margin: 10px" >Download PDF</a>-->
                        <table class="table table-bordered table-striped " id="drafted-quote-table-id">
                            <thead>
                            <tr>
                                <th>Quotes</th>
                                <th>Name</th>
                                <th>Email</th>
                                <th>Company</th>
                                <th>Sales Manager</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>

                            <?php foreach ($open_drafted_quote['data'] as $order): ?>
                                <tr>
                                    <td data-id="<?php echo $order['id'] ?>" data-user-id="<?php echo $order['user_id'] ?>" class="table_header"><b><?php echo $order['id'] ?></b></td>
                                    <td><?php echo $order['name'] ?></td>
                                    <td><?php echo $order['email'] ?></td>
                                    <td><?php echo $order['company'] ?></td>
                                    <td><?php echo $order['sales_person'] ?></td>
                                    <td><a href="<?php echo base_url(); ?>admin/quote/show/<?= $order['id']; ?>" target="_blank" class="btn btn-info">View</a></td>
                                </tr>
                            <?php endforeach;  ?>
                            </tbody>
                        </table>
                    </div>

                </div>

                <div class="row home-row-top bottom-widgets">
                    <div class="col-md-4">
                        <div class="current-status-widget">
                            <ul>
                                <li>
                                    <div class="status-box">
                                        <div class="status-box-icon label-light-green white">
                                            <i class="fa fa-shopping-cart"></i>
                                        </div>
                                    </div>
                                    <div class="status-box-content">
                                        <h5 id="sale-view"><?php echo $total_user; ?></h5>
                                        <p class="lightGreen"><i class="fa fa-arrow-up lightGreen"></i> Total sold</p>
                                    </div>
                                    <div class="clearfix"></div>
                                </li>
                                <li>
                                    <div class="status-box">
                                        <div class="status-box-icon label-red white">
                                            <i class="fa fa-download"></i>
                                        </div>
                                    </div>
                                    <div class="status-box-content">
                                        <h5 id="download-show">5340</h5>
                                        <p class="light-blue"><i class="fa fa-arrow-down light-blue"></i> Total download
                                        </p>
                                    </div>
                                    <div class="clearfix"></div>
                                </li>
                                <li>
                                    <div class="status-box">
                                        <div class="status-box-icon label-lightBlue white">
                                            <i class="fa fa-truck"></i>
                                        </div>
                                    </div>
                                    <div class="status-box-content">
                                        <h5 id="deliver-show">10490</h5>
                                        <p class="light-blue"><i class="fa fa-arrow-up light-blue"></i> Product
                                            delivered</p>
                                    </div>
                                    <div class="clearfix"></div>
                                </li>
                                <li>
                                    <div class="status-box">
                                        <div class="status-box-icon label-light-green white">
                                            <i class="fa fa-users"></i>
                                        </div>
                                    </div>
                                    <div class="status-box-content">
                                        <h5 id="user-show">132129</h5>
                                        <p class="lightGreen"><i class="fa fa-arrow-up lightGreen"></i> Total users</p>
                                    </div>
                                    <div class="clearfix"></div>
                                </li>
                                <li>
                                    <div class="status-box">
                                        <div class="status-box-icon label-success white">
                                            <i class="fa fa-github"></i>
                                        </div>
                                    </div>
                                    <div class="status-box-content">
                                        <h5 id="product-up">29</h5>
                                        <p class="text-success"><i class="fa fa-arrow-up text-success"></i> Uploaded
                                            project</p>
                                    </div>
                                    <div class="clearfix"></div>
                                </li>
                                <li>
                                    <div class="status-box">
                                        <div class="status-box-icon label-light-green white">
                                            <i class="fa fa-dollar"></i>
                                        </div>
                                    </div>
                                    <div class="status-box-content">
                                        <h5 id="income-show">10299 </h5>
                                        <p class="lightGreen"><i class="fa fa-arrow-up lightGreen"></i> Total income</p>
                                    </div>
                                    <div class="clearfix"></div>
                                </li>
                            </ul>
                        </div>

                    </div>

                </div>
                <!-- Main Content Element  End-->
            <?php } ?>
        </div>
    </div>


</section>
<!-- Modal -->
<div class="modal fade" id="quoteModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog" style="width:800px;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="myModalLabel">Quote Information</h4>
            </div>
            <div class="modal-body">

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
<!--                <button type="button" class="btn btn-primary">Save changes</button>-->
            </div>
        </div>
    </div>
</div>
<style>
    .panel-btn{
        display: flex;
        justify-content: flex-end;
        padding-right: 30px;
    }
    .panel-btn a{
        padding: 5px !important;
        font-size: 10px !important;
        margin-top: 5px;
        border-radius: 4px !important;
    }
    td.table_header.sorting_1 {
        cursor: pointer;
    }
    td.table_header:hover {
        color: #22f !important;
    }
</style>
<!--Page main section end -->

