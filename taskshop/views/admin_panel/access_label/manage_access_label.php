<!--Page main section start-->
<section id="min-wrapper">
    <div id="main-content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <!--Top header start-->
                    <h3 class="ls-top-header">Access Levels</h3>
                    <!--Top header end -->

                    <!--Top breadcrumb start -->
                    <ol class="breadcrumb">
                        <li><a href="#"><i class="fa fa-home"></i></a></li>
                        <li class="active">Level</li>
                    </ol>
                    <!--Top breadcrumb start -->
                </div>
            </div>
            <!-- Main Content Element  Start-->



            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title">Modify levels</h3>
                        </div>
                        <div class="panel-body">
                            <!--Table Wrapper Start-->
                            <div class="table-responsive ls-table">
                                <div>
                                <?php
                                $msg = $this->session->userdata('message');
                                $error = $this->session->userdata('error');

                                if ($msg) {
                                    echo "<div class='alert alert-success'>" . $msg . "</div>";
                                    $this->session->unset_userdata('message');
                                } else if ($error) {
                                    echo "<div class='alert alert-danger'>" . $error . "</div>";
                                    $this->session->unset_userdata('error');
                                }
                                ?>
                            </div>
                                <table class="table table-bordered table-striped">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Level Name</th>
                                            <th>Level</th>
                                            <th class="text-center">Privilege</th>
                                        </tr>
                                    </thead>
                                    <tbody>

                                        <?php foreach ($access_info as $v_access_info) { ?>
                                            <tr>
                                                <td><?php echo $v_access_info['access_lebel_id']; ?></td>
                                                <td>
                                                    <?php
                                                    if ($v_access_info['access_lebel'] == 1) {
                                                        echo "<span class='label label-info'>" . $v_access_info['access_lebel_name'] . "</span>";
                                                    } else if ($v_access_info['access_lebel'] == 2) {
                                                        echo "<span class='label label-warning'>" . $v_access_info['access_lebel_name'] . "</span>";
                                                    } else if ($v_access_info['access_lebel'] == 3) {
                                                        echo "<span class='label label-danger'>" . $v_access_info['access_lebel_name'] . "</span>";
                                                    }
                                                    ?>
                                                </td>
                                                <td><?php echo $v_access_info['access_lebel']; ?></td>
                                                <td class="text-center">
                                                    <button type="button" class="btn btn-xs btn-warning" data-toggle="modal" data-target="#myModal<?php echo $v_access_info['access_lebel_id']; ?>">
                                                            <i class="fa fa-pencil-square-o"></i>
                                                        </button>
                                                </td>
                                            </tr>
                                        <?php } ?>

                                    </tbody>
                                </table>
                                <!-- Button trigger modal -->
                            </div>
                            <!--Table Wrapper Finish-->
                        </div>
                    </div>
                </div>
            </div>

            <!-- Main Content Element  End-->

        </div>
    </div>



</section>
<!--Page main section end -->


<?php foreach ($access_info as $v_access_info) { ?>
<!-- Modal -->
<div class="modal fade" id="myModal<?php echo $v_access_info['access_lebel_id']; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Privilege</h4>
            </div>
            <form class="form-2" method="post" action="<?php echo admin_url(); ?>/access_label/update_privilege/<?php echo $v_access_info['access_lebel_id']; ?>">
                <div class="modal-body">
                    <div class="checkbox">
                        <label>
                            <input type="checkbox" name="block_or_suspend" value="1" <?php if($v_access_info['block_or_suspend'] != 0){echo "checked='checked'"; }?>>
                            Active Block or Suspend
                        </label>
                    </div>
                    <div class="checkbox">
                        <label>
                            <input type="checkbox" name="edit" value="1" <?php if($v_access_info['edit'] != 0){echo "checked='checked'"; }?>>
                            Edit
                        </label>
                    </div>
                    <div class="checkbox">
                        <label>
                            <input type="checkbox" name="delete" value="1" <?php if($v_access_info['delete'] != 0){echo "checked='checked'"; }?>>
                            Delete
                        </label>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Update</button>
                </div>
            </form>
        </div>
    </div>
</div>
<?php } ?>