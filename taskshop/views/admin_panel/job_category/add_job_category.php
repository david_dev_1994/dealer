<style type="text/css">
    .errorMesage{
        color: red;
    }
</style>
<!--Page main section start-->
<section id="min-wrapper">
    <div id="main-content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <!--Top header start-->
                    <h3 class="ls-top-header">Add Job Category</h3>
                    <!--Top header end -->

                    <!--Top breadcrumb start -->
                    <ol class="breadcrumb">
                        <li><a href="#"><i class="fa fa-home"></i></a></li>
                        <li class="active">Add Job Category</li>
                    </ol>
                    <!--Top breadcrumb start -->
                </div>
            </div>
            <!-- Main Content Element  Start-->

            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title">Add Job Category</h3>
                        </div>
                        <div class="panel-body">
                            <form name="add_job_category_form" id="defaultForm" method="post" class="form-horizontal ls_form" action="<?php echo admin_url(); ?>/job_category/save_job_category.html">

                                <div class="form-group">
                                    <label class="col-lg-3 control-label">Job Category Name</label>
                                    <div class="col-lg-6">
                                        <input type="text" class="form-control" name="data[job_category_name]" placeholder="Add job category name" value="<?php echo set_value('data[job_category_name]'); ?>" />
                                        <span class="errorMesage"><?php echo form_error('data[job_category_name]'); ?></span>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-lg-3 control-label">Job Category Description</label>
                                    <div class="col-lg-6">
                                        <textarea name="data[job_category_description]" class="summernote" placeholder="Add job category description"><?php echo set_value('data[job_category_description]'); ?></textarea>
                                        <span class="errorMesage"><?php echo form_error('data[job_category_description]'); ?></span>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="publication_status" class="col-lg-3 control-label">Publication Status</label>
                                    <div class="col-lg-3">
                                        <select class="form-control" name="data[publication_status]" id="publication_status">
                                            <option selected="selected" disabled="">Select One</option>
                                            <option value="1">Published</option>
                                            <option value="0">Unpublished</option>
                                        </select>
                                        <span class="errorMesage"><?php echo form_error('data[publication_status]'); ?></span>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-lg-9 col-lg-offset-3">
                                        <button type="submit" class="btn btn-primary">Add Job Category</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<script type="text/javascript">
<?php if (set_value('data[publication_status]') != null) { ?>
        document.forms['add_job_category_form'].elements['publication_status'].value = '<?php echo set_value('data[publication_status]'); ?>';
<?php } ?>
</script>