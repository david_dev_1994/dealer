<style type="text/css">
    .errorMesage{
        color: red;
    }
</style>
<!--Page main section start-->
<section id="min-wrapper">
    <div id="main-content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <!--Top header start-->
                    <h3 class="ls-top-header">Edit Job Category</h3>
                    <!--Top header end -->

                    <!--Top breadcrumb start -->
                    <ol class="breadcrumb">
                        <li><a href="#"><i class="fa fa-home"></i></a></li>
                        <li class="active">Edit Job Category</li>
                    </ol>
                    <!--Top breadcrumb start -->
                </div>
            </div>
            <!-- Main Content Element  Start-->

            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title">Edit Job Category</h3>
                        </div>
                        <div class="panel-body">
                            <div>
                                <?php
                                $msg = $this->session->userdata('message');
                                $error = $this->session->userdata('error');

                                if ($msg) {
                                    echo "<div class='alert alert-success text-center'><i class='fa fa-check-circle-o fa-5x'></i>" . $msg . "</div>";
                                    $this->session->unset_userdata('message');
                                } else if ($error) {
                                    echo "<div class='alert alert-danger text-center'><i class='fa fa-ban fa-5x'></i>" . $error . "</div>";
                                    $this->session->unset_userdata('error');
                                }
                                ?>
                            </div>
                            <form name="edit_job_category_form" id="defaultForm" method="post" class="form-horizontal ls_form" action="<?php echo admin_url(); ?>/job_category/update_job_category.html">

                                <div class="form-group">
                                    <label class="col-lg-3 control-label">Job Category Name</label>
                                    <div class="col-lg-6">
                                        <input type="text" class="form-control" name="data[job_category_name]" placeholder="Edit job_category name" value="<?php echo $job_category_info['job_category_name']; ?>" />
                                        <input type="hidden" name="job_category_id" value="<?php echo $job_category_info['job_category_id']; ?>" />
                                        <span class="errorMesage"><?php echo form_error('data[job_category_name]'); ?></span>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-lg-3 control-label">Job Category Description</label>
                                    <div class="col-lg-6">
                                        <textarea name="data[job_category_description]" class="summernote" placeholder="Edit job_category description"><?php echo $job_category_info['job_category_description']; ?></textarea>
                                        <span class="errorMesage"><?php echo form_error('data[job_category_description]'); ?></span>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="publication_status" class="col-lg-3 control-label">Publication Status</label>
                                    <div class="col-lg-3">
                                        <select class="form-control" name="data[publication_status]" id="publication_status">
                                            <option selected="selected" disabled="">Select One</option>
                                            <option value="1">Published</option>
                                            <option value="0">Unpublished</option>
                                        </select>
                                        <span class="errorMesage"><?php echo form_error('data[publication_status]'); ?></span>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-lg-9 col-lg-offset-3">
                                        <button type="submit" class="btn btn-primary">Update Job Category</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<script type="text/javascript">
    document.forms['edit_job_category_form'].elements['publication_status'].value = '<?php echo $job_category_info['publication_status']; ?>';
</script>