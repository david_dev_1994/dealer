<style type="text/css">
    .errorMesage{
        color: red;
    }
</style>
<!--Page main section start-->
<section id="min-wrapper">
    <div id="main-content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <!--Top header start-->
                    <h3 class="ls-top-header">FTP Setting</h3>
                    <!--Top header end -->

                    <!--Top breadcrumb start -->
                    <ol class="breadcrumb">
                        <li><a href="#"><i class="fa fa-home"></i></a></li>
                        <li class="active">FTP Setting</li>
                    </ol>
                    <!--Top breadcrumb start -->
                </div>
            </div>
            <!-- Main Content Element  Start-->

            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title">Update Setting</h3>
                        </div>
                        <div class="panel-body">
                            <div>
                                    <?php
                                    $msg = $this->session->userdata('message');
                                    $error = $this->session->userdata('error');

                                    if ($msg) {
                                        echo "<div class='alert alert-success text-center'><i class='fa fa-check-circle-o fa-5x'></i>" . $msg . "</div>";
                                        $this->session->unset_userdata('message');
                                    } else if ($error) {
                                        echo "<div class='alert alert-danger text-center'><i class='fa fa-ban fa-5x'></i>" . $error . "</div>";
                                        $this->session->unset_userdata('error');
                                    }
                                    ?>
                                </div>
                            <form name="update_ftp_form" id="defaultForm" method="post" class="form-horizontal ls_form" action="<?php echo admin_url(); ?>/ftp_file_manager/update_ftp_setting.html">
                                <div class="form-group">
                                    <label class="col-lg-3 control-label">Host Name</label>
                                    <div class="col-lg-6">
                                        <input type="text" class="form-control" name="data[ftp_hostname]" placeholder="Add hostname" value="<?php echo $ftp_info['ftp_hostname']; ?>" />
                                        <span class="errorMesage"><?php echo form_error('data[ftp_hostname]'); ?></span>
                                    </div>
                                </div>
                                
                                <div class="form-group">
                                    <label class="col-lg-3 control-label">Username</label>
                                    <div class="col-lg-6">
                                        <input type="text" class="form-control" name="data[ftp_username]" placeholder="Add username" value="<?php echo $ftp_info['ftp_username']; ?>" />
                                        <input type="hidden" name="ftp_id" value="<?php echo $ftp_info['ftp_id']; ?>"
                                        <span class="errorMesage"><?php echo form_error('data[ftp_username]'); ?></span>
                                    </div>
                                </div>
                                
                                <div class="form-group">
                                    <label class="col-lg-3 control-label">Password</label>
                                    <div class="col-lg-6">
                                        <input type="text" class="form-control" name="data[ftp_password]" placeholder="Add password" value="<?php echo $ftp_info['ftp_password']; ?>" />
                                        <span class="errorMesage"><?php echo form_error('data[ftp_password]'); ?></span>
                                    </div>
                                </div>
                                
                                <div class="form-group">
                                    <div class="col-lg-9 col-lg-offset-3">
                                        <button type="submit" class="btn btn-primary">Update Setting</button>
                                    </div>
                                </div>
                            </form>
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>