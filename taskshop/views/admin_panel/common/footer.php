<div id="change-color">
    <div id="change-color-control">
        <a href="javascript:void(0)"><i class="fa fa-magic"></i></a>
    </div>
    <div class="change-color-box">
        <ul>
            <li class="default active"></li>
            <li class="red-color"></li>
            <li class="blue-color"></li>
            <li class="light-green-color"></li>
            <li class="black-color"></li>
            <li class="deep-blue-color"></li>
        </ul>
    </div>
</div>
</section>

<div class="modal fade" id="confirm-delete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel">Confirm Delete</h4>
            </div>

            <div class="modal-body">
                <p>You are about to delete this record, this procedure is irreversible.Do you want to proceed?</p>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                <a class="btn btn-danger btn-ok">Delete</a>
            </div>
        </div>
    </div>
</div>


<!--Layout Script start -->
<script type="text/javascript" src="<?php echo base_url(); ?>admin_assets/js/color.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>admin_assets/js/lib/jquery-1.11.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>admin_assets/js/bootstrap.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>admin_assets/js/multipleAccordion.js"></script>

<script src="<?php echo base_url(); ?>admin_assets/plugins/ckeditor/ckeditor.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>admin_assets/js/datatable.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>admin_assets/plugins/datatables/datatables.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>admin_assets/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>admin_assets/plugins/bootstrap-fileinput-master/js/fileinput.js" type="text/javascript"></script>

<!--easing Library Script Start -->
<script src="<?php echo base_url(); ?>admin_assets/js/lib/jquery.easing.js"></script>
<!--easing Library Script End -->

<!--Nano Scroll Script Start -->
<script src="<?php echo base_url(); ?>admin_assets/js/jquery.nanoscroller.min.js"></script>
<!--Nano Scroll Script End -->

<!--switchery Script Start -->
<script src="<?php echo base_url(); ?>admin_assets/js/switchery.min.js"></script>
<!--switchery Script End -->

<!--bootstrap switch Button Script Start-->
<script src="<?php echo base_url(); ?>admin_assets/js/bootstrap-switch.js"></script>
<!--bootstrap switch Button Script End-->

<!--easypie Library Script Start -->
<script src="<?php echo base_url(); ?>admin_assets/js/jquery.easypiechart.min.js"></script>
<!--easypie Library Script Start -->

<!--bootstrap-progressbar Library script Start-->
<script src="<?php echo base_url(); ?>admin_assets/js/bootstrap-progressbar.min.js"></script>
<!--bootstrap-progressbar Library script End-->

<script type="text/javascript" src="<?php echo base_url(); ?>admin_assets/js/pages/layout.js"></script>
<!--Layout Script End -->

<!-- Select Box Script Start -->
<script src="<?php echo base_url(); ?>admin_assets/js/selectize.min.js"></script>
<!-- Select Box Script End -->

<!--Upload button Script Start-->
<script src="<?php echo base_url(); ?>admin_assets/js/fileinput.min.js"></script>
<!--Upload button Script End-->

<!-- summernote Editor Script For Layout start-->
<script src="<?php echo base_url(); ?>admin_assets/js/summernote.min.js"></script>
<!-- summernote Editor Script For Layout End-->

<!-- Compose mail Script Start -->
<script src="<?php echo base_url(); ?>admin_assets/js/pages/composMail.js"></script>
<!-- Compose mail Script End -->


<!--Form Wizard CSS Start-->
<script src="<?php echo base_url(); ?>admin_assets/js/jquery.smartWizard.js"></script>
<!--Form Wizard CSS End-->

<!--Demo Wizard Script Start-->
<script src="<?php echo base_url(); ?>admin_assets/js/pages/formWizard.js"></script>
<!--Demo Wizard Script End-->
<script src="<?php echo base_url(); ?>admin_assets/js/countUp.min.js"></script>

<!--AmaranJS library script Start -->
<script src="<?php echo base_url(); ?>admin_assets/js/jquery.amaran.js"></script>
<!--AmaranJS library script End   -->
<script src="<?php echo base_url(); ?>admin_assets/js/pages/dashboard.js"></script>

<!--Gallery Plugin Start-->
<!-- Shuffle! -->
<script type="text/javascript" src="<?php echo base_url(); ?>admin_assets/js/gallery/shuffle.js"></script>
<!-- Syntax highlighting via Prism -->
<script  type="text/javascript" src="<?php echo base_url(); ?>admin_assets/js/gallery/prism.js"></script>
<script>var site_url = "fickle-2.html";</script>
<script type="text/javascript" src="<?php echo base_url(); ?>admin_assets/js/gallery/page.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>admin_assets/js/gallery/evenheights.js"></script>
<!--Gallery Plugin Finish-->
<script type="text/javascript" src="<?php echo base_url(); ?>admin_assets/js/lightGallery.js"></script>
<!-- Gallery Js Call Start -->
<script type="text/javascript" src="<?php echo base_url(); ?>admin_assets/js/pages/demo.gallery.js"></script>
<!-- Gallery Js Finish -->

<script src="<?php echo base_url(); ?>admin_assets/js/custom.js" type="text/javascript"></script>

<script>
if($('#drafted-quote-table-id').length > 0){
    $('body').on('click','.table_header',function(e){//
        e.preventDefault();
        var quote_id = $(this).data('id');
        var user_id = $(this).data('user-id');
        $('#quoteModal .modal-body').html('');
        $.get('/dealer/quote/getQuote/'+quote_id+'/'+user_id,function (response) {
            var data = "<table cellspacing=\"0\" cellpadding=\"3\">\n" +
                "                    <thead>\n" +
                "                    <tr>\n" +
                "                        <th style=\"background: #767676;color: #fff;padding: 10px 5px;\">Product Model</th>\n" +
                "                        <th style=\"background: #767676;color: #fff;padding: 10px 5px;\">Product Description</th>\n" +
                "                        <th style=\"background: #767676;color: #fff;padding: 10px 5px;\">Product Family</th>\n" +
                "                        <th style=\"background: #767676;color: #fff;padding: 10px 5px;\">Product Type</th>\n" +
                "                        <th style=\"background: #767676;color: #fff;padding: 10px 5px;\">List Price</th>\n" +
                "                        <th style=\"background: #767676;color: #fff;padding: 10px 5px;\">Dealer Price</th>\n" +
                "                        <th style=\"background: #767676;color: #fff;padding: 10px 5px;\">Quantity</th>\n" +
                "                        <!--                    <th>Sales Price(Unit)</th>-->\n" +
                "                        <th style=\"background: #767676;color: #fff;padding: 10px 5px;\">Total Price</th>\n" +
                "                    </tr>\n" +
                "                    </thead>\n" +



                "                    <tbody>" ;

            $.each(response, function(key, val) {
            data += "<tr>\n" +
                "<td style=\"border-right: 1px solid #767676;color: #767676;padding: 10px 5px;\">\n" +
                val.model +
                "</td>\n" +
                "<td style=\"border-right: 1px solid #767676;color: #767676;padding: 10px 5px;\">\n" +
                val.description +
                "</td>\n" +
                "<td style=\"border-right: 1px solid #767676;color: #767676;padding: 10px 5px;\">\n" +
                val.family +
                "</td>\n" +
                "<td style=\"border-right: 1px solid #767676;color: #767676;padding: 10px 5px;\">\n" +
                val.type +
                "</td>\n" +
                "<td style=\"border-right: 1px solid #767676;color: #767676;padding: 10px 5px;\">\n" +
                val.list_price +
                "</td>\n" +
                "<td style=\"border-right: 1px solid #767676;color: #767676;padding: 10px 5px;\">\n" +
                val.dealer_price +
                "</td>\n" +
                "<td style=\"border-right: 1px solid #767676;color: #767676;padding: 10px 5px;\">\n" +
                val.quantity +
                "</td>\n" +
                "<td style=\"border-right: 1px solid #767676;color: #767676;padding: 10px 5px;\">\n" +
                 val.price +
                "</td>\n" +
                "</tr>\n" ;

            });

                data += "</tbody>\n" +
                "                </table>";
                console.log(data);
            $('#quoteModal .modal-body').html(data);
            $('#quoteModal').modal('show');
        }, 'json')//
    })
}
</script>

<script>

    if($('#drafted-quote-table-id').length > 0){
        $('#drafted-quote-table-id').DataTable();
    }

    function printorders() {
        var element = document.getElementById('order-table-id');
        var opt = {
            margin: 1,
            filename: (Math.floor(Math.random() * 10000) + 1) + '.pdf',
        };
        // New Promise-based usage:
        html2pdf().set(opt).from(element).save();
    }

    function printdraftedquotes() {
        var element = document.getElementById('drafted-quote-table-id');
        var opt = {
            margin: 1,
            filename: (Math.floor(Math.random() * 10000) + 1) + '.pdf',
        };
        // New Promise-based usage:
        html2pdf().set(opt).from(element).save();
    }

    $('body').on('click','.open-orders',function(){
        // $('.open-order-table').toggle();
        // $('this').attr();
        // console.log($('.open-order-table').hasClass('hidden'));
        // return;
        if($('.open-order-table').hasClass('hidden')){
            $('.open-order-table').removeClass('hidden');
            $('.open-drafted-table').addClass('hidden');
        }else{
            $('.open-order-table').addClass('hidden');
            $('.open-drafted-table').addClass('hidden');
        }

    })

    $('body').on('click','.open-drafted-orders',function(){
        // $('.open-drafted-table').toggle();
        if( $('.open-drafted-table').hasClass('hidden')){
            $('.open-drafted-table').removeClass('hidden');
            $('.open-order-table').addClass('hidden');
        }else{
            $('.open-drafted-table').addClass('hidden');
            $('.open-order-table').addClass('hidden');
        }

    })


</script>


<?php if($menu_active == "File Manager"){ ?>
<!-- For Image Uploader  -->
<!-- JavaScript Includes -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<script src="<?php echo base_url(); ?>admin_assets/uploader_assets/js/jquery.knob.js"></script>

<!-- jQuery File Upload Dependencies -->
<script src="<?php echo base_url(); ?>admin_assets/uploader_assets/js/jquery.ui.widget.js"></script>
<script src="<?php echo base_url(); ?>admin_assets/uploader_assets/js/jquery.iframe-transport.js"></script>
<script src="<?php echo base_url(); ?>admin_assets/uploader_assets/js/jquery.fileupload.js"></script>

<!-- Our main JS file -->
<script src="<?php echo base_url(); ?>admin_assets/uploader_assets/js/script.js"></script>

<!-- Only used for the demos. Please ignore and remove. --> 
<!--<script src="http://cdn.tutorialzine.com/misc/enhance/v1.js" async></script>-->
<!-- End Image Uploader -->
<?php } ?>
</body>

<!-- Mirrored from fickle.aimmate.com/compose-mail.html by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 07 Feb 2016 13:32:13 GMT -->
</html>