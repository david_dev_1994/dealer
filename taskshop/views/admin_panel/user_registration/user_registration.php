
<!DOCTYPE html>
<html lang="en">


    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="description" content="">
        <meta name="author" content="">

        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

        <!-- Viewport metatags -->
        <meta name="HandheldFriendly" content="true" />
        <meta name="MobileOptimized" content="320" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />

        <!-- iOS webapp metatags -->
        <meta name="apple-mobile-web-app-capable" content="yes" />
        <meta name="apple-mobile-web-app-status-bar-style" content="black" />

        <!-- iOS webapp icons -->
        <link rel="apple-touch-icon-precomposed" href="<?php echo base_url(); ?>admin_assets/images/ios/fickle-logo-72.png" />
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?php echo base_url(); ?>admin_assets/images/ios/fickle-logo-72.png" />
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php echo base_url(); ?>admin_assets/images/ios/fickle-logo-114.png" />

        <!-- TODO: Add a favicon -->
        <link rel="shortcut icon" href="<?php echo base_url(); ?>admin_assets/images/ico/fab.ico">

        <title>Atique-IT - Registration</title>
        <script type="text/javascript">
            //Create a boolean variable to check for a valid Internet Explorer instance.
            var xmlhttp = false;

            //Check if we are using IE.
            try {
                //If the Javascript version is greater than 5.
                xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
            } catch (e) {
                try {
                    //If we are using Very Older Internet Explorer.
                    xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
                } catch (E) {
                    //Else we must be using a non-IE browser.
                    xmlhttp = false;
                }
            }

            //If we are using a non-IE browser, create a javascript instance of the object.
            if (!xmlhttp && typeof XMLHttpRequest != 'undefined') {
                xmlhttp = new XMLHttpRequest();
            }

            function makerequest(given_email, objID) {
                //alert(given_email);
                serverPage = '<?php echo base_url(); ?>admin/authentication/ajax_check_email/' + given_email;
                
                xmlhttp.open("GET", serverPage);
                xmlhttp.onreadystatechange = function () {
                    if (xmlhttp.readyState == 4 && xmlhttp.status === 200) {
                        document.getElementById(objID).innerHTML = xmlhttp.responseText;
                        if (xmlhttp.responseText == "<span style='color:#990000;'>Already exist</span>")
                        {
                            document.getElementById('regButton').disabled = true;
                        }
                        if (xmlhttp.responseText == "<span style='color:green;'>Avilable</span>")
                        {
                            document.getElementById('regButton').disabled = false;
                        }
                    }
                }
                xmlhttp.send(null);
            }
        </script>
        <!--Page loading plugin Start -->
        <link rel="stylesheet" href="<?php echo base_url(); ?>admin_assets/css/plugins/pace.css">
        <script src="<?php echo base_url(); ?>admin_assets/js/pace.min.js"></script>
        <!--Page loading plugin End   -->

        <!-- Plugin Css Put Here -->
        <link href="<?php echo base_url(); ?>admin_assets/css/bootstrap.min.css" rel="stylesheet">
        <link rel="stylesheet" href="<?php echo base_url(); ?>admin_assets/css/plugins/bootstrap-switch.min.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>admin_assets/css/plugins/ladda-themeless.min.css">

        <link href="<?php echo base_url(); ?>admin_assets/css/plugins/humane_themes/bigbox.css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>admin_assets/css/plugins/humane_themes/libnotify.css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>admin_assets/css/plugins/humane_themes/jackedup.css" rel="stylesheet">
        <!-- Plugin Css End -->
        <!-- Custom styles Style -->
        <link href="<?php echo base_url(); ?>admin_assets/css/style.css" rel="stylesheet">
        <!-- Custom styles Style End-->

        <!-- Responsive Style For-->
        <link href="<?php echo base_url(); ?>admin_assets/css/responsive.css" rel="stylesheet">
        <!-- Responsive Style For-->

        <!-- Responsive Style For-->
        <link href="responsive2.css" rel="stylesheet">
        <!-- Responsive Style For-->

        <!-- Custom styles for this template -->


        <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
        <style type="text/css">
            .errorMesage{
                color: #990000;
            }
        </style>
    </head>
    <body class="login-screen">
        <section>
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <div class="login-box">
                            <div class="login-content">
                                <div class="login-user-icon">
                                    <i class="glyphicon glyphicon-user"></i>
                                    <!--<img src="images/login.png" alt="Logo"/>-->
                                </div>
                                <h3>Type Yourself</h3>
                            </div>

                            <div class="login-form">

                                <form action="<?php echo base_url(); ?>admin/authentication/registration.html" method="post" class="form-horizontal ls_form">
                                    <div class="input-group ls-group-input">
                                        <input class="form-control" type="text" name="full_name" placeholder="Full Name"  value="<?php echo set_value('full_name'); ?>">
                                        <span class="input-group-addon"><i class="fa fa-user"></i></span>
                                    </div>
                                    <span class="errorMesage"><?php echo form_error('full_name'); ?></span>

                                    <div class="input-group ls-group-input">
<!--                                        <input class="form-control" type="text" name="email_address" placeholder="someone@mail.com" value="<?php //echo set_value('email_address');  ?>">-->
                                        <input class="form-control" type="email" name="email_address" required="required" placeholder="someone@mail.com" onblur="makerequest(this.value, 'res')" value="<?php echo set_value('email_address');  ?>">
                                        <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
                                    </div>
                                    <?php if(!empty(form_error('email_address'))){ ?>
                                    <span class="errorMesage"><?php echo form_error('email_address'); ?></span>
                                    <?php } ?>
                                    <span id="res"></span>

                                    <div class="input-group ls-group-input">
                                        <input type="password" placeholder="Password" name="user_password"
                                               class="form-control"  value="<?php echo set_value('user_password'); ?>">
                                        <span class="input-group-addon"><i class="fa fa-lock"></i></span>
                                    </div>
                                    <span class="errorMesage"><?php echo form_error('user_password'); ?></span>

                                    <div class="input-group ls-group-input">
                                        <input type="password" placeholder="Repeat password" name="confirm_password"
                                               class="form-control"  value="<?php echo set_value('confirm_password'); ?>">
                                        <span class="input-group-addon"><i class="fa fa-lock"></i></span>
                                    </div>
                                    <span class="errorMesage"><?php echo form_error('confirm_password'); ?></span>

                                    <div class="input-group ls-group-input">
                                        <?php echo $image; ?>
                                    </div>

                                    <div class="input-group ls-group-input">
                                        <input type="hidden" name="captcha" value="<?php echo $word; ?>">
                                        <input type="text" name="retype_captcha" class="form-control" id="retype_captcha" placeholder="">
                                        <span class="input-group-addon"><i class="fa fa-lock"></i></span>
                                    </div>
                                    <span class="errorMesage"><?php echo form_error('retype_captcha'); ?></span>

                                    <div class="input-group ls-group-input login-btn-box">
                                        <button id="regButton" type="submit" class="btn ls-dark-btn ladda-button col-md-12 col-sm-12 col-xs-12"
                                                data-style="slide-down">
                                            <span class="ladda-label">Register</span>
                                        </button>
                                    </div>
                                </form>
                                <hr />
                                <div class="input-group ls-group-input login-btn-box">
                                    <span>Already Have Account</span>
                                    <a href="<?php echo base_url(); ?>admin/authentication.html" class="btn col-md-12 col-sm-12 col-xs-12" style="background-color: rgba(47, 42, 42, 0.64) !important">
                                        <span>Login Here</span>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <p class="copy-right big-screen hidden-xs hidden-sm">
                <span>&#169;</span> Atique-IT <span class="footer-year">2016</span>
            </p>
        </section>

        <script src="<?php echo base_url(); ?>admin_assets/js/lib/jquery-2.1.1.min.js"></script>
        <script src="<?php echo base_url(); ?>admin_assets/js/lib/jquery.easing.js"></script>
        <script src="<?php echo base_url(); ?>admin_assets/js/bootstrap-switch.min.js"></script>

        <!--Loading button script-->
        <script src="<?php echo base_url(); ?>admin_assets/js/loader/spin.js"></script>
        <script src="<?php echo base_url(); ?>admin_assets/js/loader/ladda.js"></script>
        <!--Loading button script-->
        <!--Notification Script-->
        <script src="<?php echo base_url(); ?>admin_assets/js/humane.min.js"></script>
        <!--Notification Script-->


        <!--Demo registration Script-->


    </body>

</html>