<!DOCTYPE html>
<html lang="en">
    <!-- Mirrored from fickle.aimmate.com/login.html by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 07 Feb 2016 13:32:05 GMT -->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="description" content="">
        <meta name="author" content="">

        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

        <!-- Viewport metatags -->
        <meta name="HandheldFriendly" content="true" />
        <meta name="MobileOptimized" content="320" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />

        <!-- iOS webapp metatags -->
        <meta name="apple-mobile-web-app-capable" content="yes" />
        <meta name="apple-mobile-web-app-status-bar-style" content="black" />

        <!-- iOS webapp icons -->
        <link rel="apple-touch-icon-precomposed" href="<?php echo base_url(); ?>/admin_assets/images/ios/fickle-logo-72.png" />
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?php echo base_url(); ?>/admin_assets/images/ios/fickle-logo-72.png" />
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php echo base_url(); ?>/admin_assets/images/ios/fickle-logo-114.png" />

        <!-- TODO: Add a favicon -->
        <!-- <link rel="shortcut icon" href="<?php echo base_url(); ?>/admin_assets/images/ico/fab.ico"> -->
        <link rel="shortcut icon" type="image/png" href="https://www.bktechnologies.com/wp-content/uploads/2018/02/bk-favicon.png"/>

        <title>Dealers Portal - <?php echo $title; ?></title>

        <!--Page loading plugin Start -->
        <link rel="stylesheet" href="<?php echo base_url(); ?>/admin_assets/css/plugins/pace.css">
        <script src="<?php echo base_url(); ?>/admin_assets/js/pace.min.js"></script>
        <!--Page loading plugin End   -->

        <!-- Plugin Css Put Here -->
        <link href="<?php echo base_url(); ?>/admin_assets/css/bootstrap.min.css" rel="stylesheet">
        <link rel="stylesheet" href="<?php echo base_url(); ?>/admin_assets/css/plugins/bootstrap-switch.min.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>/admin_assets/css/plugins/ladda-themeless.min.css">

        <link href="<?php echo base_url(); ?>/admin_assets/css/plugins/humane_themes/bigbox.css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>/admin_assets/css/plugins/humane_themes/libnotify.css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>/admin_assets/css/plugins/humane_themes/jackedup.css" rel="stylesheet">

        <!-- Plugin Css End -->
        <!-- Custom styles Style -->
        <link href="<?php echo base_url(); ?>/admin_assets/css/style.css" rel="stylesheet">
        <!-- Custom styles Style End-->

        <!-- Responsive Style For-->
        <link href="<?php echo base_url(); ?>/admin_assets/css/responsive.css" rel="stylesheet">
        <!-- Responsive Style For-->

        <!-- Responsive Style For-->
        <link href="responsive2.css" rel="stylesheet">
        <!-- Responsive Style For-->

        <!-- Custom styles for this template -->
        <link href="<?php echo base_url(); ?>/admin_assets/css/custom.css" rel="stylesheet">

        <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body class="login-screen">
        <section>
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <div class="login-box">
                            <div class="login-content">
                                <div class="login-user-icon">
                                </div>
                                <h3>Admin Login</h3>
                                <div class="social-btn-login">
                                </div>
                            </div>

                            <div>
                                <?php
                                $msg = $this->session->userdata('message');
                                $error = $this->session->userdata('error');

                                if ($msg) {
                                    echo "<div class='alert alert-success'>" . $msg . "</div>";
                                    $this->session->unset_userdata('message');
                                } else if ($error) {
                                    echo "<div class='alert alert-danger'>" . $error . "</div>";
                                    $this->session->unset_userdata('error');
                                } else {
                                    echo "<div class='alert alert-info'>Please login with your email address and password.</div>";
                                }
                                ?>
                            </div>

                            <div class="login-form">
                                <form method="post" action="<?php echo admin_url(); ?>/authentication/check_admin_login.html"  class="form-horizontal ls_form" data-toggle="validator" role="form">
                                    <div class="input-group ls-group-input">
                                        <input class="form-control" type="text" placeholder="Email Address" name="email_address">
                                        <span class="input-group-addon"><i class="fa fa-user"></i></span>
                                    </div>


                                    <div class="input-group ls-group-input">

                                        <input type="password" placeholder="Password" name="user_password"
                                               class="form-control" value="">
                                        <span class="input-group-addon"><i class="fa fa-lock"></i></span>
                                    </div>

                                    <div class="remember-me">
                                        <input class="switchCheckBox" type="checkbox" checked data-size="mini"
                                               data-on-text="<i class='fa fa-check'><i>"
                                               data-off-text="<i class='fa fa-times'><i>">
                                        <span>Remember me</span>
                                    </div>
                                    <div class="input-group ls-group-input login-btn-box">
                                        <button type="submit" class="btn ls-dark-btn ladda-button col-md-12 col-sm-12 col-xs-12" data-style="slide-down">
                                            <span class="ladda-label"><i class="fa fa-key"></i></span>
                                        </button>

                                        <a class="forgot-password" href="javascript:void(0)">Forgot password</a>
                                    </div>
                                </form>
                            </div>


                            <div class="forgot-pass-box">
                                <form action="#" class="form-horizontal ls_form">
                                    <div class="input-group ls-group-input">
                                        <input class="form-control" type="text" placeholder="someone@mail.com">
                                        <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
                                    </div>
                                    <div class="input-group ls-group-input login-btn-box">
                                        <button class="btn ls-dark-btn col-md-12 col-sm-12 col-xs-12">
                                            <i class="fa fa-rocket"></i> Send Email
                                        </button>

                                        <a class="login-view" href="javascript:void(0)">Login</a> & <a href="<?php echo base_url(); ?>admin/authentication/user_registration.html">Registration</a>

                                    </div>

                                </form>
                            </div><hr />
                            <!-- 
                            <div class="login-form">
                                <div class="input-group ls-group-input login-btn-box">
                                    <span>Don't Have Account</span>
                                    <a href="<?php echo base_url(); ?>admin/authentication/user_registration.html" class="btn col-md-12 col-sm-12 col-xs-12" style="background-color: rgba(47, 42, 42, 0.64) !important">
                                        <span>Registration a</span>
                                    </a>
                                </div>
                            </div>
                            -->

                        </div>
                    </div>
                </div>
            </div>
            <!-- <p class="copy-right big-screen hidden-xs hidden-sm">
                <span>&#169;</span> BK Technologies <span class="footer-year">2018</span>
            </p> -->
        </section>
        <script src="<?php echo base_url(); ?>/admin_assets/js/lib/jquery-2.1.1.min.js"></script>
        <script src="<?php echo base_url(); ?>/admin_assets/js/lib/jquery.easing.js"></script>
        <script src="<?php echo base_url(); ?>/admin_assets/js/bootstrap-switch.min.js"></script>
        <!--Script for notification start-->
        <script src="<?php echo base_url(); ?>/admin_assets/js/loader/spin.js"></script>
        <script src="<?php echo base_url(); ?>/admin_assets/js/loader/ladda.js"></script>
        <script src="<?php echo base_url(); ?>/admin_assets/js/humane.min.js"></script>
        <!--Script for notification end-->

        <script src="<?php echo base_url(); ?>/admin_assets/js/pages/login.js"></script>

    </body>

</html>