<!--Page main section start-->
<section id="min-wrapper">
    <div id="main-content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <!--Top header start-->
                    <h3 class="ls-top-header">Inbox</h3>
                    <!--Top header end -->

                    <!--Top breadcrumb start -->
                    <ol class="breadcrumb">
                        <li><a href="javascript:void(0)"><i class="fa fa-home"></i></a></li>
                        <li>Email</li>
                        <li class="active">Inbox</li>
                    </ol>
                    <!--Top breadcrumb start -->
                </div>
            </div>
            <!-- Main Content Element  Start-->
            <div class="row">
                <div class="col-md-12">
                    <div class="mail-area">

                        <div class="mail-box-navigation">
                            <ul class="mail-navigation">
                                <li><a href="<?php echo admin_url(); ?>/mail.html"><i class="fa fa-pencil"></i> <span>Compose</span></a></li>
                                <li><a class="active" href="<?php echo admin_url(); ?>/mail/inbox.html"><i class="fa fa-inbox"></i> <span>Inbox </span></a></li>
                                <li><a href="<?php echo admin_url(); ?>/mail/sent.html"><i class="fa fa-mail-reply"></i> <span>Sent</span></a></li>
                            </ul>
                        </div>
                        <hr/>

                    </div>
                </div>
            </div>




            <div class="row">
                <div class="col-md-3">
                    <div class="mail-box-list">
                        <div class="mail-search-box">
                            <form id="searchform2" name="searchform2" method="get" action="http://fickle.aimmate.com/index.html">
                                <div class="fieldcontainer">
                                    <input type="text" name="s2" id="s2" class="searchfieldjs" placeholder="Keywords..." tabindex="2">
                                    <input type="submit" name="search2btn" id="search2btn" value="">
                                </div>
                            </form>
                        </div>


                        <!-- Nav tabs -->
                        <ul class="nav nav-tabs mail-list" role="tablist">

                            <li role="presentation" class="active" style="float: none;">
                                <div class="mail-user-image">
                                    <i class="fa fa-facebook"></i>
                                </div>
                                <div class="mail-user-mail">
                                    <span>facebook</span>

                                    <div class="mail-action">
                                        <a href="#profile" aria-controls="profile" role="tab" data-toggle="tab"><i class="fa fa-eye"></i></a>
                                        <a href=""><i class="fa fa-star"></i></a>
                                        <a href="javascript:void (0)"><i class="fa fa-tag"></i></a>
                                        <a href="javascript:void (0)"><i class="fa fa-trash-o"></i></a>
                                    </div>
                                </div>
                            </li>

                            <li role="presentation" style="float: none;">
                                <div class="mail-user-image">
                                    <i class="fa fa-facebook"></i>
                                </div>
                                <div class="mail-user-mail">
                                    <span>facebook</span>

                                    <div class="mail-action">
                                        <a href="#profiles" aria-controls="profiles" role="tab" data-toggle="tab"><i class="fa fa-eye"></i></a>
                                        <a href=""><i class="fa fa-star"></i></a>
                                        <a href="javascript:void (0)"><i class="fa fa-tag"></i></a>
                                        <a href="javascript:void (0)"><i class="fa fa-trash-o"></i></a>
                                    </div>
                                </div>
                            </li>

                            <li role="presentation" style="float: none;">
                                <div class="mail-user-image">
                                    <i class="fa fa-facebook"></i>
                                </div>
                                <div class="mail-user-mail">
                                    <span>facebook</span>

                                    <div class="mail-action">
                                        <a href="#profilee" aria-controls="profilee" role="tab" data-toggle="tab"><i class="fa fa-eye"></i></a>
                                        <a href=""><i class="fa fa-star"></i></a>
                                        <a href="javascript:void (0)"><i class="fa fa-tag"></i></a>
                                        <a href="javascript:void (0)"><i class="fa fa-trash-o"></i></a>
                                    </div>
                                </div>
                            </li>

                            <li role="presentation" style="float: none;">
                                <div class="mail-user-image">
                                    <i class="fa fa-facebook"></i>
                                </div>
                                <div class="mail-user-mail">
                                    <span>facebook</span>

                                    <div class="mail-action">
                                        <a href="#profil" aria-controls="profil" role="tab" data-toggle="tab"><i class="fa fa-eye"></i></a>
                                        <a href=""><i class="fa fa-star"></i></a>
                                        <a href="javascript:void (0)"><i class="fa fa-tag"></i></a>
                                        <a href="javascript:void (0)"><i class="fa fa-trash-o"></i></a>
                                    </div>
                                </div>
                            </li>

                        </ul>
                    </div>
                </div>


                <div class="col-md-9">


                    <!-- Tab panes -->
                    <div class="tab-content">
                        <div role="tabpanel" class="tab-pane mail-body mail-body-inbox active" id="profile">
                            <div class="mail-action-bar">
                                <ul>
                                    <li><a class="tooltipMail" data-toggle="tooltip" data-placement="right" title="Reply only" href="#"><i
                                                class="fa fa-mail-reply"></i></a></li>
                                    <li><a class="tooltipMail" data-toggle="tooltip" data-placement="right" title="Reply to all" href="#"><i
                                                class="fa fa-mail-reply-all"></i></a></li>
                                    <li><a class="tooltipMail" data-toggle="tooltip" data-placement="right" title="Important" href="#"><i
                                                class="fa fa-star"></i></a></li>
                                    <li><a class="tooltipMail" data-toggle="tooltip" data-placement="right" title="Add tags" href="#"><i
                                                class="fa fa-tag"></i></a></li>
                                    <li><a class="tooltipMail" data-toggle="tooltip" data-placement="right" title="Mail Forward" href="#"><i
                                                class="fa fa-mail-forward"></i></a></li>
                                    <li><a class="tooltipMail" data-toggle="tooltip" data-placement="right" title="Move to trash" href="#"><i
                                                class="fa fa-trash-o"></i></a></li>
                                </ul>
                            </div>
                            <div class="mail-contact">

                                <div class="mail-contact-address">
                                    To:
                                    <div class="contact-address"><?php echo $mail_info['to']; ?></div>
                                    From:
                                    <div class="contact-address"><?php echo $mail_info['from']; ?></div>
                                    Subject:
                                    <div class="mail-subject"><?php echo $mail_info['subject']; ?></div>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div class="mail-main-content">
                                <p>
                                    <?php echo $mail_info['message']; ?>
                                </p>
                            </div>

                        </div>

                    </div>
                </div>
            </div>		
        </div>
    </div>
    <!-- Main Content Element  End-->
</section>