<!--Page main section start-->
<section id="min-wrapper">
    <div id="main-content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <!--Top header start-->
                    <h3 class="ls-top-header">Compose Mail</h3>
                    <!--Top header end -->

                    <!--Top breadcrumb start -->
                    <ol class="breadcrumb">
                        <li><a href="javascript:void(0)"><i class="fa fa-home"></i></a></li>
                        <li>Email</li>
                        <li class="active">Compose Mail</li>
                    </ol>
                    <!--Top breadcrumb start -->
                </div>
            </div>
            <!-- Main Content Element  Start-->
            <div class="row">
                <div class="col-md-12">
                    <div class="mail-area">

                        <div class="mail-box-navigation">
                            <ul class="mail-navigation">
                                <li><a class="active" href="<?php echo admin_url(); ?>/mail/inbox.html"><i class="fa fa-inbox"></i> <span>Inbox </span></a></li>
                                <li><a href="<?php echo admin_url(); ?>/mail/sent.html"><i class="fa fa-mail-reply"></i> <span>Sent</span></a></li>
                            </ul>
                        </div>
                        <hr/>
                        <div>
                            <?php
                            $msg = $this->session->userdata('message');
                            $error = $this->session->userdata('error');

                            if ($msg) {
                                echo "<div class='alert alert-success'>" . $msg . "</div>";
                                $this->session->unset_userdata('message');
                            } else if ($error) {
                                echo "<div class='alert alert-danger'>" . $error . "</div>";
                                $this->session->unset_userdata('error');
                            }
                            ?>
                        </div>
                        <div class="mail-body compose-mail-box">
                            <div class="mail-action-bar">
                                <ul>
                                    <li><a class="addCc" href="javascript:void(0)">Add Cc</a></li>
                                    <li><a class="tooltipMail" data-toggle="tooltip" data-placement="right"
                                           title="Reply to all" href="#"><i class="fa fa-mail-reply-all"></i></a></li>
                                    <li><a class="tooltipMail" data-toggle="tooltip" data-placement="right"
                                           title="Important" href="#"><i class="fa fa-star"></i></a></li>
                                    <li><a class="tooltipMail" data-toggle="tooltip" data-placement="right"
                                           title="Add tags" href="#"><i class="fa fa-tag"></i></a></li>
                                    <li><a class="tooltipMail" data-toggle="tooltip" data-placement="right"
                                           title="Mail Forward" href="#"><i class="fa fa-mail-forward"></i></a></li>
                                    <li><a class="tooltipMail" data-toggle="tooltip" data-placement="right"
                                           title="Move to trash" href="#"><i class="fa fa-trash-o"></i></a></li>
                                    <li><a class="tooltipMail" data-toggle="tooltip" data-placement="right"
                                           title="Send the mail" href="#"><i class="fa fa-envelope-o"></i></a></li>
                                </ul>
                            </div>
                            <form action="<?php echo admin_url(); ?>/mail/sent_general_mail.html" method="post" class="form-horizontal ls_form">
                                <div class="mail-contact">

                                    <div class="mail-contact-address">
                                        <div class="fromView">
                                            Sender Name:
                                            <input type="text" class="form-control bg-white" name="sender_name" placeholder="Add your company name">
                                        </div>
                                        <div class="fromView">
                                            From:
                                            <input type="text" class="form-control bg-white" name="from" placeholder="Add your email address">
                                        </div>

                                        <div class="toView">
                                            To:
                                            <input type="text" class="form-control bg-white" name="to" placeholder="Add recipents email address">
                                        </div>

                                        <div class="addCcView">
                                            Cc:
                                            <input type="text" class="form-control bg-white" name="cc" placeholder="Add recipents">
                                        </div>

                                        <div class="addSubject">
                                            Subject:
                                            <input type="text" class="form-control bg-white" name="subject" placeholder="Write your subject">
                                        </div>
                                    </div>

                                    <div class="clearfix"></div>
                                </div>
                                <div class="mail-main-content">
                                    <textarea name="message" class="summernote" placeholder="Write message">

                                    </textarea>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <button type="submit" class="mail-send-btn btn ls-light-green-btn">Send</button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>

                        <div class="clearfix"></div>

                    </div>

                </div>
            </div>

            <!-- Main Content Element  End-->

        </div>
    </div>



</section>
<!--Page main section end -->

<div id="change-color">
    <div id="change-color-control">
        <a href="javascript:void(0)"><i class="fa fa-magic"></i></a>
    </div>
    <div class="change-color-box">
        <ul>
            <li class="default active"></li>
            <li class="red-color"></li>
            <li class="blue-color"></li>
            <li class="light-green-color"></li>
            <li class="black-color"></li>
            <li class="deep-blue-color"></li>
        </ul>
    </div>
</div>
</section>