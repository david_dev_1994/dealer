<section id="min-wrapper">
    <div id="main-content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <!--Top header start-->
                    <h3 class="ls-top-header">Sent Mail</h3>
                    <!--Top header end -->

                    <!--Top breadcrumb start -->
                    <ol class="breadcrumb">
                        <li><a href="javascript:void(0)"><i class="fa fa-home"></i></a></li>
                        <li>Email</li>
                        <li class="active">Sent Mail</li>
                    </ol>
                    <!--Top breadcrumb start -->
                </div>
            </div>
            <!-- Main Content Element  Start-->
            <div class="row">
                <div class="col-md-12">
                    <div class="mail-area">

                        <div class="mail-box-navigation">
                            <ul class="mail-navigation">
                                <li><a href="<?php echo admin_url(); ?>/mail.html"><i class="fa fa-pencil"></i> <span>Compose</span></a></li>
                                <li><a class="active" href="<?php echo admin_url(); ?>/mail/inbox.html"><i class="fa fa-inbox"></i> <span>Inbox </span></a></li>
                            </ul>
                        </div>
                        <hr/>
                        <div class="col-md-12">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h3 class="panel-title">Sent Mail</h3>
                                </div>
                                <div class="panel-body">
                                    <!--Table Wrapper Start-->
                                    <div class="table-responsive ls-table">
                                        <div>
                                            <?php
                                            $msg = $this->session->userdata('message');
                                            $error = $this->session->userdata('error');

                                            if ($msg) {
                                                echo "<div class='alert alert-success'>" . $msg . "</div>";
                                                $this->session->unset_userdata('message');
                                            } else if ($error) {
                                                echo "<div class='alert alert-danger'>" . $error . "</div>";
                                                $this->session->unset_userdata('error');
                                            }
                                            ?>
                                        </div>
                                        <table class="table table-bordered table-striped">
                                            <thead>
                                                <tr>
                                                    <th>To</th>
                                                    <th>From</th>
                                                    <th>Subject</th>
                                                    <th>Date</th>
                                                    <th class="text-center">Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php foreach ($sent_mail_info as $v_sent_mail_info) { ?>
                                                    <tr>
                                                        <td><?php echo $v_sent_mail_info['to']; ?></td>
                                                        <td><?php echo $v_sent_mail_info['from']; ?></td>
                                                        <td><?php echo $v_sent_mail_info['subject']; ?></td>
                                                        <td><?php echo date("D d F Y h:ia", strtotime($v_sent_mail_info['date_added'])); ?></td>
                                                        <td class="text-center">
                                                            <a href="<?php echo admin_url(); ?>/mail/view_sent_mail/<?php echo $v_sent_mail_info['sent_mail_id']; ?>" class="btn btn-xs btn-info"><i class="fa fa-eye"></i></a>
                                                            <a href="<?php echo admin_url(); ?>/mail/delete_sent_mail/<?php echo $v_sent_mail_info['sent_mail_id']; ?>"class="btn btn-xs btn-danger" onclick="return confirm('Are you sure to delete this ?');"><i class="fa fa-trash-o"></i></a>
                                                        </td>
                                                    </tr>
                                                <?php } ?>
                                            </tbody>
                                        </table>
                                    </div>
                                    <!--Table Wrapper Finish-->
<!--                                    <div class="text-center">
                                        <div class="ls-button-group demo-btn ls-table-pagination">
                                            <ul class="pagination ls-pagination">
                                                <li><a href="#"><i class="fa fa-arrow-left"></i> </a></li>
                                                <li><a href="#">1</a></li>
                                                <li><a href="#">2</a></li>
                                                <li><a href="#">3</a></li>
                                                <li><a href="#">4</a></li>
                                                <li><a href="#">5</a></li>
                                                <li><a href="#"><i class="fa fa-arrow-right"></i></a></li>
                                            </ul>
                                        </div>
                                    </div>-->
                                </div>
                            </div>
                        </div>

                        <div class="clearfix"></div>

                    </div>

                </div>
            </div>

            <!-- Main Content Element  End-->

        </div>
    </div>



</section>
<!--Page main section end -->