<section id="min-wrapper">
    <div id="main-content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <!--Top header start-->
                    <h3 class="ls-top-header">Corporate Announcements</h3>
                    <!--Top header end -->
                </div>
                <div class="product-tools pull-right">
                    <a href="<?= base_url('admin/addCorporateAnnouncement'); ?>" class="btn btn-primary" style="margin: 15px;">New Announcement</a></li>

                </div>
            </div>

            <?php
            if($this->session->flashdata('error')){
                ?>
                <br>
                <div class="alert alert-danger alert-dismissable">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>
                    <?php echo $this->session->flashdata('error'); ?>
                </div>
                <?php
            }

            if($this->session->flashdata('success')){
                ?>
                <br>
                <div class="alert alert-success alert-dismissable">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>
                    <?php echo $this->session->flashdata('success'); ?>
                </div>
                <?php
            }
            ?>

            <!-- Main Content Element  Start-->
            <div class="row">
                <div class="col-md-12">
                    <div class="portlet light no-border-top as-box-border">
                        <div class="portlet-body">
                            <table class="table table-striped table-bordered table-hover dt-responsive" id="user_table" data-name="services">
                                <thead>
                                <tr>
                                    <th> Serial Number </th>
                                    <th class="hide"> id </th>
                                    <th> Title </th>
                                    <th> Created Date</th>
                                    <th> Action </th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php
                                foreach($data as $index=>$service){
                                ?>
                                <tr class="odd">
                                    <td><?= $index+1;?> </td>
                                    <td class="hide"> <?= $service['id']; ?> </td>
                                    <td> <?= isset($service['title'])?strip_tags($service['title']):'';?> </td>
                                    <td> <?= isset($service['created_at'])?date('m-d-Y',$service['created_at']):'';?> </td>
                                    <td>
                                        <a href="<?= base_url('admin/addCorporateAnnouncement/'.$service['id']);?>"><i class="fa fa-pencil" aria-hidden="true"></i> Edit</a>
                                        &nbsp;<a href="#" data-href="<?= base_url('admin/deleteCorporateAnnouncement/'.$service['id']);?>" data-toggle="modal" data-target="#confirm-delete"><i class="fa fa-trash-o" aria-hidden="true"></i> Delete</a>

                                    </td>
                                </tr>
                                <?php
                                 }
                                ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Main Content Element  End-->
        </div>
    </div>
</section>