<?php defined('BASEPATH') OR exit('No direct script access allowed');


class Quote extends MSN_Controller {

    public function __construct() {
        parent::__construct();
        $user_id = $this->session->userdata('d_user_id');
        if ($user_id == NULL && $this->uri->segment(1) != 'cronjob') {
            redirect('', 'refresh');
        }
        $this->load->model('admin_models/User_Quote_Model', 'quote_mdl');
        $this->load->model('admin_models/Profile_Model', 'pp_model');
        $this->load->model('user_models/Page_Model','p_model');
        $this->load->helper('functions_helper');
    }

    public function quote($id='') {
        $data['title'] = 'Request Quote';
        $data['menu_active'] = 'Quote';
        $data['user'] = $this->p_model->get_user_info();
        $data['sales_manager_info'] = $this->p_model->get_sales_manager_info($data['user']['sales_manager_id']);
        // echo'<pre>';print_r($data['user']);exit;
        $data['price_list'] = $this->quote_mdl->get_all_price_list();
        $data['raw_parts'] = $this->quote_mdl->get_all_raw_parts();
        $user_id = $this->session->userdata('d_user_id');

        if($this->input->post()){
//            echo'<pre>';print_r($this->input->post());exit;
            $this->form_validation->set_rules('name', 'Dealer Name', 'trim|required');
            $this->form_validation->set_rules('price_book', 'Price Book', 'trim|required');
            $this->form_validation->set_rules('ship_to_name', 'Ship to Person Name', 'trim|required');
            $this->form_validation->set_rules('ship_to_company', 'Ship to Company Name', 'trim|required');
            $this->form_validation->set_rules('email', 'Email', 'trim|required');
            $this->form_validation->set_rules('pnum', 'Phone Number', 'trim|required');
            $this->form_validation->set_rules('company', 'Company Name', 'trim|required');
            $this->form_validation->set_rules('sales_person', 'Sales Manager', 'trim|required');
            $this->form_validation->set_rules('sales_manager_email', 'Sales Manager Email', 'trim|required');
            $this->form_validation->set_rules('ordernumber', 'Order Number', 'trim|max_length[20]');
            $this->form_validation->set_rules('grandTotal', 'Grand Total', 'trim|required');
            $this->form_validation->set_rules('special_instruction', 'Special Instruction', 'trim');
            $address = array(
                array('field' => 'o_phone[]', 'label' => 'Billing phone', 'rules' => 'trim|required|max_length[25]'),
                array('field' => 'o_address[]', 'label' => 'Billing address', 'rules' => 'trim|required|min_length[8]|max_length[150]'),
                array('field' => 'o_state[]', 'label' => 'Billing state', 'rules' => 'trim|required|max_length[150]'),
                array('field' => 'o_city[]', 'label' => 'Billing city', 'rules' => 'trim|required|max_length[150]'),
                array('field' => 'o_zip[]', 'label' => 'Billing zip', 'rules' => 'trim|required|max_length[20]'),
                
                array('field' => 's_phone[]', 'label' => 'Shipping phone', 'rules' => 'trim|required|max_length[25]'),
                array('field' => 's_address[]', 'label' => 'Shipping address', 'rules' => 'trim|required|min_length[8]|max_length[150]'),
                array('field' => 's_state[]', 'label' => 'Shipping state', 'rules' => 'trim|required|max_length[150]'),
                array('field' => 's_city[]', 'label' => 'Shipping city', 'rules' => 'trim|required|max_length[150]'),
                array('field' => 's_zip[]', 'label' => 'Shipping zip', 'rules' => 'trim|required|max_length[20]'),


                array('field' => 'model[]', 'label' => 'Model', 'rules' => 'trim|required'),
                array('field' => 'description[]', 'label' => 'Description', 'rules' => 'trim|required'),
                array('field' => 'family[]', 'label' => 'Family', 'rules' => 'trim|required'),
                array('field' => 'type[]', 'label' => 'Type', 'rules' => 'trim'),
                array('field' => 'quantity[]', 'label' => 'Quantity', 'rules' => 'trim|required'),
                array('field' => 'price[]', 'label' => 'Price', 'rules' => 'trim|required'),
                array('field' => 'list_price[]', 'label' => 'List Price', 'rules' => 'trim'),
                array('field' => 'dealer_price[]', 'label' => 'Dealer Price', 'rules' => 'trim'),
            );
            $this->form_validation->set_rules($address);

            if ($this->form_validation->run() == FALSE) {
                $this->session->set_flashdata('error',validation_errors());
                redirect('quote/'.$id);
            } else {
                $data = $this->input->post();

                $created_time = time();

                $expiry = date('Y-m-d H:m:s', $created_time+3600*24*90);

                $expiry = strtotime($expiry) ;

                $db_data = array(
                    'name' => $this->input->post('name'),
                    'price_book' => $this->input->post('price_book'),
                    'ship_to_name' => $this->input->post('ship_to_name'),
                    'ship_to_company' => $this->input->post('ship_to_company'),
                    'email' => $this->input->post('email'),
                    'phone' => $this->input->post('pnum'),
                    'sales_person' => $this->input->post('sales_person'),
                    'sales_manager_email' => $this->input->post('sales_manager_email'),
                    'orderNumber' => $this->input->post('ordernumber'),
                    'company' => $this->input->post('company'),
                    'special_instruction' => $this->input->post('special_instruction'),
                    'grandTotal' => $this->input->post('grandTotal'),

                    'billing_address' => json_encode($data['o_address']),
                    'billing_city' => json_encode($data['o_city']),
                    'billing_contactinfo' => json_encode($data['o_phone']),
                    'billing_state' => json_encode($data['o_state']),
                    'billing_zip' =>json_encode($data['o_zip']),

                    'shipping_address' => json_encode($data['s_address']),
                    'shipping_city' => json_encode($data['s_city']),
                    'shipping_contactinfo' => json_encode($data['s_phone']),
                    'shipping_state' => json_encode($data['s_state']),
                    'shipping_zip' =>json_encode($data['s_zip']),

                    'model' => json_encode($data['model']),
                    'description' => json_encode($data['description']),
                    'family' => json_encode($data['family']),
                    'list_price' => json_encode($data['list_price']),
                    'dealer_price' => json_encode($data['dealer_price']),
                    'type' => json_encode($data['type']),
                    'quantity' =>json_encode($data['quantity']),
                    'price' =>json_encode($data['price']),
                    // 'created_date' => $created_time,
                    // 'expiry_date' => $expiry
                );

//                echo '<pre>';print_r($db_data);exit;
                if($id){
                    $this->quote_mdl->update_quote_by_id($id,$user_id,$db_data);
                    redirect('generate_quote/'.$id);
                }else {
                    $db_data['user_id'] = $user_id;
                    $db_data['created_date'] = $created_time;
                    $db_data['expiry_date'] = $expiry;
                    $inserted_id = $this->quote_mdl->insert_quote_info($db_data);
                    if (!empty($inserted_id)) {
                        redirect('generate_quote/'.$inserted_id);
                    } else {
                        $sdata['error'] = 'Quote failed! Please try again. ';

                        $this->session->set_userdata($sdata);
                        redirect('');
                    }
                }
            }
        }
        if($id)
            $data['quote_info'] = $this->quote_mdl->get_all_quote_info_by_quote_id_and_user_id($id, $user_id);
        
        $this->load->view('user_panel/common/header');
        $this->load->view('user_panel/quote_form', $data);
        $this->load->view('user_panel/common/footer');
    }
    
    //change 1/11/2019
    
    public function ddelete_quote($type,$quote_id) {
        $result = $this->quote_mdl->delete_quote_by_id($quote_id);
        if (!empty($result)) {
            $sdata['message'] = '<h3>Quote removed successfully .</h3>';
            $this->session->set_flashdata($sdata);
            redirect('page/gpsmanager');
        }
    }
    
     public function singleQuote($id) {
        $data['title'] = 'Request Quote';
        $data['menu_active'] = 'Quote';
        $data['viewonly'] = 1;
//        $data['user'] = $this->p_model->get_user_info();
        $user_id = $this->session->userdata('d_user_id');

        $data['quote_info'] = $this->quote_mdl->get_all_quote_info_by_quote_id_and_user_id($id, $user_id);

       echo json_encode($data['quote_info']);
       exit;
    }
    
    public function dviewonly($id,$user_id) {
        $data['title'] = 'Request Quote';
        $data['menu_active'] = 'Quote';
        $data['viewonly'] = 1;
//        $data['user'] = $this->p_model->get_user_info();
//        $user_id = $this->session->userdata('d_user_id');

        $data['quote_info'] = $this->quote_mdl->get_all_quote_info_by_quote_id_and_user_id($id, $user_id);

        $this->load->view('user_panel/common/header');
        $this->load->view('user_panel/generate_quote', $data);
        $this->load->view('user_panel/common/footer');
    }
    
    public function dlistquotes($id='2',$user_id){
        $data['type']=((int)$id==1)?"Submitted Quotes":"Draft Quotes";
        $data['quotes_list'] = $this->quote_mdl->get_quotes_by_userID($id,$user_id);
        $data['user'] = $this->p_model->get_user_info();
        $data['dlistquotes'] = 1;
        $this->load->view('user_panel/common/header');
        $this->load->view('user_panel/view_quotes', $data);
        $this->load->view('user_panel/common/footer');
//        echo $id;echo '<br>';
//        echo $user_id;
//        die();
    }
    
    public function dredirect($id,$user_id){
        $type = 1;
//        echo $id;echo '<br>';
//        echo $user_id;
//        die();
        if((int)$type==2){
            $sdata['message'] = '<h3>Quote Draft successfully.</h3>';
            $this->session->set_flashdata($sdata);
        }
        if((int)$type==1){
            //send mail
            $data['quote_info'] = $this->quote_mdl->get_all_quote_info_by_quote_id_and_user_id($id, $user_id);
//            echo '<pre>';print_r(json_decode($data['quote_info']['shipping_address']));die();
            if($data['quote_info']['status'] == 1){
                $sdata['message'] = '<h3>This quote has already been submitted.</h3>';
                $this->session->set_flashdata($sdata);
//                redirect('quote/view/'.$type);
            }else{
                //            echo 'hello';
//            echo '<pre>';print_r($data['quote_info']['status']);die();
                $string = $this->load->view('user_panel/generate_quote_email', $data, TRUE);

//            echo '<pre>';
//            print_r($data['quote_info']);exit;

                $to[] = $data['quote_info']['email'];
                $to[] = $data['quote_info']['sales_manager_email'];
                $to[] = 'sales@bktechnologies.com';
                sendemailtomany($to,'dealerportal@bktechnologies.com ',$string,'Price Quote');

                $sdata['message'] = '<h3>Quote Submitted successfully.</h3>';
                $this->session->set_flashdata($sdata);
                $db_data['submitted_date'] = time();

            }

        }
        $db_data['status']=(int)$type;
        $this->quote_mdl->update_quote_by_id($id,$user_id,$db_data);
        redirect('quote/dview/'.$type.'/'.$user_id);
    }
    
    public function ddredirect($id,$user_id){
        $type = 2;
//        echo $id;echo '<br>';
//        echo $user_id;
//        die();
        if((int)$type==2){
            $sdata['message'] = '<h3>Quote Draft successfully.</h3>';
            $this->session->set_flashdata($sdata);
        }
        if((int)$type==1){
            //send mail
            $data['quote_info'] = $this->quote_mdl->get_all_quote_info_by_quote_id_and_user_id($id, $user_id);
//            echo '<pre>';print_r(json_decode($data['quote_info']['shipping_address']));die();
            if($data['quote_info']['status'] == 1){
                $sdata['message'] = '<h3>This quote has already been submitted.</h3>';
                $this->session->set_flashdata($sdata);
//                redirect('quote/view/'.$type);
            }else{
                //            echo 'hello';
//            echo '<pre>';print_r($data['quote_info']['status']);die();
                $string = $this->load->view('user_panel/generate_quote_email', $data, TRUE);

//            echo '<pre>';
//            print_r($data['quote_info']);exit;

                $to[] = $data['quote_info']['email'];
                $to[] = $data['quote_info']['sales_manager_email'];
                $to[] = 'sales@bktechnologies.com';
                sendemailtomany($to,'dealerportal@bktechnologies.com ',$string,'Price Quote');

                $sdata['message'] = '<h3>Quote Submitted successfully.</h3>';
                $this->session->set_flashdata($sdata);
                $db_data['submitted_date'] = time();

            }

        }
        $db_data['status']=(int)$type;
        $this->quote_mdl->update_quote_by_id($id,$user_id,$db_data);
        redirect('quote/dview/'.$type.'/'.$user_id);
    }
    
    public function dquote($id='',$user_id) {
        $data['title'] = 'Request Quote';
        $data['menu_active'] = 'Quote';
        // $data['user'] = $this->p_model->get_user_info();
        $data['user'] = $this->pp_model->get_gps_user_info($user_id);
        $data['sales_manager_info'] = $this->p_model->get_sales_manager_info($data['user']['sales_manager_id']);
        // echo'<pre>';print_r($data['user']);exit;
        $data['price_list'] = $this->quote_mdl->get_all_price_list();
        $data['raw_parts'] = $this->quote_mdl->get_all_raw_parts();
//        $user_id = $this->session->userdata('d_user_id');

        if($this->input->post()){
//            echo '<pre>';
//            print_r($this->input->post());
//            die();


//            echo'<pre>';print_r($this->input->post());exit;
            $this->form_validation->set_rules('name', 'Dealer Name', 'trim|required');
            $this->form_validation->set_rules('price_book', 'Price Book', 'trim|required');
            $this->form_validation->set_rules('ship_to_name', 'Ship to Person Name', 'trim|required');
            $this->form_validation->set_rules('ship_to_company', 'Ship to Company Name', 'trim|required');
            $this->form_validation->set_rules('email', 'Email', 'trim|required');
            $this->form_validation->set_rules('pnum', 'Phone Number', 'trim|required');
            $this->form_validation->set_rules('company', 'Company Name', 'trim|required');
            // $this->form_validation->set_rules('sales_person', 'Sales Manager', 'trim|required');
            // $this->form_validation->set_rules('sales_manager_email', 'Sales Manager Email', 'trim|required');
            $this->form_validation->set_rules('ordernumber', 'Order Number', 'trim|max_length[20]');
            $this->form_validation->set_rules('grandTotal', 'Grand Total', 'trim|required');
            $this->form_validation->set_rules('special_instruction', 'Special Instruction', 'trim');
            $address = array(
                array('field' => 'o_phone[]', 'label' => 'Billing phone', 'rules' => 'trim|required|max_length[25]'),
                array('field' => 'o_address[]', 'label' => 'Billing address', 'rules' => 'trim|required|min_length[8]|max_length[150]'),
                array('field' => 'o_state[]', 'label' => 'Billing state', 'rules' => 'trim|required|max_length[150]'),
                array('field' => 'o_city[]', 'label' => 'Billing city', 'rules' => 'trim|required|max_length[150]'),
                array('field' => 'o_zip[]', 'label' => 'Billing zip', 'rules' => 'trim|required|max_length[20]'),

                array('field' => 's_phone[]', 'label' => 'Shipping phone', 'rules' => 'trim|required|max_length[25]'),
                array('field' => 's_address[]', 'label' => 'Shipping address', 'rules' => 'trim|required|min_length[8]|max_length[150]'),
                array('field' => 's_state[]', 'label' => 'Shipping state', 'rules' => 'trim|required|max_length[150]'),
                array('field' => 's_city[]', 'label' => 'Shipping city', 'rules' => 'trim|required|max_length[150]'),
                array('field' => 's_zip[]', 'label' => 'Shipping zip', 'rules' => 'trim|required|max_length[20]'),


                array('field' => 'model[]', 'label' => 'Model', 'rules' => 'trim|required'),
                array('field' => 'description[]', 'label' => 'Description', 'rules' => 'trim|required'),
                array('field' => 'family[]', 'label' => 'Family', 'rules' => 'trim|required'),
                array('field' => 'type[]', 'label' => 'Type', 'rules' => 'trim'),
                array('field' => 'quantity[]', 'label' => 'Quantity', 'rules' => 'trim|required'),
                array('field' => 'price[]', 'label' => 'Price', 'rules' => 'trim|required'),
                array('field' => 'list_price[]', 'label' => 'List Price', 'rules' => 'trim'),
                array('field' => 'dealer_price[]', 'label' => 'Dealer Price', 'rules' => 'trim'),
            );
            $this->form_validation->set_rules($address);

            if ($this->form_validation->run() == FALSE) {
                $this->session->set_flashdata('error',validation_errors());
                // redirect('quote/'.$id);
                redirect('dquote/'.$id.'/'.$user_id);
            } else {
                $data = $this->input->post();

                $created_time = time();

                $expiry = date('Y-m-d H:m:s', $created_time+3600*24*90);

                $expiry = strtotime($expiry) ;

                $db_data = array(
                    'name' => $this->input->post('name'),
                    'price_book' => $this->input->post('price_book'),
                    'ship_to_name' => $this->input->post('ship_to_name'),
                    'ship_to_company' => $this->input->post('ship_to_company'),
                    'email' => $this->input->post('email'),
                    'phone' => $this->input->post('pnum'),
                    'sales_person' => $this->input->post('sales_person'),
                    'sales_manager_email' => $this->input->post('sales_manager_email'),
                    'orderNumber' => $this->input->post('ordernumber'),
                    'company' => $this->input->post('company'),
                    'special_instruction' => $this->input->post('special_instruction'),
                    'grandTotal' => $this->input->post('grandTotal'),

                    'billing_address' => json_encode($data['o_address']),
                    'billing_city' => json_encode($data['o_city']),
                    'billing_contactinfo' => json_encode($data['o_phone']),
                    'billing_state' => json_encode($data['o_state']),
                    'billing_zip' =>json_encode($data['o_zip']),

                    'shipping_address' => json_encode($data['s_address']),
                    'shipping_city' => json_encode($data['s_city']),
                    'shipping_contactinfo' => json_encode($data['s_phone']),
                    'shipping_state' => json_encode($data['s_state']),
                    'shipping_zip' =>json_encode($data['s_zip']),

                    'model' => json_encode($data['model']),
                    'description' => json_encode($data['description']),
                    'family' => json_encode($data['family']),
                    'list_price' => json_encode($data['list_price']),
                    'dealer_price' => json_encode($data['dealer_price']),
                    'type' => json_encode($data['type']),
                    'quantity' =>json_encode($data['quantity']),
                    'price' =>json_encode($data['price']),
                    // 'created_date' => $created_time,
                    // 'expiry_date' => $expiry
                );

//                echo '<pre>';print_r($db_data);exit;
                if($id){
                    $this->quote_mdl->update_quote_by_id($id,$user_id,$db_data);
                    // redirect('generate_quote/'.$id);
                    redirect('generate_quote/'.$id.'/'.$user_id);
                }else {
                    $db_data['user_id'] = $user_id;
                    $db_data['created_date'] = $created_time;
                    $db_data['expiry_date'] = $expiry;
                    $inserted_id = $this->quote_mdl->insert_quote_info($db_data);
                    if (!empty($inserted_id)) {
                        redirect('generate_quote/'.$inserted_id);
                    } else {
                        $sdata['error'] = 'Quote failed! Please try again. ';

                        $this->session->set_userdata($sdata);
                        redirect('');
                    }
                }
            }
        }
        if($id)
            $data['quote_info'] = $this->quote_mdl->get_all_quote_info_by_quote_id_and_user_id($id, $user_id);

        $this->load->view('user_panel/common/header');
        $this->load->view('user_panel/quote_form', $data);
        $this->load->view('user_panel/common/footer');
    }

    public function generate_quote($id,$uid = '') {
            //         echo $id;echo '<br>';
            // echo $uid;
        if($uid){
            $user_id = $uid;
            $data['gpsdealer'] = 1 ;
            $data['uid'] = $uid;
            // echo 'if';
        }else{
            $user_id = $this->session->userdata('d_user_id');
            // echo 'else';
        }
        // die();
        $data['quote_info'] = $this->quote_mdl->get_all_quote_info_by_quote_id_and_user_id($id, $user_id);

        if(empty($data['quote_info'])){
            $sdata['error'] = 'No Quote found.';
            $this->session->set_flashdata($sdata);;
            redirect('quote');
        }
        $data['title'] = 'Request Quote';
        $data['menu_active'] = 'Quote';
//        $data['user'] = $this->p_model->get_user_info();


        $this->load->view('user_panel/common/header');
        $this->load->view('user_panel/generate_quote', $data);
        $this->load->view('user_panel/common/footer');
    }
    
    public function user_sub_quote($quote_id) {
        $data['title'] = 'User Sub Quote';
        $data['menu_active'] = 'My Quote';
        $user_id = $this->session->userdata('user_id');

        $data['quote_info'] = $this->quote_mdl->get_all_quote_info_by_quote_id_and_user_id($quote_id, $user_id);
        $data['last_quote_info'] = $this->quote_mdl->get_last_sub_quote_info_by_quote_id($quote_id);
        $data['primary_quote_id'] = $quote_id;

        $this->load->view('user_panel/common/header');
        $this->load->view('user_panel/sub_quote', $data);
        $this->load->view('user_panel/common/footer');
    }

    public function user_confirm($sub_quote_id) {
        $sub_quote_info = $this->quote_mdl->get_sub_quote_info_by_sub_quote_id($sub_quote_id);
        $result = $this->quote_mdl->user_confirm_sub_quote_by_id($sub_quote_id);

        if (!empty($result)) {
            $sdata['message'] = '<h3>Quote confirm successfully .</h3>';
            $this->session->set_userdata($sdata);
            redirect('quote/user_sub_quote/' . $sub_quote_info['quote_id'], 'refresh');
        } else {
            $sdata['error'] = '<h3>Quote confiramation failed !</h3>';
            $this->session->set_userdata($sdata);
            redirect('quote/user_sub_quote/' . $sub_quote_info['quote_id'], 'refresh');
        }
    }
    
    public function add_sub_quote($quote_id) {
        $data['title'] = 'Job Sub Quote';
        $data['menu_active'] = 'Job Quote';
        $data['primary_quote_id'] = $quote_id;
        $data['job_category_info'] = $this->quote_mdl->get_job_category_info();
        $this->load->view('user_panel/common/header');
        $this->load->view('user_panel/add_user_sub_quote', $data);
        $this->load->view('user_panel/common/footer');
    }

    public function save_sub_quote() {
        $primary_quote_id = $this->input->post('quote_id', true);
        $config = array(
            array(
                'field' => 'data[job_title]',
                'label' => 'job title',
                'rules' => 'trim|required|min_length[2]|max_length[150]'
            ),
            array(
                'field' => 'data[job_category_id]',
                'label' => 'job category id',
                'rules' => 'trim|required'
            ),
            array(
                'field' => 'data[return_file_type]',
                'label' => 'return file type',
                'rules' => 'required'
            ),
            array(
                'field' => 'data[need_path]',
                'label' => 'path',
                'rules' => 'trim|required'
            ),
            array(
                'field' => 'data[number_of_image]',
                'label' => 'job category description',
                'rules' => 'trim|required'
            ),
            array(
                'field' => 'data[instruction_message]',
                'label' => 'publication status',
                'rules' => 'required|min_length[2]|max_length[250]'
            ),
            array(
                'field' => 'data[job_type]',
                'label' => 'job type',
                'rules' => 'trim|required'
            ),
            array(
                'field' => 'data[one_time_price_usd]',
                'label' => 'price',
                'rules' => 'trim'
            ),
            array(
                'field' => 'data[one_time_dead_line_hr]',
                'label' => 'dead line hr',
                'rules' => 'trim'
            ),
            array(
                'field' => 'data[dead_line_days]',
                'label' => 'dead line day',
                'rules' => 'trim'
            ),
            array(
                'field' => 'data[monthly_work_volume]',
                'label' => 'monthly work volume',
                'rules' => 'trim'
            ),
            array(
                'field' => 'data[monthly_amount_usd]',
                'label' => 'monthly amount',
                'rules' => 'trim'
            )
        );
        $this->form_validation->set_rules($config);
        if ($this->form_validation->run() == FALSE) {
            $this->add_sub_quote($primary_quote_id);
        } else {
            $data = $this->input->post('data', TRUE);
            $data['quote_id'] = $primary_quote_id;
            $data['user_id'] = $this->session->userdata('user_id');
            $result = $this->quote_mdl->insert_sub_quote_info($data);
            if (!empty($result)) {
                $sdata['message'] = '<h3>Sub quote add successfully .</h3>';
                $this->session->set_userdata($sdata);
                redirect('quote/user_sub_quote/' . $primary_quote_id, 'refresh');
            } else {
                $sdata['error'] = '<h3>Sub quote insertion failed </h3>';
                $this->session->set_userdata($sdata);
                redirect('quote/user_sub_quote/' . $primary_quote_id, 'refresh');
            }
        }
    }
    public function view(){
        $this->db->select('*')
            ->from('price_list');
        $query_result = $this->db->get();
        $result = $query_result->result_array();
        return $result;
    }
    public function listquotes($id='1'){
        $data['type']=((int)$id==1)?"Submitted Quotes":"Draft Quotes";
        $data['quotes_list'] = $this->quote_mdl->get_quotes($id);
        $data['user'] = $this->p_model->get_user_info();
        $this->load->view('user_panel/common/header');
        $this->load->view('user_panel/view_quotes', $data);
//        echo "<pre>";print_r($data);exit;
        $this->load->view('user_panel/common/footer');
       // echo "<pre>";print_r($quotes);exit;
       
    }
    public function viewonly($id) {
        $data['title'] = 'Request Quote';
        $data['menu_active'] = 'Quote';
        $data['viewonly'] = 1;
//        $data['user'] = $this->p_model->get_user_info();
        $user_id = $this->session->userdata('d_user_id');

        $data['quote_info'] = $this->quote_mdl->get_all_quote_info_by_quote_id_and_user_id($id, $user_id);

        $this->load->view('user_panel/common/header');
        $this->load->view('user_panel/generate_quote', $data);
        $this->load->view('user_panel/common/footer');
    }

    public function delete_quote($type,$quote_id) {
        $result = $this->quote_mdl->delete_quote_by_id($quote_id);
        if (!empty($result)) {
            $sdata['message'] = '<h3>Quote removed successfully .</h3>';
            $this->session->set_flashdata($sdata);
            redirect('quote/view/'.$type);
        }
    }

//    public function redirect($type,$id){
//        if((int)$type==2){
//         $sdata['message'] = '<h3>Quote Draft Saved Successfully .</h3>';
//         $this->session->set_flashdata($sdata);
//
//        }
//        if((int)$type==1){
//
//
//            //send mail
//            $this->load->helper('custom_helper');
//            $data['quote_info'] = $this->quote_mdl->get_all_quote_info_by_quote_id_and_user_id($id, $user_id);
//            $string = $this->load->view('user_panel/generate_quote', $data, TRUE);
//            sendemail('hhammad19@gmail.com','info@bktechnologies',$string,'Test view data');
//            //
//
////            $string = "Dealer Name: ".$ldata['full_name'] .
////                "<br>Email: ".$ldata['email_address'] .
////                "<br>Phone: ". $bdata['phone'] .
////                "<br>Company Name: ".$bdata['companyname'].
////                "<br>Sales Person Name: ".$bdata['profile_picture'].
////                "<br>User Id: ".$bdata['user_id'];
////
////            $to = 'BusOps@bktechnologies.com';
////            $from = $ldata['email_address'];
////
////            sendemail($to,$from,$string,'Approval Request to Dealer Portal');
//
//            $sdata['message'] = '<h3>Quote Submitted Successfully .</h3>';
//            $this->session->set_flashdata($sdata);
//        }
//        $user_id = $this->session->userdata('d_user_id');
//        $db_data['status']=(int)$type;
//        $this->quote_mdl->update_quote_by_id($id,$user_id,$db_data);
//         redirect('quote/view/'.$type);
//    }

    public function redirect($type,$id){
        $user_id = $this->session->userdata('d_user_id');
        $user_email = $this->session->userdata('d_email_address');
        if((int)$type==2){
            $sdata['message'] = '<h3>Quote Draft successfully.</h3>';
            $this->session->set_flashdata($sdata);
        }
        if((int)$type==1){
            //send mail
            $data['quote_info'] = $this->quote_mdl->get_all_quote_info_by_quote_id_and_user_id($id, $user_id);
            if($data['quote_info']['status'] == 1){
                $sdata['message'] = '<h3>This quote has already been submitted.</h3>';
                $this->session->set_flashdata($sdata);
               // redirect('quote/view/'.$type);
            }
            else
            {
                 $string = $this->load->view('user_panel/generate_quote_email', $data, TRUE);

//            print_r($data['quote_info']);exit;

            $to[] = $data['quote_info']['email'];
            $to[] = $data['quote_info']['sales_manager_email'];
            $to[] = 'sales@bktechnologies.com';
            sendemailtomany($to,'dealerportal@bktechnologies.com ',$string,'Price Quote');

            $sdata['message'] = '<h3>Quote Submitted successfully.</h3>';
            $this->session->set_flashdata($sdata);
            $db_data['submitted_date'] = time();
            }
           
        }
        $db_data['status']=(int)$type;
        $this->quote_mdl->update_quote_by_id($id,$user_id,$db_data);
        redirect('quote/view/'.$type);
    }
    
    public function email(){
        $data = $this->quote_mdl->get_quotes_by_date();
        if(!empty($data)){
            foreach ($data as $dealers) {
                $string = 'Hi,<br/><br/> This is a reminder that your BK Technologies quote will expire in 10 days and will be deleted from the dealers portal. Please reach out to your BK sales professional with any questions. Thank you for your interest in BK Technologies.<br/><br/> Click the <a target="_blank" href="' . base_url('quote/show/'.$dealers['id']) . '">Link</a> to check. <br/><br/> <em>Please do not reply to this email address. It is used for outbound messages only.</em>';
                sendemail($dealers['email'], 'dealerportal@bktechnologies.com', $string, 'Quote Reminder');
            }
            // foreach ($data as $admins) {
            //     $string = 'Hi,<br/><br/> This is a reminder that your BK Technologies quote will expire in 10 days and will be deleted from the dealers portal. Please reach out to your BK sales professional with any questions. Thank you for your interest in BK Technologies.<br/><br/> Click the <a target="_blank" href="' . base_url('admin/quote/show/'.$admins['id']) . '">Link</a> to check. <br/><br/> <em>Please do not reply to this email address. It is used for outbound messages only.</em>';
            //     sendemailtomany($emails = array($admins['sales_manager_email'],'sales@bktechnologies.com'), 'dealerportal@bktechnologies.com', $string, 'Quote Reminder');
            // }
        }
        $this->quote_mdl->delete_quotes_by_date();
    }

    function upload_attachment()
    {
    	$this->layout = '';
        $po = false;
        $db_data = array();
        $user_id = $this->session->userdata('d_user_id');
        if($this->input->post('quote_id')) {
            if($this->input->post('po')){
                $po = true;
            }
            $id = $this->input->post('quote_id');
            $data['quote_info'] = $this->quote_mdl->get_all_quote_info_by_quote_id_and_user_id($id, $user_id);
			if (isset($_FILES['file']['name']) && !empty($_FILES['file']['name'])) {
                if($po){
                    $upload_status = $this->file_upload($po);
                }else{
                    $upload_status = $this->file_upload();
                }
                

                if ($upload_status['status'] == 200) {
                    if ($upload_status['file'] != ''){
                        if($po){
                            @unlink(BASEDIR . $data['quote_info']['po_file']);
                            $db_data['po_file'] = $upload_status['file'];
                        }
                        else{
                            @unlink(BASEDIR . $data['quote_info']['file']);
                            $db_data['file'] = $upload_status['file'];
                        }

                    }
                    
                    // print_r($upload_status);
                    // die();
                    $this->quote_mdl->update_quote_by_id($id,$user_id,$db_data);
                    $response = array("status" => 200, "message" => "Record Updated.","file" => $_FILES['file']['name']);
                }else{
                    $response["status"] = 400;
                    $response['message'] = $upload_status['message'];
                }
            }else {
                $response["status"] = 404;
                $response["message"] = 'Please Upload a file!';
            }
        }else {
            $response["status"] = 404;
            $response["message"] = 'Invalid Request';
        }
        echo json_encode($response);
        exit;
    }

    private function file_upload($po = null)
    {
		if (isset($_FILES['file']) && $_FILES['file']['size'] > 0) {
            $time = time();
            $file = $time . $_FILES['file']['name'];
            $fileName = str_replace("/\s+/", "_", $file);
            $config['file_name'] = $fileName;
            $config['upload_path'] = BASEDIR;
            /*if($po){
                $config['allowed_types'] = 'pdf|xlsx|doc|docx|opt';
            }else{
                $config['allowed_types'] = 'pdf|doc|docx|xls|xlsx';
            }*/
			$config['allowed_types'] = '*';

//            $config['max_size'] = '12048';
            $this->load->library('upload', $config);
            $response = array();
            if ($this->upload->do_upload('file')) {
                $upData = $this->upload->data();
                $name = $upData['file_name'];
                $response['status'] = 200;
                $response['file'] = $name;
            } else {
                $response['status'] = 400;
                $response['message'] = $this->upload->display_errors();
            }

        } else {
			$response['status'] = 400;
			$response['message'] = "File size must be greater than 0.";
        }
        return $response;
    }


}
