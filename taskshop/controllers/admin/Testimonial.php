<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/* * *******************************************#
  #      User Management System                 #
  #*********************************************#
  #      Author:     Atique IT                  #
  #      Email:      info@atique-it.com         #
  #      Website:    http://atique-it.com       #
  #                                             #
  #      Version:    15.2.1                     #
  #      Copyright:  (c) 2015 - Atique IT       #
  #                                             #
  #*********************************************# */

//            echo '<pre>';
//            print_r($data);
//            exit();
class Testimonial extends MSN_Controller {

    public function __construct() {
        parent::__construct();

        $this->admin_login_authentication();
        $this->load->model('admin_models/Others_Model', 'others_mdl');
        $this->load->model('admin_models/Profile_Model', 'pro_mdl');
        $this->load->model('admin_models/Testimonial_Model', 'testimonial_mdl');
    }

    public function index() {
        $data['title'] = 'Testimonial';
        $data['menu_active'] = 'Testimonial';
        $data['email_notification'] = $this->others_mdl->get_all_unred_notification();
        $data['user_info'] = $this->pro_mdl->get_user_info();
        $data['testimonial_info'] = $this->testimonial_mdl->get_all_testimonial_info();
        $this->load->view('admin_panel/common/header', $data);
        $this->load->view('admin_panel/common/menu_bar');
        $this->load->view('admin_panel/testimonial/testimonial', $data);
        $this->load->view('admin_panel/common/footer');
    }

    public function add_testimonial() {
        $data['title'] = 'Add Testimonial';
        $data['menu_active'] = 'Add Testimonial';
        $data['email_notification'] = $this->others_mdl->get_all_unred_notification();
        $data['user_info'] = $this->pro_mdl->get_user_info();
        $this->load->view('admin_panel/common/header', $data);
        $this->load->view('admin_panel/common/menu_bar');
        $this->load->view('admin_panel/testimonial/add_testimonial');
        $this->load->view('admin_panel/common/footer');
    }

    public function save_testimonial() {
        $config = array(
            array(
                'field' => 'data[testimonial]',
                'label' => 'testimonial',
                'rules' => 'trim|required|min_length[15]|max_length[150]'
            )
        );
        $this->form_validation->set_rules($config);
        if ($this->form_validation->run() == FALSE) {
            $this->add_testimonial();
        } else {
            $data = $this->input->post('data', TRUE);
            $data['user_id'] = $this->session->userdata('user_id');

            $result = $this->testimonial_mdl->insert_testimonial_info($data);
            if (!empty($result)) {
                $sdata['message'] = '<h3>Testimonial add successfully .</h3>';
                $this->session->set_userdata($sdata);
                redirect('admin/testimonial/add_testimonial', 'refresh');
            } else {
                $sdata['error'] = '<h3>Testimonial insertion failed </h3>';
                $this->session->set_userdata($sdata);
                redirect('admin/testimonial/add_testimonial', 'refresh');
            }
        }
    }

    public function unpublished_testimonial($testimonial_id) {
        $result = $this->testimonial_mdl->unpublished_testimonial_by_id($testimonial_id);

        if (!empty($result)) {
            $sdata['message'] = '<h3>Testimonial unpublished successfully .</h3>';
            $this->session->set_userdata($sdata);
            redirect('admin/testimonial', 'refresh');
        } else {
            $sdata['error'] = '<h3>Testimonial unpublication failed !</h3>';
            $this->session->set_userdata($sdata);
            redirect('admin/testimonial', 'refresh');
        }
    }

    public function published_testimonial($testimonial_id) {
        $result = $this->testimonial_mdl->published_testimonial_by_id($testimonial_id);

        if (!empty($result)) {
            $sdata['message'] = '<h3>Testimonial published successfully .</h3>';
            $this->session->set_userdata($sdata);
            redirect('admin/testimonial', 'refresh');
        } else {
            $sdata['error'] = '<h3>Testimonial publication failed !</h3>';
            $this->session->set_userdata($sdata);
            redirect('admin/testimonial', 'refresh');
        }
    }

    public function delete_testimonial($testimonial_id) {
        $result = $this->testimonial_mdl->delete_testimonial_by_id($testimonial_id);

        if (!empty($result)) {
            $sdata['message'] = '<h3>Testimonial remove successfully .</h3>';
            $this->session->set_userdata($sdata);
            redirect('admin/testimonial', 'refresh');
        } else {
            $sdata['error'] = '<h3>Testimonial removation failed !</h3>';
            $this->session->set_userdata($sdata);
            redirect('admin/testimonial', 'refresh');
        }
    }

}
