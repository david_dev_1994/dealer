<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends MSN_Controller {
    
    public function __construct() {
        parent::__construct();
        $this->load->model('admin_models/Others_Model', 'others_mdl');
        $this->load->model('admin_models/Profile_Model', 'pro_mdl');
        $this->load->model('admin_models/User_Quote_Model', 'quote_mdl');
        $this->admin_login_authentication();
    }

    public function index($days = null) {
        
        // echo '<pre>';
        // $this->session->userdata();
        // die();
        
        // $data['title'] = 'Dashboard';
        // $data['menu_active'] = 'Dashboard';
        // $data['email_notification'] = $this->others_mdl->get_all_unred_notification();
        // $data['user_info'] = $this->pro_mdl->get_user_info();
        // $data['total_user'] = $this->db->count_all('tbl_user_login_info');
        // $data['total_admin'] = $this->others_mdl->count_all_admin();
        // $data['total_dealers'] = $this->others_mdl->count_all_dealers();
        // $data['t_user'] = $this->others_mdl->count_users();
        // $data['open_quote'] = $this->others_mdl->open_quote();
        // $total_dollars = $this->others_mdl->total_dollars();
        // $data['total_dollars'] = array_column($total_dollars,'total_dollars');
        // $drafted_total_dollars = $this->others_mdl->drafted_total_dollars();
        // $data['drafted_total_dollars'] = array_column($drafted_total_dollars,'total_dollars');
        $data['title'] = 'Dashboard';
        $data['menu_active'] = 'Dashboard';
        $data['email_notification'] = $this->others_mdl->get_all_unred_notification();
        $data['user_info'] = $this->pro_mdl->get_user_info();
        $data['total_user'] = $this->db->count_all('tbl_user_login_info');
        $data['total_admin'] = $this->others_mdl->count_all_admin();
        $data['t_user'] = $this->others_mdl->count_users();
        if(!$days){

            $data['total_dealers'] = $this->others_mdl->count_all_dealers();
            $data['active_dealers'] = $this->others_mdl->active_users();
            $data['open_quote'] = $this->others_mdl->open_quote();
            // echo '<pre>';print_r($data['open_quote']);die();
            $data['open_drafted_quote'] = $this->others_mdl->open_drafted_quote();
            $total_dollars = $this->others_mdl->total_dollars();
            $data['total_dollars'] = array_column($total_dollars,'total_dollars');
            $drafted_total_dollars = $this->others_mdl->drafted_total_dollars();
            $data['drafted_total_dollars'] = array_column($drafted_total_dollars,'total_dollars');
        }else{
            $data['open_quote'] = $this->others_mdl->open_quote($days);
            $data['open_drafted_quote'] = $this->others_mdl->open_drafted_quote($days);
            $total_dollars = $this->others_mdl->total_dollars($days);
            $data['total_dollars'] = array_column($total_dollars,'total_dollars');
            $drafted_total_dollars = $this->others_mdl->drafted_total_dollars($days);
            $data['drafted_total_dollars'] = array_column($drafted_total_dollars,'total_dollars');
            $data['total_dealers'] = $this->others_mdl->count_all_dealers($days);
            $data['active_dealers'] = $this->others_mdl->active_users($days);
            $data['get_by_30'] = true;
        }
//        print_r($data);exit;
        $this->load->view('admin_panel/common/header', $data);
        $this->load->view('admin_panel/common/menu_bar', $data);
        $this->load->view('admin_panel/dashboard/dashboard');
        $this->load->view('admin_panel/common/footer');
    }
    
    
    public function getQuote($quoteId,$userId){
        $quote_info = $this->quote_mdl->get_all_quote_info_by_quote_id_and_user_id($quoteId,$userId);
        $quote_info['shipping_address'] = json_decode($quote_info['shipping_address']);
        $quote_info['shipping_city'] = json_decode($quote_info['shipping_city']);
        $quote_info['shipping_contactinfo'] = json_decode($quote_info['shipping_contactinfo']);
        $quote_info['shipping_state'] = json_decode($quote_info['shipping_state']);
        $quote_info['shipping_zip'] = json_decode($quote_info['shipping_zip']);

        $quote_info['billing_address'] = json_decode($quote_info['billing_address']);
        $quote_info['billing_city'] = json_decode($quote_info['billing_city']);
        $quote_info['billing_contactinfo'] = json_decode($quote_info['billing_contactinfo']);
        $quote_info['billing_state'] = json_decode($quote_info['billing_state']);
        $quote_info['billing_zip'] = json_decode($quote_info['billing_zip']);

        $quote_info['model'] = json_decode($quote_info['model']);
        $quote_info['description'] = json_decode($quote_info['description']);
        $quote_info['family'] = json_decode($quote_info['family']);
        $quote_info['type'] = json_decode($quote_info['type']);
        $quote_info['list_price'] = json_decode($quote_info['list_price']);
        $quote_info['dealer_price'] = json_decode($quote_info['dealer_price']);
        $quote_info['quantity'] = json_decode($quote_info['quantity']);
        $quote_info['price'] = json_decode($quote_info['price']);
        $resArray = array();
        foreach($quote_info['model'] as $i => $val ){
            $resArray[$i]['model'] = $val;
            $resArray[$i]['description'] = $quote_info['description'][$i];
            $resArray[$i]['family'] = $quote_info['family'][$i];
            $resArray[$i]['type'] = $quote_info['type'][$i];
            $resArray[$i]['list_price'] = $quote_info['list_price'][$i];
            $resArray[$i]['dealer_price'] = $quote_info['dealer_price'][$i];
            $resArray[$i]['quantity'] = $quote_info['quantity'][$i];
            $resArray[$i]['price'] = $quote_info['price'][$i];
        }
        echo json_encode($resArray);
        exit;
    }
    
    
    
    //change 1/11/2019
    public function addGpsDealerManager(){
        $data['title'] = 'Gps Dealer';
        $data['menu_active'] = 'Add Gps Manager';
        $data['access_label'] = 4;
        $data['users_info'] = '';
        $this->load->view('admin_panel/common/header', $data);
        $this->load->view('admin_panel/common/menu_bar', $data);
        $this->load->view('admin_panel/dashboard/add_gps_dealer');
        $this->load->view('admin_panel/common/footer');
    }
    
    
    
    
    
    
    
    
    

}
