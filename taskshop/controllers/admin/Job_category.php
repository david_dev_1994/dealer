<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/* * *******************************************#
  #      User Management System                 #
  #*********************************************#
  #      Author:     Atique IT                  #
  #      Email:      info@atique-it.com         #
  #      Website:    http://atique-it.com       #
  #                                             #
  #      Version:    15.2.1                     #
  #      Copyright:  (c) 2015 - Atique IT       #
  #                                             #
  #*********************************************# */

class Job_category extends MSN_Controller {

    public function __construct() {
        parent::__construct();

        $this->admin_login_authentication();
        $this->super_admin_and_admin_authentication_only();
        $this->load->model('admin_models/Others_Model', 'others_mdl');
        $this->load->model('admin_models/Profile_Model', 'pro_mdl');
        $this->load->model('admin_models/Job_Category_Model', 'jcat_mdl');
    }

    public function index() {
        $data['title'] = 'Job Category';
        $data['menu_active'] = 'Job Category';
        $data['email_notification'] = $this->others_mdl->get_all_unred_notification();
        $data['user_info'] = $this->pro_mdl->get_user_info();
        $data['job_category_info'] = $this->jcat_mdl->get_all_job_category_info();
        $this->load->view('admin_panel/common/header', $data);
        $this->load->view('admin_panel/common/menu_bar');
        $this->load->view('admin_panel/job_category/job_category', $data);
        $this->load->view('admin_panel/common/footer');
    }

    public function add_job_category() {
        $data['title'] = 'Job Category';
        $data['menu_active'] = 'Job Category';
        $data['email_notification'] = $this->others_mdl->get_all_unred_notification();
        $data['user_info'] = $this->pro_mdl->get_user_info();
        $this->load->view('admin_panel/common/header', $data);
        $this->load->view('admin_panel/common/menu_bar');
        $this->load->view('admin_panel/job_category/add_job_category', $data);
        $this->load->view('admin_panel/common/footer');
    }

    public function save_job_category() {
        $config = array(
            array(
                'field' => 'data[job_category_name]',
                'label' => 'job category name',
                'rules' => 'trim|required|min_length[2]|max_length[150]'
            ),
            array(
                'field' => 'data[job_category_description]',
                'label' => 'job category description',
                'rules' => 'trim|required'
            ),
            array(
                'field' => 'data[publication_status]',
                'label' => 'publication status',
                'rules' => 'required'
            )
        );
        $this->form_validation->set_rules($config);
        if ($this->form_validation->run() == FALSE) {
            $this->add_job_category();
        } else {
            $data = $this->input->post('data', TRUE);
            $data['user_id'] = $this->session->userdata('user_id');

            $result = $this->jcat_mdl->insert_job_category_info($data);
            if (!empty($result)) {
                $sdata['message'] = '<h3>Category add successfully .</h3>';
                $this->session->set_userdata($sdata);
                redirect('admin/job_category', 'refresh');
            } else {
                $sdata['error'] = '<h3>Category insertion failed </h3>';
                $this->session->set_userdata($sdata);
                redirect('admin/job_category', 'refresh');
            }
        }
    }

    public function unpublished_job_category($job_category_id) {
        $result = $this->jcat_mdl->unpublished_job_category_by_id($job_category_id);

        if (!empty($result)) {
            $sdata['message'] = '<h3>Category unpublished successfully .</h3>';
            $this->session->set_userdata($sdata);
            redirect('admin/job_category', 'refresh');
        } else {
            $sdata['error'] = '<h3>Category unpublication failed !</h3>';
            $this->session->set_userdata($sdata);
            redirect('admin/job_category', 'refresh');
        }
    }

    public function published_job_category($job_category_id) {
        $result = $this->jcat_mdl->published_job_category_by_id($job_category_id);

        if (!empty($result)) {
            $sdata['message'] = '<h3>Category published successfully .</h3>';
            $this->session->set_userdata($sdata);
            redirect('admin/job_category', 'refresh');
        } else {
            $sdata['error'] = '<h3>Category publication failed !</h3>';
            $this->session->set_userdata($sdata);
            redirect('admin/job_category', 'refresh');
        }
    }

    public function edit_job_category($job_category_id) {
        $data['job_category_info'] = $this->jcat_mdl->get_job_category_by_id($job_category_id);

        if (!empty($data['job_category_info'])) {
            $data['title'] = 'Job Category';
            $data['menu_active'] = 'Job Category';
            $data['email_notification'] = $this->others_mdl->get_all_unred_notification();
            $data['user_info'] = $this->pro_mdl->get_user_info();
            $this->load->view('admin_panel/common/header', $data);
            $this->load->view('admin_panel/common/menu_bar');
            $this->load->view('admin_panel/job_category/edit_job_category', $data);
            $this->load->view('admin_panel/common/footer');
        } else {
            $sdata['error'] = '<h3>Category not found for this id !</h3>';
            $this->session->set_userdata($sdata);
            redirect('admin/job_category', 'refresh');
        }
    }

    public function update_job_category() {
        $job_category_id = $this->input->post('job_category_id', TRUE);
        $config = array(
            array(
                'field' => 'data[job_category_name]',
                'label' => 'job_category name',
                'rules' => 'trim|required|min_length[2]|max_length[150]'
            ),
            array(
                'field' => 'data[job_category_description]',
                'label' => 'job_category description',
                'rules' => 'trim|required'
            ),
            array(
                'field' => 'data[publication_status]',
                'label' => 'publication status',
                'rules' => 'required'
            )
        );
        $this->form_validation->set_rules($config);
        if ($this->form_validation->run() == FALSE) {
            $this->edit_job_category($job_category_id);
        } else {
            $data = $this->input->post('data', TRUE);
            $result = $this->jcat_mdl->update_job_category_by_id($job_category_id, $data);
            if (!empty($result)) {
                $sdata['message'] = '<h3>Job Category update successfully .</h3>';
                $this->session->set_userdata($sdata);
                redirect('admin/job_category/edit_job_category/' . $job_category_id, 'refresh');
            } else {
                $sdata['error'] = '<h3>Job Category updation failed !</h3>';
                $this->session->set_userdata($sdata);
                redirect('admin/job_category/edit_job_category/' . $job_category_id, 'refresh');
            }
        }
    }

    public function delete_job_category($job_category_id) {
        $result = $this->jcat_mdl->delete_job_category_by_id($job_category_id);

        if (!empty($result)) {
            $sdata['message'] = '<h3>Category remove successfully .</h3>';
            $this->session->set_userdata($sdata);
            redirect('admin/job_category', 'refresh');
        } else {
            $sdata['error'] = '<h3>Category removation failed !</h3>';
            $this->session->set_userdata($sdata);
            redirect('admin/job_category', 'refresh');
        }
    }

}
