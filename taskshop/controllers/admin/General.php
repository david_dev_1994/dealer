<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class General extends MSN_Controller {
    
    public function __construct() {
        parent::__construct();
        $this->load->model('admin_models/Others_Model', 'others_mdl');
        $this->load->model('admin_models/Profile_Model', 'pro_mdl');
        $this->admin_login_authentication();
        $this->super_admin_and_admin_authentication_only();
        // ini_set('allow_url_fopen',1);
        error_reporting(E_ERROR | E_PARSE);
    }

    function dealerAgreement()
    {
        if($this->session->userdata('access_label') == 2){
            $sdata['error'] = 'You don\'t have access to perform this action!';
            $this->session->set_userdata($sdata);
            redirect('admin/dashboard', 'refresh');
        }
        $data['title'] = 'Dealer Agreement';
        $data['menu_active'] = 'Dealer Agreement';
        $data['data'] = $this->others_mdl->get('dealer_agreement', '', 'id', 'desc');
        $this->load->view('admin_panel/common/header',$data);
        $this->load->view('admin_panel/common/menu_bar');
        $this->load->view('admin_panel/agreement/dealerAgreement',$data);
        $this->load->view('admin_panel/common/footer');
    }

    function addDealerAgreement($id=''){
        if($this->session->userdata('access_label') == 2){
            $sdata['error'] = 'You don\'t have access to perform this action!';
            $this->session->set_userdata($sdata);
            redirect('admin/dashboard', 'refresh');
        }
        if($this->input->post()){
            $content=  $this->others_mdl->get_where('*' ,array('id'=>$id),'dealer_agreement');
            $this->form_validation->set_rules('title', 'Title', 'trim|required');

            if (isset($_FILES['file']['name'])&&empty($_FILES['file']['name'])&& empty($id))
            {
                $this->form_validation->set_rules('docFile', 'File', 'required');
            }

            if ($this->form_validation->run())
            {
                $db_data=array(
                    'title'=> $this->input->post('title')
                );

                $upload_status=$this->files_upload();
                if($upload_status['status']==200){
                    if ($id) {
                        if($upload_status['file']!=''){
                            if($content['docFile']!='')
                                @unlink(BASEDIR.$content['docFile']);
                            $db_data['docFile']=$upload_status['file'];
                        }
                        $db_data['updated_at'] = time();
                        $this->others_mdl->update($id, $db_data, 'dealer_agreement');
                    } else {
                        $db_data['docFile']=$upload_status['file'];
                        $db_data['created_at'] = time();
                        $db_data['updated_at'] = time();
                        $this->others_mdl->save('dealer_agreement', $db_data);
                    }
                    $this->session->set_flashdata('success','Record Saved successfully.');
                    redirect(base_url('admin/dealerAgreement'));
                }else {
                    $data['error'] = $upload_status['message'];
                }
            }
        }
        if($id)
            $data['data']=$this->others_mdl->get_where('*' ,array('id'=>$id),'dealer_agreement');
        $data['title'] = 'Dealer Agreement';
        $data['menu_active'] = 'Dealer Agreement';
        $this->load->view('admin_panel/common/header',$data);
        $this->load->view('admin_panel/common/menu_bar');
        $this->load->view('admin_panel/agreement/addDealerAgreement',$data);
        $this->load->view('admin_panel/common/footer');
    }

    function deleteDealerAgreement($id){
        if($this->session->userdata('access_label') == 2){
            $sdata['error'] = 'You don\'t have access to perform this action!';
            $this->session->set_userdata($sdata);
            redirect('admin/dashboard', 'refresh');
        }
        $data['title'] = 'Dealer Agreement';
        $data['menu_active'] = 'Dealer Agreement';
        $list_order = $this->others_mdl->get_where('*',array('id'=>$id),'dealer_agreement');
        if(isset($list_order)){
            if ($list_order['docFile'] != '')
                @unlink(BASEDIR . $list_order['docFile']);
            $this->others_mdl->delete('dealer_agreement',$id);
            $this->session->set_flashdata('success','Record deleted successfully');
        }
        redirect(base_url('admin/dealerAgreement'));
    }

    function marketingMaterials()
    {
        if($this->session->userdata('access_label') == 2){
            $sdata['error'] = 'You don\'t have access to perform this action!';
            $this->session->set_userdata($sdata);
            redirect('admin/dashboard', 'refresh');
        }
        $data['title'] = 'Marketing Materials';
        $data['menu_active'] = 'Marketing Materials';
        $data['data'] = $this->others_mdl->get('marketing_material', '', 'id', 'desc');
        $this->load->view('admin_panel/common/header',$data);
        $this->load->view('admin_panel/common/menu_bar');
        $this->load->view('admin_panel/marketing/marketingMaterial',$data);
        $this->load->view('admin_panel/common/footer');
    }

    function addMarketingMaterial($id=''){
        if($this->session->userdata('access_label') == 2){
            $sdata['error'] = 'You don\'t have access to perform this action!';
            $this->session->set_userdata($sdata);
            redirect('admin/dashboard', 'refresh');
        }
        if($this->input->post()){
//            print_r($_FILES['image']);exit;
            $content=  $this->others_mdl->get_where('*' ,array('id'=>$id),'marketing_material');
            $this->form_validation->set_rules('title', 'Title', 'trim|required');

            if (isset($_FILES['image']['name'])&&empty($_FILES['image']['name'])&& empty($id))
            {
                $this->form_validation->set_rules('image', 'Image', 'required');
            }

            if ($this->form_validation->run())
            {
                $db_data=array(
                    'title'=> $this->input->post('title')
                );

                $upload_status=$this->file_upload();
                if($upload_status['status']==200){
                    if ($id) {
                        if($upload_status['logo']!=''){
                            if($content['image']!='')
                                @unlink(BASEDIR.$content['image']);
                            $db_data['image']=$upload_status['logo'];
                        }
                        $db_data['updated_at'] = time();
                        $this->others_mdl->update($id, $db_data, 'marketing_material');
                    } else {
                        $db_data['image']=$upload_status['logo'];
                        $db_data['created_at'] = time();
                        $db_data['updated_at'] = time();
                        $this->others_mdl->save('marketing_material', $db_data);
                    }
                    $this->session->set_flashdata('success','Record Saved successfully.');
                    redirect(base_url('admin/marketingMaterials'));
                }else {
                    $data['error'] = $upload_status['message'];
                }
            }
        }
        if($id)
            $data['data']=$this->others_mdl->get_where('*' ,array('id'=>$id),'marketing_material');
        $data['title'] = 'Marketing Materials';
        $data['menu_active'] = 'Marketing Materials';

//        print_r($data);exit;
        $this->load->view('admin_panel/common/header',$data);
        $this->load->view('admin_panel/common/menu_bar');
        $this->load->view('admin_panel/marketing/addMarketingMaterial',$data);
        $this->load->view('admin_panel/common/footer');
    }

    function deleteMarketingMaterial($id){
        if($this->session->userdata('access_label') == 2){
            $sdata['error'] = 'You don\'t have access to perform this action!';
            $this->session->set_userdata($sdata);
            redirect('admin/dashboard', 'refresh');
        }
        $data['title'] = 'Marketing Materials';
        $data['menu_active'] = 'Marketing Materials';
        $list_order = $this->others_mdl->get_where('*',array('id'=>$id),'marketing_material');
        if(isset($list_order)){
            if ($list_order['image'] != '')
                @unlink(BASEDIR . $list_order['image']);
            $this->others_mdl->delete('marketing_material',$id);
            $this->session->set_flashdata('success','Record deleted successfully');
        }
        redirect(base_url('admin/marketingMaterials'));
    }

    function ProductAnnouncements()
    {
        if($this->session->userdata('access_label') == 2){
            $sdata['error'] = 'You don\'t have access to perform this action!';
            $this->session->set_userdata($sdata);
            redirect('admin/dashboard', 'refresh');
        }
        $data['title'] = 'Press Releases';
        $data['menu_active'] = 'Press Releases';
        $data['data'] = $this->others_mdl->get('pressreleases', '', 'id', 'desc');
        $this->load->view('admin_panel/common/header',$data);
        $this->load->view('admin_panel/common/menu_bar');
        $this->load->view('admin_panel/Press_releases/pressReleases',$data);
        $this->load->view('admin_panel/common/footer');
    }

    function addProductAnnouncement($id=''){
        if($this->session->userdata('access_label') == 2){
            $sdata['error'] = 'You don\'t have access to perform this action!';
            $this->session->set_userdata($sdata);
            redirect('admin/dashboard', 'refresh');
        }
        if($this->input->post()){
            $content=  $this->others_mdl->get_where('*' ,array('id'=>$id),'pressreleases');
            $this->form_validation->set_rules('title', 'Title', 'trim|required');

            if (isset($_FILES['file']['name'])&&empty($_FILES['file']['name'])&& empty($id))
            {
                $this->form_validation->set_rules('docFile', 'File', 'required');
            }

            if ($this->form_validation->run())
            {
                $db_data=array(
                    'title'=> $this->input->post('title')
                );

                $upload_status=$this->files_upload();
                if($upload_status['status']==200){
                    if ($id) {
                        if($upload_status['file']!=''){
                            if($content['docFile']!='')
                                @unlink(BASEDIR.$content['docFile']);
                            $db_data['docFile']=$upload_status['file'];
                        }
                        $db_data['updated_at'] = time();
                        $this->others_mdl->update($id, $db_data, 'pressreleases');
                    } else {
                        $db_data['docFile']=$upload_status['file'];
                        $db_data['created_at'] = time();
                        $db_data['updated_at'] = time();
                        $this->others_mdl->save('pressreleases', $db_data);
                    }
                    $this->session->set_flashdata('success','Record Saved successfully.');
                    redirect(base_url('admin/ProductAnnouncements'));
                }else {
                    $data['error'] = $upload_status['message'];
                }
            }
        }
        if($id)
            $data['data']=$this->others_mdl->get_where('*' ,array('id'=>$id),'pressreleases');
        $data['title'] = 'Press Releases';
        $data['menu_active'] = 'Press Releases';
        $this->load->view('admin_panel/common/header',$data);
        $this->load->view('admin_panel/common/menu_bar');
        $this->load->view('admin_panel/Press_releases/addRelease',$data);
        $this->load->view('admin_panel/common/footer');
    }

    function deleteProductAnnouncement($id){
        if($this->session->userdata('access_label') == 2){
            $sdata['error'] = 'You don\'t have access to perform this action!';
            $this->session->set_userdata($sdata);
            redirect('admin/dashboard', 'refresh');
        }
        $data['title'] = 'Press Releases';
        $data['menu_active'] = 'Press Releases';
        $list_order = $this->others_mdl->get_where('*',array('id'=>$id),'pressreleases');
        if(isset($list_order)){
            if ($list_order['docFile'] != '')
                @unlink(BASEDIR . $list_order['docFile']);
            $this->others_mdl->delete('pressreleases',$id);
            $this->session->set_flashdata('success','Record deleted successfully');
        }
        redirect(base_url('admin/ProductAnnouncements'));
    }

    function corporateAnnouncements()
    {
        if($this->session->userdata('access_label') == 2){
            $sdata['error'] = 'You don\'t have access to perform this action!';
            $this->session->set_userdata($sdata);
            redirect('admin/dashboard', 'refresh');
        }
        $data['title'] = 'Corporate';
        $data['menu_active'] = 'Corporate';
        $data['data'] = $this->others_mdl->get('corporate_announcement', '', '', 'asc');
        $this->load->view('admin_panel/common/header',$data);
        $this->load->view('admin_panel/common/menu_bar');
        $this->load->view('admin_panel/Corporate/corporateAnnouncements',$data);
        $this->load->view('admin_panel/common/footer');
    }

    function addCorporateAnnouncement($id=''){
        if($this->session->userdata('access_label') == 2){
            $sdata['error'] = 'You don\'t have access to perform this action!';
            $this->session->set_userdata($sdata);
            redirect('admin/dashboard', 'refresh');
        }
        if($this->input->post()){
            $content=  $this->others_mdl->get_where('*' ,array('id'=>$id),'corporate_announcement');
            $this->form_validation->set_rules('title', 'Title', 'trim|required');

            if (isset($_FILES['file']['name'])&&empty($_FILES['file']['name'])&& empty($id))
            {
                $this->form_validation->set_rules('docFile', 'File', 'required');
            }

            if ($this->form_validation->run())
            {
                $db_data=array(
                    'title'=> $this->input->post('title')
                );
                $upload_status = $this->files_upload();
//                print_r($upload_status);exit;
                if($upload_status['status']==200){
                    if ($id) {
                        if($upload_status['file']!=''){
                            if($content['docFile']!='')
                                @unlink(BASEDIR.$content['docFile']);
                            $db_data['docFile']=$upload_status['file'];
                        }
                        $db_data['updated_at'] = time();
                        $this->others_mdl->update($id, $db_data, 'corporate_announcement');
                    } else {
                        $db_data['docFile']=$upload_status['file'];
                        $db_data['created_at'] = time();
                        $db_data['updated_at'] = time();
                        $this->others_mdl->save('corporate_announcement', $db_data);
                    }
                    $this->session->set_flashdata('success','Record Saved successfully.');
                    redirect(base_url('admin/corporateAnnouncements'));
                }else{
                    $data['error']= $upload_status['message'];
                }
            }
        }
        if($id)
            $data['data']=$this->others_mdl->get_where('*' ,array('id'=>$id),'corporate_announcement');
        $data['title'] = 'Corporate';
        $data['menu_active'] = 'Corporate';
        $this->load->view('admin_panel/common/header',$data);
        $this->load->view('admin_panel/common/menu_bar');
        $this->load->view('admin_panel/Corporate/addCorporateAnnouncement',$data);
        $this->load->view('admin_panel/common/footer');
    }

    function deleteCorporateAnnouncement($id){
        if($this->session->userdata('access_label') == 2){
            $sdata['error'] = 'You don\'t have access to perform this action!';
            $this->session->set_userdata($sdata);
            redirect('admin/dashboard', 'refresh');
        }
        $data['title'] = 'Corporate';
        $data['menu_active'] = 'Corporate';
        $list_order = $this->others_mdl->get_where('*',array('id'=>$id),'corporate_announcement');
        if(isset($list_order)){
            if ($list_order['docFile'] != '')
                @unlink(BASEDIR . $list_order['docFile']);
            $this->others_mdl->delete('corporate_announcement',$id);
            $this->session->set_flashdata('success','Record deleted successfully');
        }
        redirect(base_url('admin/corporateAnnouncements'));
    }

    function brochures()
    {
        if($this->session->userdata('access_label') == 2){
            $sdata['error'] = 'You don\'t have access to perform this action!';
            $this->session->set_userdata($sdata);
            redirect('admin/dashboard', 'refresh');
        }
        $data['title'] = 'Brochures';
        $data['menu_active'] = 'Brochures';
        $data['data'] = $this->others_mdl->get('brochures', '', '', 'asc');
        $this->load->view('admin_panel/common/header',$data);
        $this->load->view('admin_panel/common/menu_bar');
        $this->load->view('admin_panel/brochures/brochures',$data);
        $this->load->view('admin_panel/common/footer');
    }

    function addBrochure($id=''){
        if($this->session->userdata('access_label') == 2){
            $sdata['error'] = 'You don\'t have access to perform this action!';
            $this->session->set_userdata($sdata);
            redirect('admin/dashboard', 'refresh');
        }
        if($this->input->post()){
            $content=  $this->others_mdl->get_where('*' ,array('id'=>$id),'brochures');
            $this->form_validation->set_rules('title', 'Title', 'trim|required');

            if (isset($_FILES['file']['name'])&&empty($_FILES['file']['name'])&& empty($id))
            {
                $this->form_validation->set_rules('docFile', 'File', 'required');
            }

            if ($this->form_validation->run())
            {
                $db_data=array(
                    'title'=> $this->input->post('title')
                );
                $upload_status = $this->files_upload();
                if($upload_status['status']==200){
                    if ($id) {
                        if($upload_status['file']!=''){
                            if($content['docFile']!='')
                                @unlink(BASEDIR.$content['docFile']);
                            $db_data['docFile']=$upload_status['file'];
                        }
                        $db_data['updated_at'] = time();
                        $this->others_mdl->update($id, $db_data, 'brochures');
                    } else {
                        $db_data['docFile']=$upload_status['file'];
                        $db_data['created_at'] = time();
                        $db_data['updated_at'] = time();
                        $this->others_mdl->save('brochures', $db_data);
                    }
                    $this->session->set_flashdata('success','Record Saved successfully.');
                    redirect(base_url('admin/brochures'));
                }else{
                    $data['error']= $upload_status['message'];
                }
            }
        }
        if($id)
            $data['data']=$this->others_mdl->get_where('*' ,array('id'=>$id),'brochures');
        $data['title'] = 'Brochures';
        $data['menu_active'] = 'Brochures';
        $this->load->view('admin_panel/common/header',$data);
        $this->load->view('admin_panel/common/menu_bar');
        $this->load->view('admin_panel/brochures/addBrochure',$data);
        $this->load->view('admin_panel/common/footer');
    }

    function addBrochureLink($id=''){
        if($this->session->userdata('access_label') == 2){
            $sdata['error'] = 'You don\'t have access to perform this action!';
            $this->session->set_userdata($sdata);
            redirect('admin/dashboard', 'refresh');
        }
        if($this->input->post()){
            $content=  $this->others_mdl->get_where('*' ,array('id'=>$id),'brochures');
            $this->form_validation->set_rules('title', 'Title', 'trim|required');
            $this->form_validation->set_rules('url', 'URL', 'trim|required|valid_url');

            if ($this->form_validation->run())
            {
                // $filename = basename($this->input->post('url'));
                // file_put_contents('uploads/'.$filename,file_get_contents($this->input->post('url')));

                $db_data=array(
                    'title'=> $this->input->post('title'),
                    'url'=> $this->input->post('url')
                );

                if ($id) {
                    $db_data['updated_at'] = time();
                    $this->others_mdl->update($id, $db_data, 'brochures');
                } else {
                    $db_data['created_at'] = time();
                    $db_data['updated_at'] = time();
                    $this->others_mdl->save('brochures', $db_data);
                }
                $this->session->set_flashdata('success','Record Saved successfully.');
                redirect(base_url('admin/brochures'));
            }
        }
        if($id)
            $data['data']=$this->others_mdl->get_where('*' ,array('id'=>$id),'brochures');
        $data['title'] = 'Brochures';
        $data['menu_active'] = 'Brochures';
        $this->load->view('admin_panel/common/header',$data);
        $this->load->view('admin_panel/common/menu_bar');
        $this->load->view('admin_panel/brochures/addBrochureLink',$data);
        $this->load->view('admin_panel/common/footer');
    }

    function deleteBrochure($id){
        if($this->session->userdata('access_label') == 2){
            $sdata['error'] = 'You don\'t have access to perform this action!';
            $this->session->set_userdata($sdata);
            redirect('admin/dashboard', 'refresh');
        }
        $data['title'] = 'Brochures';
        $data['menu_active'] = 'Brochures';
        $list_order = $this->others_mdl->get_where('*',array('id'=>$id),'brochures');
        if(isset($list_order)){
            if ($list_order['docFile'] != '')
                @unlink(BASEDIR . $list_order['docFile']);
            $this->others_mdl->delete('brochures',$id);
            $this->session->set_flashdata('success','Record deleted successfully');
        }
        redirect(base_url('admin/brochures'));
    }

    function videos()
    {
        if($this->session->userdata('access_label') == 2){
            $sdata['error'] = 'You don\'t have access to perform this action!';
            $this->session->set_userdata($sdata);
            redirect('admin/dashboard', 'refresh');
        }
        $data['title'] = 'Videos';
        $data['menu_active'] = 'Videos';
        $data['data'] = $this->others_mdl->get('cbt_videos', '', '', 'asc');
        $this->load->view('admin_panel/common/header',$data);
        $this->load->view('admin_panel/common/menu_bar');
        $this->load->view('admin_panel/cbt_videos/Videos',$data);
        $this->load->view('admin_panel/common/footer');
    }

    function addVideo($id=''){
        if($this->session->userdata('access_label') == 2){
            $sdata['error'] = 'You don\'t have access to perform this action!';
            $this->session->set_userdata($sdata);
            redirect('admin/dashboard', 'refresh');
        }
        if($this->input->post()){
            $content=  $this->others_mdl->get_where('*' ,array('id'=>$id),'cbt_videos');

            $this->form_validation->set_rules('title', 'Title', 'trim|required');
            $this->form_validation->set_rules('url', 'Youtube Url', 'trim|valid_url|required');

            if ($this->form_validation->run())
            {
                $url = $this->input->post('url');
                preg_match('%(?:youtube(?:-nocookie)?\.com/(?:[^/]+/.+/|(?:v|e(?:mbed)?)/|.*[?&]v=)|youtu\.be/)([^"&?/ ]{11})%i', $url, $match);
                $youtube_id = $match[1];

                $db_data=array(
                    'title'=>strip_tags($this->input->post('title'),'<span><b><i><em><strong><br><em>'),
                    'video_url'=>$this->input->post('url'),
                    'video_id'=> $youtube_id
                );

                if ($id) {
                    $db_data['updated_at'] = time();
                    $this->others_mdl->update($id, $db_data, 'cbt_videos');
                } else {
                    $db_data['created_at'] = time();
                    $db_data['updated_at'] = time();
                    $this->others_mdl->save('cbt_videos', $db_data);
                }
                //$view_data['success']="Record Saved successfully.";
                $this->session->set_flashdata('success','Record Saved successfully.');
                redirect(base_url('admin/videos'));
            }
        }
        if($id)
            $data['data']=$this->others_mdl->get_where('*' ,array('id'=>$id),'cbt_videos');
        $data['title'] = 'Videos';
        $data['menu_active'] = 'Videos';
        $this->load->view('admin_panel/common/header',$data);
        $this->load->view('admin_panel/common/menu_bar');
        $this->load->view('admin_panel/cbt_videos/addVideo',$data);
        $this->load->view('admin_panel/common/footer');
    }

    function deleteVideo($id){
        if($this->session->userdata('access_label') == 2){
            $sdata['error'] = 'You don\'t have access to perform this action!';
            $this->session->set_userdata($sdata);
            redirect('admin/dashboard', 'refresh');
        }
        $data['title'] = 'Videos';
        $data['menu_active'] = 'Videos';
        $list_order = $this->others_mdl->get_where('*',array('id'=>$id),'cbt_videos');
        if(isset($list_order)){
//            if ($list_order['docFile'] != '')
//                @unlink(BASEDIR . $list_order['docFile']);
            $this->others_mdl->delete('cbt_videos',$id);
            $this->session->set_flashdata('success','Record deleted successfully');
        }
        redirect(base_url('admin/videos'));
    }


    public function price_limit()
    {
        if($this->session->userdata('access_label') == 2){
            $sdata['error'] = 'You don\'t have access to perform this action!';
            $this->session->set_userdata($sdata);
            redirect('admin/dashboard', 'refresh');
        }
        if($this->session->userdata('access_label') == 2){
            $sdata['error'] = 'You don\'t have access to perform this action!';
            $this->session->set_userdata($sdata);
            redirect('admin/dashboard', 'refresh');
        }
        if($this->input->post()){
            $content=  $this->others_mdl->get_where('*' ,array('user_id'=>1),'tbl_user_login_info');
            $this->form_validation->set_rules('upload', '', 'trim');

            if (isset($_FILES['file']['name'])&&empty($_FILES['file']['name']))
            {
                $this->form_validation->set_rules('docFile', 'File', 'required');
            }

            if ($this->form_validation->run())
            {
                $upload_status = $this->csv_files_upload();
                if ($upload_status['status'] == 200) {
                    if ($content) {
                        if($upload_status['file']!=''){
                            if($content['docFile']!='')
                                @unlink(BASEDIR.$content['docFile']);
                            $db_data['docFile']=$upload_status['file'];
                        }
                        $this->others_mdl->updateWithUserId($content['user_id'], $db_data, 'tbl_user_login_info');
                    }
                    $data['success'] = "Record Saved successfully.";
                } else {
                    $data['error']= $upload_status['message'];
                }
            }
        }
        $data['title'] = 'Price Limit';
        $data['menu_active'] = 'Price Limit';
        $data['user_info'] = $this->pro_mdl->get_user_info();
        $this->load->view('admin_panel/common/header', $data);
        $this->load->view('admin_panel/common/menu_bar');
        $this->load->view('admin_panel/manage_user/price_limit', $data);
        $this->load->view('admin_panel/common/footer');
    }

    private function file_upload()
    {
        if (isset($_FILES['image']) && $_FILES['image']['size'] > 0) {
            $time = time();
            $file = $time . $_FILES['image']['name'];
            $fileName = str_replace("/\s+/", "_", $file);
            $config['file_name'] = $fileName;
            $config['upload_path'] = BASEDIR;
            $config['allowed_types'] = '*';
//            $config['allowed_types'] = 'gif|jpg|png|jpeg|svg|eps|pdf';
            $config['max_size'] = '12048';
            $this->load->library('upload', $config);
            $response = array();
            if ($this->upload->do_upload('image')) {
                $upData = $this->upload->data();
                $name = $upData['file_name'];
                $response['status'] = 200;
                $response['logo'] = $name;
            } else {
                $response['status'] = 400;
                $response['message'] = $this->upload->display_errors();
            }

        } else {
            $response['status'] = 200;
            $response['logo'] = '';
        }
        return $response;
    }

    private function files_upload()
    {
        if (isset($_FILES['file']) && $_FILES['file']['size'] > 0) {
            $time = time();
            $file = $time . $_FILES['file']['name'];
            $fileName = str_replace("/\s+/", "_", $file);
            $config['file_name'] = $fileName;
            $config['upload_path'] = BASEDIR;
            $config['allowed_types'] = 'pdf';
//            $config['max_size'] = '12048';
            $this->load->library('upload', $config);
            $response = array();
            if ($this->upload->do_upload('file')) {
                $upData = $this->upload->data();
                $name = $upData['file_name'];
                $response['status'] = 200;
                $response['file'] = $name;
            } else {
                $response['status'] = 400;
                $response['message'] = $this->upload->display_errors();
            }

        } else {
            $response['status'] = 200;
            $response['file'] = '';
        }
        return $response;
    }

    private function csv_files_upload()
    {
        if (isset($_FILES['file']) && $_FILES['file']['size'] > 0) {
            $time = time();
            $file = $time . $_FILES['file']['name'];
            $fileName = str_replace("/\s+/", "_", $file);
            $config['file_name'] = $fileName;
            $config['upload_path'] = BASEDIR;
            $config['allowed_types'] = 'csv';
//            $config['max_size'] = '12048';
            $this->load->library('upload', $config);
            $response = array();
            if ($this->upload->do_upload('file')) {
                $upData = $this->upload->data();
                $name = $upData['file_name'];
                $response['status'] = 200;
                $response['file'] = $name;
            } else {
                $response['status'] = 400;
                $response['message'] = $this->upload->display_errors();
            }

        } else {
            $response['status'] = 200;
            $response['file'] = '';
        }
        return $response;
    }



}
