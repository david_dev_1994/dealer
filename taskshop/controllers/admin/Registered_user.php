<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/* * *******************************************#
  #      User Management System                 #
  #*********************************************#
  #      Author:     Atique IT                  #
  #      Email:      info@atique-it.com         #
  #      Website:    http://atique-it.com       #
  #                                             #
  #      Version:    15.2.1                     #
  #      Copyright:  (c) 2015 - Atique IT       #
  #                                             #
  #*********************************************# */

class Registered_user extends MSN_Controller {

    public function __construct() {
        parent::__construct();

        $this->admin_login_authentication();
//        $this->super_admin_authentication_only();
        $this->load->model('admin_models/Registered_User_Model', 'user_mdl');
        $this->load->model('admin_models/Others_Model', 'others_mdl');
        $this->load->model('admin_models/Profile_Model', 'pro_mdl');
        $this->load->helper('functions_helper');
    }

    public function index() {
        $data['title'] = 'Registered User';
        $data['menu_active'] = 'Registered User';
        $data['email_notification'] = $this->others_mdl->get_all_unred_notification();
        $data['user_info'] = $this->pro_mdl->get_user_info();
        $data['users_info'] = $this->user_mdl->get_admins_info();
        $this->load->view('admin_panel/common/header', $data);
        $this->load->view('admin_panel/common/menu_bar');
        $this->load->view('admin_panel/manage_user/manage_user', $data);
        $this->load->view('admin_panel/common/footer');
    }
    
    //change 1/11/2019
    public function get_gps_d_info($access){
        
        // echo 'helllo';die();

        $data['title'] = 'List Gps Dealers';
        $data['menu_active'] = 'List Gps Dealer';
        if($access == 4){
            $data['menu_active'] = 'List Gps Manager';
        }
        $data['user_info'] = $this->pro_mdl->get_user_info();
        $data['users_info'] = $this->user_mdl->get_gps_info($access);
        $data['access'] = $access;
//        echo '<pre>';
//        print_r($data['user_info']);print_r($data['users_info']);die();
        $this->load->view('admin_panel/common/header', $data);
        $this->load->view('admin_panel/common/menu_bar');
        $this->load->view('admin_panel/manage_user/manage_dealers', $data);
        $this->load->view('admin_panel/common/footer');
    }

    public function block_user($user_id) {
        if($this->session->userdata('access_label') == 2){
            $sdata['error'] = 'You don\'t have access to perform this action!';
            $this->session->set_userdata($sdata);
            redirect('admin/registered_user', 'refresh');
        }
        if($this->session->userdata('access_label') == 1){
            $redirect = 'admin/registered_user';
        }

        $result = $this->user_mdl->block_user_by_user_id($user_id);

        if (!empty($result)) {
            $sdata['message'] = 'User block successfully .';
            $this->session->set_userdata($sdata);
            redirect($redirect, 'refresh');
        } else {
            $sdata['error'] = 'Failed to block !';
            $this->session->set_userdata($sdata);
            redirect($redirect, 'refresh');
        }
    }

    public function unblock_user($user_id) {
        if($this->session->userdata('access_label') == 2){
            $sdata['error'] = 'You don\'t have access to perform this action!';
            $this->session->set_userdata($sdata);
            redirect('admin/registered_user', 'refresh');
        }
        if($this->session->userdata('access_label') == 1){
            $redirect = 'admin/registered_user';
        }

        $result = $this->user_mdl->unblock_user_by_user_id($user_id);

        if (!empty($result)) {
            $sdata['message'] = 'User unblock successfully .';
            $this->session->set_userdata($sdata);
            redirect($redirect, 'refresh');
        } else {
            $sdata['error'] = 'Failed to unblock !';
            $this->session->set_userdata($sdata);
            redirect($redirect, 'refresh');
        }
    }

    public function delete_user($user_id) {
        if($this->session->userdata('access_label') == 2){
            $sdata['error'] = 'You don\'t have access to perform this action!';
            $this->session->set_userdata($sdata);
            redirect('admin/registered_user', 'refresh');
        }

        if($this->session->userdata('access_label') == 1){
            $redirect = 'admin/registered_user';
        }

        $result = $this->user_mdl->delete_user_by_user_id($user_id);

        if (!empty($result)) {
            $sdata['message'] = 'User remove successfully .';
            $this->session->set_userdata($sdata);
            redirect($redirect, 'refresh');
        } else {
            $sdata['error'] = 'User removation failed!';
            $this->session->set_userdata($sdata);
            redirect($redirect, 'refresh');
        }
    }
    
    public function block_dealer() {
        $this->layout = '';
        if(!empty($this->input->post('id'))) {
            $result = $this->user_mdl->block_user_by_user_id($this->input->post('id'));
            if (!empty($result)) {
                $response['status'] = 200;
                $response['message'] = 'User blocked successfully.';
            } else {
                $response['status'] = 400;
                $response['error'] = 'Failed to block !';
            }
        }else {
            $response["status"] = 404;
            $response["message"] = 'Invalid Request';
        }
        echo json_encode($response);
        exit;
    }

    public function unblock_dealer() {
        $this->layout = '';
        if(!empty($this->input->post('id'))) {
            $result = $this->user_mdl->unblock_user_by_user_id($this->input->post('id'));

            $sent = sendemail($this->input->post('email'), 'dealerportal@bktechnologies.com', 'Hi,<br/><br/> Welcome to the BK Dealer portal. You are now active!<br/><br/> Please do not reply to this email. It is used for outbound messages only.<br/> Please contact sales@bktechnologies.com for assistance.', 'Account Activation');

            if (!empty($result) && $sent) {
                $response['message'] = 'User unblocked successfully.';
                $response['status'] = 200;
            } else {
                $response['status'] = 400;
                $response['error'] = 'Failed to unblock !';
            }
        }else {
            $response["status"] = 404;
            $response["message"] = 'Invalid Request';
        }
        echo json_encode($response);
        exit;
    }

    public function delete_dealer() {
        $this->layout = '';
        if(!empty($this->input->post('id'))) {
        $result = $this->user_mdl->delete_user_by_user_id($this->input->post('id'));

        if (!empty($result)) {
            $response['message'] = 'User deleted successfully.';
            $response['status'] = 200;
        } else {
            $response['status'] = 400;
            $response['error'] = 'User deletion failed!';
        }
        }else {
            $response["status"] = 404;
            $response["message"] = 'Invalid Request';
        }
        echo json_encode($response);
        exit;
    }

    // public function block_dealer($user_id) {
    //     if($this->session->userdata('access_label') == 2){
    //         $sdata['error'] = 'You don\'t have access to perform this action!';
    //         $this->session->set_userdata($sdata);
    //         redirect('admin/registered_dealers', 'refresh');
    //     }
    //     if($this->session->userdata('access_label') == 1){
    //         $redirect = 'admin/registered_dealers';
    //     }

    //     $result = $this->user_mdl->block_user_by_user_id($user_id);

    //     if (!empty($result)) {
    //         $sdata['message'] = 'User block successfully .';
    //         $this->session->set_userdata($sdata);
    //         redirect($redirect, 'refresh');
    //     } else {
    //         $sdata['error'] = 'Failed to block !';
    //         $this->session->set_userdata($sdata);
    //         redirect($redirect, 'refresh');
    //     }
    // }

    // public function unblock_dealer($user_id,$email) {
    //     if($this->session->userdata('access_label') == 2){
    //         $sdata['error'] = 'You don\'t have access to perform this action!';
    //         $this->session->set_userdata($sdata);
    //         redirect('admin/registered_dealers', 'refresh');
    //     }
    //     if($this->session->userdata('access_label') == 1){
    //         $redirect = 'admin/registered_dealers';
    //     }

    //     $result = $this->user_mdl->unblock_user_by_user_id($user_id);

    //     $sent = sendemail($email,'dealerportal@bktechnologies.com','Hi,<br/><br/> Welcome to the BK Dealer portal. You are now active!<br/><br/> Please do not reply to this email. It is used for outbound messages only.<br/> Please contact sales@bktechnologies.com for assistance.','Account Activation');

    //     if (!empty($result) && $sent) {
    //         $sdata['message'] = 'User unblocked successfully .';
    //         $this->session->set_userdata($sdata);
    //         redirect($redirect, 'refresh');
    //     } else {
    //         $sdata['error'] = 'Failed to unblock !';
    //         $this->session->set_userdata($sdata);
    //         redirect($redirect, 'refresh');
    //     }
    // }

    // public function delete_dealer($user_id) {

    //     if($this->session->userdata('access_label') == 2){
    //         $sdata['error'] = 'You don\'t have access to perform this action!';
    //         $this->session->set_userdata($sdata);
    //         redirect('admin/registered_dealers', 'refresh');
    //     }

    //     if($this->session->userdata('access_label') == 1){
    //         $redirect = 'admin/registered_dealers';
    //     }

    //     $result = $this->user_mdl->delete_user_by_user_id($user_id);

    //     if (!empty($result)) {
    //         $sdata['message'] = 'User remove successfully .';
    //         $this->session->set_userdata($sdata);
    //         redirect($redirect, 'refresh');
    //     } else {
    //         $sdata['error'] = 'User removation failed!';
    //         $this->session->set_userdata($sdata);
    //         redirect($redirect, 'refresh');
    //     }
    // }

    public function update_access_label($user_id) {
        $data['access_label'] = $this->input->post('access_label', TRUE);

        $result = $this->user_mdl->update_access_label_by_user_id($user_id, $data);

        if (!empty($result)) {
            $sdata['message'] = 'Access lebel update successfully .';
            $this->session->set_userdata($sdata);
            redirect('admin/registered_user', 'refresh');
        } else {
            $sdata['error'] = 'Access lebel updation failed !';
            $this->session->set_userdata($sdata);
            redirect('admin/registered_user', 'refresh');
        }
    }

    public function registered_dealers() {
        $data['title'] = 'Registered Dealers';
        $data['menu_active'] = 'Registered Dealers';
        $data['user_info'] = $this->pro_mdl->get_user_info();
        $data['users_info'] = $this->user_mdl->get_users_info();
        $this->load->view('admin_panel/common/header', $data);
        $this->load->view('admin_panel/common/menu_bar');
        $this->load->view('admin_panel/manage_user/manage_dealers', $data);
        $this->load->view('admin_panel/common/footer');
    }

    public function add_admin(){
        if($this->session->userdata('access_label') == 2){
            $sdata['error'] = 'You don\'t have access to perform this action!';
            $this->session->set_userdata($sdata);
            redirect('admin/registered_user', 'refresh');
        }
        if ($this->input->post()) {
            $this->form_validation->set_rules('name', 'Name', 'trim|required');
            $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email|is_unique[tbl_user_login_info.email_address]');
            $this->form_validation->set_rules('password', 'Password', 'trim|required|min_length[8]|max_length[50]');
            $this->form_validation->set_rules('address', 'Address', 'trim|required');
            $this->form_validation->set_rules('phone', 'Phone Number', 'trim|required');
            $this->form_validation->set_rules('city', 'City', 'trim|required');
            $this->form_validation->set_rules('state', 'State', 'trim|required');
            $this->form_validation->set_rules('zip', 'Zip', 'trim|required|numeric|max_length[5]');

            if ($this->form_validation->run()) {

                $this->load->helper('string');

                $login_info = array(
                    'full_name' => $this->input->post('name'),
                    'email_address' => $this->input->post('email'),
                    'user_password' => md5($this->input->post('password')),
                    'access_label' => 2,
                    'activation_status' => 1,
                    'activation_key' => md5(random_string('alnum', 16))
                );

                $profile_info = array(
                    'first_name' => $this->input->post('name'),
                    'address_1' => $this->input->post('address'),
                    'phone' => $this->input->post('phone'),
                    'city' => $this->input->post('city'),
                    'state' => $this->input->post('state'),
                    'zip' => $this->input->post('zip')
                );

                $user_id = $this->user_mdl->insert_login_info($login_info);
                $profile_info['user_id'] = $user_id;
                $basic_info = $this->user_mdl->insert_basic_info($profile_info);

                if (!empty($user_id) AND !empty($basic_info)) {
                    $sdata['message'] = 'Admin Has been Created';
                    $this->session->set_userdata($sdata);
                    redirect('admin/registered_user');
                } else {
                    $sdata['message'] = 'Admin Registration failed ! Please try again. ';
                    $this->session->set_userdata($sdata);
                    redirect('admin/registered_user');
                }
            }else{
                $this->session->set_flashdata('error',validation_errors());
                redirect('admin/add_admin');
            }
        }
        $data['title'] = 'Registered User';
        $data['menu_active'] = 'Registered User';
        $data['user_info'] = $this->pro_mdl->get_user_info();
        $this->load->view('admin_panel/common/header', $data);
        $this->load->view('admin_panel/common/menu_bar');
        $this->load->view('admin_panel/manage_user/add_admin', $data);
        $this->load->view('admin_panel/common/footer');
    }

    public function add_dealer(){
        if($this->session->userdata('access_label') == 2){
            $sdata['error'] = 'You don\'t have access to perform this action!';
            $this->session->set_userdata($sdata);
            redirect('admin/registered_dealers', 'refresh');
        }
        if ($this->input->post()) {
            $this->form_validation->set_rules('name', 'Name', 'trim|required');
            $this->form_validation->set_rules('sales_manager_id', 'Sales Manager', 'trim|required');
            $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email|is_unique[tbl_user_login_info.email_address]');
            $this->form_validation->set_rules('password', 'Password', 'trim|required');
            $this->form_validation->set_rules('address[]', 'Address', 'trim|required');
            $this->form_validation->set_rules('company', 'Company Name', 'trim|required');
            $this->form_validation->set_rules('num', 'Personal Number', 'trim|required');
            $this->form_validation->set_rules('phone[]', 'Phone Number', 'trim|required');
            $this->form_validation->set_rules('city[]', 'City', 'trim|required');
            $this->form_validation->set_rules('state[]', 'State', 'trim|required');
            $this->form_validation->set_rules('zip[]', 'Zip', 'trim|required|numeric');

            if ($this->form_validation->run()) {

                $this->load->helper('string');

                $login_info = array(
                    'full_name' => $this->input->post('name'),
                    'sales_manager_id' => $this->input->post('sales_manager_id'),
                    'email_address' => $this->input->post('email'),
                    'user_password' => md5($this->input->post('password')),
                    'access_label' => 3,
                    'activation_status' => 1,
                    'activation_key' => md5(random_string('alnum', 16))
                );
                $data = $this->input->post();
                $profile_info = array(
                    'first_name' => $this->input->post('name'),
                    'companyname' => $this->input->post('company'),
                    'phone' => $this->input->post('num'),
                );

                $profile_info['address_1'] = json_encode($data['address']);
                $profile_info['contactinfo'] = json_encode($data['phone']);
                $profile_info['city'] = json_encode($data['city']);
                $profile_info['state'] = json_encode($data['state']);
                $profile_info['zip'] = json_encode($data['zip']);


                $user_id = $this->user_mdl->insert_login_info($login_info);
                $profile_info['user_id'] = $user_id;
                $basic_info = $this->user_mdl->insert_basic_info($profile_info);

                if (!empty($user_id) AND !empty($basic_info)) {
                    $sdata['message'] = 'Dealer Has been Created';
                    $this->session->set_userdata($sdata);
                    redirect('admin/registered_dealers');
                } else {
                    $sdata['error'] = 'Dealer Registration failed ! Please try again. ';
                    $this->session->set_userdata($sdata);
                    redirect('admin/registered_dealers');
                }
            }else{
                $this->session->set_flashdata('error',validation_errors());
                redirect('admin/add_dealer');
            }
        }else {
            $data['title'] = 'Registered User';
            $data['menu_active'] = 'Registered Dealers';
            $data['users_info'] = $this->user_mdl->get_active_admins_info();
            $data['user_info'] = $this->pro_mdl->get_user_info();
            $this->load->view('admin_panel/common/header', $data);
            $this->load->view('admin_panel/common/menu_bar');
            $this->load->view('admin_panel/manage_user/add_dealer', $data);
            $this->load->view('admin_panel/common/footer');
        }
    }
}
