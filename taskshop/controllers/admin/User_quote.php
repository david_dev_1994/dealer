<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/* * *******************************************#
  #      User Management System                 #
  #*********************************************#
  #      Author:     Atique IT                  #
  #      Email:      info@atique-it.com         #
  #      Website:    http://atique-it.com       #
  #                                             #
  #      Version:    15.2.1                     #
  #      Copyright:  (c) 2015 - Atique IT       #
  #                                             #
  #*********************************************# */

//            echo '<pre>';
//            print_r($data);
//            exit();
class User_quote extends MSN_Controller {

    public function __construct() {
        parent::__construct();

        $this->admin_login_authentication();
        $this->load->model('admin_models/Others_Model', 'others_mdl');
        $this->load->model('admin_models/Profile_Model', 'pro_mdl');
        $this->load->model('admin_models/User_Quote_Model', 'quote_mdl');
    }

    public function index() {
        $data['title'] = 'User Quote';
        $data['menu_active'] = 'My Quote';
        $data['email_notification'] = $this->others_mdl->get_all_unred_notification();
        $data['user_info'] = $this->pro_mdl->get_user_info();
        $data['quote_info'] = $this->quote_mdl->get_all_published_quote_info();
        $this->load->view('admin_panel/common/header', $data);
        $this->load->view('admin_panel/common/menu_bar');
        $this->load->view('admin_panel/my_quote/my_quote', $data);
        $this->load->view('admin_panel/common/footer');
    }

    public function user_sub_quote($quote_id) {
        $data['title'] = 'User Sub Quote';
        $data['menu_active'] = 'My Quote';
        $data['email_notification'] = $this->others_mdl->get_all_unred_notification();
        $data['user_info'] = $this->pro_mdl->get_user_info();
        $user_id = $this->session->userdata('user_id');
        $data['quote_info'] = $this->quote_mdl->get_all_quote_info_by_quote_id_and_user_id($quote_id, $user_id);
        $data['last_quote_info'] = $this->quote_mdl->get_last_sub_quote_info_by_quote_id($quote_id);
        $data['primary_quote_id'] = $quote_id;
        $this->load->view('admin_panel/common/header', $data);
        $this->load->view('admin_panel/common/menu_bar');
        $this->load->view('admin_panel/my_quote/sub_quote', $data);
        $this->load->view('admin_panel/common/footer');
    }

    public function add_sub_quote($quote_id) {
        $data['title'] = 'Job Sub Quote';
        $data['menu_active'] = 'Job Quote';
        $data['primary_quote_id'] = $quote_id;
        $data['email_notification'] = $this->others_mdl->get_all_unred_notification();
        $data['user_info'] = $this->pro_mdl->get_user_info();
        $data['job_category_info'] = $this->quote_mdl->get_job_category_info();
        $this->load->view('admin_panel/common/header', $data);
        $this->load->view('admin_panel/common/menu_bar');
        $this->load->view('admin_panel/my_quote/add_user_sub_quote', $data);
        $this->load->view('admin_panel/common/footer');
    }

    public function save_sub_quote() {
        $primary_quote_id = $this->input->post('quote_id', true);
        $config = array(
            array(
                'field' => 'data[job_title]',
                'label' => 'job title',
                'rules' => 'trim|required|min_length[2]|max_length[150]'
            ),
            array(
                'field' => 'data[job_category_id]',
                'label' => 'job category id',
                'rules' => 'trim|required'
            ),
            array(
                'field' => 'data[return_file_type]',
                'label' => 'return file type',
                'rules' => 'required'
            ),
            array(
                'field' => 'data[need_path]',
                'label' => 'path',
                'rules' => 'trim|required'
            ),
            array(
                'field' => 'data[number_of_image]',
                'label' => 'job category description',
                'rules' => 'trim|required'
            ),
            array(
                'field' => 'data[instruction_message]',
                'label' => 'publication status',
                'rules' => 'required|min_length[2]|max_length[250]'
            ),
            array(
                'field' => 'data[job_type]',
                'label' => 'job type',
                'rules' => 'trim|required'
            ),
            array(
                'field' => 'data[one_time_price_usd]',
                'label' => 'price',
                'rules' => 'trim'
            ),
            array(
                'field' => 'data[one_time_dead_line_hr]',
                'label' => 'dead line hr',
                'rules' => 'trim'
            ),
            array(
                'field' => 'data[dead_line_days]',
                'label' => 'dead line day',
                'rules' => 'trim'
            ),
            array(
                'field' => 'data[monthly_work_volume]',
                'label' => 'monthly work volume',
                'rules' => 'trim'
            ),
            array(
                'field' => 'data[monthly_amount_usd]',
                'label' => 'monthly amount',
                'rules' => 'trim'
            )
        );
        $this->form_validation->set_rules($config);
        if ($this->form_validation->run() == FALSE) {
            $this->add_sub_quote($primary_quote_id);
        } else {
            $data = $this->input->post('data', TRUE);
            $data['quote_id'] = $primary_quote_id;
            $data['user_id'] = $this->session->userdata('user_id');
            $result = $this->quote_mdl->insert_sub_quote_info($data);
            if (!empty($result)) {
                $sdata['message'] = '<h3>Sub quote add successfully .</h3>';
                $this->session->set_userdata($sdata);
                redirect('admin/user_quote/user_sub_quote/' . $primary_quote_id, 'refresh');
            } else {
                $sdata['error'] = '<h3>Sub quote insertion failed </h3>';
                $this->session->set_userdata($sdata);
                redirect('admin/user_quote/user_sub_quote/' . $primary_quote_id, 'refresh');
            }
        }
    }

    public function user_confirm($sub_quote_id) {
        $sub_quote_info = $this->quote_mdl->get_sub_quote_info_by_sub_quote_id($sub_quote_id);
        $result = $this->quote_mdl->user_confirm_sub_quote_by_id($sub_quote_id);

        if (!empty($result)) {
            $sdata['message'] = '<h3>Quote confirm successfully .</h3>';
            $this->session->set_userdata($sdata);
            redirect('admin/user_quote/user_sub_quote/' . $sub_quote_info['quote_id'], 'refresh');
        } else {
            $sdata['error'] = '<h3>Quote confiramation failed !</h3>';
            $this->session->set_userdata($sdata);
            redirect('admin/user_quote/user_sub_quote/' . $sub_quote_info['quote_id'], 'refresh');
        }
    }
    
    public function view_details($sub_quote_id){
        $data['title'] = 'Job Sub Quote';
        $data['menu_active'] = 'Job Quote';
        $data['email_notification'] = $this->others_mdl->get_all_unred_notification();
        $data['user_info'] = $this->pro_mdl->get_user_info();
        $data['sub_quote_info'] = $this->quote_mdl->get_sub_quote_info_by_id($sub_quote_id);
        $this->load->view('admin_panel/common/header', $data);
        $this->load->view('admin_panel/common/menu_bar');
        $this->load->view('admin_panel/my_quote/view_quote_details', $data);
        $this->load->view('admin_panel/common/footer');
    }

}
