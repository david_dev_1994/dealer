<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Admin_quote extends MSN_Controller {

    public function __construct() {
        parent::__construct();
        
        $this->admin_login_authentication();
        $this->super_admin_and_admin_authentication_only();
        $this->load->model('admin_models/Others_Model', 'others_mdl');
        $this->load->model('admin_models/Profile_Model', 'pro_mdl');
        $this->load->model('admin_models/Admin_Quote_Model', 'quote_mdl');
    }

    public function index() {
        $data['title'] = 'Admin Quote';
        $data['menu_active'] = 'My Quote';
        $data['email_notification'] = $this->others_mdl->get_all_unred_notification();
        $data['user_info'] = $this->pro_mdl->get_user_info();
        $data['quote_info'] = $this->quote_mdl->get_all_quote_info();
        $this->load->view('admin_panel/common/header', $data);
        $this->load->view('admin_panel/common/menu_bar');
        $this->load->view('admin_panel/my_quote/my_quote', $data);
        $this->load->view('admin_panel/common/footer');
    }
    
    public function add_quote() {
        $data['title'] = 'Job Quote';
        $data['menu_active'] = 'Job Quote';
        $data['email_notification'] = $this->others_mdl->get_all_unred_notification();
        $data['user_info'] = $this->pro_mdl->get_user_info();
        $data['job_category_info'] = $this->quote_mdl->get_job_category_info();
        $this->load->view('admin_panel/common/header', $data);
        $this->load->view('admin_panel/common/menu_bar');
        $this->load->view('admin_panel/my_quote/add_quote', $data);
        $this->load->view('admin_panel/common/footer');
    }

    public function save_quote() {
        $config = array(
            array(
                'field' => 'data[job_title]',
                'label' => 'job title',
                'rules' => 'trim|required|min_length[2]|max_length[150]'
            ),
            array(
                'field' => 'data[job_category_id]',
                'label' => 'job category id',
                'rules' => 'trim|required'
            ),
            array(
                'field' => 'data[return_file_type]',
                'label' => 'return file type',
                'rules' => 'required'
            ),
            array(
                'field' => 'data[need_path]',
                'label' => 'path',
                'rules' => 'trim|required'
            ),
            array(
                'field' => 'data[number_of_image]',
                'label' => 'job category description',
                'rules' => 'trim|required'
            ),
            array(
                'field' => 'data[instruction_message]',
                'label' => 'instruction message',
                'rules' => 'required|min_length[2]|max_length[250]'
            ),
            array(
                'field' => 'data[publication_status]',
                'label' => 'publication status',
                'rules' => 'required'
            )
        );
        $this->form_validation->set_rules($config);
        if ($this->form_validation->run() == FALSE) {
            $this->add_quote();
        } else {
            $data = $this->input->post('data', TRUE);
            $data['user_id'] = $this->session->userdata('user_id');

            $result = $this->quote_mdl->insert_quote_info($data);
            if (!empty($result)) {
                $sdata['message'] = '<h3>Quote add successfully .</h3>';
                $this->session->set_userdata($sdata);
                redirect('admin/admin_quote', 'refresh');
            } else {
                $sdata['error'] = '<h3>Quote insertion failed </h3>';
                $this->session->set_userdata($sdata);
                redirect('admin/admin_quote', 'refresh');
            }
        }
    }

    public function sub_quote($quote_id) {
        $data['title'] = 'My Sub Quote';
        $data['menu_active'] = 'My Quote';
        $data['email_notification'] = $this->others_mdl->get_all_unred_notification();
        $data['user_info'] = $this->pro_mdl->get_user_info();
        $data['quote_info'] = $this->quote_mdl->get_all_quote_info_by_quote_id($quote_id);
        $data['last_quote_info'] = $this->quote_mdl->get_last_sub_quote_info_by_quote_id($quote_id);
        $data['primary_quote_id'] = $quote_id;
        $this->load->view('admin_panel/common/header', $data);
        $this->load->view('admin_panel/common/menu_bar');
        $this->load->view('admin_panel/my_quote/sub_quote', $data);
        $this->load->view('admin_panel/common/footer');
    }

    public function admin_replay($sub_quote_id){
        $data['title'] = 'Job Sub Quote';
        $data['menu_active'] = 'Job Quote';
        $data['email_notification'] = $this->others_mdl->get_all_unred_notification();
        $data['user_info'] = $this->pro_mdl->get_user_info();
        $data['sub_quote_info'] = $this->quote_mdl->get_sub_quote_info_by_sub_quote_id($sub_quote_id);
        $data['job_category_info'] = $this->quote_mdl->get_job_category_info();
        $this->load->view('admin_panel/common/header', $data);
        $this->load->view('admin_panel/common/menu_bar');
        $this->load->view('admin_panel/my_quote/add_sub_quote', $data);
        $this->load->view('admin_panel/common/footer');
    }
    
    public function save_sub_quote() {
        $primary_quote_id = $this->input->post('quote_id', true);
        $config = array(
            array(
                'field' => 'data[job_title]',
                'label' => 'job title',
                'rules' => 'trim|required|min_length[2]|max_length[150]'
            ),
            array(
                'field' => 'data[job_category_id]',
                'label' => 'job category id',
                'rules' => 'trim|required'
            ),
            array(
                'field' => 'data[return_file_type]',
                'label' => 'return file type',
                'rules' => 'required'
            ),
            array(
                'field' => 'data[need_path]',
                'label' => 'path',
                'rules' => 'trim|required'
            ),
            array(
                'field' => 'data[number_of_image]',
                'label' => 'job category description',
                'rules' => 'trim|required'
            ),
            array(
                'field' => 'data[instruction_message]',
                'label' => 'publication status',
                'rules' => 'required|min_length[2]|max_length[250]'
            ),
            array(
                'field' => 'data[job_type]',
                'label' => 'job type',
                'rules' => 'trim|required'
            ),
            array(
                'field' => 'data[one_time_price_usd]',
                'label' => 'price',
                'rules' => 'trim'
            ),
            array(
                'field' => 'data[one_time_dead_line_hr]',
                'label' => 'dead line hr',
                'rules' => 'trim'
            ),
            array(
                'field' => 'data[dead_line_days]',
                'label' => 'dead line day',
                'rules' => 'trim'
            ),
            array(
                'field' => 'data[monthly_work_volume]',
                'label' => 'monthly work volume',
                'rules' => 'trim'
            ),
            array(
                'field' => 'data[monthly_amount_usd]',
                'label' => 'monthly amount',
                'rules' => 'trim'
            )
        );
        $this->form_validation->set_rules($config);
        if ($this->form_validation->run() == FALSE) {
            $this->add_sub_quote($primary_quote_id);
        } else {
            $data = $this->input->post('data', TRUE);
            $data['quote_id'] = $primary_quote_id;
            $data['admin_id'] = $this->session->userdata('user_id');
            $result = $this->quote_mdl->insert_sub_quote_info($data);
            if (!empty($result)) {
                $sdata['message'] = '<h3>Sub quote add successfully .</h3>';
                $this->session->set_userdata($sdata);
                redirect('admin/admin_quote/sub_quote/' . $primary_quote_id, 'refresh');
            } else {
                $sdata['error'] = '<h3>Sub quote insertion failed </h3>';
                $this->session->set_userdata($sdata);
                redirect('admin/admin_quote/sub_quote/' . $primary_quote_id, 'refresh');
            }
        }
    }
    
    public function admin_confirm($sub_quote_id) {
        $sub_quote_info = $this->quote_mdl->get_sub_quote_info_by_sub_quote_id($sub_quote_id);
        $result = $this->quote_mdl->admin_confirm_sub_quote_by_id($sub_quote_id);

        if (!empty($result)) {
            $sdata['message'] = '<h3>Quote confirm successfully .</h3>';
            $this->session->set_userdata($sdata);
            redirect('admin/admin_quote/sub_quote/' . $sub_quote_info['quote_id'], 'refresh');
        } else {
            $sdata['error'] = '<h3>Quote confiramation failed !</h3>';
            $this->session->set_userdata($sdata);
            redirect('admin/admin_quote/sub_quote/' . $sub_quote_info['quote_id'], 'refresh');
        }
    }

    public function unpublished_quote($quote_id) {
        $result = $this->quote_mdl->unpublished_quote_by_id($quote_id);

        if (!empty($result)) {
            $sdata['message'] = '<h3>Quote unpublished successfully .</h3>';
            $this->session->set_userdata($sdata);
            redirect('admin/admin_quote', 'refresh');
        } else {
            $sdata['error'] = '<h3>Quote unpublication failed !</h3>';
            $this->session->set_userdata($sdata);
            redirect('admin/admin_quote', 'refresh');
        }
    }

    public function published_quote($quote_id) {
        $result = $this->quote_mdl->published_quote_by_id($quote_id);

        if (!empty($result)) {
            $sdata['message'] = '<h3>Quote published successfully .</h3>';
            $this->session->set_userdata($sdata);
            redirect('admin/admin_quote', 'refresh');
        } else {
            $sdata['error'] = '<h3>Quote publication failed !</h3>';
            $this->session->set_userdata($sdata);
            redirect('admin/admin_quote', 'refresh');
        }
    }

    public function edit_quote($quote_id) {
        $data['quote_info'] = $this->quote_mdl->get_quote_by_id($quote_id);

        if (!empty($data['quote_info'])) {
            $data['title'] = 'Job Quote';
            $data['menu_active'] = 'Job Quote';
            $data['email_notification'] = $this->others_mdl->get_all_unred_notification();
            $data['job_category_info'] = $this->quote_mdl->get_job_category_info();
            $this->load->view('admin_panel/common/header', $data);
            $this->load->view('admin_panel/common/menu_bar');
            $this->load->view('admin_panel/my_quote/edit_quote', $data);
            $this->load->view('admin_panel/common/footer');
        } else {
            $sdata['error'] = '<h3>Quote not found for this id !</h3>';
            $this->session->set_userdata($sdata);
            redirect('admin/admin_quote', 'refresh');
        }
    }

    public function update_quote() {
        $quote_id = $this->input->post('quote_id', TRUE);
        $config = array(
            array(
                'field' => 'data[job_title]',
                'label' => 'job title',
                'rules' => 'trim|required|min_length[2]|max_length[150]'
            ),
            array(
                'field' => 'data[job_category_id]',
                'label' => 'job category id',
                'rules' => 'trim|required'
            ),
            array(
                'field' => 'data[return_file_type]',
                'label' => 'return file type',
                'rules' => 'required'
            ),
            array(
                'field' => 'data[need_path]',
                'label' => 'path',
                'rules' => 'trim|required'
            ),
            array(
                'field' => 'data[number_of_image]',
                'label' => 'job category description',
                'rules' => 'trim|required'
            ),
            array(
                'field' => 'data[instruction_message]',
                'label' => 'publication status',
                'rules' => 'required|min_length[2]|max_length[250]'
            ),
            array(
                'field' => 'data[publication_status]',
                'label' => 'publication status',
                'rules' => 'required'
            )
        );
        $this->form_validation->set_rules($config);
        if ($this->form_validation->run() == FALSE) {
            $this->edit_quote($quote_id);
        } else {
            $data = $this->input->post('data', TRUE);
            $result = $this->quote_mdl->update_quote_by_id($quote_id, $data);
            if (!empty($result)) {
                $sdata['message'] = '<h3>Job Quote update successfully .</h3>';
                $this->session->set_userdata($sdata);
                redirect('admin/admin_quote/edit_quote/' . $quote_id, 'refresh');
            } else {
                $sdata['error'] = '<h3>Job Quote updation failed !</h3>';
                $this->session->set_userdata($sdata);
                redirect('admin/admin_quote/edit_quote/' . $quote_id, 'refresh');
            }
        }
    }

    public function delete_quote($quote_id) {
        if($this->session->userdata('access_label') == 2){
            $sdata['error'] = 'You don\'t have access to perform this action!';
            $this->session->set_userdata($sdata);
            redirect('admin/quote_listing', 'refresh');
        }
        $result = $this->quote_mdl->delete_quote_by_id($quote_id);

        if (!empty($result)) {
            $sdata['message'] = '<h3>Quote remove successfully .</h3>';
            $this->session->set_userdata($sdata);
            redirect('admin/quote_listing', 'refresh');
        } else {
            $sdata['error'] = '<h3>Failed to Remove quote. (Try Again)!</h3>';
            $this->session->set_userdata($sdata);
            redirect('admin/quote_listing', 'refresh');
        }
    }

    public function quote_listing() {
        
        // echo 'pre>';
        // print_r($this->session->userdata('user_id'));
        // die();

        if($this->session->userdata('access_label') == 2 && $this->session->userdata('user_id') != 96 && $this->session->userdata('user_id')!= 179 && $this->session->userdata('user_id')!= 97){
            $data['quotes_list'] = $this->quote_mdl->get_quote_list_by_admin($this->session->userdata('email_address'));
        }else {
            $data['quotes_list'] = $this->quote_mdl->get_quote_list();
        }

        $data['title'] = 'Quote Listing';
        $data['menu_active'] = 'Quote Listing';
        $this->load->view('admin_panel/common/header', $data);
        $this->load->view('admin_panel/common/menu_bar');
        $this->load->view('admin_panel/my_quote/quote_list', $data);
        $this->load->view('admin_panel/common/footer');
    }

    public function viewonly($id) {
        $data['title'] = 'Quote Listing';
        $data['menu_active'] = 'Quote Listing';
        $this->load->view('admin_panel/common/header', $data);
        $this->load->view('admin_panel/common/menu_bar');

        $data['viewonly'] = 1;
        $user_id = $this->session->userdata('d_user_id');
        $data['quote_info'] = $this->quote_mdl->get_all_quote_info_by_quote_id_and_user_id($id);

        $this->load->view('admin_panel/my_quote/view_quote', $data);
        $this->load->view('admin_panel/common/footer');    }



}
