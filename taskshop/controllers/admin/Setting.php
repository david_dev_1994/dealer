<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/* * *******************************************#
  #      User Management System                 #
  #*********************************************#
  #      Author:     Atique IT                  #
  #      Email:      info@atique-it.com         #
  #      Website:    http://atique-it.com       #
  #                                             #
  #      Version:    15.2.1                     #
  #      Copyright:  (c) 2015 - Atique IT       #
  #                                             #
  #*********************************************# */

class Setting extends MSN_Controller {

    public function __construct() {
        parent::__construct();

        $this->admin_login_authentication();
        $this->super_admin_and_admin_authentication_only();
        $this->load->model('admin_models/Setting_Model', 'set_mdl');
        $this->load->model('admin_models/Others_Model', 'others_mdl');
        $this->load->model('admin_models/Profile_Model', 'pro_mdl');
    }

    public function index() {
        $data['title'] = 'General Setting';
        $data['menu_active'] = 'General Setting';
        $data['email_notification'] = $this->others_mdl->get_all_unred_notification();
        $data['user_info'] = $this->pro_mdl->get_user_info();
        $data['site_info'] = $this->set_mdl->get_site_info();
        $this->load->view('admin_panel/common/header', $data);
        $this->load->view('admin_panel/common/menu_bar', $data);
        $this->load->view('admin_panel/setting/general_setting', $data);
        $this->load->view('admin_panel/common/footer');
    }

    public function update_general_setting() {
        $data = $this->input->post('data', TRUE);
        $result = $this->set_mdl->update_general_info($data);

        if (!empty($result)) {
            $sdata['message'] = 'Setting update successfully .';
            $this->session->set_userdata($sdata);
            redirect('admin/setting', 'refresh');
        } else {
            $sdata['error'] = 'Setting updation failed !';
            $this->session->set_userdata($sdata);
            redirect('admin/setting', 'refresh');
        }
    }

    public function update_logo() {
        $prev_img_name = $this->input->post('prev_logo_name', TRUE);

        if (isset($_FILES['logo']['name']) && !empty($_FILES['logo']['name'])) {
            $config['upload_path'] = 'uploaded_files/logo/';
            $config['allowed_types'] = 'jpg|png|jpeg';
            $config['max_size'] = ''; //kb
            $config['max_width'] = '';
            $config['max_height'] = '';
            $config['overwrite'] = false;

            $fdata = array();
            $this->load->library('upload', $config);

            if (!$this->upload->do_upload('logo')) {
                $sdata['error'] = $this->upload->display_errors();
                $this->session->set_userdata($sdata);
                redirect('admin/setting');
            } else {
                $fdata = $this->upload->data();
                $picture_name = $fdata['file_name'];
                $data = array('upload_data' => $this->upload->data());
                $this->logo_resize($data['upload_data']['full_path'], $data['upload_data']['file_name']);
                $this->delete_logo($prev_img_name);

                $result = $this->set_mdl->update_logo_info($picture_name);
                if ($result) {
                    $sdata['message'] = 'Logo update successfully.';
                    $this->session->set_userdata($sdata);
                    redirect('admin/setting');
                } else {
                    $sdata['error'] = 'Logo updation failed !';
                    $this->session->set_userdata($sdata);
                    redirect('admin/setting');
                }
            }
        } else {
            $sdata['error'] = 'No file chosen !';
            $this->session->set_userdata($sdata);
            redirect('admin/setting');
        }
    }

    public function logo_resize($path, $file) {
        $config_resize = array();
        $config_resize['image_library'] = 'gd2';
        $config_resize['source_image'] = $path;
        $config_resize['create_thumb'] = FALSE;
        $config_resize['maintain_ratio'] = TRUE;
        $config_resize['overwrite'] = false;
        $config_resize['width'] = 240;
        $config_resize['height'] = 180;
        $config_resize['new_image'] = 'uploaded_files/logo/thumb/' . $file;

        $this->load->library('image_lib', $config_resize);
        $this->image_lib->resize();
    }

    public function delete_logo($picture_name) {
        if (!empty($picture_name)) {
            unlink('uploaded_files/logo/' . $picture_name);
            unlink('uploaded_files/logo/thumb/' . $picture_name);
        } else {
            return TRUE;
        }
    }

    public function update_favicon() {
        $prev_img_name = $this->input->post('prev_favicon_name', TRUE);

        if (isset($_FILES['favicon']['name']) && !empty($_FILES['favicon']['name'])) {
            $config['upload_path'] = 'uploaded_files/favicon/';
            $config['allowed_types'] = 'jpg|png|jpeg';
            $config['max_size'] = ''; //kb
            $config['max_width'] = '';
            $config['max_height'] = '';
            $config['overwrite'] = false;

            $fdata = array();
            $this->load->library('upload', $config);

            if (!$this->upload->do_upload('favicon')) {
                $sdata['error'] = $this->upload->display_errors();
                $this->session->set_userdata($sdata);
                redirect('admin/setting');
            } else {
                $fdata = $this->upload->data();
                $picture_name = $fdata['file_name'];
                $data = array('upload_data' => $this->upload->data());
                $this->favicon_resize($data['upload_data']['full_path'], $data['upload_data']['file_name']);
                $this->delete_favicon($prev_img_name);

                $result = $this->set_mdl->update_favicon_info($picture_name);
                if ($result) {
                    $sdata['message'] = 'Favicon update successfully.';
                    $this->session->set_userdata($sdata);
                    redirect('admin/setting');
                } else {
                    $sdata['error'] = 'Favicon updation failed !';
                    $this->session->set_userdata($sdata);
                    redirect('admin/setting');
                }
            }
        } else {
            $sdata['error'] = 'No file chosen !';
            $this->session->set_userdata($sdata);
            redirect('admin/setting');
        }
    }

    public function favicon_resize($path, $file) {
        $config_resize = array();
        $config_resize['image_library'] = 'gd2';
        $config_resize['source_image'] = $path;
        $config_resize['create_thumb'] = FALSE;
        $config_resize['maintain_ratio'] = TRUE;
        $config_resize['overwrite'] = false;
        $config_resize['width'] = 32;
        $config_resize['height'] = 32;
        $config_resize['new_image'] = 'uploaded_files/favicon/thumb/' . $file;

        $this->load->library('image_lib', $config_resize);
        $this->image_lib->resize();
    }

    public function delete_favicon($picture_name) {
        if (!empty($picture_name)) {
            unlink('uploaded_files/favicon/' . $picture_name);
            unlink('uploaded_files/favicon/thumb/' . $picture_name);
        } else {
            return TRUE;
        }
    }

    public function mail_setting() {
        $data['title'] = 'Mail Setting';
        $data['menu_active'] = 'Mail Setting';
        $data['email_notification'] = $this->others_mdl->get_all_unred_notification();
        $data['user_info'] = $this->pro_mdl->get_user_info();
        $data['mail_set_info'] = $this->set_mdl->get_mail_set_info();
        $this->load->view('admin_panel/common/header', $data);
        $this->load->view('admin_panel/common/menu_bar', $data);
        $this->load->view('admin_panel/setting/mail_setting', $data);
        $this->load->view('admin_panel/common/footer');
    }

    public function update_mail_setting() {
        $data = $this->input->post('data', TRUE);
        $result = $this->set_mdl->update_mail_set_info($data);

        if (!empty($result)) {
            $sdata['message'] = 'Mail setting update successfully .';
            $this->session->set_userdata($sdata);
            redirect('admin/setting/mail_setting', 'refresh');
        } else {
            $sdata['error'] = 'Mail setting updation failed !';
            $this->session->set_userdata($sdata);
            redirect('admin/setting/mail_setting', 'refresh');
        }
    }

    public function privacy() {
        $data['title'] = 'Privacy Setting';
        $data['menu_active'] = 'Privacy';
        $data['email_notification'] = $this->others_mdl->get_all_unred_notification();
        $data['user_info'] = $this->pro_mdl->get_user_info();
        $data['privacy_info'] = $this->set_mdl->get_privacy_info();
        $this->load->view('admin_panel/common/header', $data);
        $this->load->view('admin_panel/common/menu_bar',$data);
        $this->load->view('admin_panel/setting/privacy', $data);
        $this->load->view('admin_panel/common/footer');
    }

    public function update_privacy() {
        $privacy = $this->input->post('privacy', TRUE);
        $result = $this->set_mdl->update_privacy($privacy);

        if (!empty($result)) {
            $sdata['message'] = 'Privacy update successfully .';
            $this->session->set_userdata($sdata);
            redirect('admin/setting/privacy', 'refresh');
        } else {
            $sdata['error'] = 'Privacy updation failed !';
            $this->session->set_userdata($sdata);
            redirect('admin/setting/privacy', 'refresh');
        }
    }
    
    public function terms() {
        $data['title'] = 'Tearms Setting';
        $data['menu_active'] = 'Tearms';
        $data['email_notification'] = $this->others_mdl->get_all_unred_notification();
        $data['user_info'] = $this->pro_mdl->get_user_info();
        $data['terms_info'] = $this->set_mdl->get_privacy_info();
        $this->load->view('admin_panel/common/header', $data);
        $this->load->view('admin_panel/common/menu_bar', $data);
        $this->load->view('admin_panel/setting/terms', $data);
        $this->load->view('admin_panel/common/footer');
    }

    public function update_terms() {
        $terms = $this->input->post('terms', TRUE);
        $result = $this->set_mdl->update_terms($terms);

        if (!empty($result)) {
            $sdata['message'] = 'Terms update successfully .';
            $this->session->set_userdata($sdata);
            redirect('admin/setting/terms', 'refresh');
        } else {
            $sdata['error'] = 'Terms updation failed !';
            $this->session->set_userdata($sdata);
            redirect('admin/setting/terms', 'refresh');
        }
    }

    public function update() {
        $data['title'] = 'Update';
        $data['menu_active'] = 'Update';
        $data['email_notification'] = $this->others_mdl->get_all_unred_notification();
        $data['user_info'] = $this->pro_mdl->get_user_info();
        $this->load->view('admin_panel/common/header', $data);
        $this->load->view('admin_panel/common/menu_bar', $data);
        $this->load->view('admin_panel/setting/update');
        $this->load->view('admin_panel/common/footer');
    }

}
