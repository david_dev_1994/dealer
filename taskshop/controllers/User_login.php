<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/* * *******************************************#
  #      User Management System                 #
  #*********************************************#
  #      Author:     Atique IT                  #
  #      Email:      info@atique-it.com         #
  #      Website:    http://atique-it.com       #
  #                                             #
  #      Version:    15.2.1                     #
  #      Copyright:  (c) 2015 - Atique IT       #
  #                                             #
  #*********************************************# */
class User_login extends MSN_Controller {

    public function __construct() {
        parent::__construct();
//        $user_id = $this->session->userdata('user_id');
//        if ($user_id != NULL) {
//            redirect('page', 'refresh');
//        }
        $this->load->model('admin_models/Authentication_Model', 'a_model');
        $this->load->model('user_models/Page_Model','p_model');
        $this->load->model('admin_models/Registered_User_Model', 'user_mdl');
        $this->load->helper('functions_helper');

    }

    public function index() {

        if ($this->session->userdata('d_is_logged')){
            $this->session->set_flashdata('error',"You are already logged in");
            redirect('page', 'refresh');
        }

        $data['title'] = 'Sign In';
        $data['menu_active'] = 'Sign In';
        $this->load->view('user_panel/common/header');
        $this->load->view('user_panel/sign_in');
        $this->load->view('user_panel/common/footer');
    }
    
    //change 1/11/2019
    
    public function check_user_login() {

            $this->form_validation->set_rules('email_address', 'email address', 'trim|required|max_length[100]');
            $this->form_validation->set_rules('user_password', 'password', 'trim|required|max_length[50]');

            if ($this->form_validation->run() == FALSE) {
                $this->session->set_flashdata('error',validation_errors());
                $this->index();
            } else {
                $result = $this->a_model->check_login();
                if ($result && (int)$result->access_label==3 || $result && (int)$result->access_label == 5 || $result && (int)$result->access_label == 4) {
                    $sdata['d_user_id'] = $result->user_id;
                    $sdata['d_full_name'] = $result->full_name;
                    $sdata['d_email_address'] = $result->email_address;
                    $sdata['d_access_label'] = $result->access_label;
                    $sdata['d_is_logged'] = TRUE;

                    $this->session->set_userdata($sdata);
                    if((int)$result->access_label == 4){
                        redirect('page/gpsmanager');
                    }else{
                        redirect('page');
                    }

                } else {

                    if ($result && $result->access_label!=3){
                        $this->session->set_flashdata('error',"You are using wrong portal for Login");
                    }else{
                        $this->session->set_flashdata('error',"The email or password you’ve entered are incorrect");
                    }
                    redirect('user_login');
                }
            }

    }

    // public function check_user_login() {

    //         $this->form_validation->set_rules('email_address', 'email address', 'trim|required|max_length[100]');
    //         $this->form_validation->set_rules('user_password', 'password', 'trim|required|max_length[50]');

    //         if ($this->form_validation->run() == FALSE) {
    //             $this->session->set_flashdata('error',validation_errors());
    //             $this->index();
    //         } else {
    //             $result = $this->a_model->check_login();
    //             // echo 'hello';
    //             // print_r($result);die();
    //             if ($result && (int)$result->access_label==3) {
    //                 $sdata['d_user_id'] = $result->user_id;
    //                 $sdata['d_full_name'] = $result->full_name;
    //                 $sdata['d_email_address'] = $result->email_address;
    //                 $sdata['d_access_label'] = $result->access_label;
    //                 $sdata['d_is_logged'] = TRUE;

    //                 $this->session->set_userdata($sdata);
    //                 redirect('page');
    //             } else {

    //                 if ($result && $result->access_label!=3){
    //                     $this->session->set_flashdata('error',"You are using wrong portal for Login");
    //                 }else{
    //                     $this->session->set_flashdata('error',"The email or password you’ve entered are incorrect");
    //                 }
    //                 redirect('user_login');
    //             }
    //         }

    // }
    
    public function forgot_password(){
        
        if($this->input->post('reset_password')){
            
            //$sdata['message']="An email has been sent at your email address. Please visit the link to change your Password";
            //$this->session->set_userdata($sdata);
            $data=array('email_address'=>$this->input->post('email_address'));
            //check for the associated email address
            
            $result=$this->p_model->check_info($data);
            if($result<=0) {
                
                $sdata['message']="Your Email Address is Not associated with any account.";
                $this->session->set_userdata($sdata);
            } else {
                
                $this->load->helper('string');
                $random_string = random_string('alnum',8);
                // $this->load->library('email');
                // $this->email->from('bktechnologies.com','Bk Technologies');
                // $this->email->to($this->input->post('email_address'));
                // $this->email->subject('Password Restet Link');
                // $this->email->message('This is your password reset link '. base_url().'user_login/reset_password/'.$random_string.'');
                // $sent = sendemail($this->input->post('email_address'),'admin@bktechnologies.com','This is your password reset link '. base_url().'user_login/reset_password/'.$random_string.'','Password Restet Link');
                $sent = sendemail($this->input->post('email_address'),'dealerportal@bktechnologies.com','Hi,<br/><br/> We received a request to reset your dealer portal password.<br/><br/> <a href="'.base_url('user_login/reset_password/'.$random_string).'" target="_blank">Click here to change your password</a>. <br/><br/> Please do not reply to this email. It is used for outbound messages only.<br/> Please contact sales@bktechnologies.com for assistance.','Password Reset Link');

                if($sent){
                	$data['random_string']=$random_string;
                	//print_r($data);
                	$result=$this->p_model->insert($data,'tbl_reset_pass');
                	$sdata['message']="Password Reset Link Has been Sent to your Email. Thank you";
                	$this->session->set_userdata($sdata);
                	

                }
                                
            }
            
        }  
        $data['title'] = 'Password Reset';
        $data['menu_active'] = 'Password Reset';
        $this->load->view('user_panel/common/header');
        $this->load->view('user_panel/reset_password');
        $this->load->view('user_panel/common/footer');
        
    }

//     public function user_profile(){

//         if (!$this->session->userdata('d_is_logged')){
//             $this->session->set_flashdata('error',"Access Denied. Please Log-in first");
//             redirect('page', 'refresh');
//         }
//         if ($this->input->post()){

// // echo '<pre>';print_r($this->input->post());exit;
//             $config = array(
//                 array('field' => 'bdata[first_name]', 'label' => 'first name', 'rules' => 'trim|required|max_length[50]'),
// //                array('field' => 'ldata[user_password]', 'label' => 'password', 'rules' => 'trim|required|min_length[8]|max_length[50]'),
// //                array('field' => 're_password', 'label' => 're-password', 'rules' => 'trim|required|matches[ldata[user_password]]'),
// //                array('field' => 'ldata[email_address]', 'label' => 'email address', 'rules' => 'trim|required|valid_email|is_unique[tbl_user_login_info.email_address]'),
//                 array('field' => 'bdata[contactinfo]', 'label' => 'Dealer phone', 'rules' => 'trim|required'),
//                 array('field' => 'bdata[companyname]', 'label' => 'company name', 'rules' => 'trim|required|min_length[5]'),
//                 array('field' => 'bdata[sales_manager_id]', 'label' => 'Sales Manager', 'rules' => 'trim|required'),
//                 array('field' => 'o_phone[]', 'label' => 'Office phone', 'rules' => 'trim|required|max_length[25]'),
//                 array('field' => 'o_address[]', 'label' => 'Office address', 'rules' => 'trim|required|min_length[8]|max_length[150]'),
//                 array('field' => 'o_state[]', 'label' => 'Office state', 'rules' => 'trim|required|max_length[150]'),
//                 array('field' => 'o_city[]', 'label' => 'Office city', 'rules' => 'trim|required|max_length[150]'),
//                 array('field' => 'o_zip[]', 'label' => 'Office zip', 'rules' => 'trim|required|max_length[20]'),
//             );
//             if($this->session->userdata('d_email_address') == $this->input->post('ldata[email_address]')){
//                 $config[] = array('field' => 'ldata[email_address]', 'label' => 'email address', 'rules' => 'trim|required|valid_email');
//             }else{
//                 $config[] = array('field' => 'ldata[email_address]', 'label' => 'email address', 'rules' => 'trim|required|valid_email|is_unique[tbl_user_login_info.email_address]');
//             }

//             $pass = '0';
//             if($this->input->post('user_password') && !empty($this->input->post('user_password')) || ($this->input->post('re-password') && !empty($this->input->post('re-password')))){
//                 $config[] = array('field' => 'user_password', 'label' => 'password', 'rules' => 'trim|required|min_length[8]|max_length[50]');
//                 $config[] = array('field' => 're_password', 'label' => 're-password', 'rules' => 'trim|required|matches[user_password]');
//                 $pass = '1';
//             }


//             $this->form_validation->set_rules($config);
//             if ($this->form_validation->run() == FALSE) {
//                 $this->session->set_flashdata('error',validation_errors());
//                 redirect('dealer_profile');
//             } else {
//                 $this->load->helper('string');

//                 $ldata = $this->input->post('ldata');
//                 $ldata['full_name'] = $this->input->post('bdata[first_name]', true);

//                 if($pass == '1') {
//                     $ldata['user_password'] = md5($this->input->post('user_password'));
//                 }

//                 $ldata['email_address'] = $this->input->post('ldata[email_address]');
//                 $ldata['sales_manager_id'] = $this->input->post('bdata[sales_manager_id]');
// //                $ldata['access_label'] = 3;
// //                $ldata['activation_key'] = md5(random_string('alnum', 16));

//                 $data = $this->input->post('bdata');

//                 $bdata['first_name']= $ldata['full_name'];
//                 $bdata['companyname']= $data['companyname'];
//                 // $bdata['profile_picture']= $data['sale_name'];
//                 $bdata['phone']= $data['contactinfo'];
//                 $data = $this->input->post();
//                 $bdata['address_1'] = json_encode($data['o_address']);
//                 $bdata['city'] = json_encode($data['o_city']);
//                 $bdata['contactinfo'] = json_encode($data['o_phone']);
//                 $bdata['state'] = json_encode($data['o_state']);
//                 $bdata['zip'] = json_encode($data['o_zip']);

//                 $where['user_id'] = $this->session->userdata('d_user_id');

//                 $login_info = $this->p_model->updateInfo($ldata,$where,'tbl_user_login_info');

//                 $basic_info = $this->p_model->updateInfo($bdata,$where,'tbl_user_profile_info');

//                 if (!empty($login_info) AND ! empty($basic_info)) {

//                     $this->session->set_flashdata('success',"Profile Updated Successfully");
//                 } else {
//                     $this->session->set_flashdata('error',"Updation failed ! Please try again. ");
//                 }
//                 redirect('dealer_profile');
//             }
//         }else{
//             $result = $this->p_model->get_user_info();
//             $result['users_info'] = $this->user_mdl->get_active_admins_info();
//             // echo '<pre>';print_r($result['users_info']);exit;

// //            print_r($result);exit;
//             $this->load->view('user_panel/common/header');
//             $this->load->view('user_panel/dealer_profile',array('user'=>$result));
//             $this->load->view('user_panel/common/footer');
//         }
//     }

    public function user_profile(){

        if (!$this->session->userdata('d_is_logged')){
            $this->session->set_flashdata('error',"Access Denied. Please Log-in first");
            redirect('page', 'refresh');
        }
        if ($this->input->post()){

// echo '<pre>';print_r($this->session->userdata());exit;
            $config = array(
                array('field' => 'bdata[first_name]', 'label' => 'first name', 'rules' => 'trim|required|max_length[50]'),
//                array('field' => 'ldata[user_password]', 'label' => 'password', 'rules' => 'trim|required|min_length[8]|max_length[50]'),
//                array('field' => 're_password', 'label' => 're-password', 'rules' => 'trim|required|matches[ldata[user_password]]'),
//                array('field' => 'ldata[email_address]', 'label' => 'email address', 'rules' => 'trim|required|valid_email|is_unique[tbl_user_login_info.email_address]'),
                array('field' => 'bdata[contactinfo]', 'label' => 'Dealer phone', 'rules' => 'trim|required'),
                array('field' => 'bdata[companyname]', 'label' => 'company name', 'rules' => 'trim|required|min_length[5]'),
//                array('field' => 'bdata[sales_manager_id]', 'label' => 'Sales Manager', 'rules' => 'trim|required'),
                array('field' => 'o_phone[]', 'label' => 'Office phone', 'rules' => 'trim|required|max_length[25]'),
                array('field' => 'o_address[]', 'label' => 'Office address', 'rules' => 'trim|required|min_length[8]|max_length[150]'),
                array('field' => 'o_state[]', 'label' => 'Office state', 'rules' => 'trim|required|max_length[150]'),
                array('field' => 'o_city[]', 'label' => 'Office city', 'rules' => 'trim|required|max_length[150]'),
                array('field' => 'o_zip[]', 'label' => 'Office zip', 'rules' => 'trim|required|max_length[20]'),
            );
            if($this->session->userdata('d_access_label') != 4){
                $config[] = array('field' => 'bdata[sales_manager_id]', 'label' => 'Sales Manager', 'rules' => 'trim|required');
            }

            if($this->session->userdata('d_email_address') == $this->input->post('ldata[email_address]')){
                $config[] = array('field' => 'ldata[email_address]', 'label' => 'email address', 'rules' => 'trim|required|valid_email');
            }else{
                $config[] = array('field' => 'ldata[email_address]', 'label' => 'email address', 'rules' => 'trim|required|valid_email|is_unique[tbl_user_login_info.email_address]');
            }

            $pass = '0';
            if($this->input->post('user_password') && !empty($this->input->post('user_password')) || ($this->input->post('re-password') && !empty($this->input->post('re-password')))){
                $config[] = array('field' => 'user_password', 'label' => 'password', 'rules' => 'trim|required|min_length[8]|max_length[50]');
                $config[] = array('field' => 're_password', 'label' => 're-password', 'rules' => 'trim|required|matches[user_password]');
                $pass = '1';
            }


            $this->form_validation->set_rules($config);
            if ($this->form_validation->run() == FALSE) {
                $this->session->set_flashdata('error',validation_errors());
                redirect('dealer_profile');
            } else {
                $this->load->helper('string');

                $ldata = $this->input->post('ldata');
                $ldata['full_name'] = $this->input->post('bdata[first_name]', true);

                if($pass == '1') {
                    $ldata['user_password'] = md5($this->input->post('user_password'));
                }

                $ldata['email_address'] = $this->input->post('ldata[email_address]');
                if($this->session->userdata('d_access_label') != 4){
                    $ldata['sales_manager_id'] = $this->input->post('bdata[sales_manager_id]');
                }else{
                    $ldata['sales_manager_id'] = 0 ;
                }

//                $ldata['access_label'] = 3;
//                $ldata['activation_key'] = md5(random_string('alnum', 16));

                $data = $this->input->post('bdata');

                $bdata['first_name']= $ldata['full_name'];
                $bdata['companyname']= $data['companyname'];
                // $bdata['profile_picture']= $data['sale_name'];
                $bdata['phone']= $data['contactinfo'];
                $data = $this->input->post();
                $bdata['address_1'] = json_encode($data['o_address']);
                $bdata['city'] = json_encode($data['o_city']);
                $bdata['contactinfo'] = json_encode($data['o_phone']);
                $bdata['state'] = json_encode($data['o_state']);
                $bdata['zip'] = json_encode($data['o_zip']);

                $where['user_id'] = $this->session->userdata('d_user_id');

                $login_info = $this->p_model->updateInfo($ldata,$where,'tbl_user_login_info');

                $basic_info = $this->p_model->updateInfo($bdata,$where,'tbl_user_profile_info');

                if (!empty($login_info) AND ! empty($basic_info)) {

                    $this->session->set_flashdata('success',"Profile Updated Successfully");
                } else {
                    $this->session->set_flashdata('error',"Updation failed ! Please try again. ");
                }
                redirect('dealer_profile');
            }
        }else{
            $result = $this->p_model->get_user_info();
            $result['users_info'] = $this->user_mdl->get_active_admins_info();
            // echo '<pre>';print_r($result['users_info']);exit;

//            print_r($result);exit;
            $this->load->view('user_panel/common/header');
            $this->load->view('user_panel/dealer_profile',array('user'=>$result));
            $this->load->view('user_panel/common/footer');
        }
    }

    public function reset_password($random_string){
    
    	$data=array('random_string'=>$random_string);
    	$result=$this->p_model->checkInfo($data,'tbl_reset_pass');

    	if($result>=1){
    	
    		$result=$this->p_model->getInfo($data,'tbl_reset_pass');
    		
    		foreach($result as $row){
    			$data['email_address']=$row->email_address;
    		}
    		
    		$data['title'] = 'Update Password';
        	$data['menu_active'] = 'Update Password';
        	$this->load->view('user_panel/common/header');
        	$this->load->view('user_panel/update_password',$data);
        	$this->load->view('user_panel/common/footer');	
    	
    	} else {
    	
    		exit();
    	}
    	  
    }
    
    public function update_password() {
    
    	
    	$data=array(	
    			'user_password'=>md5($this->input->post('password'))
    	);
    	$where['email_address']=$this->input->post('email_address');
    	
    	if($result=$this->p_model->updateInfo($data,$where,'tbl_user_login_info')){
    		$sdata['message']="Password Changed.";
    		$this->session->set_userdata($sdata);
    		$data['title'] = 'Sign In';
        	$data['menu_active'] = 'Sign In';
        	$this->load->view('user_panel/common/header');
        	$this->load->view('user_panel/sign_in');
        	$this->load->view('user_panel/common/footer');
    	}
    	
    	
    }

}
