<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Front extends MSN_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('admin_models/User_Order_Model', 'order_mdl');
        $this->load->model('admin_models/Invoice_Model', 'inv_mdl');
        $this->load->model('admin_models/User_Quote_Model', 'quote_mdl');
        $this->load->model('user_models/Page_Model', 'content');
    }
    public function training() {
        $this->load->view('user_panel/training/videos');
    }
    

    public function training_course() {
        $data['title'] = 'Training Course';
        $data['menu_active'] = 'Training Course';
        $data['data'] = $this->content->get('cbt_videos', '', 'id', 'desc');
        $this->load->view('user_panel/common/header');
        $this->load->view('user_panel/training_course', $data);
        $this->load->view('user_panel/common/footer');
    }

    public function viewVideo($id) {
        $data['data']=$this->content->get_where('*' ,array('id'=>$id),'cbt_videos');
        $data['title'] = 'Corporate Announcement';
        $data['menu_active'] = 'Corporate Announcement';
        $this->load->view('user_panel/common/header');
        $this->load->view('user_panel/viewVideo', $data);
        $this->load->view('user_panel/common/footer');
    }


    public function cbt() {
        $data['title'] = 'CBT';
        $data['menu_active'] = 'CBT';
        $this->load->view('user_panel/common/header');
        $this->load->view('user_panel/cbt', $data);
        $this->load->view('user_panel/common/footer');
    }

    public function schedule_training() {
        $data['title'] = 'Schedule Training';
        $data['menu_active'] = 'Schedule Training';
        $this->load->view('user_panel/common/header');
        $this->load->view('user_panel/schedule_training', $data);
        $this->load->view('user_panel/common/footer');
    }

    public function manuals() {
        $data['title'] = 'Manuals';
        $data['menu_active'] = 'Manuals';
        $this->load->view('user_panel/common/header');
        $this->load->view('user_panel/manuals', $data);
        $this->load->view('user_panel/common/footer');
    }

    public function press_release() {
        $data['title'] = 'Press Release';
        $data['menu_active'] = 'Press Release';
        $this->load->view('user_panel/common/header');
        $this->load->view('user_panel/press_release', $data);
        $this->load->view('user_panel/common/footer');
    }

    public function product_announcement() {
        $data['title'] = 'Product Announcement';
        $data['menu_active'] = 'Product Announcement';
        $data['data'] = $this->content->get('pressreleases', '','id', 'desc');
        $this->load->view('user_panel/common/header');
        $this->load->view('user_panel/product_announcement', $data);
        $this->load->view('user_panel/common/footer');
    }

    public function dealer_agreement() {
        $data['title'] = 'Dealer Agreement';
        $data['menu_active'] = 'Dealer Agreement';
        $data['data'] = $this->content->get('dealer_agreement', '','id', 'desc');
        $this->load->view('user_panel/common/header');
        $this->load->view('user_panel/dealer_agreement', $data);
        $this->load->view('user_panel/common/footer');
    }

    public function marketing_materials() {
        $data['title'] = 'Marketing Material';
        $data['menu_active'] = 'Marketing Material';
        $data['data'] = $this->content->get('marketing_material', '','id', 'desc');
        $this->load->view('user_panel/common/header');
        $this->load->view('user_panel/marketing_material', $data);
        $this->load->view('user_panel/common/footer');
    }

    public function viewProAnnouncement($id) {
        $data['data']=$this->content->get_where('*' ,array('id'=>$id),'pressreleases');
        $data['title'] = 'Corporate Announcement';
        $data['menu_active'] = 'Corporate Announcement';
        $this->load->view('user_panel/common/header');
        $this->load->view('user_panel/viewProAnnouncement', $data);
        $this->load->view('user_panel/common/footer');
    }


    public function corporate_announcement() {
        $data['title'] = 'Corporate Announcement';
        $data['menu_active'] = 'Corporate Announcement';
        $data['data'] = $this->content->get('corporate_announcement', '', 'id', 'desc');
        $this->load->view('user_panel/common/header');
        $this->load->view('user_panel/corporate_announcement', $data);
        $this->load->view('user_panel/common/footer');
    }
    public function viewCorpAnnouncement($id) {
        $data['data']=$this->content->get_where('*' ,array('id'=>$id),'corporate_announcement');
        $data['title'] = 'Corporate Announcement';
        $data['menu_active'] = 'Corporate Announcement';
        $this->load->view('user_panel/common/header');
        $this->load->view('user_panel/viewCorpAnnouncement', $data);
        $this->load->view('user_panel/common/footer');
    }

    public function brochures() {
        $data['title'] = 'Brochures';
        $data['menu_active'] = 'Brochures';
        $data['data'] = $this->content->get('brochures', '', 'id', 'desc');
        $this->load->view('user_panel/common/header');
        $this->load->view('user_panel/brochures', $data);
        $this->load->view('user_panel/common/footer');
    }

    public function notices() {
        $data['title'] = 'Notices';
        $data['menu_active'] = 'Notices';
        $this->load->view('user_panel/common/header');
        $this->load->view('user_panel/notices', $data);
        $this->load->view('user_panel/common/footer');
    }

    public function documents() {
        $data['title'] = 'Documents';
        $data['menu_active'] = 'Documents';
        $this->load->view('user_panel/common/header');
        $this->load->view('user_panel/documents', $data);
        $this->load->view('user_panel/common/footer');
    }
}