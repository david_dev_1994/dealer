<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/* * *******************************************#
  #      User Management System                 #
  #*********************************************#
  #      Author:     Atique IT                  #
  #      Email:      info@atique-it.com         #
  #      Website:    http://atique-it.com       #
  #                                             #
  #      Version:    15.2.1                     #
  #      Copyright:  (c) 2015 - Atique IT       #
  #                                             #
  #*********************************************# */

class Order extends MSN_Controller {

    public function __construct() {
        parent::__construct();
        $user_id = $this->session->userdata('user_id');
        if ($user_id == NULL) {
            redirect('user_login', 'refresh');
        }
        $this->load->model('admin_models/User_Order_Model', 'order_mdl');
        $this->load->model('admin_models/Invoice_Model', 'inv_mdl');
        $this->load->model('admin_models/User_Quote_Model', 'quote_mdl');
    }
    public function index() {
        $data['title'] = 'Placed Order';
        $data['menu_active'] = 'Order';
        $user_id = $this->session->userdata('user_id');
        $data['order_info'] = $this->order_mdl->get_all_order_info_by_user_id($user_id);
        $this->load->view('user_panel/common/header');
        $this->load->view('user_panel/placed_order', $data);
        $this->load->view('user_panel/common/footer');
    }
    
    public function make_invoice($order_id) {
        $data['title'] = 'Invoice';
        $data['menu_active'] = 'Invoice';
        $data['invoice_info'] = $this->inv_mdl->get_invoice_info($order_id);

        $this->load->view('user_panel/common/header');
        $this->load->view('user_panel/invoice', $data);
        $this->load->view('user_panel/common/footer');
    }
    
    public function view_details($sub_quote_id){
        $data['title'] = 'Job Sub Quote';
        $data['menu_active'] = 'Job Quote';
        $data['sub_quote_info'] = $this->quote_mdl->get_sub_quote_info_by_id($sub_quote_id);
        $this->load->view('user_panel/common/header');
        $this->load->view('user_panel/view_details', $data);
        $this->load->view('user_panel/common/footer');
    }
}
