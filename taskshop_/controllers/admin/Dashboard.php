<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/* * *******************************************#
  #      User Management System                 #
  #*********************************************#
  #      Author:     Atique IT                  #
  #      Email:      info@atique-it.com         #
  #      Website:    http://atique-it.com       #
  #                                             #
  #      Version:    15.2.1                     #
  #      Copyright:  (c) 2015 - Atique IT       #
  #                                             #
  #*********************************************# */

class Dashboard extends MSN_Controller {
    
    public function __construct() {
        parent::__construct();
        $this->load->model('admin_models/Others_Model', 'others_mdl');
        $this->load->model('admin_models/Profile_Model', 'pro_mdl');
        $this->admin_login_authentication();
    }

    public function index() {
        $data['title'] = 'Dashboard';
        $data['menu_active'] = 'Dashboard';
        $data['email_notification'] = $this->others_mdl->get_all_unred_notification();
        $data['user_info'] = $this->pro_mdl->get_user_info();
        $data['total_user'] = $this->db->count_all('tbl_user_login_info');
        $data['total_admin'] = $this->others_mdl->count_all_admin();
        $data['t_user'] = $this->others_mdl->count_users();
        $this->load->view('admin_panel/common/header', $data);
        $this->load->view('admin_panel/common/menu_bar', $data);
        $this->load->view('admin_panel/dashboard/dashboard');
        $this->load->view('admin_panel/common/footer');
    }

}
