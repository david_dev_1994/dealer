<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/* * *******************************************#
  #      User Management System                 #
  #*********************************************#
  #      Author:     Atique IT                  #
  #      Email:      info@atique-it.com         #
  #      Website:    http://atique-it.com       #
  #                                             #
  #      Version:    15.2.1                     #
  #      Copyright:  (c) 2015 - Atique IT       #
  #                                             #
  #*********************************************# */

class Ftp_file_manager extends MSN_Controller {

    public function __construct() {
        parent::__construct();

        $this->admin_login_authentication();
        $this->super_admin_and_admin_authentication_only();
        $this->load->model('admin_models/Others_Model', 'others_mdl');
        $this->load->model('admin_models/Profile_Model', 'pro_mdl');
        $this->load->model('admin_models/Ftp_File_Manager_Model', 'ftp_mdl');
    }

    public function index() {
        $data['title'] = 'FTP File Manager';
        $data['menu_active'] = 'FTP';
        $data['email_notification'] = $this->others_mdl->get_all_unred_notification();
        $data['user_info'] = $this->pro_mdl->get_user_info();
        $data['ftp_info'] = $this->ftp_mdl->get_ftp_info();
        $this->load->view('admin_panel/common/header', $data);
        $this->load->view('admin_panel/common/menu_bar');
        $this->load->view('admin_panel/ftp_file_manager/add_ftp_info');
        $this->load->view('admin_panel/common/footer');
    }

    public function add_file() {
        $data['title'] = 'FTP File Manager';
        $data['menu_active'] = 'FTP';
        $data['email_notification'] = $this->others_mdl->get_all_unred_notification();
        $data['user_info'] = $this->pro_mdl->get_user_info();
        $this->load->view('admin_panel/common/header', $data);
        $this->load->view('admin_panel/common/menu_bar');
        $this->load->view('admin_panel/ftp_file_manager/add_file');
        $this->load->view('admin_panel/common/footer');
    }

    public function update_ftp_setting() {
        $ftp_id = $this->input->post('ftp_id', TRUE);
        $config = array(
            array(
                'field' => 'data[ftp_hostname]',
                'label' => 'hostname',
                'rules' => 'trim|required'
            ),
            array(
                'field' => 'data[ftp_username]',
                'label' => 'username',
                'rules' => 'trim|required'
            ),
            array(
                'field' => 'data[ftp_password]',
                'label' => 'password',
                'rules' => 'trim|required'
            )
        );
        $this->form_validation->set_rules($config);
        if ($this->form_validation->run() == FALSE) {
            $this->index();
        } else {
            $data = $this->input->post('data', TRUE);
            $result = $this->ftp_mdl->update_ftp_info_by_id($ftp_id, $data);
            if (!empty($result)) {
                $sdata['message'] = '<h3>FTP info update successfully .</h3>';
                $this->session->set_userdata($sdata);
                redirect('admin/ftp_file_manager', 'refresh');
            } else {
                $sdata['error'] = '<h3>FTP updation failed !</h3>';
                $this->session->set_userdata($sdata);
                redirect('admin/ftp_file_manager', 'refresh');
            }
        }
    }

    public function upload_file() {
        if ($this->input->post('submit')) {
            //Upload to the local server
            $config['upload_path'] = 'uploaded_files/file_manager/';
            $config['allowed_types'] = '*';
            $this->load->library('upload', $config);

            if ($this->upload->do_upload('file')) {
                //Get uploaded file information
                $upload_data = $this->upload->data();
                $fileName = $upload_data['file_name'];

                //File path at local server
                $source = 'uploaded_files/file_manager/' . $fileName;

                //Load codeigniter FTP class
                $this->load->library('ftp');

                //FTP configuration
                $ftp_config['hostname'] = 'ftp.example.com';
                $ftp_config['username'] = 'ftp_username';
                $ftp_config['password'] = 'ftp_password';
                $ftp_config['debug'] = TRUE;

                //Connect to the remote server
                $this->ftp->connect($ftp_config);

                //File upload path of remote server
                $destination = '/uploaded_files/file_manager/' . $fileName;

                //Upload file to the remote server
                $this->ftp->upload($source, "." . $destination);

                //Close FTP connection
                $this->ftp->close();

                //Delete file from local server
                @unlink($source);
            }
        }else{
            $sdata['error'] = 'No file chosen !';
            $this->session->set_userdata($sdata);
            redirect('admin/file_manager', 'refresh');
        }
        
    }

    public function save_file() {
        if (isset($_FILES['upl']['name']) && !empty($_FILES['upl']['name'])) {
            $config['upload_path'] = 'uploaded_files/file_manager/';
            $config['allowed_types'] = 'jpg|png|jpeg|docx|zip|pdf';
            $config['max_size'] = ''; //kb
            $config['max_width'] = '';
            $config['max_height'] = '';
            $config['overwrite'] = false;

            $fdata = array();
            $this->load->library('upload', $config);

            if (!$this->upload->do_upload('upl')) {
                $sdata['error'] = $this->upload->display_errors();
                $this->session->set_userdata($sdata);
                redirect('admin/file_manager', 'refresh');
            } else {
                $fdata = $this->upload->data();
                $file_name = $fdata['file_name'];

                $result = $this->file_mdl->insert_file_info($file_name);

                if (!empty($result)) {
                    $sdata['message'] = 'File upload successfully .';
                    $this->session->set_userdata($sdata);
                    redirect('admin/file_manager', 'refresh');
                } else {
                    $sdata['error'] = 'File to upload !';
                    $this->session->set_userdata($sdata);
                    redirect('admin/file_manager', 'refresh');
                }
            }
        } else {
            $sdata['error'] = 'No file chosen !';
            $this->session->set_userdata($sdata);
            redirect('admin/file_manager', 'refresh');
        }
    }

}
