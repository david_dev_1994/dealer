<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/* * *******************************************#
  #      User Management System                 #
  #*********************************************#
  #      Author:     Atique IT                  #
  #      Email:      info@atique-it.com         #
  #      Website:    http://atique-it.com       #
  #                                             #
  #      Version:    15.2.1                     #
  #      Copyright:  (c) 2015 - Atique IT       #
  #                                             #
  #*********************************************# */

class File_manager extends MSN_Controller {

    public function __construct() {
        parent::__construct();

        $this->admin_login_authentication();
        $this->load->model('admin_models/Others_Model', 'others_mdl');
        $this->load->model('admin_models/Profile_Model', 'pro_mdl');
        $this->load->model('admin_models/File_Manager_Model', 'file_mdl');
    }

    public function index() {
        $data['title'] = 'File Manager';
        $data['menu_active'] = 'File Manager';
        $data['email_notification'] = $this->others_mdl->get_all_unred_notification();
        $data['user_info'] = $this->pro_mdl->get_user_info();
        $user_id = $this->session->userdata('user_id');
        $data['file_info'] = $this->file_mdl->get_all_file_info_by_user_id($user_id);
        $this->load->view('admin_panel/common/header', $data);
        $this->load->view('admin_panel/common/menu_bar');
        $this->load->view('admin_panel/file_manager/file_manager', $data);
        $this->load->view('admin_panel/common/footer');
    }

    public function image_upload($album_id) {
        $data['title'] = 'Gallery';
        $data['menu_active'] = 'Gallery';
        $data['album_id'] = $album_id;
        $data['email_notification'] = $this->others_mdl->get_all_unred_notification();
        $data['user_info'] = $this->pro_mdl->get_user_info();
        $this->load->view('admin_panel/common/header', $data);
        $this->load->view('admin_panel/common/menu_bar');
        $this->load->view('admin_panel/gallery/image_upload');
        $this->load->view('admin_panel/common/footer');
    }

    public function save_file() {
        if (isset($_FILES['upl']['name']) && !empty($_FILES['upl']['name'])) {
            $config['upload_path'] = 'uploaded_files/file_manager/';
            $config['allowed_types'] = 'jpg|png|jpeg|docx|zip|pdf';
            $config['max_size'] = ''; //kb
            $config['max_width'] = '';
            $config['max_height'] = '';
            $config['overwrite'] = false;

            $fdata = array();
            $this->load->library('upload', $config);

            if (!$this->upload->do_upload('upl')) {
                $sdata['error'] = $this->upload->display_errors();
                $this->session->set_userdata($sdata);
                redirect('admin/file_manager', 'refresh');
            } else {
                $fdata = $this->upload->data();
                $file_name = $fdata['file_name'];

                $result = $this->file_mdl->insert_file_info($file_name);

                if (!empty($result)) {
                    $sdata['message'] = 'File upload successfully .';
                    $this->session->set_userdata($sdata);
                    redirect('admin/file_manager', 'refresh');
                } else {
                    $sdata['error'] = 'File to upload !';
                    $this->session->set_userdata($sdata);
                    redirect('admin/file_manager', 'refresh');
                }
            }
        } else {
            $sdata['error'] = 'No file chosen !';
            $this->session->set_userdata($sdata);
            redirect('admin/file_manager', 'refresh');
        }
    }

}
