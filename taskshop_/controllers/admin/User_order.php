<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/* * *******************************************#
  #      User Management System                 #
  #*********************************************#
  #      Author:     Atique IT                  #
  #      Email:      info@atique-it.com         #
  #      Website:    http://atique-it.com       #
  #                                             #
  #      Version:    15.2.1                     #
  #      Copyright:  (c) 2015 - Atique IT       #
  #                                             #
  #*********************************************# */
//            echo '<pre>';
//            print_r($data);
//            exit();
class User_order extends MSN_Controller {

    public function __construct() {
        parent::__construct();

        $this->admin_login_authentication();
        $this->load->model('admin_models/Others_Model', 'others_mdl');
        $this->load->model('admin_models/Profile_Model', 'pro_mdl');
        $this->load->model('admin_models/User_Order_Model', 'order_mdl');
    }

    public function index() {
        $data['title'] = 'My Order';
        $data['menu_active'] = 'My Order';
        $data['email_notification'] = $this->others_mdl->get_all_unred_notification();
        $data['user_info'] = $this->pro_mdl->get_user_info();
        $user_id = $this->session->userdata('user_id');
        $data['order_info'] = $this->order_mdl->get_all_order_info_by_user_id($user_id);
        $this->load->view('admin_panel/common/header', $data);
        $this->load->view('admin_panel/common/menu_bar');
        $this->load->view('admin_panel/my_order/order', $data);
        $this->load->view('admin_panel/common/footer');
    }
    
    
}
