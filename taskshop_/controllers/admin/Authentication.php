<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/* * *******************************************#
  #      User Management System                 #
  #*********************************************#
  #      Author:     Atique IT                  #
  #      Email:      info@atique-it.com         #
  #      Website:    http://atique-it.com       #
  #                                             #
  #      Version:    15.2.1                     #
  #      Copyright:  (c) 2015 - Atique IT       #
  #                                             #
  #*********************************************# */

class Authentication extends MSN_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('admin_models/Authentication_Model', 'a_model');

        $logged_info = $this->session->userdata('logged_info');
        if ($logged_info != FALSE) {
            redirect('admin/dashboard', 'refresh');
        }
    }

    public function index() {
        $data['title'] = 'Admin Login';
        $this->load->view('admin_panel/user_login/user_login', $data);
    }

    public function check_admin_login() {

        $this->form_validation->set_rules('email_address', 'email address', 'trim|required|max_length[100]');
        $this->form_validation->set_rules('user_password', 'password', 'trim|required|max_length[50]');

        if ($this->form_validation->run() == FALSE) {
            $sdata['error'] = validation_errors();
            $this->session->set_userdata($sdata);
            redirect('admin/authentication');
        } else {
            $result = $this->a_model->check_login();
            if ($result && ($result->access_label == 1 || $result->access_label == 2) ) {
                $sdata['user_id'] = $result->user_id;
                $sdata['full_name'] = $result->full_name;
                $sdata['email_address'] = $result->email_address;
                $sdata['access_label'] = $result->access_label;
                $sdata['logged_info'] = TRUE;
                $this->session->set_userdata($sdata);
                redirect('admin/dashboard');
            } else {
                $sdata['error'] = 'The email or password you’ve entered doesn’t match any account !';
                $this->session->set_userdata($sdata);
                redirect('admin/authentication');
            }
        }
    }

    public function user_registration() {
        /* For Captcha */
        $vals = array(
            'img_path' => './captcha/',
            'img_url' => base_url() . 'captcha/',
            'img_width' => '260',
            'img_height' => 35
        );
        $captcha = create_captcha($vals);
        $data['image'] = $captcha['image'];
        $data['word'] = $captcha['word'];
        /* End Captcha */
        $data['title'] = 'User Registration';
        $this->load->view('admin_panel/user_registration/user_registration', $data);
    }

    public function registration() {
        $config = array(
            array(
                'field' => 'full_name',
                'label' => 'full name',
                'rules' => 'trim|required|min_length[3]|max_length[50]'
            ),
            array(
                'field' => 'email_address',
                'label' => 'email address',
                'rules' => 'trim|required|valid_email|is_unique[tbl_user_login_info.email_address]'
            ),
            array(
                'field' => 'user_password',
                'label' => 'password',
                'rules' => 'trim|required|min_length[8]|max_length[15]'
            ),
            array(
                'field' => 'confirm_password',
                'label' => 'confirm password',
                'rules' => 'trim|required|matches[user_password]'
            ),
            array(
                'field' => 'captcha',
                'label' => 'captcha',
                'rules' => 'trim|required'
            ),
            array(
                'field' => 'retype_captcha',
                'label' => 'captcha',
                'rules' => 'trim|required|matches[captcha]'
            )
        );

        $this->form_validation->set_rules($config);
        if ($this->form_validation->run() == FALSE) {
            $this->user_registration();
        } else {
            $data['full_name'] = $this->input->post('full_name', TRUE);
            $data['email_address'] = $this->input->post('email_address', TRUE);
            $data['user_password'] = md5($this->input->post('user_password', TRUE));
            $data['access_label'] = 3;
            $data['activation_status'] = 1;
            $insert_id = $this->a_model->insert_user_registration_info($data);
            if (!empty($insert_id)) {
                $pdata['user_id'] = $insert_id;
                $this->a_model->create_user_profile($pdata);
                $sdata['message'] = 'Registration successfully . You may login now';
                $this->session->set_userdata($sdata);
                redirect('admin/authentication', 'refresh');
            } else {
                $sdata['error'] = 'Registration failed, Please try again !';
                $this->session->set_userdata($sdata);
                redirect('admin/authentication', 'refresh');
            }
        }
    }

    public function ajax_check_email($email_address = null) {
        //echo $email_address;
        $result = $this->a_model->ajax_email_check_info($email_address);
        if ($result) {
            echo "<span style='color:#990000;'>Already exist</span>";
        } else {
            echo "<span style='color:green;'>Avilable</span>";
        }
    }

}
