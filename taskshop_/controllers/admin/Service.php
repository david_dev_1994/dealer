<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/* * *******************************************#
  #      User Management System                 #
  #*********************************************#
  #      Author:     Atique IT                  #
  #      Email:      info@atique-it.com         #
  #      Website:    http://atique-it.com       #
  #                                             #
  #      Version:    15.2.1                     #
  #      Copyright:  (c) 2015 - Atique IT       #
  #                                             #
  #*********************************************# */

class Service extends MSN_Controller {

    public function __construct() {
        parent::__construct();

        $this->admin_login_authentication();
        $this->super_admin_authentication_only();
        $this->load->model('admin_models/Others_Model', 'others_mdl');
        $this->load->model('admin_models/Profile_Model', 'pro_mdl');
        $this->load->model('admin_models/Service_Model', 'serv_mdl');
    }

    public function index() {
        $data['title'] = 'Service';
        $data['menu_active'] = 'Service';
        $data['email_notification'] = $this->others_mdl->get_all_unred_notification();
        $data['user_info'] = $this->pro_mdl->get_user_info();
        $data['service_info'] = $this->serv_mdl->get_all_service_info();
        $this->load->view('admin_panel/common/header', $data);
        $this->load->view('admin_panel/common/menu_bar');
        $this->load->view('admin_panel/service/service', $data);
        $this->load->view('admin_panel/common/footer');
    }

    public function add_service() {
        $data['title'] = 'Add Service';
        $data['menu_active'] = 'Service';
        $data['email_notification'] = $this->others_mdl->get_all_unred_notification();
        $data['user_info'] = $this->pro_mdl->get_user_info();
        $this->load->view('admin_panel/common/header', $data);
        $this->load->view('admin_panel/common/menu_bar');
        $this->load->view('admin_panel/service/add_service');
        $this->load->view('admin_panel/common/footer');
    }

    public function save_service() {
        $config = array(
            array(
                'field' => 'data[service_name]',
                'label' => 'service name',
                'rules' => 'trim|required|min_length[5]'
            ),
            array(
                'field' => 'data[short_description]',
                'label' => 'short description',
                'rules' => 'trim|required|min_length[5]'
            ),
            array(
                'field' => 'data[description]',
                'label' => 'description',
                'rules' => 'trim|required|min_length[5]'
            ),
            array(
                'field' => 'data[service_price]',
                'label' => 'service price',
                'rules' => 'trim|required'
            ),
            array(
                'field' => 'data[publication_status]',
                'label' => 'publication status',
                'rules' => 'required'
            )
        );
        $this->form_validation->set_rules($config);
        if ($this->form_validation->run() == FALSE) {
            $this->add_service();
        } else {
            $data = $this->input->post('data', TRUE);
            $data['user_id'] = $this->session->userdata('user_id');
            $data['product_photo_before'] = $this->add_image_one();
            $data['product_photo_after'] = $this->add_image_two();
            $data['service_one_photo_before'] = $this->add_image_three();
            $data['service_one_photo_after'] = $this->add_image_four();
            $data['service_two_photo_before'] = $this->add_image_five();
            $data['service_two_photo_after'] = $this->add_image_six();
//            echo '<pre>';
//            print_r($data);
//            exit();
            $result = $this->serv_mdl->insert_service_info($data);
            if (!empty($result)) {
                $sdata['message'] = 'Service add successfully .';
                $this->session->set_userdata($sdata);
                redirect('admin/service', 'refresh');
            } else {
                $sdata['error'] = 'Service insertion failed !';
                $this->session->set_userdata($sdata);
                redirect('admin/service', 'refresh');
            }
        }
    }

    public function unpublished_service($service_id) {
        $result = $this->serv_mdl->unpublished_service_by_id($service_id);

        if (!empty($result)) {
            $sdata['message'] = 'Service unpublished successfully .';
            $this->session->set_userdata($sdata);
            redirect('admin/service', 'refresh');
        } else {
            $sdata['error'] = 'Service unpublication failed !';
            $this->session->set_userdata($sdata);
            redirect('admin/service', 'refresh');
        }
    }

    public function published_service($service_id) {
        $result = $this->serv_mdl->published_service_by_id($service_id);

        if (!empty($result)) {
            $sdata['message'] = 'Service published successfully .';
            $this->session->set_userdata($sdata);
            redirect('admin/service', 'refresh');
        } else {
            $sdata['error'] = 'Service publication failed !';
            $this->session->set_userdata($sdata);
            redirect('admin/service', 'refresh');
        }
    }

    public function delete_service($service_id) {
        $result = $this->serv_mdl->delete_service_by_id($service_id);

        if (!empty($result)) {
            $sdata['message'] = 'Service remove successfully .';
            $this->session->set_userdata($sdata);
            redirect('admin/service', 'refresh');
        } else {
            $sdata['error'] = 'Service removation failed !';
            $this->session->set_userdata($sdata);
            redirect('admin/service', 'refresh');
        }
    }

    public function add_image_one() {
        if (isset($_FILES['image_one']['name']) && !empty($_FILES['image_one']['name'])) {
            $config['upload_path'] = 'uploaded_files/service_pictures/';
            $config['allowed_types'] = 'jpg|png|jpeg|gif';
            $config['max_size'] = ''; //kb
            $config['max_width'] = '';
            $config['max_height'] = '';
            $config['overwrite'] = false;

            $fdata = array();
            $this->load->library('upload', $config);

            if (!$this->upload->do_upload('image_one')) {
                $sdata['error'] = $this->upload->display_errors();
                $this->session->set_userdata($sdata);
                redirect('admin/service/add_service');
            } else {
                $fdata = $this->upload->data();
                $picture_name = $fdata['file_name'];
                return $picture_name;
            }
        } else {
            $sdata['error'] = 'No file chosen !';
            $this->session->set_userdata($sdata);
            redirect('admin/service/add_service');
        }
    }

    public function add_image_two() {
        if (isset($_FILES['image_two']['name']) && !empty($_FILES['image_two']['name'])) {
            $config['upload_path'] = 'uploaded_files/service_pictures/';
            $config['allowed_types'] = 'jpg|png|jpeg|gif';
            $config['max_size'] = ''; //kb
            $config['max_width'] = '';
            $config['max_height'] = '';
            $config['overwrite'] = false;

            $fdata = array();
            $this->load->library('upload', $config);

            if (!$this->upload->do_upload('image_two')) {
                $sdata['error'] = $this->upload->display_errors();
                $this->session->set_userdata($sdata);
                redirect('admin/service/add_service');
            } else {
                $fdata = $this->upload->data();
                $picture_name = $fdata['file_name'];
                return $picture_name;
            }
        }
    }

    public function add_image_three() {
        if (isset($_FILES['image_three']['name']) && !empty($_FILES['image_three']['name'])) {
            $config['upload_path'] = 'uploaded_files/service_pictures/';
            $config['allowed_types'] = 'jpg|png|jpeg|gif';
            $config['max_size'] = ''; //kb
            $config['max_width'] = '';
            $config['max_height'] = '';
            $config['overwrite'] = false;

            $fdata = array();
            $this->load->library('upload', $config);

            if (!$this->upload->do_upload('image_three')) {
                $sdata['error'] = $this->upload->display_errors();
                $this->session->set_userdata($sdata);
                redirect('admin/service/add_service');
            } else {
                $fdata = $this->upload->data();
                $picture_name = $fdata['file_name'];
                return $picture_name;
            }
        }
    }

    public function add_image_four() {
        if (isset($_FILES['image_four']['name']) && !empty($_FILES['image_four']['name'])) {
            $config['upload_path'] = 'uploaded_files/service_pictures/';
            $config['allowed_types'] = 'jpg|png|jpeg|gif';
            $config['max_size'] = ''; //kb
            $config['max_width'] = '';
            $config['max_height'] = '';
            $config['overwrite'] = false;

            $fdata = array();
            $this->load->library('upload', $config);

            if (!$this->upload->do_upload('image_four')) {
                $sdata['error'] = $this->upload->display_errors();
                $this->session->set_userdata($sdata);
                redirect('admin/service/add_service');
            } else {
                $fdata = $this->upload->data();
                $picture_name = $fdata['file_name'];
                return $picture_name;
            }
        }
    }

    public function add_image_five() {
        if (isset($_FILES['image_five']['name']) && !empty($_FILES['image_five']['name'])) {
            $config['upload_path'] = 'uploaded_files/service_pictures/';
            $config['allowed_types'] = 'jpg|png|jpeg|gif';
            $config['max_size'] = ''; //kb
            $config['max_width'] = '';
            $config['max_height'] = '';
            $config['overwrite'] = false;

            $fdata = array();
            $this->load->library('upload', $config);

            if (!$this->upload->do_upload('image_five')) {
                $sdata['error'] = $this->upload->display_errors();
                $this->session->set_userdata($sdata);
                redirect('admin/service/add_service');
            } else {
                $fdata = $this->upload->data();
                $picture_name = $fdata['file_name'];
                return $picture_name;
            }
        }
    }
    
    public function add_image_six() {
        if (isset($_FILES['image_six']['name']) && !empty($_FILES['image_six']['name'])) {
            $config['upload_path'] = 'uploaded_files/service_pictures/';
            $config['allowed_types'] = 'jpg|png|jpeg|gif';
            $config['max_size'] = ''; //kb
            $config['max_width'] = '';
            $config['max_height'] = '';
            $config['overwrite'] = false;

            $fdata = array();
            $this->load->library('upload', $config);

            if (!$this->upload->do_upload('image_six')) {
                $sdata['error'] = $this->upload->display_errors();
                $this->session->set_userdata($sdata);
                redirect('admin/service/add_service');
            } else {
                $fdata = $this->upload->data();
                $picture_name = $fdata['file_name'];
                return $picture_name;
            }
        }
    }

}
