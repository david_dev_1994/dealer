<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/* * *******************************************#
  #      User Management System                 #
  #*********************************************#
  #      Author:     Atique IT                  #
  #      Email:      info@atique-it.com         #
  #      Website:    http://atique-it.com       #
  #                                             #
  #      Version:    15.2.1                     #
  #      Copyright:  (c) 2015 - Atique IT       #
  #                                             #
  #*********************************************# */
//            echo '<pre>';
//            print_r($data);
//            exit();
class Admin_order extends MSN_Controller {

    public function __construct() {
        parent::__construct();

        $this->admin_login_authentication();
        $this->super_admin_and_admin_authentication_only();
        $this->load->model('admin_models/Others_Model', 'others_mdl');
        $this->load->model('admin_models/Profile_Model', 'pro_mdl');
        $this->load->model('admin_models/Admin_Order_Model', 'order_mdl');
    }

    public function index() {
        $data['title'] = 'My Order';
        $data['menu_active'] = 'My Order';
        $data['email_notification'] = $this->others_mdl->get_all_unred_notification();
        $data['user_info'] = $this->pro_mdl->get_user_info();
        $data['order_info'] = $this->order_mdl->get_all_order_info_by_user_id();
        $this->load->view('admin_panel/common/header', $data);
        $this->load->view('admin_panel/common/menu_bar');
        $this->load->view('admin_panel/my_order/order', $data);
        $this->load->view('admin_panel/common/footer');
    }
    
    public function paid_order($sub_quote_id){
        $result = $this->order_mdl->invoice_paid_by_id($sub_quote_id);

        if (!empty($result)) {
            $sdata['message'] = '<h3>Order paid successfully .</h3>';
            $this->session->set_userdata($sdata);
            redirect('admin/admin_order', 'refresh');
        } else {
            $sdata['error'] = '<h3>Order paid failed !</h3>';
            $this->session->set_userdata($sdata);
            redirect('admin/admin_order', 'refresh');
        }
    }
}
