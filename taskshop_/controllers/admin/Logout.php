<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/* * *******************************************#
  #      User Management System                 #
  #*********************************************#
  #      Author:     Atique IT                  #
  #      Email:      info@atique-it.com         #
  #      Website:    http://atique-it.com       #
  #                                             #
  #      Version:    15.2.1                     #
  #      Copyright:  (c) 2015 - Atique IT       #
  #                                             #
  #*********************************************# */

class Logout extends MSN_Controller {

    public function __construct() {
        parent::__construct();
        $this->admin_login_authentication();
    }

    public function index() {
        $this->session->unset_userdata('user_id');
        $this->session->unset_userdata('full_name');
        $this->session->unset_userdata('email_address');
        $this->session->unset_userdata('access_label');
        $this->session->unset_userdata('logged_info');
//        $this->session->sess_destroy();
        $sadat['message'] = 'Logout successfully.';
        $this->session->set_userdata($sadat);
        redirect('cms');
    }

}
