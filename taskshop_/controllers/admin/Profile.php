<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/* * *******************************************#
  #      User Management System                 #
  #*********************************************#
  #      Author:     Atique IT                  #
  #      Email:      info@atique-it.com         #
  #      Website:    http://atique-it.com       #
  #                                             #
  #      Version:    15.2.1                     #
  #      Copyright:  (c) 2015 - Atique IT       #
  #                                             #
  #*********************************************# */

class Profile extends MSN_Controller {

    public function __construct() {
        parent::__construct();

        $this->admin_login_authentication();
        $this->load->model('admin_models/Profile_Model', 'pro_mdl');
        $this->load->model('admin_models/Others_Model', 'others_mdl');
    }

    public function index() {
        $data['title'] = 'Manage Access Label';
        $data['menu_active'] = '';
        $data['email_notification'] = $this->others_mdl->get_all_unred_notification();
        $data['user_info'] = $this->pro_mdl->get_user_info();
        $this->load->view('admin_panel/common/header', $data);
        $this->load->view('admin_panel/common/menu_bar',$data);
        $this->load->view('admin_panel/profile/profile');
        $this->load->view('admin_panel/common/footer');
    }

    public function update_profile() {
        $data = $this->input->post('data', TRUE);
        $result = $this->pro_mdl->update_profile_info($data);
        if (!empty($result)) {
            $sdata['message'] = 'Profile update successfully .';
            $this->session->set_userdata($sdata);
            redirect('admin/profile', 'refresh');
        } else {
            $sdata['error'] = 'Profile updation failed !';
            $this->session->set_userdata($sdata);
            redirect('admin/profile', 'refresh');
        }
    }

    public function update_profile_picture() {
        $prev_img_name = $this->input->post('prev_img_name', TRUE);

        if (isset($_FILES['profile_picture']['name']) && !empty($_FILES['profile_picture']['name'])) {
            $config['upload_path'] = 'uploaded_files/profile_pictures/';
            $config['allowed_types'] = 'jpg|png|jpeg';
            $config['max_size'] = ''; //kb
            $config['max_width'] = '';
            $config['max_height'] = '';
            $config['overwrite'] = false;

            $fdata = array();
            $this->load->library('upload', $config);

            if (!$this->upload->do_upload('profile_picture')) {
                $sdata['error'] = $this->upload->display_errors();
                $this->session->set_userdata($sdata);
                redirect('admin/profile');
            } else {
                $fdata = $this->upload->data();
                $picture_name = $fdata['file_name'];
                $data = array('upload_data' => $this->upload->data());
                $this->image_resize($data['upload_data']['full_path'], $data['upload_data']['file_name']);
                $this->delete_picture($prev_img_name);

                $result = $this->pro_mdl->update_profile_picture_info($picture_name);
                if ($result) {
                    $sdata['message'] = 'Picture update successfully.';
                    $this->session->set_userdata($sdata);
                    redirect('admin/profile');
                } else {
                    $sdata['error'] = 'Picture updation failed !';
                    $this->session->set_userdata($sdata);
                    redirect('admin/profile');
                }
            }
        } else {
            $sdata['error'] = 'No file chosen !';
            $this->session->set_userdata($sdata);
            redirect('admin/profile');
        }
    }

    public function image_resize($path, $file) {
        $config_resize = array();
        $config_resize['image_library'] = 'gd2';
        $config_resize['source_image'] = $path;
        $config_resize['create_thumb'] = FALSE;
        $config_resize['maintain_ratio'] = TRUE;
        $config_resize['overwrite'] = false;
        $config_resize['width'] = 240;
        $config_resize['height'] = 180;
        $config_resize['new_image'] = 'uploaded_files/profile_pictures/thumb/' . $file;

        $this->load->library('image_lib', $config_resize);
        $this->image_lib->resize();
    }

    public function delete_picture($picture_name) {
        if (!empty($picture_name)) {
            unlink('uploaded_files/profile_pictures/' . $picture_name);
            unlink('uploaded_files/profile_pictures/thumb/' . $picture_name);
        } else {
            return TRUE;
        }
    }

    public function change_password() {
        $user_id = $this->session->userdata('user_id');
        $check_old_pass = $this->pro_mdl->check_old_password($user_id);
        if (!empty($check_old_pass)) {
            $result = $this->pro_mdl->update_old_password($user_id);
            if (!empty($result)) {
                $sdata['message'] = 'Password change successfully . Please login again .';
                $this->session->set_userdata($sdata);
                redirect('admin/profile', 'refresh');
            } else {
                $sdata['error'] = 'Password change operation failed ! Try again .';
                $this->session->set_userdata($sdata);
                redirect('admin/profile', 'refresh');
            }
        } else {
            $sdata['error'] = 'Old password does not match ! Please insert correct password .';
            $this->session->set_userdata($sdata);
            redirect('admin/profile', 'refresh');
        }
    }

}
