<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/* * *******************************************#
  #      User Management System                 #
  #*********************************************#
  #      Author:     Atique IT                  #
  #      Email:      info@atique-it.com         #
  #      Website:    http://atique-it.com       #
  #                                             #
  #      Version:    15.2.1                     #
  #      Copyright:  (c) 2015 - Atique IT       #
  #                                             #
  #*********************************************# */

class Mail extends MSN_Controller {

    public function __construct() {
        parent::__construct();

        $this->admin_login_authentication();
        $this->super_admin_and_admin_authentication_only();
        $this->load->model('admin_models/Mail_Model', 'mail_mdl');
        $this->load->model('admin_models/Others_Model', 'others_mdl');
        $this->load->model('admin_models/Profile_Model', 'pro_mdl');
    }

    public function index() {
        $data['title'] = 'Compose Mail';
        $data['menu_active'] = 'Compose Mail';
        $data['email_notification'] = $this->others_mdl->get_all_unred_notification();
        $data['user_info'] = $this->pro_mdl->get_user_info();
        $this->load->view('admin_panel/common/header', $data);
        $this->load->view('admin_panel/common/menu_bar', $data);
        $this->load->view('admin_panel/mail/compose_mail');
        $this->load->view('admin_panel/common/footer');
    }

    public function inbox() {
        $data['title'] = 'Inbox';
        $data['menu_active'] = 'Inbox';
        $data['email_notification'] = $this->others_mdl->get_all_unred_notification();
        $data['user_info'] = $this->pro_mdl->get_user_info();
        $data['inbox_mail_info'] = $this->mail_mdl->get_inbox_mail();
        $this->load->view('admin_panel/common/header', $data);
        $this->load->view('admin_panel/common/menu_bar', $data);
        $this->load->view('admin_panel/mail/inbox', $data);
        $this->load->view('admin_panel/common/footer');
    }

    public function view_inbox_mail($mail_id) {
        if (!empty($mail_id)) {
            $data['title'] = 'Mail Details';
            $data['menu_active'] = 'Inbox';
            $data['mail_info'] = $this->mail_mdl->get_inbox_mail_by_id($mail_id);
            $this->mail_mdl->read_by_id($mail_id);
            $this->load->view('admin_panel/common/header', $data);
            $this->load->view('admin_panel/common/menu_bar');
            $this->load->view('admin_panel/mail/view_inbox_mail', $data);
            $this->load->view('admin_panel/common/footer');
        } else {
            redirect('admin/mail', 'refresh');
        }
    }

    public function delete_inbox_mail($mail_id) {
        $result = $this->mail_mdl->delete_mail_by_id($mail_id);

        if (!empty($result)) {
            $sdata['message'] = 'Mail remove successfully .';
            $this->session->set_userdata($sdata);
            redirect('admin/mail/inbox', 'refresh');
        } else {
            $sdata['error'] = 'Mail removation failed!';
            $this->session->set_userdata($sdata);
            redirect('admin/mail/inbox', 'refresh');
        }
    }

    public function sent() {
        $data['title'] = 'Sent';
        $data['menu_active'] = 'Sent';
        $data['email_notification'] = $this->others_mdl->get_all_unred_notification();
        $data['user_info'] = $this->pro_mdl->get_user_info();
        $data['sent_mail_info'] = $this->mail_mdl->get_sent_mail();
        $this->load->view('admin_panel/common/header', $data);
        $this->load->view('admin_panel/common/menu_bar', $data);
        $this->load->view('admin_panel/mail/sent', $data);
        $this->load->view('admin_panel/common/footer');
    }

    public function view_sent_mail($mail_id) {
        if (!empty($mail_id)) {
            $data['title'] = 'Mail Details';
            $data['menu_active'] = 'Sent';
            $data['mail_info'] = $this->mail_mdl->get_sent_mail_by_id($mail_id);
            $this->mail_mdl->read_by_id($mail_id);
            $this->load->view('admin_panel/common/header', $data);
            $this->load->view('admin_panel/common/menu_bar');
            $this->load->view('admin_panel/mail/view_sent_mail', $data);
            $this->load->view('admin_panel/common/footer');
        } else {
            redirect('admin/mail', 'refresh');
        }
    }

    public function delete_sent_mail($mail_id) {
        $result = $this->mail_mdl->delete_sent_mail_by_id($mail_id);

        if (!empty($result)) {
            $sdata['message'] = 'Mail remove successfully .';
            $this->session->set_userdata($sdata);
            redirect('admin/mail/sent', 'refresh');
        } else {
            $sdata['error'] = 'Mail removation failed!';
            $this->session->set_userdata($sdata);
            redirect('admin/mail/sent', 'refresh');
        }
    }

    public function sent_general_mail() {
        $mdata = array();
        $mdata['from'] = $this->input->post('from', true);
        $mdata['sender_name'] = $this->input->post('sender_name', true);
        $mdata['to'] = $this->input->post('to', true);
        $mdata['cc'] = $this->input->post('cc', true);
        $mdata['subject'] = $this->input->post('subject', true);
        $mdata['message'] = $this->input->post('message', true);
        $this->mail_mdl->send_email($mdata, 'general_mail');
        /* --------------End Send Contact Email------------ */
        $mdata['user_id'] = $this->session->userdata('user_id');
        $result = $this->mail_mdl->save_sent_email($mdata);
        if (!empty($result)) {
            $sdata['message'] = "Email Has Been Send, Thank You.";
            $this->session->set_userdata($sdata);
            redirect('admin/mail/index', 'refresh');
        }else{
            $sdata['error'] = "Failed, Please try again";
            $this->session->set_userdata($sdata);
            redirect('admin/mail/index', 'refresh');
        }
    }

}
