<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/* * *******************************************#
  #      User Management System                 #
  #*********************************************#
  #      Author:     Atique IT                  #
  #      Email:      info@atique-it.com         #
  #      Website:    http://atique-it.com       #
  #                                             #
  #      Version:    15.2.1                     #
  #      Copyright:  (c) 2015 - Atique IT       #
  #                                             #
  #*********************************************# */

class Access_label extends MSN_Controller {

    public function __construct() {
        parent::__construct();

        $this->admin_login_authentication();
        $this->super_admin_authentication_only();
        $this->load->model('admin_models/Access_Model', 'access_mdl');
        $this->load->model('admin_models/Others_Model', 'others_mdl');
        $this->load->model('admin_models/Profile_Model', 'pro_mdl');
    }

    public function index() {
        $data['title'] = 'Manage Access Label';
        $data['menu_active'] = 'Access Label';
        $data['email_notification'] = $this->others_mdl->get_all_unred_notification();
        $data['user_info'] = $this->pro_mdl->get_user_info();
        $data['access_info'] = $this->access_mdl->get_all_access_lebel();
        $this->load->view('admin_panel/common/header', $data);
        $this->load->view('admin_panel/common/menu_bar');
        $this->load->view('admin_panel/access_label/manage_access_label', $data);
        $this->load->view('admin_panel/common/footer');
    }

    public function update_privilege($access_id) {
        $block_or_suspend = $this->input->post('block_or_suspend', TRUE);
        $edit = $this->input->post('edit', TRUE);
        $delete = $this->input->post('delete', TRUE);
        
        if (empty($block_or_suspend)) {
            $data['block_or_suspend'] = 0;
        } else {
            $data['block_or_suspend'] = 1;
        }

        if (empty($edit)) {
            $data['edit'] = 0;
        } else {
            $data['edit'] = 1;
        }

        if (empty($delete)) {
            $data['delete'] = 0;
        } else {
            $data['delete'] = 1;
        }

        $result = $this->access_mdl->update_privilege_by_id($access_id, $data);

        if (!empty($result)) {
            $sdata['message'] = 'Privilege update successfully .';
            $this->session->set_userdata($sdata);
            redirect('admin/access_label', 'refresh');
        } else {
            $sdata['error'] = 'Privilege updation failed !';
            $this->session->set_userdata($sdata);
            redirect('admin/access_label', 'refresh');
        }
    }

}
