<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/* * *******************************************#
  #      User Management System                 #
  #*********************************************#
  #      Author:     Atique IT                  #
  #      Email:      info@atique-it.com         #
  #      Website:    http://atique-it.com       #
  #                                             #
  #      Version:    15.2.1                     #
  #      Copyright:  (c) 2015 - Atique IT       #
  #                                             #
  #*********************************************# */

class Page extends MSN_Controller {

    public function __construct() {

        parent::__construct();
        $this->load->model('user_models/Page_Model', 'page_mdl');
    }

    public function index() {

        if (!$this->session->userdata('d_is_logged')){
            redirect('user_login');
        }

        $data['title'] = 'Home';
        $data['menu_active'] = 'Home';
        $data['service_info'] = $this->page_mdl->select_all_service_info();
        $this->load->view('user_panel/common/header');
        $this->load->view('user_panel/main_content', $data);
        $this->load->view('user_panel/common/footer');
    }

    public function sign_up() {
        $data['title'] = 'Sign Up';
        $data['menu_active'] = 'Sign Up';
        $this->load->view('user_panel/common/header');
        $this->load->view('user_panel/sign_up');
        $this->load->view('user_panel/common/footer');
    }

    public function user_logout() {

        if ($this->session->userdata('d_is_logged')){

        $this->session->unset_userdata('d_user_id');
        $this->session->unset_userdata('d_full_name');
        $this->session->unset_userdata('d_email_address');
        $this->session->unset_userdata('d_access_label');
        $this->session->unset_userdata('d_is_logged');
//        $this->session->sess_destroy();
        $this->session->set_flashdata('success',"Logout successfully.");
        }else{
            $this->session->set_flashdata('error',"You are already logged out");
        }
        redirect('user_login');

    }

    public function registration() {

        if ($this->input->post()) {

        $config = array(
            array('field' => 'bdata[first_name]', 'label' => 'first name', 'rules' => 'trim|required|max_length[50]'),
            array('field' => 'ldata[user_password]', 'label' => 'password', 'rules' => 'trim|required|min_length[8]|max_length[50]'),
            array('field' => 'ldata[email_address]', 'label' => 'email address', 'rules' => 'trim|required|valid_email|is_unique[tbl_user_login_info.email_address]'),
            array('field' => 're_password', 'label' => 're-password', 'rules' => 'trim|required|matches[ldata[user_password]]'),
            array('field' => 'bdata[contactinfo]', 'label' => 'Dealer phone', 'rules' => 'trim|required'),
            array('field' => 'bdata[companyname]', 'label' => 'company name', 'rules' => 'trim|required|min_length[5]'),
            array('field' => 'bdata[sale_name]', 'label' => 'sale name', 'rules' => 'trim|required|max_length[50]'),
            array('field' => 'o_phone[]', 'label' => 'Office phone', 'rules' => 'trim|required|max_length[25]'),
            array('field' => 'o_address[]', 'label' => 'Office address', 'rules' => 'trim|required|min_length[8]|max_length[150]'),
            array('field' => 'o_state[]', 'label' => 'Office state', 'rules' => 'trim|required|max_length[150]'),
            array('field' => 'o_city[]', 'label' => 'Office city', 'rules' => 'trim|required|max_length[150]'),
            array('field' => 'o_zip[]', 'label' => 'Office zip', 'rules' => 'trim|required|max_length[20]'),
        );

         $this->form_validation->set_rules($config);
        if ($this->form_validation->run() == FALSE) {
            $this->session->set_flashdata('error',validation_errors());
            $this->sign_up();
        } else {

            $this->load->helper('string');

            $ldata = $this->input->post('ldata');
            $ldata['full_name'] = $this->input->post('bdata[first_name]', true);
            $ldata['user_password'] = md5($this->input->post('ldata[user_password]'));
            $ldata['email_address'] = $this->input->post('ldata[email_address]');
            $ldata['access_label'] = 3;
            $ldata['activation_status'] = 0;
            $ldata['activation_key'] = md5(random_string('alnum', 16));

            $data = $this->input->post('bdata');

            $bdata['first_name']= $ldata['full_name'];
            $bdata['companyname']= $data['companyname'];
            $bdata['profile_picture']= $data['sale_name'];
            $bdata['phone']= $data['contactinfo'];
            $data = $this->input->post();
            $bdata['address_1'] = json_encode($data['o_address']);
            $bdata['city'] = json_encode($data['o_city']);
            $bdata['contactinfo'] = json_encode($data['o_phone']);
            $bdata['state'] = json_encode($data['o_state']);
            $bdata['zip'] = json_encode($data['o_zip']);


            $login_info = $this->page_mdl->insert_login_info($ldata);
            $bdata['user_id'] = $login_info;
            $basic_info = $this->page_mdl->insert_basic_info($bdata);

            if (!empty($login_info) AND ! empty($basic_info)) {

                $this->session->set_flashdata('success',"Registration successfull! Admin approval required");
                redirect('page/sign_up');
            } else {
                $this->session->set_flashdata('error',"Registration failed ! Please try again. ");
                redirect('page/sign_up');
            }
        }
        }else{
            $this->session->set_flashdata('error',"Invalid Request");
            redirect('page/sign_up');
        }
    }

    public function verify_email($activation_key) {

        $data = array('activation_key' => $activation_key);

        $result = $this->page_mdl->checkInfo($data, 'tbl_user_login_info');
        if ($result >= 1) {

            $update = array('activation_status' => 1);

            $result = $this->page_mdl->updateInfo($update, $data, 'tbl_user_login_info');
            if ($result) {
                $sdata['message'] = '<h3>Account Verified. Please Log In</h3>';
                $this->session->set_userdata($sdata);
                redirect('page/sign_up', 'refresh');
            } else {

                echo "OPPS!! Sorry something went wrong. Please try again after some time. Thank you.";
            }
        } else {
            exit();
        }
    }

}