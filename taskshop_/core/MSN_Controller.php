<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class MSN_Controller extends CI_Controller {

    function __construct() {
        parent::__construct();

        $this->output->set_header('Last-Modified: ' . gmdate("D, d M Y H:i:s") . ' GMT');
        $this->output->set_header('Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0');
        $this->output->set_header('Pragma: no-cache');
        $this->output->set_header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
    }

    public function admin_login_authentication() {
        if ($this->session->userdata('logged_info') == FALSE) {
            redirect(admin_url('/authentication', 'refresh'));
        }
    }

    public function super_admin_authentication_only() {
        if ($this->session->userdata('access_label') != 1) {
            redirect('admin/dashboard', 'refresh');
        }
    }
    
    public function super_admin_and_admin_authentication_only(){
        if ($this->session->userdata('access_label') > 2) {
            redirect('admin/dashboard', 'refresh');
        }
    }

}
