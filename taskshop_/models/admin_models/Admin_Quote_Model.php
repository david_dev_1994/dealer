<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/* * *******************************************#
  #      User Management System                 #
  #*********************************************#
  #      Author:     Atique IT                  #
  #      Email:      info@atique-it.com         #
  #      Website:    http://atique-it.com       #
  #                                             #
  #      Version:    15.2.1                     #
  #      Copyright:  (c) 2015 - Atique IT       #
  #                                             #
  #*********************************************# */

class Admin_Quote_Model extends MSN_Model {

    public function __construct() {
        parent::__construct();
    }

    private $_quote = 'tbl_quote';
    private $_sub_quote = 'tbl_sub_quote';
    private $_jcat = 'tbl_job_category';

    public function get_all_quote_info() {
        $this->db->select('log_info.full_name, jcat.job_category_name, quote.*')
                ->from('tbl_user_login_info as log_info')
                ->join('tbl_quote as quote', 'log_info.user_id = quote.user_id')
                ->join('tbl_job_category as jcat', 'quote.job_category_id = jcat.job_category_id')
                ->where('quote.deletion_status', 0)
                ->order_by('quote.quote_id', 'desc');
        $query_result = $this->db->get();
        $result = $query_result->result_array();
        return $result;
    }

    public function get_all_quote_info_by_quote_id($quote_id) {
        $this->db->select('log_info.full_name, jcat.job_category_name, sub_quote.*')
                ->from('tbl_user_login_info as log_info')
                ->join('tbl_sub_quote as sub_quote', 'log_info.user_id = sub_quote.user_id')
                ->join('tbl_quote as quote', 'quote.quote_id = sub_quote.quote_id')
                ->join('tbl_job_category as jcat', 'quote.job_category_id = jcat.job_category_id')
                ->where('sub_quote.quote_id', $quote_id)
                ->where('quote.deletion_status', 0)
                ->order_by('quote.quote_id', 'desc');
        $query_result = $this->db->get();
        $result = $query_result->result_array();
        return $result;
    }

    public function get_all_quote_info_by_user_id($user_id) {
        $this->db->select('log_info.full_name, jcat.job_category_name, quote.*')
                ->from('tbl_user_login_info as log_info')
                ->join('tbl_quote as quote', 'log_info.user_id = quote.user_id')
                ->join('tbl_job_category as jcat', 'quote.job_category_id = jcat.job_category_id')
                ->where('quote.user_id', $user_id)
                ->where('quote.deletion_status', 0)
                ->order_by('quote.quote_id', 'desc');
        $query_result = $this->db->get();
        $result = $query_result->result_array();
        return $result;
    }

    public function get_job_category_info() {
        $result = $this->db->order_by('job_category_id', 'desc')->get_where($this->_jcat, array('publication_status' => 1));
        return $result->result_array();
    }

    public function insert_quote_info($data) {
        $this->db->insert($this->_quote, $data);
        return $this->db->insert_id();
    }

    public function insert_sub_quote_info($data) {
        $this->db->insert($this->_sub_quote, $data);
        return $this->db->insert_id();
    }

    public function unpublished_quote_by_id($quote_id) {
        $this->db->update($this->_quote, array('publication_status' => 0), array('quote_id' => $quote_id));
        return $this->db->affected_rows();
    }

    public function published_quote_by_id($quote_id) {
        $this->db->update($this->_quote, array('publication_status' => 1), array('quote_id' => $quote_id));
        return $this->db->affected_rows();
    }

    public function get_quote_by_id($quote_id) {
        $result = $this->db->get_where($this->_quote, array('quote_id' => $quote_id));
        return $result->row_array();
    }

    public function update_quote_by_id($quote_id, $data) {
        $this->db->where('quote_id', $quote_id)->update($this->_quote, $data);
        return $this->db->affected_rows();
    }

    public function delete_quote_by_id($quote_id) {
        $this->db->update($this->_quote, array('deletion_status' => 1), array('quote_id' => $quote_id));
        return $this->db->affected_rows();
    }

    public function get_last_sub_quote_info_by_quote_id($quote_id) {
        $result = $this->db->where('quote_id', $quote_id)->order_by('sub_quote_id', 'desc')->get($this->_sub_quote);
        return $result->row_array();
    }

    public function get_sub_quote_info_by_sub_quote_id($sub_quote_id) {
        $result = $this->db->get_where($this->_sub_quote, array('sub_quote_id' => $sub_quote_id));
        return $result->row_array();
    }
    
    public function user_confirm_sub_quote_by_id($sub_quote_id){
        $this->db->update($this->_sub_quote, array('user_cinfirm' => 1), array('sub_quote_id' => $sub_quote_id));
        return $this->db->affected_rows();
    }
    
    public function admin_confirm_sub_quote_by_id($sub_quote_id){
        $this->db->update($this->_sub_quote, array('admin_confirm' => 1), array('sub_quote_id' => $sub_quote_id));
        return $this->db->affected_rows();
    }

}
