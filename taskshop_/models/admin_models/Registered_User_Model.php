<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/* * *******************************************#
  #      User Management System                 #
  #*********************************************#
  #      Author:     Atique IT                  #
  #      Email:      info@atique-it.com         #
  #      Website:    http://atique-it.com       #
  #                                             #
  #      Version:    15.2.1                     #
  #      Copyright:  (c) 2015 - Atique IT       #
  #                                             #
  #*********************************************# */

class Registered_User_Model extends MSN_Model {

    public function __construct() {
        parent::__construct();
    }

    private $_log_info = 'tbl_user_login_info';
    private $_pro_info = 'tbl_user_profile_info';
    private $_login = 'tbl_user_login_info';
    private $_basic = 'tbl_user_profile_info';


    public function insert_login_info($ldata) {
        $this->db->insert($this->_login, $ldata);
        return $this->db->insert_id();
    }

    public function insert_basic_info($bdata) {
        $this->db->insert($this->_basic, $bdata);
        return $this->db->insert_id();
    }

    public function get_admins_info() {
        $this->db->select('pro_info.*, log_info.user_id as u_id, log_info.full_name, log_info.email_address, log_info.access_label, log_info.activation_status, log_info.deletion_status, log_info.date_added')
                ->from('tbl_user_login_info as log_info')
                ->join('tbl_user_profile_info as pro_info', 'log_info.user_id = pro_info.user_id', 'left')
                ->where('log_info.deletion_status', 0)
                ->where('log_info.access_label', 2)
                ->order_by('log_info.user_id', 'desc');
        $query_result = $this->db->get();
        $result = $query_result->result_array();
        return $result;
    }

    public function get_users_info() {
        $this->db->select('pro_info.*, log_info.user_id as u_id, log_info.full_name, log_info.email_address, log_info.access_label, log_info.activation_status, log_info.deletion_status, log_info.date_added')
                ->from('tbl_user_login_info as log_info')
                ->join('tbl_user_profile_info as pro_info', 'log_info.user_id = pro_info.user_id', 'left')
                ->where('log_info.deletion_status', 0)
                ->where('log_info.access_label', 3)
                ->order_by('log_info.user_id', 'desc');
        $query_result = $this->db->get();
        $result = $query_result->result_array();
        return $result;
    }

    public function block_user_by_user_id($user_id) {
        $this->db->update($this->_log_info, array('activation_status' => 0), array('user_id' => $user_id));
        return $this->db->affected_rows();
    }

    public function unblock_user_by_user_id($user_id) {
        $this->db->update($this->_log_info, array('activation_status' => 1), array('user_id' => $user_id));
        return $this->db->affected_rows();
    }

    public function delete_user_by_user_id($user_id) {
        $this->db->update($this->_log_info, array('deletion_status' => 1), array('user_id' => $user_id));
        return $this->db->affected_rows();
    }

    public function update_access_label_by_user_id($user_id, $data) {
        $this->db->where('user_id', $user_id)->update($this->_log_info, $data);
        return $this->db->affected_rows();
    }

}
