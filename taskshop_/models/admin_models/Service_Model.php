<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/* * *******************************************#
  #      User Management System                 #
  #*********************************************#
  #      Author:     Atique IT                  #
  #      Email:      info@atique-it.com         #
  #      Website:    http://atique-it.com       #
  #                                             #
  #      Version:    15.2.1                     #
  #      Copyright:  (c) 2015 - Atique IT       #
  #                                             #
  #*********************************************# */

class Service_Model extends MSN_Model {

    public function __construct() {
        parent::__construct();
    }

    private $_service = 'tbl_service';

    public function get_all_service_info() {
        $this->db->select('log_info.full_name, serv.*')
                ->from('tbl_user_login_info as log_info')
                ->join('tbl_service as serv', 'log_info.user_id = serv.user_id')
                ->where('serv.deletion_status', 0)
                ->order_by('serv.service_id', 'desc');
        $query_result = $this->db->get();
        $result = $query_result->result_array();
        return $result;
    }

    public function insert_service_info($data) {
        $this->db->insert($this->_service, $data);
        return $this->db->insert_id();
    }

    public function unpublished_service_by_id($service_id) {
        $this->db->update($this->_service, array('publication_status' => 0), array('service_id' => $service_id));
        return $this->db->affected_rows();
    }

    public function published_service_by_id($service_id) {
        $this->db->update($this->_service, array('publication_status' => 1), array('service_id' => $service_id));
        return $this->db->affected_rows();
    }

    public function get_service_location_by_id($service_location_id) {
        $result = $this->db->get_where($this->_service, array('service_location_id' => $service_location_id));
        return $result->row_array();
    }

    public function get_service_type_by_name($service_type) {
        $result = $this->db->get_where($this->_service, array('service_type' => $service_type));
        return $result->row_array();
    }

    public function update_service_location_by_id($service_location_id, $data) {
        $this->db->where('service_location_id', $service_location_id)->update($this->_service, $data);
        return $this->db->affected_rows();
    }

    public function delete_service_by_id($service_id) {
        $this->db->update($this->_service, array('deletion_status' => 1), array('service_id' => $service_id));
        return $this->db->affected_rows();
    }

}
