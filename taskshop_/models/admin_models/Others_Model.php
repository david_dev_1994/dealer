<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/* * *******************************************#
  #      User Management System                 #
  #*********************************************#
  #      Author:     Atique IT                  #
  #      Email:      info@atique-it.com         #
  #      Website:    http://atique-it.com       #
  #                                             #
  #      Version:    15.2.1                     #
  #      Copyright:  (c) 2015 - Atique IT       #
  #                                             #
  #*********************************************# */

class Others_Model extends MSN_Model {

    public function __construct() {
        parent::__construct();
    }

    private $_inbox = 'tbl_mail_inbox';
    private $_general = 'tbl_general_settings';

    public function get_all_unred_notification() {
        $this->db->from('tbl_mail_inbox')
                ->where('reading_status', 0);
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function count_all_admin() {
        $this->db->from('tbl_user_login_info')
                ->where('access_label', 2);
        $query = $this->db->get();
        return $query->num_rows();
    }
    
    public function count_users() {
        $this->db->from('tbl_user_login_info')
                ->where('access_label', 3);
        $query = $this->db->get();
        return $query->num_rows();
    }

}
