<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/* * *******************************************#
  #      User Management System                 #
  #*********************************************#
  #      Author:     Atique IT                  #
  #      Email:      info@atique-it.com         #
  #      Website:    http://atique-it.com       #
  #                                             #
  #      Version:    15.2.1                     #
  #      Copyright:  (c) 2015 - Atique IT       #
  #                                             #
  #*********************************************# */

class Profile_Model extends MSN_Model {

    public function __construct() {
        parent::__construct();
    }

    private $_log_info = 'tbl_user_login_info';
    private $_pro_info = 'tbl_user_profile_info';

    public function get_user_info() {
        $user_id = $this->session->userdata('user_id');

//        $query = "Select * from tbl_user_login_info log_info JOIN tbl_user_profile_info pro_info ON log_info.user_id=pro_info.user_id WHERE log_info.user_id=$user_id AND log_info.activation_status=1 AND log_info.deletion_status =0";

        $this->db->select('*')
                ->from('tbl_user_login_info as log_info')
                ->join('tbl_user_profile_info as pro_info', 'log_info.user_id=pro_info.user_id','LEFT')
                ->where('log_info.user_id', $user_id)
                ->where('log_info.activation_status', 1)
                ->where('log_info.deletion_status', 0);
        $query_result = $this->db->get();
        $result = $query_result->row_array();
        return $result;
    }

    public function update_profile_info($data) {
        $user_id = $this->session->userdata('user_id');

        $this->db->where('user_id', $user_id)->update($this->_pro_info, $data);
        return $this->db->affected_rows();
    }

    public function update_profile_picture_info($picture_name) {
        $user_id = $this->session->userdata('user_id');
        
        $this->db->update($this->_pro_info, array('profile_picture' => $picture_name), array('user_id' => $user_id));
        return $this->db->affected_rows();
    }
    
    public function check_old_password($user_id) {
        $old_password = $this->input->post('old_password', TRUE);

        $result = $this->db->get_where($this->_log_info, array('user_id' => $user_id, 'user_password' => md5($old_password)));
        return $result->row_array();
    }

    public function update_old_password($user_id) {
        $data['user_password'] = md5($this->input->post('new_password', TRUE));

        $this->db->where('user_id', $user_id)->update($this->_log_info, $data);
        return $this->db->affected_rows();
    }

}
