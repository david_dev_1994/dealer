<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/* * *******************************************#
  #      User Management System                 #
  #*********************************************#
  #      Author:     Atique IT                  #
  #      Email:      info@atique-it.com         #
  #      Website:    http://atique-it.com       #
  #                                             #
  #      Version:    15.2.1                     #
  #      Copyright:  (c) 2015 - Atique IT       #
  #                                             #
  #*********************************************# */

class Job_Category_Model extends MSN_Model {

    public function __construct() {
        parent::__construct();
    }

    private $_job_category = 'tbl_job_category';

    public function get_all_job_category_info() {
        $this->db->select('log_info.full_name, job_cat.*')
                ->from('tbl_user_login_info as log_info')
                ->join('tbl_job_category as job_cat', 'log_info.user_id = job_cat.user_id')
                ->where('job_cat.deletion_status', 0)
                ->order_by('job_cat.job_category_id', 'desc');
        $query_result = $this->db->get();
        $result = $query_result->result_array();
        return $result;
    }
    
    public function insert_job_category_info($data) {
        $this->db->insert($this->_job_category, $data);
        return $this->db->insert_id();
    }

    public function unpublished_job_category_by_id($job_category_id) {
        $this->db->update($this->_job_category, array('publication_status' => 0), array('job_category_id' => $job_category_id));
        return $this->db->affected_rows();
    }

    public function published_job_category_by_id($job_category_id) {
        $this->db->update($this->_job_category, array('publication_status' => 1), array('job_category_id' => $job_category_id));
        return $this->db->affected_rows();
    }

    public function get_job_category_by_id($job_category_id) {
        $result = $this->db->get_where($this->_job_category, array('job_category_id' => $job_category_id));
        return $result->row_array();
    }

    public function update_job_category_by_id($job_category_id, $data) {
        $this->db->where('job_category_id', $job_category_id)->update($this->_job_category, $data);
        return $this->db->affected_rows();
    }

    public function delete_job_category_by_id($job_category_id) {
        $this->db->update($this->_job_category, array('deletion_status' => 1), array('job_category_id' => $job_category_id));
        return $this->db->affected_rows();
    }


}
