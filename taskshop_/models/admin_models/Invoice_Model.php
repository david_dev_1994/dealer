<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/* * *******************************************#
  #      User Management System                 #
  #*********************************************#
  #      Author:     Atique IT                  #
  #      Email:      info@atique-it.com         #
  #      Website:    http://atique-it.com       #
  #                                             #
  #      Version:    15.2.1                     #
  #      Copyright:  (c) 2015 - Atique IT       #
  #                                             #
  #*********************************************# */

class Invoice_Model extends MSN_Model {

    public function __construct() {
        parent::__construct();
    }

    private $_sub_quote = 'tbl_sub_quote';

    public function get_invoice_info($order_id){
        $this->db->select('pro_info.*, jcat.job_category_name, sub_quote.*')
                ->from('tbl_user_profile_info as pro_info')
                ->join('tbl_sub_quote as sub_quote', 'pro_info.user_id = sub_quote.user_id')
                ->join('tbl_job_category as jcat', 'sub_quote.job_category_id = jcat.job_category_id')
                ->where('sub_quote.sub_quote_id', $order_id)
                ->where('sub_quote.user_confirm', 1)
                ->where('sub_quote.admin_confirm', 1)
                ->order_by('sub_quote.quote_id', 'desc');
        $query_result = $this->db->get();
        $result = $query_result->row_array();
        return $result;
    }

}
