<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/* * *******************************************#
  #      User Management System                 #
  #*********************************************#
  #      Author:     Atique IT                  #
  #      Email:      info@atique-it.com         #
  #      Website:    http://atique-it.com       #
  #                                             #
  #      Version:    15.2.1                     #
  #      Copyright:  (c) 2015 - Atique IT       #
  #                                             #
  #*********************************************# */

class Access_Model extends MSN_Model {

    public function __construct() {
        parent::__construct();
    }

    private $_access_lebel = 'tbl_access_lebel';

    public function get_all_access_lebel() {
        $result = $this->db->order_by('access_lebel_id', 'asc')->get($this->_access_lebel);
        return $result->result_array();
    }

    public function update_privilege_by_id($access_id, $data) {
        $this->db->where('access_lebel_id', $access_id)->update($this->_access_lebel, $data);
        return $this->db->affected_rows();
    }

}
