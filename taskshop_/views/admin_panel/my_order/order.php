<!--Page main section start-->
<section id="min-wrapper">
    <div id="main-content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <!--Top header start-->
                    <h3 class="ls-top-header">Order</h3>
                    <!--Top header end -->

                    <!--Top breadcrumb start -->
                    <ol class="breadcrumb">
                        <li><a href="#"><i class="fa fa-home"></i></a></li>
                        <li class="active">Order</li>
                    </ol>
                    <!--Top breadcrumb start -->
                </div>
            </div>
            <!-- Main Content Element  Start-->

            <div class="row">
                <div class="col-md-5">
                    <div class="input-group ls-group-input">
                        <span class="input-group-btn">
                            <button type="button" class="btn btn-success">Search</button>
                        </span>
                        <input class="form-control" type="text">
                    </div>
                </div>
                <div class="col-md-7">

                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title">My Order</h3>
                        </div>

                        <div class="panel-body">
                            <!--Table Wrapper Start-->
                            <div class="table-responsive ls-table">
                                <div>
                                    <?php
                                    $msg = $this->session->userdata('message');
                                    $error = $this->session->userdata('error');

                                    if ($msg) {
                                        echo "<div class='alert alert-success text-center'><i class='fa fa-check-circle-o fa-5x'></i>" . $msg . "</div>";
                                        $this->session->unset_userdata('message');
                                    } else if ($error) {
                                        echo "<div class='alert alert-danger text-center'><i class='fa fa-ban fa-5x'></i>" . $error . "</div>";
                                        $this->session->unset_userdata('error');
                                    }
                                    ?>
                                </div>
                                <table class="table table-bordered table-striped">
                                    <thead>
                                        <tr>
                                            <th>SL#</th>
                                            <th>Order Id</th>
                                            <th>Job Title</th>
                                            <th>Category</th>
                                            <th>Deadline (Hr)</th>
                                            <th>Deadline (Day)</th>
                                            <th>Remaining</th>
                                            <th>Date Added</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>

                                        <?php
                                        $i = 1;
                                        foreach ($order_info as $v_order_info) {
                                            ?>
                                            <tr>
                                                <td><?php echo $i++; ?></td>
                                                <td><?php echo $i; ?></td>
                                                <td><?php echo $v_order_info['job_title']; ?></td>
                                                <td><?php echo $v_order_info['job_category_name']; ?></td>
                                                <td><?php echo $v_order_info['one_time_dead_line_hr']; ?></td>
                                                <td><?php echo $v_order_info['dead_line_days']; ?></td>
                                                <td>
                                                    <?php
                                                    $this->load->helper('date');
                                                    $datestring = '%d-%m-%Y %h:%i:%s';
                                                    $time = time();


                                                    $date1 = date("d-m-Y h:i:s", strtotime($v_order_info['date_added']));
                                                    $date2 = mdate($datestring, $time);

                                                    $date3 = date_create($date1);
                                                    $date4 = date_create($date2);

                                                    $diff34 = date_diff($date4, $date3);
                                                    $days = $diff34->d;
                                                    $months = $diff34->m;
                                                    $years = $diff34->y;
                                                    $hours = $diff34->h;
                                                    $minutes = $diff34->i;
                                                    $seconds = $diff34->s;


                                                    if (!empty($v_order_info['dead_line_days'])) {
                                                        echo $v_order_info['dead_line_days'] - $days . " Day(s)";
                                                    }

                                                    if (!empty($v_order_info['one_time_dead_line_hr'])) {
                                                        if ($days == 0) {
                                                            echo $v_order_info['one_time_dead_line_hr'] - $hours . " Hour(s)";
                                                        }else{
                                                            echo "-" . $days . " Day(s)";
                                                        }
                                                    }
                                                    ?>
                                                </td>

                                                <td><?php echo date("D d F Y h:ia", strtotime($v_order_info['date_added'])); ?></td>

                                                <td class="text-center">
                                                    <?php if ($this->session->userdata('access_label') == 1 OR $this->session->userdata('access_label') == 2) { ?>
                                                        <?php if ($v_order_info['payment_status'] == 0) { ?>
                                                            <a href="<?php echo admin_url(); ?>/admin_order/paid_order/<?php echo $v_order_info['sub_quote_id']; ?>" class="btn btn-xs btn-warning" title="Pending"><i class="fa fa-arrow-down"></i></a>
                                                        <?php } else { ?>
                                                            <a href="#" class="btn btn-xs btn-success" title="Paid"><i class="fa fa-arrow-up"></i></a>
                                                        <?php } ?>
                                                    <?php } ?>

                                                    <a href="<?php echo admin_url(); ?>/invoice/make_invoice/<?php echo $v_order_info['sub_quote_id']; ?>" class="btn btn-xs btn-primary">Invoice</a>
                                                    <a href="<?php echo admin_url(); ?>/user_quote/view_details/<?php echo $v_order_info['sub_quote_id']; ?>" class="btn btn-xs btn-primary"><i class="fa fa-eye"></i></a>
                                                </td>
                                            </tr>
                                        <?php } ?>

                                    </tbody>
                                </table>
                            </div>
                            <!--Table Wrapper Finish-->
                            <!--                            <div class="ls-button-group demo-btn ls-table-pagination">
                                                            <ul class="pagination ls-pagination" style="float: right;">
                                                                <li><a href="#"><i class="fa fa-arrow-left"></i> </a></li>
                                                                <li><a href="#">1</a></li>
                                                                <li><a href="#">2</a></li>
                                                                <li><a href="#">3</a></li>
                                                                <li><a href="#">4</a></li>
                                                                <li><a href="#">5</a></li>
                                                                <li><a href="#"><i class="fa fa-arrow-right"></i></a></li>
                                                            </ul>
                                                        </div>-->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!--Page main section end -->