<!--Page main section start-->
<section id="min-wrapper">
    <div id="main-content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <!--Top header start-->
                    <h3 class="ls-top-header">Register Dealers</h3>
                    <!--Top header end -->
                    <!--Top breadcrumb start -->
                    <ol class="breadcrumb">
                        <li><a href="<?= base_url('admin/dashboard');?>"><i class="fa fa-home"></i></a></li>
                        <li class="active">Register User</li>
                    </ol>
                    <!--Top breadcrumb start -->
                </div>
            </div>

            <!-- Main Content Element  Start-->
            <div>
                <?php
                $msg = $this->session->userdata('message');
                $error = $this->session->userdata('error');

                if ($msg) {
                    echo "<div class='alert alert-success'>" . $msg . "</div>";
                    $this->session->unset_userdata('message');
                } else if ($error) {
                    echo "<div class='alert alert-danger'>" . $error . "</div>";
                    $this->session->unset_userdata('error');
                }
                ?>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title">Register User</h3>
                        </div>
                        <div class="row pull-right" style="padding:10px;">
                            <div class="col-md-12">
                                <a class="btn btn-default" href="<?= base_url('admin/add_dealer');?>">Add Dealer</a>
                            </div>
                        </div>

                        <div class="panel-body">
                            <!--Table Wrapper Start-->
                            <div class="table-responsive ls-table">
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th>Full Name</th>
                                            <th>Email</th>
                                            <th>Phone</th>
                                            <th>User Level</th>
                                            <th>Registered Date</th>
                                            <th>Actions</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php foreach ($users_info as $v_user_info) { ?>
                                            <?php if ($v_user_info['u_id'] != $this->session->userdata('user_id')) { ?>
                                                <tr>
                                                    <td><?php echo $v_user_info['full_name']; ?></td>
                                                    <td><?php echo $v_user_info['email_address']; ?></td>
                                                    <td><?php echo $v_user_info['phone']; ?></td>
                                                    <td>
                                                        <?php
                                                        if ($v_user_info['access_label'] == 1) {
                                                            echo "<span class='label label-info'> Super Admin</span>";
                                                        } else if ($v_user_info['access_label'] == 2) {
                                                            echo "<span class='label label-warning'> Admin</span>";
                                                        } else if ($v_user_info['access_label'] == 3) {
                                                            echo "<span class='label label-danger'>Dealer</span>";
                                                        }
                                                        ?>
                                                    </td>
                                                    <td><?php echo date("D d F Y h:ia", strtotime($v_user_info['date_added'])); ?></td>
                                                    <td>

                                                        <?php if ($v_user_info['activation_status'] == 1) { ?>
                                                            <a href="<?php echo admin_url(); ?>/registered_user/block_user/<?php echo $v_user_info['u_id']; ?>" id="block" class="btn btn-xs btn-warning"  title="Block">
                                                                </span><i class="fa fa-lock"></i>
                                                            </a>
                                                        <?php } else { ?>
                                                            <a href="<?php echo admin_url(); ?>/registered_user/unblock_user/<?php echo $v_user_info['u_id']; ?>" id="unblock" class="btn btn-xs btn-success"  title="Unblock">
                                                                </span><i class="fa fa-unlock"></i>
                                                            </a>
                                                        <?php } ?>


<!--                                                        <a id="edit" class="btn btn-xs btn-info" data-toggle="modal" data-target="#myModal--><?php //echo $v_user_info['u_id']; ?><!--" title="Edit">-->
<!--                                                            <i class="fa fa-pencil-square-o"></i>-->
<!--                                                        </a>-->

                                                        <a href="<?php echo admin_url(); ?>/registered_user/delete_user/<?php echo $v_user_info['u_id']; ?>" id="edit" class="btn btn-xs btn-danger" data-toggle="tooltip" data-placement="top" title="Delete"  onclick="return confirm('Are you sure to delete this ?');">
                                                            </span><i class="fa fa-trash-o"></i>
                                                        </a>
                                                    </td>
                                                </tr>
                                            <?php } ?>
                                        <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                            <!--Table Wrapper Finish-->
                        </div>
                    </div>
                </div>
            </div>


            <!-- Main Content Element  End-->

        </div>
    </div>



</section>
<!--Page main section end -->

<?php foreach ($users_info as $v_user_info) { ?>
<?php if ($v_user_info['u_id'] != $this->session->userdata('user_id')) { ?>
<!-- Modal -->
<div class="modal fade" id="myModal<?php echo $v_user_info['u_id']; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Privilege</h4>
            </div>
            <form class="form-2" method="post" action="<?php echo admin_url(); ?>/registered_user/update_access_label/<?php echo $v_user_info['u_id']; ?>" data-toggle="validator" role="form">
                <div class="modal-body">
                    <div class="radio">
                        <label>
                            <input type="radio" name="access_label" id="optionsRadios1" value="1" <?php if($v_user_info['access_label'] == 1){echo "checked='checked'"; }?> >
                            Super Admin
                        </label>
                    </div>
                    <div class="radio">
                        <label>
                            <input type="radio" name="access_label" id="optionsRadios2" value="2" <?php if($v_user_info['access_label'] == 2){echo "checked='checked'"; }?>>
                            Admin
                        </label>
                    </div>
                    <div class="radio">
                        <label>
                            <input type="radio" name="access_label" id="optionsRadios3" value="3" <?php if($v_user_info['access_label'] == 3){echo "checked='checked'"; }?>>
                            User
                        </label>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Update</button>
                </div>
            </form>
        </div>
    </div>
</div>
<?php } ?>
<?php } ?>