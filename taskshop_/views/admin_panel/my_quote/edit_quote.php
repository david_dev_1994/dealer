<style type="text/css">
    .errorMesage{
        color: red;
    }
</style>
<!--Page main section start-->
<section id="min-wrapper">
    <div id="main-content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <!--Top header start-->
                    <h3 class="ls-top-header">Edit Order</h3>
                    <!--Top header end -->

                    <!--Top breadcrumb start -->
                    <ol class="breadcrumb">
                        <li><a href="#"><i class="fa fa-home"></i></a></li>
                        <li class="active">Edit Order</li>
                    </ol>
                    <!--Top breadcrumb start -->
                </div>
            </div>
            <!-- Main Content Element  Start-->

            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title">Edit Order</h3>
                        </div>
                        <div class="panel-body">
                            <div>
                                <?php
                                $msg = $this->session->userdata('message');
                                $error = $this->session->userdata('error');

                                if ($msg) {
                                    echo "<div class='alert alert-success text-center'><i class='fa fa-check-circle-o fa-5x'></i>" . $msg . "</div>";
                                    $this->session->unset_userdata('message');
                                } else if ($error) {
                                    echo "<div class='alert alert-danger text-center'><i class='fa fa-ban fa-5x'></i>" . $error . "</div>";
                                    $this->session->unset_userdata('error');
                                }
                                ?>
                            </div>
                            <form name="edit_job_quote_form" id="defaultForm" method="post" class="form-horizontal ls_form" action="<?php echo admin_url(); ?>/admin_quote/update_quote.html">

                                <div class="form-group">
                                    <label class="col-lg-3 control-label">Job Title</label>
                                    <div class="col-lg-6">
                                        <input type="text" class="form-control" name="data[job_title]" placeholder="Edit job title" value="<?php echo $quote_info['job_title']; ?>" />
                                        <input type="hidden" name="quote_id" value="<?php echo $quote_info['quote_id']; ?>">
                                        <span class="errorMesage"><?php echo form_error('data[job_title]'); ?></span>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="job_category_id" class="col-lg-3 control-label">Job Category</label>
                                    <div class="col-lg-3">
                                        <select class="form-control" name="data[job_category_id]" id="job_category_id">
                                            <option selected="selected" disabled="">Select One</option>
                                            <?php foreach ($job_category_info as $v_job_category_info) { ?>
                                                <option value="<?php echo $v_job_category_info['job_category_id']; ?>"><?php echo $v_job_category_info['job_category_name']; ?></option>
                                            <?php } ?>
                                        </select>
                                        <span class="errorMesage"><?php echo form_error('data[job_category_id]'); ?></span>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="return_file_type" class="col-lg-3 control-label">Return File Type</label>
                                    <div class="col-lg-3">
                                        <select class="form-control" name="data[return_file_type]" id="return_file_type">
                                            <option selected="selected" disabled="">Select One</option>
                                            <option value="jpg">JPG</option>
                                            <option value="psd">PSD</option>
                                            <option value="tif">TIF</option>
                                            <option value="png">PNG</option>
                                            <option value="ai">AI</option>
                                            <option value="eps">EPS</option>
                                            <option value="pdf">PDF</option>
                                            <option value="other">OTHER</option>
                                        </select>
                                        <span class="errorMesage"><?php echo form_error('data[return_file_type]'); ?></span>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="need_path" class="col-lg-3 control-label">Do you need path ?</label>
                                    <div class="col-lg-3">
                                        <select class="form-control" name="data[need_path]" id="need_path">
                                            <option selected="selected" disabled="">Select One</option>
                                            <option value="0">No need Path</option>
                                            <option value="1">Yes Clipping Path active</option>
                                            <option value="2">Yes Clipping Path inactive</option>
                                            <option value="3">Don't know please advice</option>
                                        </select>
                                        <span class="errorMesage"><?php echo form_error('data[need_path]'); ?></span>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-lg-3 control-label">Number of Image</label>
                                    <div class="col-lg-6">
                                        <input type="number" class="form-control" name="data[number_of_image]" placeholder="Edit number of images" value="<?php echo $quote_info['number_of_image']; ?>" />
                                        <span class="errorMesage"><?php echo form_error('data[number_of_image]'); ?></span>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-lg-3 control-label">
                                        Instruction Message
                                    </label>
                                    <div class="col-lg-6">
                                        <textarea name="data[instruction_message]" class="summernote" placeholder="Instruction Message"><?php echo $quote_info['instruction_message']; ?></textarea>
                                        <span class="errorMesage"><?php echo form_error('data[instruction_message]'); ?></span>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="publication_status" class="col-lg-3 control-label">Publication Status</label>
                                    <div class="col-lg-3">
                                        <select class="form-control" name="data[publication_status]" id="publication_status">
                                            <option selected="selected" disabled="">Select One</option>
                                            <option value="1">Published</option>
                                            <option value="0">Unpublished</option>
                                        </select>
                                        <span class="errorMesage"><?php echo form_error('data[publication_status]'); ?></span>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-lg-9 col-lg-offset-3">
                                        <button type="submit" class="btn btn-primary">Update Order</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<script type="text/javascript">
    document.forms['edit_job_quote_form'].elements['publication_status'].value = '<?php echo $quote_info['publication_status']; ?>';
    document.forms['edit_job_quote_form'].elements['need_path'].value = '<?php echo $quote_info['need_path']; ?>';
    document.forms['edit_job_quote_form'].elements['return_file_type'].value = '<?php echo $quote_info['return_file_type']; ?>';
    document.forms['edit_job_quote_form'].elements['job_category_id'].value = '<?php echo $quote_info['job_category_id']; ?>';
</script>