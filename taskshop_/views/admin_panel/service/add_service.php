<style type="text/css">
    .errorMesage{
        color: red;
    }
</style>
<!--Page main section start-->
<section id="min-wrapper">
    <div id="main-content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <!--Top header start-->
                    <h3 class="ls-top-header">Add Service</h3>
                    <!--Top header end -->

                    <!--Top breadcrumb start -->
                    <ol class="breadcrumb">
                        <li><a href="#"><i class="fa fa-home"></i></a></li>
                        <li class="active">Add Service</li>
                    </ol>
                    <!--Top breadcrumb start -->
                </div>
            </div>
            <!-- Main Content Element  Start-->

            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title">Add Service</h3>
                        </div>
                        <div class="panel-body">
                            <div>
                                    <?php
                                    $msg = $this->session->userdata('message');
                                    $error = $this->session->userdata('error');

                                    if ($msg) {
                                        echo "<div class='alert alert-success'>" . $msg . "</div>";
                                        $this->session->unset_userdata('message');
                                    } else if ($error) {
                                        echo "<div class='alert alert-danger'>" . $error . "</div>";
                                        $this->session->unset_userdata('error');
                                    }
                                    ?>
                                </div>
                            <form name="add_service_form" id="defaultForm" method="post" class="form-horizontal ls_form" action="<?php echo admin_url(); ?>/service/save_service.html" enctype="multipart/form-data">

                                <div class="form-group">
                                    <label class="col-lg-3 control-label">Service Name</label>
                                    <div class="col-lg-7">
                                        <input type="text" class="form-control" name="data[service_name]" placeholder="Add Service Name" value="<?php echo set_value('data[service_name]'); ?>" />
                                        <span class="errorMesage"><?php echo form_error('data[service_name]'); ?></span>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-lg-3 control-label">Product Image</label>
                                    <div class="col-lg-3">
                                        <input type="file" name="image_one" class="form-control" id="image_one">
                                    </div>
                                    <label class="col-lg-1 control-label">After</label>
                                    <div class="col-lg-3">
                                        <input type="file" name="image_two" class="form-control" id="image_one">
                                    </div>
                                </div>
                                
                                <div class="form-group">
                                    <label class="col-lg-3 control-label">Service 1 Image</label>
                                    <div class="col-lg-3">
                                        <input type="file" name="image_three" class="form-control" id="image_one">
                                    </div>
                                    <label class="col-lg-1 control-label">After</label>
                                    <div class="col-lg-3">
                                        <input type="file" name="image_four" class="form-control" id="image_one">
                                    </div>
                                </div>
                                
                                <div class="form-group">
                                    <label class="col-lg-3 control-label">Service 2 Image</label>
                                    <div class="col-lg-3">
                                        <input type="file" name="image_five" class="form-control" id="image_one">
                                    </div>
                                    <label class="col-lg-1 control-label">After</label>
                                    <div class="col-lg-3">
                                        <input type="file" name="image_six" class="form-control" id="image_one">
                                    </div>
                                </div>

                                
                                
                                <div class="form-group">
                                    <label class="col-lg-3 control-label">Short Description</label>
                                    <div class="col-lg-7">
                                        <textarea name="data[short_description]" class="summernote" placeholder="Write message"> <?php echo set_value('data[short_description]'); ?></textarea>
                                        <span class="errorMesage"><?php echo form_error('data[short_description]'); ?></span>
                                    </div>
                                </div>
                                
                                <div class="form-group">
                                    <label class="col-lg-3 control-label">Description</label>
                                    <div class="col-lg-7">
                                        <textarea name="data[description]" class="summernote" placeholder="Write message"> <?php echo set_value('data[description]'); ?></textarea>
                                        <span class="errorMesage"><?php echo form_error('data[description]'); ?></span>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-lg-3 control-label">Service Price</label>
                                    <div class="col-lg-7">
                                        <input type="text" class="form-control" name="data[service_price]" placeholder="Service Price" value="<?php echo set_value('data[service_price]'); ?>" />
                                        <span class="errorMesage"><?php echo form_error('data[service_price]'); ?></span>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="publication_status" class="col-lg-3 control-label">Publication Status</label>
                                    <div class="col-lg-3">
                                        <select class="form-control" name="data[publication_status]" id="publication_status">
                                            <option selected="selected" disabled="">Select One</option>
                                            <option value="1">Published</option>
                                            <option value="0">Unpublished</option>
                                        </select>
                                        <span class="errorMesage"><?php echo form_error('data[publication_status]'); ?></span>
                                    </div>
                                </div>


                                <div class="form-group">
                                    <div class="col-lg-9 col-lg-offset-3">
                                        <button type="submit" class="btn btn-primary">Add Service</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<script type="text/javascript">
<?php if (set_value('data[publication_status]') != null) { ?>
        document.forms['add_service_form'].elements['publication_status'].value = '<?php echo set_value('data[publication_status]'); ?>';
<?php } ?>
</script>