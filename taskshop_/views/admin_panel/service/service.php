<!--Page main section start-->
<section id="min-wrapper">
    <div id="main-content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <!--Top header start-->
                    <h3 class="ls-top-header">Service</h3>
                    <!--Top header end -->

                    <!--Top breadcrumb start -->
                    <ol class="breadcrumb">
                        <li><a href="#"><i class="fa fa-home"></i></a></li>
                        <li class="active">Service</li>
                    </ol>
                    <!--Top breadcrumb start -->
                </div>
            </div>
            <!-- Main Content Element  Start-->

            <div class="row">
                <div class="col-md-5">
                    <div class="input-group ls-group-input">
                        <span class="input-group-btn">
                            <button type="button" class="btn btn-success">Search</button>
                        </span>
                        <input class="form-control" type="text">
                    </div>
                </div>
                <div class="col-md-7">
                    <!-- Button trigger modal -->
                    <a href="<?php echo admin_url(); ?>/service/add_service.html" class="btn btn-success" style="position: absolute;right:20px">
                        <i class="fa fa-plus"></i> Add Service
                    </a>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title">Service</h3>
                        </div>

                        <div class="panel-body">
                            <!--Table Wrapper Start-->
                            <div class="table-responsive ls-table">
                                <div>
                                    <?php
                                    $msg = $this->session->userdata('message');
                                    $error = $this->session->userdata('error');

                                    if ($msg) {
                                        echo "<div class='alert alert-success'>" . $msg . "</div>";
                                        $this->session->unset_userdata('message');
                                    } else if ($error) {
                                        echo "<div class='alert alert-danger'>" . $error . "</div>";
                                        $this->session->unset_userdata('error');
                                    }
                                    ?>
                                </div>
                                <table class="table table-bordered table-striped">
                                    <thead>
                                        <tr>
                                            <th>SL#</th>
                                            <th>Service Name</th>
                                            <th>Product Photo</th>
                                            <th>Service Photo 1</th>
                                            <th>Service Photo 2</th>
                                            <th>Price</th>
<!--                                            <th>Description</th>-->
                                            <th>Added By</th>
                                            <th>Date Added</th>
                                            <th>Status</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>

                                        <?php
                                        $i = 1;
                                        foreach ($service_info as $v_service_info) {
                                            ?>
                                            <tr>
                                                <td><?php echo $i++; ?></td>
                                                <td><?php echo $v_service_info['service_name']; ?></td>
                                                <td>
                                                    <?php if (!empty($v_service_info['product_photo_before'])) { ?>
                                                        <img src="<?php echo base_url() . "uploaded_files/service_pictures/" . $v_service_info['product_photo_before']; ?>" alt="<?php echo $v_service_info['product_photo_before']; ?>" class="img-responsive" width="80px">
                                                    <?php } ?>
                                                </td>
                                                <td>
                                                    <?php if (!empty($v_service_info['service_one_photo_before'])) { ?>
                                                        <img src="<?php echo base_url() . "uploaded_files/service_pictures/" . $v_service_info['service_one_photo_before']; ?>" alt="<?php echo $v_service_info['service_one_photo_before']; ?>" class="img-responsive" width="80px">
                                                    <?php } ?>
                                                </td>
                                                <td>
                                                    <?php if (!empty($v_service_info['service_two_photo_before'])) { ?>
                                                        <img src="<?php echo base_url() . "uploaded_files/service_pictures/" . $v_service_info['service_two_photo_before']; ?>" alt="<?php echo $v_service_info['service_two_photo_before']; ?>" class="img-responsive" width="80px">
                                                    <?php } ?>
                                                </td>
                                                <td><?php echo $v_service_info['service_price']; ?></td>
    <!--                                                <td><?php //echo $v_service_info['short_description'];  ?></td>-->
                                                <td><?php echo $v_service_info['full_name']; ?></td>
                                                <td><?php echo date("D d F Y", strtotime($v_service_info['date_added'])); ?></td>
                                                <td>
                                                    <?php if ($v_service_info['publication_status'] == 1) { ?>
                                                        <span class="label-success label label-default">Published</span>
                                                    <?php } else { ?>
                                                        <span class="label-warning label label-default">Unpublished</span>
                                                    <?php } ?>
                                                </td>
                                                <td class="text-center">
                                                    <?php if ($v_service_info['publication_status'] == 1) { ?>
                                                        <a href="<?php echo admin_url(); ?>/service/unpublished_service/<?php echo $v_service_info['service_id']; ?>" class="btn btn-xs btn-warning"><i class="fa fa-arrow-down"></i></a>
                                                    <?php } else { ?>
                                                        <a href="<?php echo admin_url(); ?>/service/published_service/<?php echo $v_service_info['service_id']; ?>" class="btn btn-xs btn-success"><i class="fa fa-arrow-up"></i></a>
                                                    <?php } ?>
<!--                                                    <a href="<?php //echo admin_url(); ?>/service/edit_service/<?php //echo $v_service_info['service_id']; ?>" class="btn btn-xs btn-primary"><i class="fa fa-pencil-square-o"></i></a>-->
                                                    <a href="<?php echo admin_url(); ?>/service/delete_service/<?php echo $v_service_info['service_id']; ?>" class="btn btn-xs btn-danger" onclick="return confirm('Are you sure to delete this ?');"><i class="fa fa-trash-o"></i></a>
                                                </td>
                                            </tr>
                                        <?php } ?>

                                    </tbody>
                                </table>
                            </div>
                            <!--Table Wrapper Finish-->
                            <!--                            <div class="ls-button-group demo-btn ls-table-pagination">
                                                            <ul class="pagination ls-pagination" style="float: right;">
                                                                <li><a href="#"><i class="fa fa-arrow-left"></i> </a></li>
                                                                <li><a href="#">1</a></li>
                                                                <li><a href="#">2</a></li>
                                                                <li><a href="#">3</a></li>
                                                                <li><a href="#">4</a></li>
                                                                <li><a href="#">5</a></li>
                                                                <li><a href="#"><i class="fa fa-arrow-right"></i></a></li>
                                                            </ul>
                                                        </div>-->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!--Page main section end -->