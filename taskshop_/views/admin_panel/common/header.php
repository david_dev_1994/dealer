<!DOCTYPE html>
<html lang="en" xmlns="http://www.w3.org/1999/html">

    <!-- Mirrored from fickle.aimmate.com/compose-mail.html by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 07 Feb 2016 13:32:06 GMT -->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="description" content="">
        <meta name="author" content="">

        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

        <!-- Viewport metatags -->
        <meta name="HandheldFriendly" content="true" />
        <meta name="MobileOptimized" content="320" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />

        <!-- iOS webapp metatags -->
        <meta name="apple-mobile-web-app-capable" content="yes" />
        <meta name="apple-mobile-web-app-status-bar-style" content="black" />

        <!-- iOS webapp icons -->
        <link rel="apple-touch-icon-precomposed" href="assets/images/ios/fickle-logo-72.png" />
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="assets/images/ios/fickle-logo-72.png" />
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="assets/images/ios/fickle-logo-114.png" />

        <!-- TODO: Add a favicon -->
        <link rel="shortcut icon" href="assets/images/ico/fab.ico">

        <title>Atique-IT - Compose Mail</title>

        <!--Page loading plugin Start -->
        <link rel="stylesheet" href="<?php echo base_url(); ?>admin_assets/css/plugins/pace.css">
        <script src="<?php echo base_url(); ?>admin_assets/js/pace.min.js"></script>
        <!--Page loading plugin End   -->

        <!-- Plugin Css Put Here -->
        <link href="<?php echo base_url(); ?>admin_assets/css/bootstrap.min.css" rel="stylesheet">
        <link rel="stylesheet" href="<?php echo base_url(); ?>admin_assets/css/plugins/popup form/style.css">
        
        <link rel="stylesheet" href="<?php echo base_url(); ?>admin_assets/css/plugins/lightGallery.css">
        <!--[if gt IE 8]> <link href="assets/css/ie/ie9-gallery.css" rel="stylesheet" type="text/css"> <!--<![endif]-->

        <link href="<?php echo base_url(); ?>admin_assets/css/plugins/shuffle.css" rel="stylesheet">
        <!-- Plugin Css End -->
        <link rel="stylesheet" href="<?php echo base_url(); ?>admin_assets/css/plugins/custom_smart_wizard.css">


        <link rel="stylesheet" href="<?php echo base_url(); ?>admin_assets/css/plugins/fileinput.min.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>admin_assets/css/plugins/selectize.bootstrap3.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>admin_assets/css/plugins/summernote.css">

        <!-- Plugin Css End -->
        <!-- Custom styles Style -->
        <link href="<?php echo base_url(); ?>admin_assets/css/style.css" rel="stylesheet">
        <!-- Custom styles Style End-->

        <!-- Responsive Style For-->
        <link href="<?php echo base_url(); ?>admin_assets/css/responsive.css" rel="stylesheet">
        <!-- Responsive Style For-->

        <!-- Custom styles for this template -->

        <!-- Custom styles for this template -->
        <script src="<?php echo base_url(); ?>admin_assets/js/lib/modernizr.js"></script>

        <?php if($menu_active == "File Manager"){ ?>
        <!-- For Image Uploader -->
        <!-- Google web fonts -->
        <link href="http://fonts.googleapis.com/css?family=PT+Sans+Narrow:400,700" rel='stylesheet' />
        <!-- The main CSS file -->
        <link href="<?php echo base_url(); ?>admin_assets/uploader_assets/css/style.css" rel="stylesheet" />
        <link href="<?php echo base_url(); ?>admin_assets/css/custom.css" rel="stylesheet" />
        <!-- End Image Uploader -->
        <?php } ?>

        <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
        <style type="text/css">
            .errorMessage{
                color:red;
            }
        </style>
    </head>
    <body class="">
        <!--Navigation Top Bar Start-->
        <nav class="navigation">
            <div class="container-fluid">
                <!--Logo text start-->
                <div class="header-logo">
                    <a href="<?= base_url('/cms');?>" title="">
                        <h1>Admin Portal</h1>
                    </a>
                </div>
                <!--Logo text End-->
                <div class="top-navigation">
                    <!--Collapse navigation menu icon start -->
                    <div class="menu-control hidden-xs">
                        <a href="javascript:void(0)">
                            <i class="fa fa-bars"></i>
                        </a>
                    </div>
                    <div class="search-box">
                        <ul>
                            <li class="dropdown">
                                <a class="dropdown-toggle" data-toggle="dropdown" href="javascript:void(0)">
                                    <span class="fa fa-search"></span>
                                </a>
                                <div class="dropdown-menu  top-dropDown-1">
                                    <h4>Search</h4>
                                    <form>
                                        <input type="search" placeholder="what you want to see ?">
                                    </form>
                                </div>

                            </li>
                        </ul>
                    </div>

                    <!--Collapse navigation menu icon end -->
                    <!--Top Navigation Start-->

                    <ul>
                        <?php if ($this->session->userdata('access_label') == 1 || $this->session->userdata('access_label') == 2) { ?>
                            <li class="dropdown">
                                <!--Email drop down start-->
                                <a href="<?php echo admin_url(); ?>/mail/inbox.html">
                                    <span class="fa fa-envelope-o"></span>
                                    <?php if (!empty($email_notification)) { ?>
                                        <span class="badge badge-red">
                                            <?php echo $email_notification; ?>
                                        </span>
                                    <?php } ?>
                                </a>
                                <!--Email drop down end-->
                            </li>
                        <?php } ?>

                        <li class="dropdown">
                            <!--Email drop down start-->
                            <a class="dropdown-toggle" data-toggle="dropdown" href="javascript:void(0)">
                                <span class="fa fa-home"> <?php echo $this->session->userdata('full_name'); ?></span>
                            </a>

                            <div class="dropdown-menu right email-notification">
                                <ul class="email-top">
                                    <li>
                                        <a href="<?php echo admin_url(); ?>/administrator.html">
                                            <div class="email-top-content">
                                                <h6>Dashboard</h6>
                                            </div>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="<?php echo base_url(); ?>" target="_blank">
                                            <div class="email-top-content">
                                                <h6>Visit Site</h6>
                                            </div>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="<?php echo admin_url(); ?>/profile.html">
                                            <div class="email-top-content">
                                                <h6>Profile</h6>
                                            </div>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="<?php echo admin_url(); ?>/logout.html">
                                            <div class="email-top-content">
                                                <h6>Logout</h6>
                                            </div>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                            <!--Email drop down end-->
                        </li>

                    </ul>
                    <!--Top Navigation End-->
                </div>
            </div>
        </nav>
        <!--Navigation Top Bar End-->