<section id="main-container">

    <!--Left navigation section start-->
    <section id="left-navigation">
        <!--Left navigation user details start-->
        <div class="user-image">
            <?php if(!empty($user_info['profile_picture'])){ ?>
            <img src="<?php echo base_url(); ?>/uploaded_files/profile_pictures/thumb/<?php echo $user_info['profile_picture']; ?>" alt="" style="width: 100%; height: 80px;"/>
            <?php } else{ ?>
            <img src="<?php echo base_url(); ?>admin_assets/images/userimage/avatar2-80.png" alt=""/>
            <?php } ?>
            <div class="user-online-status"><span class="user-status is-online  "></span> </div>
        </div>
        <ul class="social-icon">
            <?php if(!empty($user_info['facebook'])){ ?>
            <li><a href="<?php echo $user_info['facebook']; ?>"><i class="fa fa-facebook"></i></a></li>
            <?php } ?>
            
            <?php if(!empty($user_info['twitter'])){ ?>
            <li><a href="<?php echo $user_info['twitter']; ?>"><i class="fa fa-twitter"></i></a></li>
            <?php } ?>
            
            <?php if(!empty($user_info['google_plus'])){ ?>
            <li><a href="<?php echo $user_info['google_plus']; ?>"><i class="fa fa-github"></i></a></li>
            <?php } ?>
            
            <?php if(!empty($user_info['linkedin'])){ ?>
            <li><a href="<?php echo $user_info['linkedin']; ?>"><i class="fa fa-bitbucket"></i></a></li>
            <?php } ?>
            
        </ul>
        <!--Left navigation user details end-->

        <!--Phone Navigation Menu icon start-->
        <div class="phone-nav-box visible-xs">
            <a class="phone-logo" href="index.html" title="">
                <h1>Dealer's Portal</h1>
            </a>
            <a class="phone-nav-control" href="javascript:void(0)">
                <span class="fa fa-bars"></span>
            </a>
            <div class="clearfix"></div>
        </div>
        <!--Phone Navigation Menu icon start-->

        <!--Left navigation start-->
        <ul class="mainNav">
            <li class="active">
                <a <?php if($menu_active == 'Dashboard'){echo "class='active'"; }?> href="<?php echo admin_url(); ?>/dashboard.html">
                    <i class="fa fa-home"></i> <span>Dashboard</span>
                </a>
            </li>
            
            <?php if($this->session->userdata('access_label') == 1){ ?>
<!--            <li>-->
<!--                <a --><?php //if($menu_active == 'Access Label'){echo "class='active'"; }?><!-- href="--><?php //echo admin_url(); ?><!--/access_label.html">-->
<!--                    <i class="fa fa-dashboard"></i> <span>Level</span>-->
<!--                </a>-->
<!--            </li>-->
            <?php } ?>
            
            <?php if($this->session->userdata('access_label') == 1 || $this->session->userdata('access_label') == 2){ ?>
<!--            <li>-->
<!--                <a href="#">-->
<!--                    <i class="fa fa-gear"></i> <span>Task shop</span>-->
<!--                </a>-->
<!--                <ul>-->
<!--                    <li><a --><?php //if($menu_active == 'Job Category'){echo "class='active'"; }?><!-- href="--><?php //echo admin_url(); ?><!--/job_category.html">Job Category</a></li>-->
<!--                    <li><a --><?php //if($menu_active == 'My Quote'){echo "class='active'"; }?><!-- href="--><?php //echo admin_url(); ?><!--/admin_quote.html">All Quote</a></li>-->
<!--                    <li><a --><?php //if($menu_active == 'My Order'){echo "class='active'"; }?><!-- href="--><?php //echo admin_url(); ?><!--/admin_order.html">All Order</a></li>-->
<!--                    <li><a --><?php //if($menu_active == 'Testimonial'){echo "class='active'"; }?><!-- href="--><?php //echo admin_url(); ?><!--/testimonial.html">Testimonial</a></li>-->
<!--                    <li><a --><?php //if($menu_active == 'FTP'){echo "class='active'"; }?><!-- href="--><?php //echo admin_url(); ?><!--/ftp_file_manager.html">FTP Setting</a></li>-->
<!--                    <li><a --><?php //if($menu_active == 'FTP'){echo "class='active'"; }?><!-- href="--><?php //echo admin_url(); ?><!--/ftp_file_manager/add_file.html">FTP File Upload</a></li>-->
<!--                    <li><a --><?php //if($menu_active == 'Service'){echo "class='active'"; }?><!-- href="--><?php //echo admin_url(); ?><!--/service.html">Content Management</a></li>-->
<!--                </ul>-->
<!--            </li>-->
            <?php } ?>
            
            <?php if($this->session->userdata('access_label') == 3){ ?>
<!--            <li>-->
<!--                <a href="#">-->
<!--                    <i class="fa fa-gear"></i> <span>Task shop</span>-->
<!--                </a>-->
<!--                <ul>-->
<!--                    <li><a --><?php //if($menu_active == 'My Quote'){echo "class='active'"; }?><!-- href="--><?php //echo admin_url(); ?><!--/user_quote.html">My Quote</a></li>-->
<!--                    <li><a --><?php //if($menu_active == 'My Order'){echo "class='active'"; }?><!-- href="--><?php //echo admin_url(); ?><!--/user_order.html">My Order</a></li>-->
<!--                    <li><a --><?php //if($menu_active == 'Add Testimonial'){echo "class='active'"; }?><!-- href="--><?php //echo admin_url(); ?><!--/testimonial/add_testimonial.html">Add Testimonial</a></li>-->
<!--                    <li><a --><?php //if($menu_active == 'File Manager'){echo "class='active'"; }?><!-- href="--><?php //echo admin_url(); ?><!--/file_manager.html">File Manager</a></li>-->
<!--                </ul>-->
<!--            </li>-->
            <?php } ?>
            
            <?php if($this->session->userdata('access_label') == 1 || $this->session->userdata('access_label') == 2){ ?>
<!--            <li>-->
<!--                <a href="#">-->
<!--                    <i class="fa fa-envelope-o"></i> <span>Email</span>-->
<!--                    --><?php //if (!empty($email_notification)) { ?>
<!--                        <span class="badge badge-red">--><?php //echo $email_notification; ?><!--</span>-->
<!--                    --><?php //} ?>
<!--                </a>-->
<!--                <ul>-->
<!--                    <li><a --><?php //if($menu_active == 'Compose Mail'){echo "class='active'"; }?><!-- href="--><?php //echo admin_url(); ?><!--/mail.html">Compose Mail</a></li>-->
<!--                    <li><a --><?php //if($menu_active == 'Inbox'){echo "class='active'"; }?><!-- href="--><?php //echo admin_url(); ?><!--/mail/inbox.html">Inbox</a></li>-->
<!--                    <li><a --><?php //if($menu_active == 'Sent'){echo "class='active'"; }?><!-- href="--><?php //echo admin_url(); ?><!--/mail/sent.html">Sent Mail</a></li>-->
<!--                </ul>-->
<!--            </li>-->
            <?php } ?>
            
            
            <?php if($this->session->userdata('access_label') == 1 || $this->session->userdata('access_label') == 2){ ?>
<!--            <li>-->
<!--                <a href="#">-->
<!--                    <i class="fa fa-gear"></i> <span>Setting</span>-->
<!--                </a>-->
<!--                <ul>-->
<!--                    <li><a --><?php //if($menu_active == 'General Setting'){echo "class='active'"; }?><!-- href="--><?php //echo admin_url(); ?><!--/setting.html">Genaral</a></li>-->
<!--                    <li><a --><?php //if($menu_active == 'Mail Setting'){echo "class='active'"; }?><!-- href="--><?php //echo admin_url(); ?><!--/setting/mail_setting.html">Mail Setting</a></li>-->
<!--                    <li><a --><?php //if($menu_active == 'Privacy'){echo "class='active'"; }?><!-- href="--><?php //echo admin_url(); ?><!--/setting/privacy.html">Privecy</a></li>-->
<!--                    <li><a --><?php //if($menu_active == 'Tearms'){echo "class='active'"; }?><!-- href="--><?php //echo admin_url(); ?><!--/setting/terms.html">Tearms</a></li>-->
<!--                    <li><a --><?php //if($menu_active == 'Update'){echo "class='active'"; }?><!-- href="--><?php //echo admin_url(); ?><!--/setting/update.html">Update</a></li>-->
<!--                </ul>-->
<!--            </li>-->
            <?php } ?>
            
            <?php if($this->session->userdata('access_label') == 1){ ?>
            <li>
                <a <?php if($menu_active == 'Registered User'){echo "class='active'"; }?> href="<?php echo admin_url(); ?>/registered_user.html">
                    <i class="fa fa-user"></i> <span>Registered Admins</span>
                </a>
            </li>
            <?php } ?>


            <?php if($this->session->userdata('access_label') == 2){ ?>
            <li>
                <a <?php if($menu_active == 'Registered Dealers'){echo "class='active'"; }?> href="<?php echo base_url('admin/registered_dealers'); ?>">
                    <i class="fa fa-user"></i> <span>Registered Dealers</span>
                </a>
            </li>
            <?php } ?>
            
            
<!--            <li>-->
<!--                <a href="http://loginsystem.atiqueit.com/ums/help/" target="blank">-->
<!--                    <i class="fa fa-question"></i> <span>Help</span>-->
<!--                </a>-->
<!--            </li>-->
        </ul>
        <!--Left navigation end-->
    </section>
    <!--Left navigation section end-->
