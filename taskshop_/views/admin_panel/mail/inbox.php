<section id="min-wrapper">
    <div id="main-content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <!--Top header start-->
                    <h3 class="ls-top-header">Inbox</h3>
                    <!--Top header end -->

                    <!--Top breadcrumb start -->
                    <ol class="breadcrumb">
                        <li><a href="javascript:void(0)"><i class="fa fa-home"></i></a></li>
                        <li>Email</li>
                        <li class="active">Inbox</li>
                    </ol>
                    <!--Top breadcrumb start -->
                </div>
            </div>
            <!-- Main Content Element  Start-->
            <div class="row">
                <div class="col-md-12">
                    <div class="mail-area">

                        <div class="mail-box-navigation">
                            <ul class="mail-navigation">
                                <li><a href="<?php echo admin_url(); ?>/mail.html"><i class="fa fa-pencil"></i> <span>Compose</span></a></li>
                                <li><a href="<?php echo admin_url(); ?>/mail/sent.html"><i class="fa fa-mail-reply"></i> <span>Sent</span></a></li>
                            </ul>
                        </div>
                        <hr/>
                        <div class="col-md-12">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h3 class="panel-title">Inbox</h3>
                                </div>
                                <div class="panel-body">
                                    <!--Table Wrapper Start-->
                                    <div class="table-responsive ls-table">
                                        <div>
                                            <?php
                                            $msg = $this->session->userdata('message');
                                            $error = $this->session->userdata('error');

                                            if ($msg) {
                                                echo "<div class='alert alert-success'>" . $msg . "</div>";
                                                $this->session->unset_userdata('message');
                                            } else if ($error) {
                                                echo "<div class='alert alert-danger'>" . $error . "</div>";
                                                $this->session->unset_userdata('error');
                                            }
                                            ?>
                                        </div>
                                        <table class="table table-bordered table-striped">
                                            <thead>
                                                <tr>
                                                    <th>Name</th>
                                                    <th>From</th>
                                                    <th>Subject</th>
                                                    <th>Date</th>
                                                    <th>Status</th>
                                                    <th class="text-center">Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php foreach ($inbox_mail_info as $v_inbox_mail_info) { ?>
                                                    <tr>
                                                        <td><?php echo $v_inbox_mail_info['full_name']; ?></td>
                                                        <td><?php echo $v_inbox_mail_info['from']; ?></td>
                                                        <td><?php echo $v_inbox_mail_info['subject']; ?></td>
                                                        <td><?php echo date("D d F Y h:ia", strtotime($v_inbox_mail_info['date_added'])); ?></td>
                                                        <td>
                                                            <?php if ($v_inbox_mail_info['reading_status'] == 0) { ?>
                                                                <span class="label label-warning">Unread</span>
                                                            <?php } else { ?>
                                                                <span class="label label-success">Read</span>
                                                            <?php } ?>
                                                        </td>
                                                        <td class="text-center">
                                                            <a href="<?php echo admin_url(); ?>/mail/view_inbox_mail/<?php echo $v_inbox_mail_info['inbox_mail_id']; ?>" class="btn btn-xs btn-info"><i class="fa fa-eye"></i></a>
                                                            <a href="<?php echo admin_url(); ?>/mail/delete_inbox_mail/<?php echo $v_inbox_mail_info['inbox_mail_id']; ?>"class="btn btn-xs btn-danger" onclick="return confirm('Are you sure to delete this ?');"><i class="fa fa-trash-o"></i></a>
                                                        </td>
                                                    </tr>
                                                <?php } ?>
                                            </tbody>
                                        </table>
                                    </div>
                                    <!--Table Wrapper Finish-->
<!--                                    <div class="text-center">
                                        <div class="ls-button-group demo-btn ls-table-pagination">
                                            <ul class="pagination ls-pagination">
                                                <li><a href="#"><i class="fa fa-arrow-left"></i> </a></li>
                                                <li><a href="#">1</a></li>
                                                <li><a href="#">2</a></li>
                                                <li><a href="#">3</a></li>
                                                <li><a href="#">4</a></li>
                                                <li><a href="#">5</a></li>
                                                <li><a href="#"><i class="fa fa-arrow-right"></i></a></li>
                                            </ul>
                                        </div>
                                    </div>-->
                                </div>
                            </div>
                        </div>

                        <div class="clearfix"></div>

                    </div>

                </div>
            </div>

            <!-- Main Content Element  End-->

        </div>
    </div>



</section>
<!--Page main section end -->