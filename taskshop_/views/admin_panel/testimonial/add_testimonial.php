<style type="text/css">
    .errorMesage{
        color: red;
    }
</style>
<!--Page main section start-->
<section id="min-wrapper">
    <div id="main-content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <!--Top header start-->
                    <h3 class="ls-top-header">Add Testimonial</h3>
                    <!--Top header end -->

                    <!--Top breadcrumb start -->
                    <ol class="breadcrumb">
                        <li><a href="#"><i class="fa fa-home"></i></a></li>
                        <li class="active">Add Testimonial</li>
                    </ol>
                    <!--Top breadcrumb start -->
                </div>
            </div>
            <!-- Main Content Element  Start-->

            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title">Add Testimonial</h3>
                        </div>
                        <div class="panel-body">
                            <div>
                                    <?php
                                    $msg = $this->session->userdata('message');
                                    $error = $this->session->userdata('error');

                                    if ($msg) {
                                        echo "<div class='alert alert-success text-center'><i class='fa fa-check-circle-o fa-5x'></i>" . $msg . "</div>";
                                        $this->session->unset_userdata('message');
                                    } else if ($error) {
                                        echo "<div class='alert alert-danger text-center'><i class='fa fa-ban fa-5x'></i>" . $error . "</div>";
                                        $this->session->unset_userdata('error');
                                    }
                                    ?>
                                </div>
                            <form name="add_testimonial_form" id="add_testimonial_form" method="post" class="form-horizontal" action="<?php echo admin_url(); ?>/testimonial/save_testimonial.html">
                                
                                <div class="form-group">
                                    <label class="col-lg-3 control-label">
                                        Testimonial
                                    </label>
                                    <div class="col-lg-7">
                                        <textarea name="data[testimonial]" class="summernote" placeholder="Write testimonial"><?php echo set_value('data[testimonial]'); ?></textarea>
                                        <span class="errorMesage"><?php echo form_error('data[testimonial]'); ?></span>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-lg-9 col-lg-offset-3">
                                        <button type="submit" class="btn btn-primary">Add Testimonial</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<script type="text/javascript">
<?php if (set_value('data[status]') != null) { ?>
        document.forms['add_job_order_form'].elements['status'].value = '<?php echo set_value('data[status]'); ?>';
<?php } if (set_value('data[need_path]') != null) { ?>
        document.forms['add_job_order_form'].elements['need_path'].value = '<?php echo set_value('data[need_path]'); ?>';
<?php }if (set_value('data[return_file_type]') != null) { ?>
        document.forms['add_job_order_form'].elements['return_file_type'].value = '<?php echo set_value('data[return_file_type]'); ?>';
<?php }if (set_value('data[job_category_id]') != null) { ?>
        document.forms['add_job_order_form'].elements['job_category_id'].value = '<?php echo set_value('data[job_category_id]'); ?>';
<?php }if (set_value('data[job_type]') != null) { ?>
        document.forms['add_job_order_form'].elements['job_type'].value = '<?php echo set_value('data[job_type]'); ?>';
<?php }?>
</script>