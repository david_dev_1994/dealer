<!--Page main section start-->
<section id="min-wrapper">
    <div id="main-content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <h3 class="ls-top-header">File Manager</h3>
                    <!--Top header end -->

                    <!--Top breadcrumb start -->
                    <ol class="breadcrumb">
                        <li><a href="#"><i class="fa fa-home"></i></a></li>
                        <li class="active">File Manager</li>
                    </ol>
                    <!--Top breadcrumb start -->



                </div>
            </div>
            <!-- Main Content Element  Start-->
            <div class="col-md-12">
                <div class="row filter">
                    <div class="col-md-12 ls-gallery-paddingless-wrap">
                        <div class="row ls-gallery-filter-wrap">
                            <div class="col-md-12 ls-gallery-paddingless-wrap">
                                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal" data-whatever="@mdo">Upload File</button>
                            </div>
                        </div>


                        <div>
                            <?php
                            $msg = $this->session->userdata('message');
                            $error = $this->session->userdata('error');

                            if ($msg) {
                                echo "<div class='alert alert-success'>" . $msg . "</div>";
                                $this->session->unset_userdata('message');
                            } else if ($error) {
                                echo "<div class='alert alert-danger'>" . $error . "</div>";
                                $this->session->unset_userdata('error');
                            }
                            ?>
                        </div>
                        <!-- File Manager Start-->
                        <table class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>SL#</th>
                                    <th>File Name</th>
                                    <th>Path</th>
                                    <th>Date Added</th>
                                </tr>
                            </thead>
                            <tbody>

                                <?php
                                $i = 1;
                                foreach ($file_info as $v_file_info) {
                                    ?>
                                    <tr>
                                        <td><?php echo $i++; ?></td>
                                        <td><?php echo $v_file_info['file_name']; ?></td>
                                        <td>
                                            <a href="<?php echo base_url(); ?>uploaded_files/file_manager/<?php echo $v_file_info['file_name']; ?>">
                                                <?php echo base_url(); ?>uploaded_files/file_manager/<?php echo $v_file_info['file_name']; ?>
                                            </a>
                                        </td>
                                        <td><?php echo date("D d F Y h:ia", strtotime($v_file_info['date_added'])); ?></td>
                                    </tr>
                                <?php } ?>

                            </tbody>
                        </table>

                    </div>
                </div>

            </div>
            <!-- Main Content Element  End-->
        </div>
    </div>
</section>
<!--Page main section end -->

<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">


    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="exampleModalLabel">Upload Image </h4>
            </div>
            <div class="modal-body">
                <form id="upload" method="post" action="<?php echo admin_url(); ?>/file_manager/save_file.html" enctype="multipart/form-data">
                    <div id="drop">
                        Drop Here

                        <a>Browse</a>
                        <input type="file" name="upl" multiple />
                    </div>
                    <ul>
                        <!-- The file uploads will be shown here -->
                    </ul>
                </form>
            </div>
            <div class="modal-footer">
                <a href="<?php echo admin_url(); ?>/file_manager.html" class="btn btn-danger">Close</a>
            </div>
        </div>
    </div>
</div>