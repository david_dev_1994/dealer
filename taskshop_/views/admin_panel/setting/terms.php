<!--Page main section start-->
<section id="min-wrapper">
    <div id="main-content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <!--Top header start-->
                    <h3 class="ls-top-header">Terms & Conditions</h3>
                    <!--Top header end -->

                    <!--Top breadcrumb start -->
                    <ol class="breadcrumb">
                        <li><a href="javascript:void(0)"><i class="fa fa-home"></i></a></li>
                        <li>Setting</li>
                        <li class="active">Terms & Conditions</li>
                    </ol>
                    <!--Top breadcrumb start -->
                </div>
            </div>

            <div>
                <?php
                $msg = $this->session->userdata('message');
                $error = $this->session->userdata('error');

                if ($msg) {
                    echo "<div class='alert alert-success'>" . $msg . "</div>";
                    $this->session->unset_userdata('message');
                } else if ($error) {
                    echo "<div class='alert alert-danger'>" . $error . "</div>";
                    $this->session->unset_userdata('error');
                }
                ?>
            </div>
            
            
            <!-- Main Content Element  Start-->
            <div class="row">
                <div class="col-md-12">
                    <div class="mail-area">


                        <div class="mail-body compose-mail-box">

                            <form id="formid2" method="post" action="<?php echo admin_url(); ?>/setting/update_terms.html">
                                <div class="mail-main-content">
                                    <textarea name="terms" class="summernote">
                                        <?php echo $terms_info['terms']; ?>
                                    </textarea> 
                                    <div class="row">
                                        <div class="col-md-12">
                                            <button class="mail-send-btn btn ls-light-green-btn">Send</button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>

                        <div class="clearfix"></div>

                    </div>

                </div>
            </div>

            <!-- Main Content Element  End-->

        </div>
    </div>



</section>
<!--Page main section end -->