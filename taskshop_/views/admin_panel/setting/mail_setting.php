<script type="text/javascript">
    $(document).ready(function () {
        $('.successMsg').fadeOut(10000);
    });

    function mailSettingValidation() {
        var error = 0;

        var NormalPattern = /^[a-zA-Z0-9-_,. ]*$/i;
        var ActivationSubj = $("#activation_subj").val();
        var ActivationBody = $("#activation_body").val();
        var ForgotSubj = $("#forgot_subj").val();
        var ForgotBody = $("#forgot_body").val();
        var AddUserSubj = $("#add_user_subj").val();
        var AddUserBody = $("#add_user_body").val();

        if (!NormalPattern.test(ActivationSubj)) {
            error++;
            $("#msg_activation_subj").html("Special character not allowed !");
        } else if (ActivationSubj.length < 1) {
            error++;
            $("#msg_activation_subj").html("Activation subj character required !");
        } else if (ActivationSubj.length > 150) {
            error++;
            $("#msg_activation_subj").html("Maximum 150 character required !");
        } else {
            $("#msg_activation_subj").html("");
        }

        if (!NormalPattern.test(ActivationBody)) {
            error++;
            $("#msg_activation_body").html("Special character Not Allowed !");
        } else if (ActivationSubj.length < 1) {
            error++;
            $("#msg_activation_body").html("Activation body is required !");
        } else if (ActivationSubj.length > 150) {
            error++;
            $("#msg_activation_body").html("Maximum 150 character required !");
        } else {
            $("#msg_activation_body").html("");
        }

        if (!NormalPattern.test(ForgotSubj)) {
            error++;
            $("#msg_forgot_subj").html("Special character not allowed !");
        } else if (ForgotSubj.length < 1) {
            error++;
            $("#msg_forgot_subj").html("Forgot subj is required !");
        } else if (ForgotSubj.length > 150) {
            error++;
            $("#msg_forgot_subj").html("Maximum 150 character required !");
        } else {
            $("#msg_forgot_subj").html("");
        }

        if (!NormalPattern.test(ForgotBody)) {
            error++;
            $("#msg_forgot_body").html("Special character not allowed !");
        } else if (ForgotBody.length < 1) {
            error++;
            $("#msg_forgot_body").html("Forgot body is required !");
        } else if (ForgotBody.length > 150) {
            error++;
            $("#msg_forgot_body").html("Maximum 150 character required !");
        } else {
            $("#msg_forgot_body").html("");
        }

        if (!NormalPattern.test(AddUserSubj)) {
            error++;
            $("#msg_add_user_subj").html("Special character not allowed !");
        } else if (AddUserSubj.length < 1) {
            error++;
            $("#msg_add_user_subj").html("Add User subj is required !");
        } else if (AddUserSubj.length > 150) {
            error++;
            $("#msg_add_user_subj").html("Maximum 150 character required !");
        } else {
            $("#msg_add_user_subj").html("");
        }

        if (!NormalPattern.test(AddUserBody)) {
            error++;
            $("#msg_add_user_body").html("Special character not allowed !");
        } else if (AddUserBody.length < 1) {
            error++;
            $("#msg_add_user_body").html("Add user body is required !");
        } else if (AddUserBody.length > 150) {
            error++;
            $("#msg_add_user_body").html("Maximum 150 character required !");
        } else {
            $("#msg_add_user_body").html("");
        }

        if (error === 0) {
            return true;
        } else {
            return false;
        }

    }
</script>

<!--Page main section start-->
<section id="min-wrapper">
    <div id="main-content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">

                    <!--Top breadcrumb start -->
                    <ol class="breadcrumb">
                        <li><a href="#"><i class="fa fa-home"></i></a></li>
                        <li><a href="#">Setting</a></li>
                        <li class="active">Mail Setting</li>
                    </ol>
                    <!--Top breadcrumb start -->
                </div>
            </div>
            <!-- Main Content Element  Start-->
            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <div>
                                        <?php
                                        $msg = $this->session->userdata('message');
                                        $error = $this->session->userdata('error');

                                        if ($msg) {
                                            echo "<div class='alert alert-success'>" . $msg . "</div>";
                                            $this->session->unset_userdata('message');
                                        } else if ($error) {
                                            echo "<div class='alert alert-danger'>" . $error . "</div>";
                                            $this->session->unset_userdata('error');
                                        }
                                        ?>
                                    </div>
                                    <form id="formid2" class="formular form-horizontal ls_form" method="post" action="<?php echo admin_url(); ?>/setting/update_mail_setting.html">
                                        <div id="wizard" class="swMain">
                                            <ul class="anchor">
                                                <li>
                                                    <a rel="1" isdone="1" class="selected" href="#step-Login">
                                                        <span class="stepDesc">
                                                            Activation
                                                        </span>
                                                    </a>
                                                </li>
                                                <li>
                                                    <a rel="2" isdone="1" class="done" href="#step-user">
                                                        <span class="stepDesc">
                                                            Forgot
                                                        </span>
                                                    </a>
                                                </li>
                                                <li>
                                                    <a rel="3" isdone="1" class="done" href="#step-bio">
                                                        <span class="stepDesc">
                                                            Add User
                                                        </span>
                                                    </a>
                                                </li>
                                            </ul>

                                            <div style="height: 186px;" class="stepContainer">
                                                <div style="display: block;" class="content" id="step-Login">
                                                    <h2 class="StepTitle">Activation</h2>

                                                    <div class="container-fluid">
                                                        <div class="row ">
                                                            <div class="form-group">
                                                                <label class="col-md-2 control-label">Subject</label>

                                                                <div class="col-md-10">
                                                                    <input class="form-control validate[required] text-input" name="data[activation_subj]" id="activation_subj" type="text" value="<?php echo $mail_set_info['activation_subj']; ?>">
                                                                    <span class="errorMessage" id="msg_activation_subj"></span>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="row ">
                                                            <div class="form-group">
                                                                <label class="col-md-2 control-label">Message Body </label>

                                                                <div class="col-md-10">
                                                                    <input class="form-control validate[required] text-input" id="activation_body" name="data[activation_body]" type="text" value="<?php echo $mail_set_info['activation_body']; ?>">
                                                                    <span class="errorMessage" id="msg_activation_body"></span>
                                                                </div>
                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>
                                                <div style="display: none;" class="content" id="step-user">

                                                    <h2 class="StepTitle">Forgot</h2>
                                                    <div class="container-fluid">
                                                        <div class="row ">
                                                            <div class="form-group">
                                                                <label class="col-md-2 control-label">Subject</label>

                                                                <div class="col-md-10">
                                                                    <input class="form-control validate[required] text-input" id="forgot_subj" name="data[forgot_subj]" type="text"  value="<?php echo $mail_set_info['forgot_subj']; ?>">
                                                                    <span class="errorMessage" id="msg_forgot_subj"></span>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="row ">
                                                            <div class="form-group">
                                                                <label class="col-md-2 control-label">Message Body </label>

                                                                <div class="col-md-10">
                                                                    <input class="form-control validate[required] text-input" id="forgot_body" name="data[forgot_body]" type="text"  value="<?php echo $mail_set_info['forgot_body']; ?>">
                                                                    <span class="errorMessage" id="msg_forgot_body"></span>
                                                                </div>
                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>
                                                <div style="display: none;" class="content" id="step-bio">

                                                    <h2 class="StepTitle">Add User</h2>
                                                    <div class="container-fluid">
                                                        <div class="row ">
                                                            <div class="form-group">
                                                                <label class="col-md-2 control-label">Subject</label>

                                                                <div class="col-md-10">
                                                                    <input class="form-control validate[required] text-input" id="add_user_subj" name="data[add_user_subj]" type="text"  value="<?php echo $mail_set_info['add_user_subj']; ?>">
                                                                    <span class="errorMessage" id="msg_add_user_subj"></span>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="row ">
                                                            <div class="form-group">
                                                                <label class="col-md-2 control-label">Message Body </label>

                                                                <div class="col-md-10">
                                                                    <input class="form-control validate[required] text-input" id="add_user_body" name="data[add_user_body]" type="text"  value="<?php echo $mail_set_info['add_user_body']; ?>">
                                                                    <span class="errorMessage" id="msg_add_user_body"></span>
                                                                </div>
                                                            </div>
                                                        </div>

                                                    </div>

                                                </div>
                                            </div>

                                            <div class="actionBar"><div class="msgBox"><div class="content"></div><a href="#" class="close">X</a></div><div class="loader">Loading</div></div></div>
                                        <!-- End SmartWizard Content -->
                                        <button class="btn ls-red-btn" type="submit" onclick="return mailSettingValidation();" >Save Changes</button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


            <!-- Main Content Element  End-->

        </div>
    </div>

</section>
<!--Page main section end -->