<style type="text/css">
    .errorMesage{
        color: red;
    }
</style>
<!--Page main section start-->
<section id="min-wrapper">
    <div id="main-content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <!--Top header start-->
                    <h3 class="ls-top-header">FTP File Upload</h3>
                    <!--Top header end -->

                    <!--Top breadcrumb start -->
                    <ol class="breadcrumb">
                        <li><a href="#"><i class="fa fa-home"></i></a></li>
                        <li class="active">FTP File Upload</li>
                    </ol>
                    <!--Top breadcrumb start -->
                </div>
            </div>
            <!-- Main Content Element  Start-->

            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title">Add File</h3>
                        </div>
                        <div class="panel-body">

                            <form name="ftp_file_form" id="defaultForm" method="post" class="form-horizontal ls_form" action="<?php echo admin_url(); ?>/ftp_file_manager/upload_file.html" enctype="multipart/form-data">
                                <div class="form-group">
                                    <label class="col-lg-3 control-label">Choose File</label>
                                    <div class="col-lg-6">
                                        <input type="file"class="form-control" name="file" />
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-lg-9 col-lg-offset-3">
                                        <input type="submit" class="btn btn-primary" name="submit" value="Upload">
                                    </div>
                                </div>
                            </form>
                            <?php
                            $this->load->helper('string');
                            echo random_string('nozero', 6);
                            ?>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
