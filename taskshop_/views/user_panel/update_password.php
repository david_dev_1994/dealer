<section>
    <div class="block extra-gap blackish">
        <div class="container">
            <div class="row">
                <div class="col-md-12">

                    <div class="heading">
                        <h2>Update Password</h2>
                        <div>
                            <?php
                            $msg = $this->session->userdata('message');
                            $error = $this->session->userdata('error');

                            if ($msg) {
                                echo "<div class='alert alert-success text-center'>" . $msg . "</div>";
                                $this->session->unset_userdata('message');
                            } else if ($error) {
                                echo "<div class='alert alert-danger text-center'>" . $error . "</div>";
                                $this->session->unset_userdata('error');
                            }
                            ?>
                        </div>
                        <div class="lines"><span><i>Please enter your associated email address</i></span></div>
                    </div>
                    <div class="col-md-4 col-md-offset-4 column">
                        <div id="message"></div>
                        
                        <form class="contact" method="post" action="<?php echo base_url(); ?>user_login/update_password.html">
                            <div class="row">
                                <div class="col-md-12"><input type="email" name="email_address" value="<?php echo $email_address;?>" hidden/></div>
                                <div class="col-md-12"><input type="password" placeholder="New Password" name="password" value="" required/></div>
                                
                                <div class="col-md-12"><input type="submit" name="update_password" value="Update Password" class="btn btn-success btn-block"/></div>
                                
                            </div>
                        </form><!-- Contact  -->
                    </div>

                </div>
            </div>
        </div>
    </div>
</section>