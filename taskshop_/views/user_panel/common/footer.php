        </div>
    </div>
</div>
<br><br><br>
<footer>
    <div class="block no-padding">
        <div class="container">
            <div class="row">
                <div class="col-lg-4">
                    <h3 class="widget-title"><span>Navigation Links</span></h3>
                    <ul id="menu-footer-navigation-1" class="menu">
                        <li class="menu-item"><a href="https://www.bktechnologies.com/">Home</a></li>
                        <li class="menu-item"><a href="https://www.bktechnologies.com/about-us/">About Us</a></li>
                        <li class="menu-item"><a href="https://www.bktechnologies.com/product-category/all-products/">Solutions</a></li>
                        <li class="menu-item"><a href="https://www.bktechnologies.com/bk-sales/">Where To Buy</a></li>
                        <li class="menu-item"><a href="http://www.relmservice.com/" class="gtrackexternal">Support</a></li>
                        <li class="menu-item"><a href="https://www.bktechnologies.com/press/">Press</a></li>
                        <li class="menu-item"><a href="https://www.bktechnologies.com/investor-relations/">Investor Relations</a></li>
                    </ul>
                </div>
                <div class="col-lg-4">
                    <h3 class="widget-title"><span>Connect With Us</span></h3>
                    <ul class="ut-sociallinks">
                        <li><a href="https://www.facebook.com/BK-Technologies-106082389889629/" target="_blank" class="gtrackexternal"><i class="fa fa-facebook"></i></a></li>
                        <li><a href="https://www.instagram.com/bktechnologies/" target="_blank" class="gtrackexternal"><i class="fa fa-instagram"></i></a></li>
                        <li><a href="https://www.linkedin.com/company/bk-technologies" target="_blank" class="gtrackexternal"><i class="fa fa-linkedin"></i></a></li>
                        <li><a href="https://twitter.com/BKTechUSA" target="_blank" class="gtrackexternal"><i class="fa fa-twitter"></i></a></li>
                        <li><a href="https://www.youtube.com/channel/UCzC8Qwm8JL7g1OR7OOfPZTQ" target="_blank" class="gtrackexternal"><i class="fa fa-youtube"></i></a></li>
                    </ul>
                </div>
                <div class="col-lg-4">
                    <h3 class="widget-title"><span>Company Resources</span></h3>
                    <div class="menu-footer-navigation-2-container">
                        <ul id="menu-footer-navigation-2" class="menu">
                            <li class="menu-item"><a href="https://www.bktechnologies.com/ourevents/">Community Events</a></li>
                            <li class="menu-item"><a href="https://www.bktechnologies.com/career/">Careers</a></li>
                            <li class="menu-item"><a href="https://www.bktechnologies.com/contact-us/">Contact Us</a></li>
                            <li class="menu-item"><a href="https://www.bktechnologies.com/privacy-policy/">Privacy Policy</a></li>
                            <li class="menu-item"><a href="https://www.bktechnologies.com/terms-of-service/">Terms Of Service</a></li>
                            <li class="menu-item"><a href="https://www.bktechnologies.com/html-sitemap/">Sitemap</a></li>
                            <li class="menu-item"><a href="https://www.bktechnologies.com/508-accessibility-template/">508 Compliance</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer><!-- FOOTER -->

<div class="bottom-footer">
    <div class="container">
        <div class="row">
            <div class="col-lg-6">
                <p>© 2018 BK Technologies</p>
            </div>
            <div class="col-lg-6">
                <p style="float:right;">Web Design By <a href="http://theadleaf.com/" target="_blank">The AD Leaf</a></p>
            </div>
        </div>
    </div>
</div><!-- Bottom Footer -->

</div>




<!-- SCRIPTS-->


<script type="text/javascript" src="<?php echo base_url(); ?>user_assets/js/modernizr.custom.17475.js"></script>

<script src="<?php echo base_url(); ?>user_assets/js/jquery2.1.1.js" type="text/javascript"></script>
<script src='<?php echo base_url(); ?>user_assets/js/jquery.min.js'></script>
<script src="<?php echo base_url(); ?>user_assets/js/bootstrap.min.js"></script>
<script src="<?php echo base_url(); ?>user_assets/js/jquery.scrolly.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>user_assets/js/jquery.prettyPhoto.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>user_assets/js/owl.carousel.min.js"></script>
<script src="<?php echo base_url(); ?>user_assets/js/TableBarChart.js"></script>
<script src="<?php echo base_url(); ?>user_assets/js/script.js"></script>
<script src="<?php echo base_url(); ?>user_assets/js/waypoints.js"></script> 
<script src="<?php echo base_url(); ?>user_assets/js/zoominout.js"></script>
<script src="<?php echo base_url(); ?>user_assets/js/comparison.js"></script>

        <script>
    jQuery(document).ready(function ($) {
        /*================== COUNTER =====================*/
        $('.count').counterUp({
            delay: 10,
            time: 2000
        });

        /*================== BAR CHART =====================*/
        $('#source').tableBarChart('#target', '', false);

        /*================== SERVICE CAROUSEL =====================*/
        $(".services-carousel").owlCarousel({
            autoplay: true,
            autoplayTimeout: 3000,
            smartSpeed: 2000,
            loop: true,
            dots: false,
            nav: true,
            margin: 0,
            items: 1,
            singleItem: true
        });


        gallery_set = [
            'http://placehold.it/1366x700',
            'http://placehold.it/1366x700',
            'http://placehold.it/1366x700',
        ]
        "use strict";
        jQuery('#kenburns').attr('width', jQuery(window).width());
        jQuery('#kenburns').attr('height', 500);
        jQuery('#kenburns').kenburns({
            images: gallery_set,
            frames_per_second: 30,
            display_time: 5000,
            fade_time: 1000,
            zoom: 1.2,
            background_color: '#000000'
        });

    });
</script>

</body>