<!DOCTYPE html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>BK Dealer Portal</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="" />
    <meta name="keywords" content="" />

    <!-- Google Fonts -->
    <link href='http://fonts.googleapis.com/css?family=Arimo:400,400italic,700,700italic' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Oswald:400,300,700' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Archivo+Narrow:400,400italic,700,700italic' rel='stylesheet' type='text/css'>
    <!-- Styles -->
    <link href="<?php echo base_url(); ?>user_assets/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url(); ?>user_assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="<?php echo base_url(); ?>user_assets/css/style.css" type="text/css" />
    <link rel="stylesheet" href="<?php echo base_url(); ?>user_assets/css/owl-carousel.css" type="text/css" />
    <link href="<?php echo base_url(); ?>user_assets/css/responsive.css" rel="stylesheet" type="text/css" />

    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>user_assets/css/colors/color.css" title="color1" />
    
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>user_assets/css/custom.css" />


    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script src='<?=base_url("user_assets/js/jquery.multifield.min.js")?>'></script>

    <style type="text/css">
        .errorMessage{
            color: #ff6600;
        }
        /* NOTE: The styles were added inline because Prefixfree needs access to your styles and they must be inlined if they are on local disk! */
        div#comparison {
            margin:auto;
            width: 200px;
            height: 200px;
            max-width: 200px;
            max-height: 200px;
            overflow: hidden; }
        div#comparison figure { 
            background-size: cover;
            position: relative;
            font-size: 0;
            width: 100%; 
            height: 100%;
        }
        div#comparison figure > img { 
            position: relative;
            width: 100%;
        }
        div#comparison figure div { 
            background-size: cover;
            position: absolute;
            width: 50%; 
            box-shadow: 0 5px 10px -2px rgba(0,0,0,0.3);
            overflow: hidden;
            bottom: 0;
            height: 100%;
        }

        input[type=range]{
            -webkit-appearance:none;
            -moz-appearance:none;
            position: relative;
            top: -2rem; left: -2%;
            background-color: rgba(255,255,255,0.1);
            width: 102%; 
        }
        input[type=range]:focus { 
            outline: none; 
        }
        input[type=range]:active { 
            outline: none;  
        }

        input[type=range]::-moz-range-track { 
            -moz-appearance:none;
            height:15px;
            width: 98%;
            background-color: rgba(255,255,255,0.1); 
            position: relative;
            outline: none;    
        }
        input[type=range]::active { 
            border: none; 
            outline: none;
        }
        input[type=range]::-webkit-slider-thumb {
            -webkit-appearance:none;
            width: 20px; height: 15px;   
            background: #fff;
            border-radius: 0;
        }
        input[type=range]::-moz-range-thumb {
            -moz-appearance: none;
            width: 20px;
            height: 15px;
            background: #fff;
            border-radius: 0;
        }   
        input[type=range]:focus::-webkit-slider-thumb {
            background: rgba(255,255,255,0.5);
        }
        input[type=range]:focus::-moz-range-thumb {
            background: rgba(255,255,255,0.5);
        }
        .portfolio_title{margin:auto;width:200px; height:30px; background-color:#404040;color:#fff;font-size:15px;padding:5px;text-align:right;}
        
    </style>

    <script src="<?php echo base_url(); ?>user_assets/js/prefixfree.min.js"></script>
    <link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
    <link rel="stylesheet" href="https://use.typekit.com/hsu0ayg.css">
</head>

<body>
    <div class="theme-layout">
        
        <header class="header4">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-2" style="margin-left: -20px;">
                        <div class="logo">
                            <a href="<?= base_url(); ?>" title=""><img src="<?php echo base_url(); ?>user_assets/images/bk-logo.png" alt="" /></a>
                        </div>
                    </div>
                    <div class="col-lg-10">
                        <div class="full-navigation">
                            <div class="container">
                                <nav>
                                    <ul class="pull-right" style="margin-right: 25px;">
                                        <li><a href="<?php echo base_url(); ?>" title=""><i class="fa fa-home"></i>Home</a></li>
<!--                                        <li><a href="--><?php //echo base_url(); ?><!--quote.html" title=""><i class="fa fa-arrows"></i>Submit Quote</a></li>-->
<!--                                        <li><a href="--><?php //echo base_url(); ?><!--order.html" title=""><i class="fa fa-tty"></i>Training</a></li>-->
<!--                                        <li><a href="--><?php //echo base_url(); ?><!--order.html" title=""><i class="fa fa-tty"></i>Press Release</a></li>-->
                                <?php if (!$this->session->userdata('d_is_logged')) { ?>
                                        <li><a href="<?php echo base_url(); ?>page/sign_up" title=""><i class="fa fa-user-plus"></i>Request Access</a></li>
                                        <li><a href="<?php echo base_url(); ?>user_login" title=""><i class="fa fa-sign-in"></i>Sign in</a></li>
                                <?php } else { ?>
                                    <li><a href="<?php echo base_url(); ?>dealer_profile" title=""><i class="fa fa-user"></i>Profile</a></li>
                                        <li><a href="<?php echo base_url(); ?>page/user_logout" title=""><i class="fa fa-sign-in"></i>Logout</a></li>
                                <?php } ?>
                                    </ul>
                                </nav><!-- Navigation -->
                            </div>
                        </div>
                    </div>    
                </div>
            </div>
        </header>

        <div class="container">
            <div class="row">
                <div class="col-md-12">

