<!--Page main section start-->
<section id="min-wrapper">
    <div id="main-content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title">My Quote</h3>
                        </div>

                        <div class="panel-body">
                            <!--Table Wrapper Start-->
                            <div class="table-responsive ls-table">
                                <div>
                                    <?php
                                    $msg = $this->session->userdata('message');
                                    $error = $this->session->userdata('error');

                                    if ($msg) {
                                        echo "<div class='alert alert-success text-center'><i class='fa fa-check-circle-o fa-5x'></i>" . $msg . "</div>";
                                        $this->session->unset_userdata('message');
                                    } else if ($error) {
                                        echo "<div class='alert alert-danger text-center'><i class='fa fa-ban fa-5x'></i>" . $error . "</div>";
                                        $this->session->unset_userdata('error');
                                    }
                                    ?>
                                </div>
                                <table class="table table-bordered table-striped">
                                    <thead>
                                        <tr>
                                            <th>SL#</th>
                                            <th>Job Title</th>
                                            <th>My Quote</th>
                                            <th>Added By</th>
                                            <th>Date Added</th>
                                            <?php if ($this->session->userdata('access_label') == 1 OR $this->session->userdata('access_label') == 2) { ?>
                                            <th>Publication Status</th>
                                            <?php } ?>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>

                                        <?php
                                        $i = 1;
                                        foreach ($quote_info as $v_quote_info) {
                                            ?>
                                            <tr>
                                                <td><?php echo $i++; ?></td>
                                                <td><?php echo $v_quote_info['job_title']; ?></td>
                                                <td><?php echo $v_quote_info['job_category_name']; ?></td>
                                                <td><?php echo $v_quote_info['full_name']; ?></td>
                                                <td><?php echo date("D d F Y h:ia", strtotime($v_quote_info['date_added'])); ?></td>
                                                <?php if ($this->session->userdata('access_label') == 1 OR $this->session->userdata('access_label') == 2) { ?>
                                                <td>
                                                    <?php if ($v_quote_info['publication_status'] == 1) { ?>
                                                        <span class="label-success label label-default">Published</span>
                                                    <?php } else { ?>
                                                        <span class="label-warning label label-default">Unpublished</span>
                                                    <?php } ?>
                                                </td>
                                                <?php } ?>
                                                <td class="text-center">
                                                    <a href="<?php echo base_url(); ?>quote/user_sub_quote/<?php echo $v_quote_info['quote_id']; ?>" class="btn btn-xs btn-primary"><i class="fa fa-eye"></i></a>
                                                </td>
                                            </tr>
                                        <?php } ?>

                                    </tbody>
                                </table>
                            </div>
                            <!--Table Wrapper Finish-->
                            <!--                            <div class="ls-button-group demo-btn ls-table-pagination">
                                                            <ul class="pagination ls-pagination" style="float: right;">
                                                                <li><a href="#"><i class="fa fa-arrow-left"></i> </a></li>
                                                                <li><a href="#">1</a></li>
                                                                <li><a href="#">2</a></li>
                                                                <li><a href="#">3</a></li>
                                                                <li><a href="#">4</a></li>
                                                                <li><a href="#">5</a></li>
                                                                <li><a href="#"><i class="fa fa-arrow-right"></i></a></li>
                                                            </ul>
                                                        </div>-->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!--Page main section end -->