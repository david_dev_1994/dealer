<?php
//echo "<pre>";
//print_r($user);
//exit;
$user['address_1'] = json_decode($user['address_1']);
$user['city'] = json_decode($user['city']);
$user['contactinfo'] = json_decode($user['contactinfo']);
$user['state'] = json_decode($user['state']);
$user['zip'] = json_decode($user['zip']);

?>
<section>
    <div class="block extra-gap blackish">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12 shadow-container" style="background-color: ivory">
                    <br>
                    <br>
                    <div class="col-md-10 col-md-offset-1 column">
                        <div id="message"></div>
                        <!--                        <form class="form-box contact" method="post" action="--><?php //echo base_url(); ?><!--page/registration.html">-->
                        <form class=" form-opened text-center border border-light p-5" method="post" action="">

                            <section>
                                <div class="creative-section row">
                                    <h2 class="text-capitalize text-center circle-text"><?= strtoupper($user['full_name']); ?> Profile Data </h2>
                                </div>
                                <br>
                            </section>
                            <section>
                                <?php if ($this->session->flashdata('error')){?>
                                    <div class=" alert alert-danger alert-dismissible text-center row">
                                        <?=$this->session->flashdata('error')?>
                                    </div>
                                <?php }?>
                                <?php if ($this->session->flashdata('success')){?>
                                    <div class="alert alert-success alert-dismissible text-center row">
                                        <?=$this->session->flashdata('success')?>
                                    </div>
                                <?php }?>

                                <br>
                            </section>
                            <div class="row">
                                <div class="col-md-12 form-group row">
                                    <input class="form-control" name="bdata[first_name]" value="<?php echo $user['full_name'] ?>" type="text" id="first_name"  placeholder="Full Name" />
                                    <!--                                    <span class="errorMessage">--><?php //echo form_error('bdata[first_name]'); ?><!--</span>-->
                                </div>
                                <div class="col-md-12 form-group row form-group row ">
                                    <input class="form-control" name="ldata[email_address]" value="<?php echo $user['email_address'] ; ?>" type="text" id="email_address"  placeholder="Email Address" required="required" />
                                    <!--                                    <span class="errorMessage">--><?php //echo form_error('ldata[email_address]'); ?><!--</span>-->
                                </div>
                                <div class="col-md-12 form-group row">
                                    <input class="form-control" name="bdata[contactinfo]" value="<?php echo $user['phone']; ?>" type="text" id="phone"  placeholder="Phone" required="required" />
                                    <!--                                    <span class="errorMessage">--><?php //echo form_error('bdata[phone]'); ?><!--</span>-->
                                </div>
                                <div class="col-md-12 form-group row">
                                    <input class="form-control" name="user_password" type="password" id="user_password"  placeholder=" New Password" />
                                    <!--                                    <span class="errorMessage">--><?php //echo form_error('ldata[user_password]'); ?><!--</span>-->
                                </div>
                                <div class="col-md-12 form-group row">
                                    <input class="form-control" name="re_password" type="password" id="re_password"  placeholder="Confirm Password" />
                                    <!--                                    <span class="errorMessage">--><?php //echo form_error('re_password'); ?><!--</span>-->
                                </div>
                                <div class="col-md-12 form-group row">
                                    <input class="form-control" name="bdata[sale_name]" value="<?php echo $user['profile_picture'] ;?>" type="text" id="sale_name"  placeholder="Sales Person Name" required="required" />
                                    <!--                                    <span class="errorMessage">--><?php //echo form_error('bdata[first_name]'); ?><!--</span>-->
                                </div>

                                <div class="col-md-12 form-group row">
                                    <input class="form-control" name="bdata[companyname]" value="<?php echo $user['companyname'] ;  ?>" type="text" id="company_name"  placeholder="Company Name" required="required" />
                                    <!--                                    <span class="errorMessage">--><?php //echo form_error('bdata[company_name]'); ?><!--</span>-->
                                </div>


                                <div class="form-content1">
                                    <div class="inner">
                                        <p class="initial"></p>
                                    </div>

                                    <h2 class="circle-text">Office Address</h2>
                                    <div class="row" style="padding-right: 28px; padding-bottom: 10px;">
                                        <button type="button" id="btnAdd" class="btn btn-primary col-md-1 col-md-offset-11 ">Add More</button>
                                    </div>
                                    <?php foreach ($user['address_1'] as $i => $value){ ?>
                                    <div class="col-md-12 form-group row group">
                                        <div class="col-md-3 form-group">
                                            <input class="req form-control" type="text" name="o_address[]" id="address" placeholder="Address" value="<?= $value?>" required="required" >
                                        </div>
                                        <div class="col-md-2 form-group">
                                            <input class="req form-control" type="text" name="o_phone[]" id="b_name" placeholder="Office Phone" value="<?=$user['contactinfo'][$i]; ?>" required="required">
                                        </div>

                                        <div class="col-md-2 form-group">
                                            <input class="req form-control" type="text" name='o_city[]' id="city" placeholder="City" value="<?=$user['city'][$i]; ?>" required="required">
                                        </div>
                                        <div class="col-md-2 form-group">
                                            <input class="req form-control" type="text" name='o_state[]' id="state" placeholder="State" value="<?=$user['state'][$i]; ?>" required="required">
                                        </div>
                                        <div class="col-md-2 form-group">
                                            <input class="req form-control" type="text" name='o_zip[]' id="b_zip" placeholder="Zip Code" value="<?=$user['zip'][$i];?>" required="required">
                                        </div>
                                        <div class="col-md-1 form-group">
                                            <button type="button" class="btn btn-danger btnRemove bt_pos">Remove</button>
                                        </div>
                                    </div>
                                    <?php } ?>
                                </div>

                                <!--                                <h3 class="circle-text">Shipping Address</h3><br>-->
                                <!--                                <div class="col-md-12 form-group row">-->
                                <!--                                    <div class="col-md-2">-->
                                <!--                                        <input class="req form-control" type="text" name="s__name" id="b_name" placeholder="Name">-->
                                <!--                                    </div>-->
                                <!--                                    <div class="col-md-3">-->
                                <!--                                        <input class="req form-control" type="text" name="s_address" id="address" placeholder="Street Address" >-->
                                <!--                                    </div>-->
                                <!--                                    <div class="col-md-3">-->
                                <!--                                        <input class="req form-control" type="text" name='s_city' id="city" placeholder="City">-->
                                <!--                                    </div>-->
                                <!--                                    <div class="col-md-2">-->
                                <!--                                        <input class="req form-control" type="text" name='s_state' id="state" placeholder="State">-->
                                <!--                                    </div>-->
                                <!--                                    <div class="col-md-2">-->
                                <!--                                        <input class="req form-control" type="text" name='s_zip' id="b_zip" placeholder="Zip Code">-->
                                <!--                                    </div>-->
                                <!--                                </div>-->
                                <div class="form-group row"><button class="col-xs-3 col-xs-offset-4 btn btn-success" type="update"><strong>Update</strong></button></div>
                            </div>
                        </form><!-- Contact Form -->
                    </div>
                    <!---->
                </div>
            </div>
        </div>
    </div>
</section>

<style>
    @media screen and (min-width: 769px) {
        .shadow-container {
            box-shadow: 0px 0px 25px hsla(0, 0%, 0%, 0.70);
        }
    }
</style>
<script>
    $('.form-content1').multifield({
        section: '.group',
        btnAdd:'#btnAdd',
        btnRemove:'.btnRemove',
    });

    $("#btnAdd").one('click', function () {
        $(".inner").append("<p></p>");
        $("p.initial").addClass("hide");
    });
</script>

