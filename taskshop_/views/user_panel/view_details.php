<!--Page main section start-->
<section id="min-wrapper">
    <div id="main-content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title">My Quote</h3>
                        </div>

                        <div class="panel-body">
                            <!--Table Wrapper Start-->
                            <div class="table-responsive ls-table">
                                <table class="table table-bordered table-striped">
                                    <tbody>
                                        <tr>
                                            <td width="30%">Quote SL#</td>
                                            <td><?php echo $sub_quote_info['quote_id']; ?></td>
                                        </tr>
                                        <tr>
                                            <td width="30%">User Name</td>
                                            <td><?php echo $sub_quote_info['full_name']; ?></td>
                                        </tr>
                                        <tr>
                                            <td width="30%">Category Name</td>
                                            <td><?php echo $sub_quote_info['job_category_name']; ?></td>
                                        </tr>
                                        <tr>
                                            <td width="30%">Job Title</td>
                                            <td><?php echo $sub_quote_info['job_title']; ?></td>
                                        </tr>
                                        <tr>
                                            <td width="30%">Return File Type</td>
                                            <td><?php echo $sub_quote_info['return_file_type']; ?></td>
                                        </tr>
                                        <tr>
                                            <td width="30%">Need Path</td>
                                            <td>
                                                <?php
                                                if ($sub_quote_info['need_path'] == 0) {
                                                    echo 'No need Path';
                                                } else if ($sub_quote_info['need_path'] == 1) {
                                                    echo 'Yes Clipping Path active';
                                                } else if ($sub_quote_info['need_path'] == 2) {
                                                    echo 'Yes Clipping Path inactive';
                                                } else if ($sub_quote_info['need_path'] == 3) {
                                                    echo "Don't know please advice";
                                                }
                                                ?>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="30%">Number of Image</td>
                                            <td>
                                                <?php echo $sub_quote_info['number_of_image']; ?>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="30%">Job Type</td>
                                            <td>
                                                <?php
                                                if ($sub_quote_info['job_type'] == 0) {
                                                    echo 'Monthly';
                                                } else if ($sub_quote_info['need_path'] == 1) {
                                                    echo 'One time';
                                                }
                                                ?>
                                        </tr>
                                        <tr>
                                            <td width="30%">One Time Price USD</td>
                                            <td><?php echo $sub_quote_info['one_time_price_usd']; ?> $</td>
                                        </tr>
                                        <tr>
                                            <td width="30%">Dead Line in Days</td>
                                            <td><?php echo $sub_quote_info['dead_line_days']; ?></td>
                                        </tr>
                                        <tr>
                                            <td width="30%">One Time Dead Line Hour</td>
                                            <td><?php echo $sub_quote_info['one_time_dead_line_hr']; ?></td>
                                        </tr>
                                        <tr>
                                            <td width="30%">monthly_work_volume</td>
                                            <td><?php echo $sub_quote_info['monthly_work_volume']; ?></td>
                                        </tr>
                                        <tr>
                                            <td width="30%">Monthly Amount USD</td>
                                            <td><?php echo $sub_quote_info['monthly_amount_usd']; ?> $</td>
                                        </tr>
                                        <tr>
                                            <td width="30%">instruction_message</td>
                                            <td><?php echo $sub_quote_info['instruction_message']; ?></td>
                                        </tr>
                                        <tr>
                                            <td width="30%">User Confirm</td>
                                            <td>
                                                <?php if ($sub_quote_info['user_confirm'] == 1) { ?>
                                                    <span class="label-success label label-default">Confirmed</span>
                                                <?php } else { ?>
                                                    <span class="label-warning label label-default">Pending</span>
                                                <?php } ?>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="30%">Admin Confirm</td>
                                            <td>
                                                <?php if ($sub_quote_info['admin_confirm'] == 1) { ?>
                                                    <span class="label-success label label-default">Confirmed</span>
                                                <?php } else { ?>
                                                    <span class="label-warning label label-default">Pending</span>
                                                <?php } ?>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="30%">Date Added</td>
                                            <td><?php echo date("D d F Y h:ia", strtotime($sub_quote_info['date_added'])); ?></td>
                                        </tr>

                                    </tbody>
                                </table>
                            </div>
                            <!--Table Wrapper Finish-->
                            <!--                            <div class="ls-button-group demo-btn ls-table-pagination">
                                                            <ul class="pagination ls-pagination" style="float: right;">
                                                                <li><a href="#"><i class="fa fa-arrow-left"></i> </a></li>
                                                                <li><a href="#">1</a></li>
                                                                <li><a href="#">2</a></li>
                                                                <li><a href="#">3</a></li>
                                                                <li><a href="#">4</a></li>
                                                                <li><a href="#">5</a></li>
                                                                <li><a href="#"><i class="fa fa-arrow-right"></i></a></li>
                                                            </ul>
                                                        </div>-->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!--Page main section end -->