$(document).ready(function () {

    // var savedGrandTotal = $('.totalGrandPrice').val();
    // $('.grandTotal').html('$'+Number(savedGrandTotal).toLocaleString('en'));

    var formatter = new Intl.NumberFormat('en-US', {
        style: 'currency',
        currency: 'USD',
    });


    if ($('#data_table').length) {
        // alert();
        $('#data_table').DataTable({
             ordering:false,
            // rowReorder: true,
//           "aaSorting": [],
            columnDefs: [
                // { "width": "50%", "targets": 0 }
                // {orderable: false, className: 'reorder', targets: 0},
                // {"targets": 1, "visible": false, "searchable": false},
                // {orderable: true, targets: '_all'}
            ],
            // scrollY: 500,
//            paging: false

        });
    }

    if ($('#user_table').length) {
        // alert();
        $('#user_table').DataTable({
             ordering:false,
            columnDefs: [
            ]
        });
    }

    $('#videoIframe').on('show.bs.modal', function (e) {
        $(this).find('.modalTitle').html($(e.relatedTarget).data('title'));
        $(this).find('.iframeSrc').attr('src',$(e.relatedTarget).data('src'));
    });
    $("#videoIframe").on('hidden.bs.modal', function (e) {
        $("#videoIframe iframe").attr("src", $("#videoIframe iframe").attr("src"));
    });

    $('#select-Product').on('show.bs.modal', function (e) {


        $(this).find('.pmodeldiv').html($(e.relatedTarget).data('model'));
        $(this).find('.pdescdiv').html($(e.relatedTarget).data('desc'));
        $(this).find('.ptypediv').html($(e.relatedTarget).data('type'));
        $(this).find('.pfamilydiv').html($(e.relatedTarget).data('family'));
        $(this).find('.plistdiv').html($(e.relatedTarget).data('list'));
        $(this).find('.pdealerdiv').html($(e.relatedTarget).data('dealer'));
        $(this).find('.quantityTotaldiv').html($(e.relatedTarget).data('dealer'));

        $(this).find('.pid').attr('value',$(e.relatedTarget).data('id'));
        $(this).find('.pmodel').attr('value', $(e.relatedTarget).data('model'));
        $(this).find('.pdesc').attr('value', $(e.relatedTarget).data('desc'));
        $(this).find('.ptype').attr('value', $(e.relatedTarget).data('type'));
        $(this).find('.pfamily').attr('value', $(e.relatedTarget).data('family'));
        $(this).find('.plist').attr('value', $(e.relatedTarget).data('list'));
        $(this).find('.pdealer').attr('value', $(e.relatedTarget).data('dealer'));
        // var quantityTotal_fi = ;
        // quantityTotal_fi = $.isNumeric(quantityTotal_fi)?quantityTotal_fi.replace(',',''):'';
        // alert(quantityTotal_fi);
        $(this).find('#quantitytotal_fi').attr('value', $(e.relatedTarget).data('dealer') != "CFQ"?$(e.relatedTarget).data('dealer').substring(1):0);
    });


    // $('input[name="pquantity"]').change(function () {
    $('input[name="pquantity"]').unbind('keyup change input paste').bind('keyup change input paste',function(e){
        // console.log(e.type);
        if ($('.ptotal').val() > 0) {
            var dealerprice = $('.pdealer').val();
            if(dealerprice != "CFQ") {
                dealerprice = dealerprice.substring(1);
                dealerprice = dealerprice.replace(',', '');
            }
            dealerprice = $.isNumeric(dealerprice)?dealerprice:0;

            var qty = $('.ptotal').val();
            $('.quantityTotaldiv').html(formatter.format(dealerprice * qty));
            $('.quantityTotal-fi').attr('value',dealerprice * qty);
        }
        if ($('.ptotal').val() == 0) {
            $('.quantityTotaldiv').html('');
            // $('.quantityTotal').val('');
        }
    });

    $('.orderNumber').unbind('keyup change input paste').bind('keyup change input paste',function(e){
        this.value = this.value;
        var $this = $(this);
        var val = $this.val();
        var valLength = val.length;
        var maxCount = 20;
        if(valLength>maxCount){
            $this.val($this.val().substring(0,maxCount));
            $('.orderNumberAlert').show();
            $('.orderNumberAlert').html('Length cannot exceed more than 20 digits');
        }
        setTimeout(function(){
            $(".orderNumberAlert").fadeOut(5000);
            $(".orderNumberAlert").hide();
            $(".orderNumberAlert").html('');
        }, 5000);
    });

    var i = 0;
    var priceTotal = $('.totalGrandPrice').val();
    $('.saveAndMore').click(function () {

        if($("#kngFamily").attr("checked")) {
            $('#bkrFamily').attr('disabled', true);
            $('#rawPart').attr('disabled', true);
        }
        if($("#rawPart").attr("checked")) {
            $('#bkrFamily').attr('disabled', true);
            $('#kngFamily').attr('disabled', true);
        }
        if($("#bkrPart").attr("checked")) {
            $('#rawPart').attr('disabled', true);
            $('#kngFamily').attr('disabled', true);
        }
        // alert($(".quantityTotal-fi").val());
        var fid = $(".quantityTotal-fi").val();
        if(!fid < 1000) {
            var subStringed_1  = fid.replace(',','');
        }
        var subStringed = $.isNumeric(subStringed_1)?subStringed_1:0;
        var totalGrandPrice = subStringed;
        // console.log(totalGrandPrice);
        // console.log(priceTotal);
        // alert(subStringed);
        if($('.ptotal').val() > 0 && $.isNumeric(subStringed)) {
            i++;
            var grandTotal = Number(priceTotal)+Number(totalGrandPrice);
            var model = $('input[name="pmodel"]').val();
            var desc = $('input[name="pdesc"]').val();
            var type = $('input[name="ptype"]').val();
            var family = $('input[name="pfamily"]').val();
            var totalQuantity = $('input[name="pquantity"]').val();
            var totalPrice = $('input[name="quantityTotal"]').val();
            var listPrice = $('input[name="plist"]').val();
            var dealerPrice = $('input[name="pdealer"]').val();
            $("#newInput").append('<div class="container-fluid">' +
                '<div class="row newAddedRows" id="row'+ i +'">' +
                '<div class="form-row">' +
                '<div class="col-md-2 mb-3">' +
                '<label for="validationCustom02">Product Model</label>' +
                '<div>'+ model +'</div>'+
                '<input value="'+ model +'" class="form-control hidden" name="model[]">' +
                '</div>' +
                '<div class="col-md-2 mb-3">' +
                '<label for="validationCustom02">Product Description</label>' +
                '<div>'+ desc +'</div>'+
                '<input value="'+ desc +'" class="form-control hidden" name="description[]">' +
                '</div>' +
                '<div class="col-md-2 mb-3">' +
                '<label for="validationCustom02">Product Family</label>' +
                '<div>'+ family +'</div>'+
                '<input value="'+ family +'" class="form-control hidden" name="family[]">' +
                '</div>' +
                '<div class="col-md-1 mb-3">' +
                '<label for="validationCustom02">Product Type</label>' +
                '<div>'+ type +'</div>'+
                '<input value="'+ type +'" class="form-control hidden" name="type[]">' +
                '</div>' +
                '<div class="col-md-1 mb-3">' +
                '<label for="validationCustom02">List Price</label>' +
                '<div>'+ listPrice +'</div>'+
                '<input value="'+ listPrice +'" class="form-control hidden" name="list_price[]">' +
                '</div>' +
                '<div class="col-md-1 mb-3">' +
                '<label for="validationCustom02">Dealer Price</label>' +
                '<div>'+ dealerPrice +'</div>'+
                '<input value="'+ dealerPrice +'" class="form-control hidden" name="dealer_price[]">' +
                '</div>' +
                '<div class="col-md-1 mb-3">' +
                '<label for="validationCustom02">Total Quantity</label>' +
                '<div class="hidden">'+ Number(totalQuantity).toLocaleString('en') +'</div>'+
                '<input value="'+ totalQuantity +'" class="form-control quote-quantity" name="quantity[]" type="number" min="1">' +
                '</div>' +
                '<div class="col-md-1 mb-3">' +
                '<label for="validationCustom02">Total Price</label>' +
                '<div>'+ (subStringed != 0 ?formatter.format(subStringed):'CFQ') +'</div>'+
                '<input id="thisInputPriceValue' + i + '" value="'+ subStringed +'" class="form-control hidden" name="price[]">' +
                '</div>' +
                '<div class="col-md-1">' +
                '<button type="button" id="' + i + '" class="btn btn-danger remove_btn">' +
                '<i class="fa fa-remove">' +
                '</i>' +
                '</button>' +
                '</div>' +
                '</div>' +
                '</div>' +
                '</div>' +
                '<br>');
            var previousTotal= $('.totalGrandPrice ').val();
            if(!previousTotal){

            }
            else{

                grandTotal = parseFloat(previousTotal) + parseFloat(totalPrice.replace(',', ''));
            }
            $('.generateQuote').removeClass('hide');
            priceTotal = grandTotal;
            $('.grandTotal').html(formatter.format(grandTotal));
            // $('.grandTotal').html('$'+grandTotal+'.00');
            $('.totalGrandPrice').attr('value',grandTotal);
            // console.log(grandTotal);
            // console.log(previousTotal);
            // console.log(totalPrice);



        }
    });

    $('body').on('blur change keyup paste','input[name="quantity[]"]',function(e){
        // if($(this).val() > 0 ){
        var quantity = $(this).val();
        var perUnitPrice=$(this).parent().siblings('div').children('input[name="dealer_price[]"]').val();
        var p = perUnitPrice.split('$');
        // console.log(p[1].indexOf(','));
        if(p[1].indexOf(',') > -1){
            var price = parseFloat(p[1].replace(',', ''));
        }else{
            var price = parseFloat(p[1]);
        }
        var totalPrice = quantity * price;
        // $(this).parent().siblings('div').children('input[name="price[]"]').val(totalPrice)  ;
        $(this).parent().siblings('div').children('input[name="price[]"]').attr('value',totalPrice)  ;
        var priceWithComma = numberWithCommas(totalPrice.toFixed(2));
        $(this).parent().siblings('div').children('input[name="price[]"]').siblings('div').html('$'+priceWithComma);

        var grandTotal = 0 ;
        $('input[name="price[]"]').each(function(){
            grandTotal += parseFloat($(this).val());
        })

        grandTotal = grandTotal.toFixed(2);
        var commaGrandTotal = numberWithCommas(grandTotal);
        // var grand = parseFloat(commaGrandTotal);
        //  console.log(commaGrandTotal);
        $('#grandTotal').html('$'+commaGrandTotal);
        $('.totalGrandPrice').attr('value',grandTotal);

        // }
        if($(this).val() == 0 ){

            $(this).parent().siblings('div').children('input[name="price[]"]').attr('value',0)  ;

            $(this).parent().siblings('div').children('input[name="price[]"]').siblings('div').html('');
        }

    });

    function numberWithCommas(x) {
        return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    }

    // $('input[name="quantity[]"]').click(function(e){
    //
    //     // name="quantity[]"
    //     console.log('hello');
    //
    // });

    // if($('.grandTotal').val()) {
    //     alert();
    //     $('.grandTotal').formatCurrency();
    // }

    $('.saved_remove_btn').click(function () {
        var button_id = $(this).attr("id");
        var currentGrandTotalValue = $('.totalGrandPrice').val();
        var currentRowTotalValue = $('#thisInputSavedPriceValue'+button_id).val();
        var grandTotal = (Number(currentGrandTotalValue) - Number(currentRowTotalValue));
        if(grandTotal != 0) {
            $('.grandTotal').html(formatter.format(grandTotal));
            $('.totalGrandPrice').attr('value', grandTotal);
            priceTotal = grandTotal;
        }else{
            $('.grandTotal').html(formatter.format(grandTotal));
            $('.totalGrandPrice').attr('value', grandTotal);
            priceTotal = grandTotal;
        }
        // if(grandTotal >= 0) {
        //     $('.grandTotal').html('$' + Number(grandTotal).toLocaleString('en')+'.00');
        //     // alert();
        //     $('.totalGrandPrice').attr('value', grandTotal);
        //     priceTotal = grandTotal;
        // }
        // alert(priceTotal);
        $('#savedRow'+button_id).remove();
        if($('.newAddedRows').length == 0 && $('.savedRows').length == 0){
            $('.generateQuote').addClass('hide');
            $('#rawPart').removeAttr('disabled');
            $('#bkrFamily').removeAttr('disabled');
            $('#kngFamily').removeAttr('disabled');
        }
    });

    if($("#kngFamily").attr("checked")){
        $('.main-table-div').removeClass('hide');
        $('.main-table-div').addClass('show');
        $('.addedProductHeading').addClass('show');
    }

    if($("#rawPart").attr("checked")){
        $('.main-table-div').removeClass('show');
        $('.main-table-div').addClass('hide');
        $('.addedProductHeading').addClass('show');
        $('.raw-table-div').removeClass('hide');
        $('.raw-table-div').addClass('show');
    }

    $("#bkrFamily").click(function(){
        if (this.checked) {
            $("#bkrFamily").attr('checked',true);
            $("#rawPart").removeAttr('checked');
            $("#kngFamily").removeAttr('checked');
            $('.main-table-div').removeClass('show');
            $('.main-table-div').addClass('hide');
            $('.addedProductHeading').removeClass('show');
            $('.addedProductHeading').addClass('hide');
            $('.raw-table-div').removeClass('show');
            $('.raw-table-div').addClass('hide');
        }
    });

    $("#rawPart").click(function(){
        if (this.checked) {
            $("#rawPart").attr('checked',true);
            $("#bkrFamily").removeAttr('checked');
            $("#kngFamily").removeAttr('checked');
            $('.main-table-div').removeClass('show');
            $('.main-table-div').addClass('hide');
            $('.addedProductHeading').removeClass('hide');
            $('.addedProductHeading').addClass('show');
            $('.raw-table-div').removeClass('hide');
            $('.raw-table-div').addClass('show');
        }
    });

    $("#kngFamily").click(function(){
        if (this.checked) {
            $("#kngFamily").attr('checked',true);
            $("#rawPart").removeAttr('checked');
            $("#bkrFamily").removeAttr('checked');
            $('.main-table-div').removeClass('hide');
            $('.main-table-div').addClass('show');
            $('.addedProductHeading').removeClass('hide');
            $('.addedProductHeading').addClass('show');
            $('.raw-table-div').removeClass('show');
            $('.raw-table-div').addClass('hide');
        }
    });

    $(document).on('click','.remove_btn',function () {
        var button_id = $(this).attr("id");
        var currentGrandTotalValue = $('.totalGrandPrice').val();
        var currentRowTotalValue = $('#thisInputPriceValue'+button_id).val();
        var grandTotal = (Number(currentGrandTotalValue) - Number(currentRowTotalValue));
        if(grandTotal != 0) {
            $('.grandTotal').html(formatter.format(grandTotal));
        }else{
            $('.grandTotal').html(formatter.format(grandTotal));
        }
        // $('.grandTotal').html('$'+grandTotal+'.00');
        $('.totalGrandPrice').attr('value',grandTotal);
        priceTotal = grandTotal;
        // alert(priceTotal);
        $('#row'+button_id).remove();
        if($('.newAddedRows').length == 0 && $('.savedRows').length == 0){
            $('.generateQuote').addClass('hide');
            $('#rawPart').removeAttr('disabled');
            $('#bkrFamily').removeAttr('disabled');
            $('#kngFamily').removeAttr('disabled');
        }
    });

    $('form.quote-form').submit(function () {
        var error = 0;
        var value= $('.orderNumber').val();
        if($.isNumeric(value) && value == 0 ){
            error++;
            $('.orderNumberAlert').show();
            $('.orderNumberAlert').html('Order Number cannot be 0');
            $('html, body').animate({
                scrollTop: $(".quote-form").offset().top
            }, 500);
            return false;
        }else if(error === 0){
            return true;
        }
    });

    $('#select-Product').on('hide.bs.modal', function (e) {
        setTimeout(function(){
            $("#error").html('');
            $("#error").addClass('hide');
            $("#error").fadeOut(500);
        }, 5000);
        $('.ptotal').val(1);
        $('.quantityTotaldiv').html('');
        $('#quantitytotal_fi').removeAttr('value');
    });

    $('#attachmentForm').on('submit', function (e) {
        e.preventDefault();
        var form_data = new FormData(this);
        $.ajax({
            url: base_url+'quote/upload_attachment', // point to server-side controller method
            dataType: 'json', // what to expect back from the server
            cache: false,
            contentType: false,
            processData: false,
            data: form_data,
            type: 'post',
            success: function (response) {
                if (response.status == 200) {
                    // location.reload();
                    // alert(response.file);
                    $(".attachedFileDiv").html('File Added Successfully!');
                    $(".attachedFileDiv").show();
                    $("#addAttachmentModal").modal('hide');
                    $('body').removeClass('modal-open');
                    $(".modal-backdrop").remove();
                    $('.fileDiv').show();
                    $('.attachedFile').html(response.file);
                    $('.attachedFile').attr('href', base_url+'uploads/'+response.file);
                } else {
                    $('.errorToModal').html(response.message)
                    $(".errorToModal").show();
                    setTimeout(function(){
                        $(".errorToModal").html('');
                        $(".errorToModal").addClass('hide');
                        $(".errorToModal").fadeOut(500);
                    }, 5000);
                    console.log(response.message);
                }
            }
        });
    });
    
    $('.po-upload-btn').click(function(){
        $('.po-upload-error').addClass('hidden');
    })
    var f = false;

    $('.submitAsOrder').on('click',function(e){
        e.preventDefault();
        var quoteId = $(this).data('id');
        var location = $(this).data('href');

        $.get(base_url+'quote/singleQuote/'+quoteId,function(res){
            // console.log(res.grandTotal)
            if(res.grandTotal >= 5000){
                console.log('hello');
                $('#quote_id_id').val(quoteId);
                $('#poAttachmentModal2').modal('show');
                f= true;

                // alert('Your order is over $5K and you will need to attach a hard copy PO');
            }else{
                window.location = location;
            }
        },'json')

    })

    $('#poattachmentForm').on('submit', function (e) {

        e.preventDefault();
        var form_data = new FormData(this);
        if(form_data.get('file').name == ''){
            $('.po-upload-error').removeClass('hidden');
            return;
        }
        $.ajax({
            url: base_url+'quote/upload_attachment', // point to server-side controller method
            dataType: 'json', // what to expect back from the server
            cache: false,
            contentType: false,
            processData: false,
            data: form_data,
            type: 'post',
            success: function (response) {
                if (response.status == 200) {
                    // location.reload();
                    // alert(response.file);
                    // $(".attachedFileDiv").html('File Added Successfully!');
                    // $(".attachedFileDiv").show();

                    $("#poAttachmentModal").modal('hide');
                    if(f){
                        window.location.href = $('.submitAsOrder').attr('href');
                        f = false;
                    }else{
                        window.location.href = $('.submit-or-draft').attr('href');
                    }
                    // location.href = $('.submit-or-draft').attr('href');
                    // $('body').removeClass('modal-open');
                    // $(".modal-backdrop").remove();
                    // $('.fileDiv').show();
                    // $('.attachedFile').html(response.file);
                    // $('.attachedFile').attr('href', base_url+'uploads/'+response.file);
                } else {
                    $('.errorToModal').html(response.message)
                    $(".errorToModal").show();
                    setTimeout(function(){
                        $(".errorToModal").html('');
                        $(".errorToModal").addClass('hide');
                        $(".errorToModal").fadeOut(500);
                    }, 5000);
                    console.log(response.message);
                }
            }
        });
    });


    // $('input[type=file]').change(function () {
    //     var input = this;
    //     if (input.files && input.files[0]) {
    //         var reader = new FileReader();
    //
    //         reader.onload = function (e) {
    //             $(input).closest('.fileinput-new').find('img').attr('src', e.target.result);
    //         };
    //
    //         reader.readAsDataURL(input.files[0]);
    //     }
    // });

});